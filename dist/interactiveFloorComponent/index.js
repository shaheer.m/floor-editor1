"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InteractiveFloorComponent = InteractiveFloorComponent;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/toConsumableArray"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/slicedToArray"));

var _react = _interopRequireWildcard(require("react"));

var _Typography = _interopRequireDefault(require("@mui/material/Typography"));

var _reactSvgmt = require("react-svgmt");

var _reactZoomPanPinch = require("react-zoom-pan-pinch");

var _material = require("@mui/material");

var _individualSeats = _interopRequireDefault(require("../assets/individualSeats.svg"));

var _zoomIn = _interopRequireDefault(require("../assets/zoom-in.svg"));

var _zoomOut = _interopRequireDefault(require("../assets/zoom-out.svg"));

var _fitToScreen = _interopRequireDefault(require("../assets/fit-to-screen.svg"));

var _excluded = ["zoomIn", "zoomOut", "resetTransform"];

var CustomPopover = function CustomPopover(_ref) {
  var open = _ref.open,
      anchorEl = _ref.anchorEl,
      children = _ref.children,
      sx = _ref.sx,
      handleClose = _ref.handleClose,
      anchorOrigin = _ref.anchorOrigin;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.Popover, {
    open: open,
    anchorEl: anchorEl,
    anchorOrigin: anchorOrigin,
    onClose: handleClose
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: sx
  }, children)));
};

var Legend = function Legend(_ref2) {
  var color = _ref2.color,
      title = _ref2.title,
      showBorder = _ref2.showBorder;
  return /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      p: 1,
      display: "flex",
      alignItems: "center"
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      background: color,
      width: "1.25rem",
      height: "1.25rem"
    },
    border: showBorder ? "1px solid black" : ""
  }), /*#__PURE__*/_react.default.createElement(_material.Box, {
    pl: 1
  }, title));
};

function InteractiveFloorComponent(props) {
  var _paperData$customAttr, _paperData$slotData, _paperData$slotData$t, _paperData$slotData$t2, _paperData$slotData$t3;

  console.log("InteractiveFloorComponent1", props.data, props);
  (0, _react.useEffect)(function () {
    console.log("InteractiveFloorComponent2", props.data, props);
  }, [props]); // seats (path or group) with clickable attributes

  var selectAllSeats = ['path[data-paper-data*="clickable"]']; // id of all booked seats

  var bookedIds = props.bookedIds ? Object.keys(props.bookedIds) : [];
  var maxCapacity = props.bookingLimit;

  var _React$useState = _react.default.useState(),
      _React$useState2 = (0, _slicedToArray2.default)(_React$useState, 2),
      anchorEl = _React$useState2[0],
      setAnchorEl = _React$useState2[1];

  var _useState = (0, _react.useState)(),
      _useState2 = (0, _slicedToArray2.default)(_useState, 2),
      paperData = _useState2[0],
      setPaperData = _useState2[1]; // selected seat properties


  var _useState3 = (0, _react.useState)([]),
      _useState4 = (0, _slicedToArray2.default)(_useState3, 2),
      newSelectedSeats = _useState4[0],
      setNewSelectedSeats = _useState4[1]; // check if user has selected any meeting room (seat with more than 1 capacity)


  var _useState5 = (0, _react.useState)(false),
      _useState6 = (0, _slicedToArray2.default)(_useState5, 2),
      meetingRoomChosen = _useState6[0],
      setMeetingRoomChosen = _useState6[1];

  var _useState7 = (0, _react.useState)(0),
      _useState8 = (0, _slicedToArray2.default)(_useState7, 2),
      bookedCapacity = _useState8[0],
      setBookedCapacity = _useState8[1];

  var _useState9 = (0, _react.useState)(0),
      _useState10 = (0, _slicedToArray2.default)(_useState9, 2),
      timeSlot = _useState10[0],
      setTimeSlot = _useState10[1];

  var _useState11 = (0, _react.useState)(props.otherUserIds && props.otherUserIds[0] ? props.otherUserIds[0].userId : null),
      _useState12 = (0, _slicedToArray2.default)(_useState11, 2),
      otherUserId = _useState12[0],
      setOtherUserId = _useState12[1];

  var handleSelectSeat = function handleSelectSeat() {
    if (paperData !== null && paperData !== void 0 && paperData.capacity) {
      var capacity = Number(paperData === null || paperData === void 0 ? void 0 : paperData.capacity);

      if (capacity === 1) {
        setBookedCapacity(function (count) {
          return count + 1;
        });
      }

      if (capacity > 1) {
        setMeetingRoomChosen(true);
      }
    }

    if (props.isBusinessManager) {
      props.seatSelectionCallback(paperData === null || paperData === void 0 ? void 0 : paperData.id, newSelectedSeats, otherUserId);
    } else {
      props.seatSelectionCallback(paperData === null || paperData === void 0 ? void 0 : paperData.id, newSelectedSeats, props.currentUserId);
    }

    setNewSelectedSeats([].concat((0, _toConsumableArray2.default)(newSelectedSeats), [paperData === null || paperData === void 0 ? void 0 : paperData.id]));
    handleClose(); // Laravel callback function can write here to pass the clicked seat id
  }; // When the user clicks on a seat that he has already booked


  var updateNewSelectedSeats = function updateNewSelectedSeats(id, capacity) {
    props.removeSeatCallback(id, newSelectedSeats);
    var newSelectedSeatsCopy = newSelectedSeats.slice(0);
    var index = newSelectedSeatsCopy.indexOf(id);

    if (index > -1) {
      newSelectedSeatsCopy.splice(index, 1);
    }

    var seatCapacity = Number(capacity);

    if (seatCapacity > 1) {
      // if the seat the user is trying to remove a  meeting room
      setMeetingRoomChosen(false);
    } else if (seatCapacity === 1) {
      setBookedCapacity(function (count) {
        return count - 1;
      });
    }

    setNewSelectedSeats(newSelectedSeatsCopy);
  }; // if a meeting room is chosen
  // or bookedCapacity is greater than max capacity
  // or user tryuing to book a meeting room when he has already chosen a seat
  //      then no other seat should be selected


  var restrictedCases = function restrictedCases(capacity) {
    if (meetingRoomChosen) {
      // alert("Cant book....Meeting Room already chosen");
      props.sendAlert("Cant book....Meeting Room already chosen"); //   dispatch(warningAlert(['Cant book....Meeting Room already chosen']))

      return true;
    }

    if (capacity + bookedCapacity > maxCapacity && capacity == 1) {
      // alert("Cant book....Max capacity reached");
      props.sendAlert("Cant book....Max capacity reached"); //  dispatch(warningAlert(['Cant book....Max capacity reached']))

      return true;
    }

    if (bookedCapacity > 0 && capacity > 1) {
      // alert(
      //   "Cant book....Seat already chosen, you cant chose another meeting room"
      // );
      props.sendAlert("Cant book....Seat already chosen, you cant chose another meeting room"); //  dispatch(warningAlert(['Cant book....Seat already chosen, you cant chose another meeting room']))

      return true;
    }
  };

  var getBookingDetails = function getBookingDetails(selectedID) {
    var details = [];

    if (props.bookedIds && props.bookedIds[selectedID]) {
      details = props.bookedIds[selectedID];
    }

    return details;
  };

  var handleClick = function handleClick(event) {
    setTimeSlot(0);
    var target = event.target;
    var pathPaperInfo = target.getAttribute("data-paper-data") || "";
    var pathAttributes = JSON.parse(pathPaperInfo);

    if (pathAttributes !== null && pathAttributes !== void 0 && pathAttributes.parentData) {
      if (pathAttributes !== null && pathAttributes !== void 0 && pathAttributes.parentData.unavailable) {
        return null;
      } // In booking view, clicks on booked seats should be restricted
      // if (!props.loadingInMapView && bookedIds.includes(pathAttributes.parentData.id)) {
      //   return null;
      // }


      if (newSelectedSeats.includes(pathAttributes.parentData.id)) {
        updateNewSelectedSeats(pathAttributes.parentData.id, pathAttributes.parentData.capacity);
        return null;
      }

      var capacity = Number(pathAttributes.parentData.capacity);

      if (restrictedCases(capacity)) {
        return null;
      } // setPaperData(pathAttributes.parentData);


      var details = getBookingDetails(pathAttributes.parentData.id);
      var wholeData = (0, _objectSpread2.default)((0, _objectSpread2.default)({}, pathAttributes.parentData), {}, {
        slotData: details
      });
      setPaperData(wholeData);
    } else {
      if (pathAttributes !== null && pathAttributes !== void 0 && pathAttributes.unavailable) {
        return null;
      } // // In booking view, clicks on booked seats should be restricted
      // if (!props.loadingInMapView && bookedIds.includes(pathAttributes.id)) {
      //   return null;
      // }


      if (newSelectedSeats.includes(pathAttributes.id)) {
        updateNewSelectedSeats(pathAttributes.id, pathAttributes.capacity);
        return null;
      }

      var _capacity = Number(pathAttributes.capacity);

      if (restrictedCases(_capacity)) {
        return null;
      } // setPaperData(pathAttributes);


      var _details = getBookingDetails(pathAttributes.id);

      var _wholeData = (0, _objectSpread2.default)((0, _objectSpread2.default)({}, pathAttributes), {}, {
        slotData: _details
      });

      setPaperData(_wholeData);
    }

    setAnchorEl(event.currentTarget);
  };

  var handleClose = function handleClose() {
    setAnchorEl(null);
  };

  var open = Boolean(anchorEl);
  var id = open ? "simple-popover" : undefined;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.Box, {
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "column"
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      display: "flex",
      flexWrap: "wrap",
      minHeight: "8%"
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      display: "flex",
      flexWrap: "wrap",
      flexGrow: 1
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      p: 1,
      display: "flex",
      alignItems: "center"
    }
  }, /*#__PURE__*/_react.default.createElement(_Typography.default, {
    color: "#4b4b4b",
    variant: "subtitle1"
  }, "Total Seats\xA0:\xA0", props.totalSeats)), /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      p: 1,
      display: "flex",
      alignItems: "center"
    }
  }, /*#__PURE__*/_react.default.createElement(_Typography.default, {
    color: "#4b4b4b",
    variant: "subtitle1"
  }, "Booked Seats\xA0:\xA0", props.bookedSeats))), /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      display: "flex",
      flexWrap: "wrap"
    }
  }, /*#__PURE__*/_react.default.createElement(Legend, {
    color: "#5ccb8b",
    title: /*#__PURE__*/_react.default.createElement(_Typography.default, {
      color: "#4b4b4b",
      variant: "subtitle1"
    }, "Booked")
  }), /*#__PURE__*/_react.default.createElement(Legend, {
    color: "#f9f7f5",
    title: /*#__PURE__*/_react.default.createElement(_Typography.default, {
      color: "#4b4b4b",
      variant: "subtitle1"
    }, "Available"),
    showBorder: true
  }), /*#__PURE__*/_react.default.createElement(Legend, {
    color: "#d1d1d1",
    title: /*#__PURE__*/_react.default.createElement(_Typography.default, {
      color: "#4b4b4b",
      variant: "subtitle1"
    }, "Unavailable")
  }))), /*#__PURE__*/_react.default.createElement(_material.Box, {
    pb: "1.538rem",
    flexGrow: 1,
    flexShrink: 1
  }, /*#__PURE__*/_react.default.createElement(_reactZoomPanPinch.TransformWrapper, {
    initialScale: 1
  }, function (_ref3) {
    var zoomIn = _ref3.zoomIn,
        zoomOut = _ref3.zoomOut,
        resetTransform = _ref3.resetTransform,
        rest = (0, _objectWithoutProperties2.default)(_ref3, _excluded);
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.Box, {
      sx: {
        height: "98%",
        width: "100%"
      }
    }, /*#__PURE__*/_react.default.createElement(_reactZoomPanPinch.TransformComponent, {
      wrapperStyle: {
        height: "100%",
        width: "100%"
      },
      contentStyle: {
        height: "100%",
        width: "100%",
        position: "absolute"
      }
    }, /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgLoader, {
      height: "100%",
      width: "100%",
      svgXML: props.image
    }, props.loadingInMapView ? selectAllSeats.map(function (selector, ix) {
      return /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
        key: ix,
        selector: selector,
        stroke: "#bcbcbc",
        fill: "white",
        onClick: function onClick() {
          alert('In map view, user should not click on other seat');
        }
      });
    }) : selectAllSeats.map(function (selector, ix) {
      return /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
        key: ix,
        selector: selector,
        stroke: "#bcbcbc",
        fill: "white",
        onClick: handleClick
      });
    }), /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
      selector: 'path[data-paper-data*="unavailable"]',
      stroke: "#bcbcbc",
      fill: "#bcbcbc"
    }), !props.loadingInMapView ? bookedIds.map(function (selector, ix) {
      return /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
        key: ix,
        selector: 'path[data-paper-data*="\\"' + selector + '\\""]',
        fill: "#5ccb8b",
        onClick: function onClick() {
          alert('In booking view, user should not click on booked seat');
        }
      });
    }) : bookedIds.map(function (selector, ix) {
      return /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
        key: ix,
        selector: 'path[data-paper-data*="\\"' + selector + '\\""]',
        fill: "#5ccb8b",
        onClick: handleClick
      });
    }), newSelectedSeats.map(function (selector, ix) {
      return /*#__PURE__*/_react.default.createElement(_reactSvgmt.SvgProxy, {
        key: ix,
        selector: 'path[data-paper-data*="\\"' + selector + '\\""]',
        fill: "#ffd538"
      });
    })))), /*#__PURE__*/_react.default.createElement(_material.Box, {
      sx: {
        display: "flex",
        justifyContent: "flex-end"
      },
      pr: 2
    }, /*#__PURE__*/_react.default.createElement(_material.ButtonGroup, {
      size: "medium"
    }, /*#__PURE__*/_react.default.createElement(_material.Button, {
      key: "one",
      sx: {
        border: "1px solid grey",
        borderRadius: "4px",
        height: "2rem"
      },
      onClick: function onClick() {
        return zoomOut();
      }
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _zoomOut.default,
      alt: ""
    })), /*#__PURE__*/_react.default.createElement(_material.Button, {
      key: "two",
      sx: {
        border: "1px solid grey"
      },
      onClick: function onClick() {
        return resetTransform();
      }
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _fitToScreen.default,
      alt: ""
    })), /*#__PURE__*/_react.default.createElement(_material.Button, {
      key: "three",
      sx: {
        borderRadius: "4px",
        border: "1px solid grey"
      },
      onClick: function onClick() {
        return zoomIn();
      }
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _zoomIn.default,
      alt: ""
    })))));
  })), /*#__PURE__*/_react.default.createElement(CustomPopover, {
    id: id,
    open: open,
    anchorEl: anchorEl,
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "left"
    },
    handleClose: handleClose,
    sx: {
      minWidth: "17.188rem",
      maxWidth: "17.188rem",
      padding: "8px",
      margin: "3px"
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    sx: {
      display: "flex",
      flexDirection: "column"
    }
  }, /*#__PURE__*/_react.default.createElement(_material.Box, {
    my: 1
  }, /*#__PURE__*/_react.default.createElement(Legend, {
    color: "#5ccb8b",
    title: /*#__PURE__*/_react.default.createElement(_Typography.default, {
      variant: "h6",
      fontWeight: 600
    }, paperData === null || paperData === void 0 ? void 0 : paperData.id)
  }), (paperData === null || paperData === void 0 ? void 0 : paperData.capacity) !== 1 && /*#__PURE__*/_react.default.createElement(_Typography.default, {
    variant: "subtitle2",
    p: 1
  }, "Capacity\xA0:\xA0", paperData === null || paperData === void 0 ? void 0 : paperData.capacity)), /*#__PURE__*/_react.default.createElement(_material.Box, {
    style: {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap"
    }
  }, paperData === null || paperData === void 0 ? void 0 : (_paperData$customAttr = paperData.customAttributes) === null || _paperData$customAttr === void 0 ? void 0 : _paperData$customAttr.filter(function (item) {
    return item.value !== "clickable";
  }).map(function (item) {
    return /*#__PURE__*/_react.default.createElement(_material.Chip, {
      key: item.value,
      label: item.label,
      sx: {
        background: "#e1e1e1",
        m: "3px",
        mb: "15px"
      }
    });
  })), !props.loadingInMapView && props.isBusinessManager ? /*#__PURE__*/_react.default.createElement(_material.TextField, {
    select: true,
    fullWidth: true,
    size: "small" // sx={{ marginTop: "1rem" }}
    ,
    value: otherUserId,
    onChange: function onChange(e) {
      setOtherUserId(e.target.value);
    },
    inputProps: {
      "aria-label": "Without label"
    }
  }, props.otherUserIds && props.otherUserIds.map(function (item, i) {
    return /*#__PURE__*/_react.default.createElement(_material.MenuItem, {
      key: i,
      value: item.userId
    }, item.emailId);
  })) : null, props.loadingInMapView ? /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_material.TextField, {
    select: true,
    fullWidth: true,
    size: "small" // sx={{ marginTop: "1rem" }}
    ,
    value: timeSlot,
    onChange: function onChange(e) {
      setTimeSlot(e.target.value);
    },
    inputProps: {
      "aria-label": "Without label"
    }
  }, paperData === null || paperData === void 0 ? void 0 : (_paperData$slotData = paperData.slotData) === null || _paperData$slotData === void 0 ? void 0 : _paperData$slotData.map(function (val, i) {
    return /*#__PURE__*/_react.default.createElement(_material.MenuItem, {
      key: i,
      value: i
    }, val.time);
  })), /*#__PURE__*/_react.default.createElement(_Typography.default, {
    variant: "subtitle1",
    p: 1
  }, paperData === null || paperData === void 0 ? void 0 : (_paperData$slotData$t = paperData.slotData[timeSlot]) === null || _paperData$slotData$t === void 0 ? void 0 : _paperData$slotData$t.name), /*#__PURE__*/_react.default.createElement(_Typography.default, {
    variant: "subtitle2",
    p: 1
  }, paperData === null || paperData === void 0 ? void 0 : (_paperData$slotData$t2 = paperData.slotData[timeSlot]) === null || _paperData$slotData$t2 === void 0 ? void 0 : _paperData$slotData$t2.mob), /*#__PURE__*/_react.default.createElement(_Typography.default, {
    variant: "subtitle2",
    p: 1
  }, paperData === null || paperData === void 0 ? void 0 : (_paperData$slotData$t3 = paperData.slotData[timeSlot]) === null || _paperData$slotData$t3 === void 0 ? void 0 : _paperData$slotData$t3.email)) : null, /*#__PURE__*/_react.default.createElement(_material.Stack, {
    mt: "1rem",
    direction: {
      xs: "column",
      sm: "row"
    },
    spacing: 1
  }, !props.loadingInMapView ? /*#__PURE__*/_react.default.createElement(_material.Button, {
    variant: "outlined",
    color: "error",
    sx: {
      borderRadius: "2rem",
      fontSize: ".875rem",
      fontWeight: "600",
      width: "6rem"
    },
    onClick: handleSelectSeat
  }, "Select") : null)))));
}