"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ScratchPaintReducer", {
  enumerable: true,
  get: function get() {
    return _scratchPaintReducer.default;
  }
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _paintEditor.default;
  }
});

var _paintEditor = _interopRequireDefault(require("./containers/paint-editor"));

var _scratchPaintReducer = _interopRequireDefault(require("./reducers/scratch-paint-reducer"));