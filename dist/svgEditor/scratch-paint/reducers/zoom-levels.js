"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setZoomLevelId = exports.saveZoomLevel = exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../log/log"));

var SAVE_ZOOM_LEVEL = 'scratch-paint/zoom-levels/SAVE_ZOOM_LEVEL';
var SET_ZOOM_LEVEL_ID = 'scratch-paint/zoom-levels/SET_ZOOM_LEVEL_ID';
var initialState = {};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case SET_ZOOM_LEVEL_ID:
      if (action.zoomLevelId === 'currentZoomLevelId') {
        _log.default.warn("currentZoomLevelId is an invalid string for zoomLevel");

        return state;
      }

      return Object.assign({}, state, {
        currentZoomLevelId: action.zoomLevelId
      });

    case SAVE_ZOOM_LEVEL:
      return Object.assign({}, state, (0, _defineProperty2.default)({}, state.currentZoomLevelId, action.zoomLevel));

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var saveZoomLevel = function saveZoomLevel(zoomLevel) {
  if (!(zoomLevel instanceof _paper.default.Matrix)) {
    _log.default.warn("Not a matrix: ".concat(zoomLevel));
  }

  return {
    type: SAVE_ZOOM_LEVEL,
    zoomLevel: new _paper.default.Matrix(zoomLevel)
  };
};

exports.saveZoomLevel = saveZoomLevel;

var setZoomLevelId = function setZoomLevelId(zoomLevelId) {
  return {
    type: SET_ZOOM_LEVEL_ID,
    zoomLevelId: zoomLevelId
  };
};

exports.setZoomLevelId = setZoomLevelId;