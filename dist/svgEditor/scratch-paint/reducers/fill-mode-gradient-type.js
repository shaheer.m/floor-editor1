"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeGradientType = void 0;

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _log = _interopRequireDefault(require("../log/log"));

var _fillStyle = require("./fill-style");

// Gradient type shown in the fill tool. This is the last gradient type explicitly chosen by the user,
// and isn't overwritten by changing the selection.
var initialState = null;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case _fillStyle.CHANGE_FILL_GRADIENT_TYPE:
      if (action.gradientType in _gradientTypes.default) {
        return action.gradientType;
      }

      _log.default.warn("Gradient type does not exist: ".concat(action.gradientType));

    /* falls through */

    default:
      return state;
  }
}; // Action creators ==================================
// Use this for user-initiated gradient type selections only.
// See reducers/fill-style.js for other ways gradient type changes.


exports.default = reducer;

var changeGradientType = function changeGradientType(gradientType) {
  return {
    type: _fillStyle.CHANGE_FILL_GRADIENT_TYPE,
    gradientType: gradientType
  };
};

exports.changeGradientType = changeGradientType;