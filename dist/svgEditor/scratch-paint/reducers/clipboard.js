"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setClipboardItems = exports.incrementPasteOffset = exports.default = exports.clearPasteOffset = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var SET = 'scratch-paint/clipboard/SET';
var INCREMENT_PASTE_OFFSET = 'scratch-paint/clipboard/INCREMENT_PASTE_OFFSET';
var CLEAR_PASTE_OFFSET = 'scratch-paint/clipboard/CLEAR_PASTE_OFFSET';
var initialState = {
  items: [],
  pasteOffset: 0
};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case SET:
      if (!action.clipboardItems || !(action.clipboardItems instanceof Array) || action.clipboardItems.length === 0) {
        _log.default.warn("Invalid clipboard item format");

        return state;
      }

      return {
        items: action.clipboardItems,
        pasteOffset: 1
      };

    case INCREMENT_PASTE_OFFSET:
      return {
        items: state.items,
        pasteOffset: state.pasteOffset + 1
      };

    case CLEAR_PASTE_OFFSET:
      return {
        items: state.items,
        pasteOffset: 0
      };

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var setClipboardItems = function setClipboardItems(clipboardItems) {
  return {
    type: SET,
    clipboardItems: clipboardItems
  };
};

exports.setClipboardItems = setClipboardItems;

var incrementPasteOffset = function incrementPasteOffset() {
  return {
    type: INCREMENT_PASTE_OFFSET
  };
};

exports.incrementPasteOffset = incrementPasteOffset;

var clearPasteOffset = function clearPasteOffset() {
  return {
    type: CLEAR_PASTE_OFFSET
  };
};

exports.clearPasteOffset = clearPasteOffset;