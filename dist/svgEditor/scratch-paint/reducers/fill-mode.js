"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _fillModeGradientType = _interopRequireDefault(require("./fill-mode-gradient-type"));

var _colorIndex = _interopRequireDefault(require("./color-index"));

var _default = (0, _redux.combineReducers)({
  gradientType: _fillModeGradientType.default,
  colorIndex: _colorIndex.default
});

exports.default = _default;