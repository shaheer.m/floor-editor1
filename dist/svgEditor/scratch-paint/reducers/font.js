"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeFont = void 0;

var _fonts = _interopRequireDefault(require("../lib/fonts"));

var CHANGE_FONT = 'scratch-paint/fonts/CHANGE_FONT';
var initialState = _fonts.default.SANS_SERIF;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_FONT:
      if (!action.font) return state;
      return action.font;

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeFont = function changeFont(font) {
  return {
    type: CHANGE_FONT,
    font: font
  };
};

exports.changeFont = changeFont;