"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeStrokeWidth = exports.MAX_STROKE_WIDTH = exports.CHANGE_STROKE_WIDTH = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var _selectedItems = require("./selected-items");

var _stylePath = require("../helper/style-path");

var CHANGE_STROKE_WIDTH = 'scratch-paint/stroke-width/CHANGE_STROKE_WIDTH';
exports.CHANGE_STROKE_WIDTH = CHANGE_STROKE_WIDTH;
var MAX_STROKE_WIDTH = 100;
exports.MAX_STROKE_WIDTH = MAX_STROKE_WIDTH;
var initialState = 4;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_STROKE_WIDTH:
      if (isNaN(action.strokeWidth)) {
        _log.default.warn("Invalid brush size: ".concat(action.strokeWidth));

        return state;
      }

      return Math.min(MAX_STROKE_WIDTH, Math.max(0, action.strokeWidth));

    case _selectedItems.CHANGE_SELECTED_ITEMS:
      // Don't change state if no selection
      if (!action.selectedItems || !action.selectedItems.length) {
        return state;
      } // Bitmap mode doesn't have stroke width


      if (action.bitmapMode) {
        return state;
      }

      return (0, _stylePath.getColorsFromSelection)(action.selectedItems, action.bitmapMode).strokeWidth;

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeStrokeWidth = function changeStrokeWidth(strokeWidth) {
  return {
    type: CHANGE_STROKE_WIDTH,
    strokeWidth: strokeWidth
  };
};

exports.changeStrokeWidth = changeStrokeWidth;