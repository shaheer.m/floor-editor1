"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeBrushSize = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_ERASER_SIZE = 'scratch-paint/eraser-mode/CHANGE_ERASER_SIZE';
var initialState = {
  brushSize: 40
};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_ERASER_SIZE:
      if (isNaN(action.brushSize)) {
        _log.default.warn("Invalid brush size: ".concat(action.brushSize));

        return state;
      }

      return {
        brushSize: Math.max(1, action.brushSize)
      };

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeBrushSize = function changeBrushSize(brushSize) {
  return {
    type: CHANGE_ERASER_SIZE,
    brushSize: brushSize
  };
};

exports.changeBrushSize = changeBrushSize;