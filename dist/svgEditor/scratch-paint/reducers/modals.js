"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.openStrokeColor = exports.openFillColor = exports.default = exports.closeStrokeColor = exports.closeFillColor = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _initialState;

var OPEN_MODAL = 'scratch-paint/modals/OPEN_MODAL';
var CLOSE_MODAL = 'scratch-paint/modals/CLOSE_MODAL';
var MODAL_FILL_COLOR = 'fillColor';
var MODAL_STROKE_COLOR = 'strokeColor';
var initialState = (_initialState = {}, (0, _defineProperty2.default)(_initialState, MODAL_FILL_COLOR, false), (0, _defineProperty2.default)(_initialState, MODAL_STROKE_COLOR, false), _initialState);

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case OPEN_MODAL:
      return Object.assign({}, initialState, (0, _defineProperty2.default)({}, action.modal, true));

    case CLOSE_MODAL:
      return Object.assign({}, initialState, (0, _defineProperty2.default)({}, action.modal, false));

    default:
      return state;
  }
};

exports.default = reducer;

var openModal = function openModal(modal) {
  return {
    type: OPEN_MODAL,
    modal: modal
  };
};

var closeModal = function closeModal(modal) {
  return {
    type: CLOSE_MODAL,
    modal: modal
  };
}; // Action creators ==================================


var openFillColor = function openFillColor() {
  return openModal(MODAL_FILL_COLOR);
};

exports.openFillColor = openFillColor;

var openStrokeColor = function openStrokeColor() {
  return openModal(MODAL_STROKE_COLOR);
};

exports.openStrokeColor = openStrokeColor;

var closeFillColor = function closeFillColor() {
  return closeModal(MODAL_FILL_COLOR);
};

exports.closeFillColor = closeFillColor;

var closeStrokeColor = function closeStrokeColor() {
  return closeModal(MODAL_STROKE_COLOR);
};

exports.closeStrokeColor = closeStrokeColor;