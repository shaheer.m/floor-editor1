"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeBitEraserSize = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_BIT_ERASER_SIZE = 'scratch-paint/eraser-mode/CHANGE_BIT_ERASER_SIZE';
var initialState = 40;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_BIT_ERASER_SIZE:
      if (isNaN(action.eraserSize)) {
        _log.default.warn("Invalid eraser size: ".concat(action.eraserSize));

        return state;
      }

      return Math.max(1, action.eraserSize);

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeBitEraserSize = function changeBitEraserSize(eraserSize) {
  return {
    type: CHANGE_BIT_ERASER_SIZE,
    eraserSize: eraserSize
  };
};

exports.changeBitEraserSize = changeBitEraserSize;