"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setShapesFilled = exports.default = void 0;

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _selectedItems = require("./selected-items");

var SET_FILLED = 'scratch-paint/fill-bitmap-shapes/SET_FILLED';
var initialState = true;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case SET_FILLED:
      return action.filled;

    case _selectedItems.CHANGE_SELECTED_ITEMS:
      if (action.bitmapMode && action.selectedItems && action.selectedItems[0] instanceof _paper.default.Shape) {
        return action.selectedItems[0].strokeWidth === 0;
      }

      return state;

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var setShapesFilled = function setShapesFilled(filled) {
  return {
    type: SET_FILLED,
    filled: filled
  };
};

exports.setShapesFilled = setShapesFilled;