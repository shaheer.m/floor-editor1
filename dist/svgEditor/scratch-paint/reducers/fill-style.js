"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.clearFillGradient = exports.changeFillGradientType = exports.changeFillColor2 = exports.changeFillColor = exports.DEFAULT_COLOR = exports.CHANGE_FILL_GRADIENT_TYPE = void 0;

var _makeColorStyleReducer = _interopRequireDefault(require("../lib/make-color-style-reducer"));

var CHANGE_FILL_COLOR = 'scratch-paint/fill-style/CHANGE_FILL_COLOR';
var CHANGE_FILL_COLOR_2 = 'scratch-paint/fill-style/CHANGE_FILL_COLOR_2';
var CHANGE_FILL_GRADIENT_TYPE = 'scratch-paint/fill-style/CHANGE_FILL_GRADIENT_TYPE';
exports.CHANGE_FILL_GRADIENT_TYPE = CHANGE_FILL_GRADIENT_TYPE;
var CLEAR_FILL_GRADIENT = 'scratch-paint/fill-style/CLEAR_FILL_GRADIENT';
var DEFAULT_COLOR = '#000000';
exports.DEFAULT_COLOR = DEFAULT_COLOR;
var reducer = (0, _makeColorStyleReducer.default)({
  changePrimaryColorAction: CHANGE_FILL_COLOR,
  changeSecondaryColorAction: CHANGE_FILL_COLOR_2,
  changeGradientTypeAction: CHANGE_FILL_GRADIENT_TYPE,
  clearGradientAction: CLEAR_FILL_GRADIENT,
  defaultColor: DEFAULT_COLOR,
  selectionPrimaryColorKey: 'fillColor',
  selectionSecondaryColorKey: 'fillColor2',
  selectionGradientTypeKey: 'fillGradientType'
}); // Action creators ==================================

exports.default = reducer;

var changeFillColor = function changeFillColor(fillColor) {
  return {
    type: CHANGE_FILL_COLOR,
    color: fillColor
  };
};

exports.changeFillColor = changeFillColor;

var changeFillColor2 = function changeFillColor2(fillColor) {
  return {
    type: CHANGE_FILL_COLOR_2,
    color: fillColor
  };
};

exports.changeFillColor2 = changeFillColor2;

var changeFillGradientType = function changeFillGradientType(gradientType) {
  return {
    type: CHANGE_FILL_GRADIENT_TYPE,
    gradientType: gradientType
  };
};

exports.changeFillGradientType = changeFillGradientType;

var clearFillGradient = function clearFillGradient() {
  return {
    type: CLEAR_FILL_GRADIENT
  };
};

exports.clearFillGradient = clearFillGradient;