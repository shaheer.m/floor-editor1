"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeColorIndex = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var _fillStyle = require("./fill-style");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var CHANGE_COLOR_INDEX = 'scratch-paint/color-index/CHANGE_COLOR_INDEX';
var initialState = 0;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_COLOR_INDEX:
      if (action.index !== 1 && action.index !== 0) {
        _log.default.warn("Invalid color index: ".concat(action.index));

        return state;
      }

      return action.index;

    case _fillStyle.CHANGE_FILL_GRADIENT_TYPE:
      if (action.gradientType === _gradientTypes.default.SOLID) return 0;

    /* falls through */

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeColorIndex = function changeColorIndex(index) {
  return {
    type: CHANGE_COLOR_INDEX,
    index: index
  };
};

exports.changeColorIndex = changeColorIndex;