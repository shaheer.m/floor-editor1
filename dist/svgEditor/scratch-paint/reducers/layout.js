"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setLayout = exports.default = exports.SET_LAYOUT = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var SET_LAYOUT = 'scratch-paint/layout/SET_LAYOUT';
exports.SET_LAYOUT = SET_LAYOUT;
var initialState = {
  rtl: false
};
var layouts = ['ltr', 'rtl'];

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case SET_LAYOUT:
      if (layouts.indexOf(action.layout) === -1) {
        _log.default.warn("Unrecognized layout provided: ".concat(action.layout));

        return state;
      }

      return {
        rtl: action.layout === 'rtl'
      };

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Change the layout to the new layout
 * @param {string} layout either 'ltr' or 'rtl'
 * @return {object} Redux action to change the selected items.
 */


exports.default = reducer;

var setLayout = function setLayout(layout) {
  return {
    type: SET_LAYOUT,
    layout: layout
  };
};

exports.setLayout = setLayout;