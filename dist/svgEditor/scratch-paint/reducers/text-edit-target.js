"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setTextEditTarget = exports.default = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_TEXT_EDIT_TARGET = 'scratch-paint/text-tool/CHANGE_TEXT_EDIT_TARGET';
var initialState = null;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_TEXT_EDIT_TARGET:
      if (typeof action.textEditTargetId === 'undefined') {
        _log.default.warn("Text edit target should not be set to undefined. Use null.");

        return state;
      } else if (typeof action.textEditTargetId === 'undefined' || isNaN(action.textEditTargetId)) {
        _log.default.warn("Text edit target should be an item ID number. Got: ".concat(action.textEditTargetId));

        return state;
      }

      return action.textEditTargetId;

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Set the currently-being-edited text field to the given item ID
 * @param {?number} textEditTargetId The paper.Item ID of the active text field.
 *     Leave empty if there is no text editing target.
 * @return {object} Redux action to change the text edit target.
 */


exports.default = reducer;

var setTextEditTarget = function setTextEditTarget(textEditTargetId) {
  return {
    type: CHANGE_TEXT_EDIT_TARGET,
    textEditTargetId: textEditTargetId ? textEditTargetId : null
  };
};

exports.setTextEditTarget = setTextEditTarget;