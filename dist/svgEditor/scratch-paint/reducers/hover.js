"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setHoveredItem = exports.default = exports.clearHoveredItem = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_HOVERED = 'scratch-paint/hover/CHANGE_HOVERED';
var initialState = null;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_HOVERED:
      if (typeof action.hoveredItemId === 'undefined') {
        _log.default.warn("Hovered item should not be set to undefined. Use null.");

        return state;
      } else if (typeof action.hoveredItemId === 'undefined' || isNaN(action.hoveredItemId)) {
        _log.default.warn("Hovered item should be an item ID number. Got: ".concat(action.hoveredItemId));

        return state;
      }

      return action.hoveredItemId;

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Set the hovered item state to the given item ID
 * @param {number} hoveredItemId The paper.Item ID of the hover indicator item.
 * @return {object} Redux action to change the hovered item.
 */


exports.default = reducer;

var setHoveredItem = function setHoveredItem(hoveredItemId) {
  return {
    type: CHANGE_HOVERED,
    hoveredItemId: hoveredItemId
  };
};

exports.setHoveredItem = setHoveredItem;

var clearHoveredItem = function clearHoveredItem() {
  return {
    type: CHANGE_HOVERED,
    hoveredItemId: null
  };
};

exports.clearHoveredItem = clearHoveredItem;