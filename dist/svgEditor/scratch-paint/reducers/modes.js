"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeMode = void 0;

var _modes = _interopRequireDefault(require("../lib/modes"));

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_MODE = 'scratch-paint/modes/CHANGE_MODE';
var initialState = _modes.default.SELECT;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_MODE:
      if (action.mode in _modes.default) {
        return action.mode;
      }

      _log.default.warn("Mode does not exist: ".concat(action.mode));

    /* falls through */

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeMode = function changeMode(mode) {
  return {
    type: CHANGE_MODE,
    mode: mode
  };
};

exports.changeMode = changeMode;