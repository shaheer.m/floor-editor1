"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setAlertText = exports.hideAlertText = exports.default = void 0;
var SHOW_ALERT = 'SHOW ALERT ON TOP';
var HIDE_ALERT = 'HIDE ALERT ON TOP';
var initialState = {
  status: false,
  msg: ''
};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case SHOW_ALERT:
      return {
        status: true,
        msg: action.alertText
      };

    case HIDE_ALERT:
      return {
        status: false,
        msg: ''
      };

    default:
      return state;
  }
};

exports.default = reducer;

var setAlertText = function setAlertText(alertText) {
  return {
    type: SHOW_ALERT,
    alertText: alertText ? alertText : null
  };
};

exports.setAlertText = setAlertText;

var hideAlertText = function hideAlertText() {
  return {
    type: HIDE_ALERT,
    alertText: ''
  };
};

exports.hideAlertText = hideAlertText;