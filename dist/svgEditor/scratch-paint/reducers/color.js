"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _eyeDropper = _interopRequireDefault(require("./eye-dropper"));

var _fillStyle = _interopRequireDefault(require("./fill-style"));

var _strokeStyle = _interopRequireDefault(require("./stroke-style"));

var _strokeWidth = _interopRequireDefault(require("./stroke-width"));

var _default = (0, _redux.combineReducers)({
  eyeDropper: _eyeDropper.default,
  fillColor: _fillStyle.default,
  strokeColor: _strokeStyle.default,
  strokeWidth: _strokeWidth.default
});

exports.default = _default;