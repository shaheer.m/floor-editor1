"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setCursor = exports.default = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var _cursors = _interopRequireDefault(require("../lib/cursors"));

var _eyeDropper = require("./eye-dropper");

var CHANGE_CURSOR = 'scratch-paint/cursor/CHANGE_CURSOR';
var initialState = _cursors.default.DEFAULT;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_CURSOR:
      if (typeof action.cursorString === 'undefined') {
        _log.default.warn("Cursor should not be set to undefined. Use 'default'.");

        return state;
      } else if (!Object.values(_cursors.default).includes(action.cursorString)) {
        _log.default.warn("Cursor should be a valid cursor string. Got: ".concat(action.cursorString));
      }

      return action.cursorString;

    case _eyeDropper.ACTIVATE_EYE_DROPPER:
      return _cursors.default.NONE;

    case _eyeDropper.DEACTIVATE_EYE_DROPPER:
      return _cursors.default.DEFAULT;

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Set the mouse cursor state to the given string
 * @param {string} cursorString The CSS cursor string.
 * @return {object} Redux action to change the cursor.
 */


exports.default = reducer;

var setCursor = function setCursor(cursorString) {
  return {
    type: CHANGE_CURSOR,
    cursorString: cursorString
  };
};

exports.setCursor = setCursor;