"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setSelectedItems = exports.redrawSelectionBox = exports.default = exports.clearSelectedItems = exports.CHANGE_SELECTED_ITEMS = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var CHANGE_SELECTED_ITEMS = 'scratch-paint/select/CHANGE_SELECTED_ITEMS';
exports.CHANGE_SELECTED_ITEMS = CHANGE_SELECTED_ITEMS;
var REDRAW_SELECTION_BOX = 'scratch-paint/select/REDRAW_SELECTION_BOX';
var initialState = [];

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case REDRAW_SELECTION_BOX:
      if (state.length > 0) return state.slice(0); // Sends an update even though the items haven't changed

      return state;

    case CHANGE_SELECTED_ITEMS:
      if (!action.selectedItems || !(action.selectedItems instanceof Array)) {
        _log.default.warn("No selected items or wrong format provided: ".concat(action.selectedItems));

        return state;
      }

      if (action.selectedItems.length > 1 && action.bitmapMode) {
        _log.default.warn("Multiselect should not be possible in bitmap mode: ".concat(action.selectedItems));

        return state;
      } // If they are both empty, no change


      if (action.selectedItems.length === 0 && state.length === 0) {
        return state;
      }

      return action.selectedItems;

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Set the selected item state to the given array of items
 * @param {Array<paper.Item>} selectedItems from paper.project.selectedItems
 * @param {?boolean} bitmapMode True if the items are being selected in bitmap mode
 * @return {object} Redux action to change the selected items.
 */


exports.default = reducer;

var setSelectedItems = function setSelectedItems(selectedItems, bitmapMode) {
  return {
    type: CHANGE_SELECTED_ITEMS,
    selectedItems: selectedItems,
    bitmapMode: bitmapMode
  };
};

exports.setSelectedItems = setSelectedItems;

var clearSelectedItems = function clearSelectedItems() {
  return {
    type: CHANGE_SELECTED_ITEMS,
    selectedItems: []
  };
};

exports.clearSelectedItems = clearSelectedItems;

var redrawSelectionBox = function redrawSelectionBox() {
  return {
    type: REDRAW_SELECTION_BOX
  };
};

exports.redrawSelectionBox = redrawSelectionBox;