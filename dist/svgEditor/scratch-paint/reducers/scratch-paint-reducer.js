"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _modes = _interopRequireDefault(require("./modes"));

var _bitBrushSize = _interopRequireDefault(require("./bit-brush-size"));

var _bitEraserSize = _interopRequireDefault(require("./bit-eraser-size"));

var _brushMode = _interopRequireDefault(require("./brush-mode"));

var _eraserMode = _interopRequireDefault(require("./eraser-mode"));

var _color = _interopRequireDefault(require("./color"));

var _clipboard = _interopRequireDefault(require("./clipboard"));

var _cursor = _interopRequireDefault(require("./cursor"));

var _fillBitmapShapes = _interopRequireDefault(require("./fill-bitmap-shapes"));

var _fillMode = _interopRequireDefault(require("./fill-mode"));

var _font = _interopRequireDefault(require("./font"));

var _format = _interopRequireDefault(require("./format"));

var _hover = _interopRequireDefault(require("./hover"));

var _layout = _interopRequireDefault(require("./layout"));

var _modals = _interopRequireDefault(require("./modals"));

var _selectedItems = _interopRequireDefault(require("./selected-items"));

var _textEditTarget = _interopRequireDefault(require("./text-edit-target"));

var _viewBounds = _interopRequireDefault(require("./view-bounds"));

var _undo = _interopRequireDefault(require("./undo"));

var _zoomLevels = _interopRequireDefault(require("./zoom-levels"));

var _alertTopError = _interopRequireDefault(require("./alert-top-error"));

var _default = (0, _redux.combineReducers)({
  mode: _modes.default,
  bitBrushSize: _bitBrushSize.default,
  bitEraserSize: _bitEraserSize.default,
  brushMode: _brushMode.default,
  color: _color.default,
  clipboard: _clipboard.default,
  cursor: _cursor.default,
  eraserMode: _eraserMode.default,
  fillBitmapShapes: _fillBitmapShapes.default,
  fillMode: _fillMode.default,
  font: _font.default,
  format: _format.default,
  hoveredItemId: _hover.default,
  layout: _layout.default,
  modals: _modals.default,
  selectedItems: _selectedItems.default,
  textEditTarget: _textEditTarget.default,
  undo: _undo.default,
  viewBounds: _viewBounds.default,
  zoomLevels: _zoomLevels.default,
  showAlertOnTop: _alertTopError.default
});

exports.default = _default;