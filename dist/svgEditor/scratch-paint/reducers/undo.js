"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.undoSnapshot = exports.undo = exports.redo = exports.default = exports.clearUndoState = exports.UNDO = exports.REDO = exports.MAX_STACK_SIZE = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var UNDO = 'scratch-paint/undo/UNDO';
exports.UNDO = UNDO;
var REDO = 'scratch-paint/undo/REDO';
exports.REDO = REDO;
var SNAPSHOT = 'scratch-paint/undo/SNAPSHOT';
var CLEAR = 'scratch-paint/undo/CLEAR';
var MAX_STACK_SIZE = 100;
exports.MAX_STACK_SIZE = MAX_STACK_SIZE;
var initialState = {
  stack: [],
  pointer: -1
};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case UNDO:
      if (state.pointer <= 0) {
        _log.default.warn("Can't undo, undo stack is empty");

        return state;
      }

      return {
        stack: state.stack,
        pointer: state.pointer - 1
      };

    case REDO:
      if (state.pointer <= -1 || state.pointer === state.stack.length - 1) {
        _log.default.warn("Can't redo, redo stack is empty");

        return state;
      }

      return {
        stack: state.stack,
        pointer: state.pointer + 1
      };

    case SNAPSHOT:
      if (!action.snapshot) {
        _log.default.warn("Couldn't create undo snapshot, no data provided");

        return state;
      } // Overflowed or about to overflow


      if (state.pointer >= MAX_STACK_SIZE - 1) {
        return {
          // Make a stack of size MAX_STACK_SIZE, cutting off the oldest snapshots.
          stack: state.stack.slice(state.pointer - MAX_STACK_SIZE + 2, state.pointer + 1).concat(action.snapshot),
          pointer: MAX_STACK_SIZE - 1
        };
      }

      return {
        // Performing an action clears the redo stack
        stack: state.stack.slice(0, state.pointer + 1).concat(action.snapshot),
        pointer: state.pointer + 1
      };

    case CLEAR:
      return initialState;

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var undoSnapshot = function undoSnapshot(snapshot) {
  return {
    type: SNAPSHOT,
    snapshot: snapshot
  };
};
/**
 * @param {Format} format Either VECTOR_SKIP_CONVERT or BITMAP_SKIP_CONVERT
 * @return {Action} undo action
 */


exports.undoSnapshot = undoSnapshot;

var undo = function undo(format) {
  return {
    type: UNDO,
    format: format
  };
};
/**
 * @param {Format} format Either VECTOR_SKIP_CONVERT or BITMAP_SKIP_CONVERT
 * @return {Action} undo action
 */


exports.undo = undo;

var redo = function redo(format) {
  return {
    type: REDO,
    format: format
  };
};

exports.redo = redo;

var clearUndoState = function clearUndoState() {
  return {
    type: CLEAR
  };
};

exports.clearUndoState = clearUndoState;