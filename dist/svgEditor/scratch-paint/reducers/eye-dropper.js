"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.deactivateEyeDropper = exports.activateEyeDropper = exports.DEACTIVATE_EYE_DROPPER = exports.ACTIVATE_EYE_DROPPER = void 0;
var ACTIVATE_EYE_DROPPER = 'scratch-paint/eye-dropper/ACTIVATE_COLOR_PICKER';
exports.ACTIVATE_EYE_DROPPER = ACTIVATE_EYE_DROPPER;
var DEACTIVATE_EYE_DROPPER = 'scratch-paint/eye-dropper/DEACTIVATE_COLOR_PICKER';
exports.DEACTIVATE_EYE_DROPPER = DEACTIVATE_EYE_DROPPER;
var initialState = {
  active: false,
  callback: function callback() {},
  // this will either be `onChangeFillColor` or `onChangeOutlineColor`
  previousTool: null // the tool that was previously active before eye dropper

};

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case ACTIVATE_EYE_DROPPER:
      return Object.assign({}, state, {
        active: true,
        callback: action.callback,
        previousTool: action.previousMode
      });

    case DEACTIVATE_EYE_DROPPER:
      return Object.assign({}, state, {
        active: false,
        callback: function callback() {},
        previousTool: null
      });

    default:
      return state;
  }
};

exports.default = reducer;

var activateEyeDropper = function activateEyeDropper(currentMode, callback) {
  return {
    type: ACTIVATE_EYE_DROPPER,
    callback: callback,
    previousMode: currentMode
  };
};

exports.activateEyeDropper = activateEyeDropper;

var deactivateEyeDropper = function deactivateEyeDropper() {
  return {
    type: DEACTIVATE_EYE_DROPPER
  };
};

exports.deactivateEyeDropper = deactivateEyeDropper;