"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateViewBounds = exports.default = void 0;

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../log/log"));

var UPDATE_VIEW_BOUNDS = 'scratch-paint/view/UPDATE_VIEW_BOUNDS';
var initialState = new _paper.default.Matrix(); // Identity

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case UPDATE_VIEW_BOUNDS:
      if (!(action.viewBounds instanceof _paper.default.Matrix)) {
        _log.default.warn("View bounds should be a paper.Matrix.");

        return state;
      }

      return action.viewBounds;

    default:
      return state;
  }
}; // Action creators ==================================

/**
 * Set the view bounds, which defines the zoom and scroll of the paper canvas.
 * @param {paper.Matrix} matrix The matrix applied to the view
 * @return {object} Redux action to set the view bounds
 */


exports.default = reducer;

var updateViewBounds = function updateViewBounds(matrix) {
  return {
    type: UPDATE_VIEW_BOUNDS,
    viewBounds: matrix.clone()
  };
};

exports.updateViewBounds = updateViewBounds;