"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeBitBrushSize = void 0;

var _log = _interopRequireDefault(require("../log/log"));

var _selectedItems = require("./selected-items");

var _stylePath = require("../helper/style-path");

// Bit brush size affects bit brush width, circle/rectangle outline drawing width, and line width
// in the bitmap paint editor.
var CHANGE_BIT_BRUSH_SIZE = 'scratch-paint/brush-mode/CHANGE_BIT_BRUSH_SIZE';
var initialState = 10;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case CHANGE_BIT_BRUSH_SIZE:
      if (isNaN(action.brushSize)) {
        _log.default.warn("Invalid brush size: ".concat(action.brushSize));

        return state;
      }

      return Math.max(1, action.brushSize);

    case _selectedItems.CHANGE_SELECTED_ITEMS:
      {
        // Don't change state if no selection
        if (!action.selectedItems || !action.selectedItems.length) {
          return state;
        } // Vector mode doesn't have bit width


        if (!action.bitmapMode) {
          return state;
        }

        var colorState = (0, _stylePath.getColorsFromSelection)(action.selectedItems, action.bitmapMode);
        if (colorState.thickness) return colorState.thickness;
        return state;
      }

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeBitBrushSize = function changeBitBrushSize(brushSize) {
  return {
    type: CHANGE_BIT_BRUSH_SIZE,
    brushSize: brushSize
  };
};

exports.changeBitBrushSize = changeBitBrushSize;