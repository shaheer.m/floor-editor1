"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.changeFormat = void 0;

var _format = _interopRequireDefault(require("../lib/format"));

var _log = _interopRequireDefault(require("../log/log"));

var _undo = require("./undo");

var CHANGE_FORMAT = 'scratch-paint/formats/CHANGE_FORMAT';
var initialState = null;

var reducer = function reducer(state, action) {
  if (typeof state === 'undefined') state = initialState;

  switch (action.type) {
    case _undo.UNDO:
    /* falls through */

    case _undo.REDO:
    /* falls through */

    case CHANGE_FORMAT:
      if (!action.format) return state;

      if (action.format in _format.default) {
        return action.format;
      }

      _log.default.warn("Format does not exist: ".concat(action.format));

    /* falls through */

    default:
      return state;
  }
}; // Action creators ==================================


exports.default = reducer;

var changeFormat = function changeFormat(format) {
  return {
    type: CHANGE_FORMAT,
    format: format
  };
};

exports.changeFormat = changeFormat;