"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.clearStrokeGradient = exports.changeStrokeGradientType = exports.changeStrokeColor2 = exports.changeStrokeColor = exports.DEFAULT_COLOR = exports.CHANGE_STROKE_GRADIENT_TYPE = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _makeColorStyleReducer = _interopRequireDefault(require("../lib/make-color-style-reducer"));

var _strokeWidth = require("./stroke-width");

var CHANGE_STROKE_COLOR = 'scratch-paint/stroke-style/CHANGE_STROKE_COLOR';
var CHANGE_STROKE_COLOR_2 = 'scratch-paint/stroke-style/CHANGE_STROKE_COLOR_2';
var CHANGE_STROKE_GRADIENT_TYPE = 'scratch-paint/stroke-style/CHANGE_STROKE_GRADIENT_TYPE';
exports.CHANGE_STROKE_GRADIENT_TYPE = CHANGE_STROKE_GRADIENT_TYPE;
var CLEAR_STROKE_GRADIENT = 'scratch-paint/stroke-style/CLEAR_STROKE_GRADIENT';
var DEFAULT_COLOR = '#000000';
exports.DEFAULT_COLOR = DEFAULT_COLOR;
var reducer = (0, _makeColorStyleReducer.default)({
  changePrimaryColorAction: CHANGE_STROKE_COLOR,
  changeSecondaryColorAction: CHANGE_STROKE_COLOR_2,
  changeGradientTypeAction: CHANGE_STROKE_GRADIENT_TYPE,
  clearGradientAction: CLEAR_STROKE_GRADIENT,
  defaultColor: DEFAULT_COLOR,
  selectionPrimaryColorKey: 'strokeColor',
  selectionSecondaryColorKey: 'strokeColor2',
  selectionGradientTypeKey: 'strokeGradientType'
}); // This is mostly the same as the generated reducer, but with one piece of extra logic to set the color to null when the
// stroke width is set to 0.
// https://redux.js.org/recipes/structuring-reducers/reusing-reducer-logic

var strokeReducer = function strokeReducer(state, action) {
  if (action.type === _strokeWidth.CHANGE_STROKE_WIDTH && Math.max(action.strokeWidth, 0) === 0) {
    // TODO: this preserves the gradient type when you change the stroke width to 0.
    // Alternatively, we could set gradientType to SOLID instead of setting secondary to null, but since
    // the stroke width is automatically set to 0 as soon as a "null" color is detected (including a gradient for
    // which both colors are null), that would change the gradient type back to solid if you selected null for both
    // gradient colors.
    return (0, _objectSpread2.default)((0, _objectSpread2.default)({}, state), {}, {
      primary: null,
      secondary: null
    });
  }

  return reducer(state, action);
}; // Action creators ==================================


exports.default = strokeReducer;

var changeStrokeColor = function changeStrokeColor(strokeColor) {
  return {
    type: CHANGE_STROKE_COLOR,
    color: strokeColor
  };
};

exports.changeStrokeColor = changeStrokeColor;

var changeStrokeColor2 = function changeStrokeColor2(strokeColor) {
  return {
    type: CHANGE_STROKE_COLOR_2,
    color: strokeColor
  };
};

exports.changeStrokeColor2 = changeStrokeColor2;

var changeStrokeGradientType = function changeStrokeGradientType(gradientType) {
  return {
    type: CHANGE_STROKE_GRADIENT_TYPE,
    gradientType: gradientType
  };
};

exports.changeStrokeGradientType = changeStrokeGradientType;

var clearStrokeGradient = function clearStrokeGradient() {
  return {
    type: CLEAR_STROKE_GRADIENT
  };
};

exports.clearStrokeGradient = clearStrokeGradient;