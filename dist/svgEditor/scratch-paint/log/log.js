"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _minilog = _interopRequireDefault(require("minilog"));

_minilog.default.enable();

var _default = (0, _minilog.default)('scratch-paint');

exports.default = _default;