"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _lodash2 = _interopRequireDefault(require("lodash.omit"));

var _reactRedux = require("react-redux");

var _copyPasteHoc = _interopRequireDefault(require("./copy-paste-hoc"));

var _bitmap = require("../helper/bitmap");

var _selection = require("../helper/selection");

var _group = require("../helper/group");

var _selectedItems = require("../reducers/selected-items");

var _modes = require("../reducers/modes");

var _format = _interopRequireWildcard(require("../lib/format"));

var _modes2 = _interopRequireDefault(require("../lib/modes"));

var KeyboardShortcutsHOC = function KeyboardShortcutsHOC(WrappedComponent) {
  var KeyboardShortcutsWrapper = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(KeyboardShortcutsWrapper, _React$Component);

    var _super = (0, _createSuper2.default)(KeyboardShortcutsWrapper);

    function KeyboardShortcutsWrapper(props) {
      var _this;

      (0, _classCallCheck2.default)(this, KeyboardShortcutsWrapper);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleKeyPress', 'changeToASelectMode', 'selectAll']);
      return _this;
    }

    (0, _createClass2.default)(KeyboardShortcutsWrapper, [{
      key: "handleKeyPress",
      value: function handleKeyPress(event) {
        if (event.target instanceof HTMLInputElement) {
          // Ignore keyboard shortcuts if a text input field is focused
          return;
        } // Don't activate keyboard shortcuts during text editing


        if (this.props.textEditing) return;

        if (event.key === 'Escape') {
          event.preventDefault();
          (0, _selection.clearSelection)(this.props.clearSelectedItems);
        } else if (event.key === 'Delete' || event.key === 'Backspace') {
          event.preventDefault();

          if ((0, _selection.deleteSelection)(this.props.mode, this.props.onUpdateImage)) {
            this.props.setSelectedItems(this.props.format);
          }
        } else if (event.metaKey || event.ctrlKey) {
          if (event.shiftKey && event.key.toLowerCase() === 'z') {
            this.props.onRedo();
          } else if (event.key === 'z') {
            this.props.onUndo();
          } else if (event.shiftKey && event.key.toLowerCase() === 'g') {
            if ((0, _group.shouldShowUngroup)()) {
              (0, _group.ungroupSelection)(_selectedItems.clearSelectedItems, _selectedItems.setSelectedItems, this.props.onUpdateImage);
            }

            event.preventDefault();
          } else if (event.key === 'g') {
            if ((0, _group.shouldShowGroup)()) {
              (0, _group.groupSelection)(_selectedItems.clearSelectedItems, _selectedItems.setSelectedItems, this.props.onUpdateImage);
            }

            event.preventDefault();
          } else if (event.key === 'c') {
            this.props.onCopyToClipboard();
          } else if (event.key === 'v') {
            this.changeToASelectMode();
            this.props.onPasteFromClipboard();
          } else if (event.key === 'x') {
            var selectedItems = (0, _selection.getSelectedRootItems)();

            if (selectedItems.length > 0) {
              this.props.onCopyToClipboard();

              if ((0, _selection.deleteSelection)(this.props.mode, this.props.onUpdateImage)) {
                this.props.setSelectedItems(this.props.format);
              }
            }

            event.preventDefault();
          } else if (event.key === 'a') {
            this.changeToASelectMode();
            event.preventDefault();
            this.selectAll();
          }
        }
      }
    }, {
      key: "changeToASelectMode",
      value: function changeToASelectMode() {
        if ((0, _format.isBitmap)(this.props.format)) {
          if (this.props.mode !== _modes2.default.BIT_SELECT) {
            this.props.changeMode(_modes2.default.BIT_SELECT);
          }
        } else if (this.props.mode !== _modes2.default.SELECT && this.props.mode !== _modes2.default.RESHAPE) {
          this.props.changeMode(_modes2.default.SELECT);
        }
      }
    }, {
      key: "selectAll",
      value: function selectAll() {
        if ((0, _format.isBitmap)(this.props.format)) {
          (0, _bitmap.selectAllBitmap)(this.props.clearSelectedItems);
          this.props.setSelectedItems(this.props.format);
        } else if (this.props.mode === _modes2.default.RESHAPE) {
          if ((0, _selection.selectAllSegments)()) this.props.setSelectedItems(this.props.format);
        } else if ((0, _selection.selectAllItems)()) {
          this.props.setSelectedItems(this.props.format);
        }
      }
    }, {
      key: "render",
      value: function render() {
        var componentProps = (0, _lodash2.default)(this.props, ['changeMode', 'clearSelectedItems', 'format', 'mode', 'onCopyToClipboard', 'onPasteFromClipboard', 'setSelectedItems', 'textEditing']);
        return /*#__PURE__*/_react.default.createElement(WrappedComponent, Object.assign({
          onKeyPress: this.handleKeyPress
        }, componentProps));
      }
    }]);
    return KeyboardShortcutsWrapper;
  }(_react.default.Component);

  var mapStateToProps = function mapStateToProps(state) {
    return {
      mode: state.scratchPaint.mode,
      format: state.scratchPaint.format,
      textEditing: state.scratchPaint.textEditTarget !== null
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
      changeMode: function changeMode(mode) {
        dispatch((0, _modes.changeMode)(mode));
      },
      clearSelectedItems: function clearSelectedItems() {
        dispatch((0, _selectedItems.clearSelectedItems)());
      },
      setSelectedItems: function setSelectedItems(format) {
        dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
      }
    };
  };

  return (0, _copyPasteHoc.default)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(KeyboardShortcutsWrapper));
};

var _default = KeyboardShortcutsHOC;
exports.default = _default;