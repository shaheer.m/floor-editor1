"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _excluded = ["hoveredItemId"];

var SelectionHOC = function SelectionHOC(WrappedComponent) {
  var SelectionComponent = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(SelectionComponent, _React$Component);

    var _super = (0, _createSuper2.default)(SelectionComponent);

    function SelectionComponent(props) {
      var _this;

      (0, _classCallCheck2.default)(this, SelectionComponent);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['removeItemById']);
      return _this;
    }

    (0, _createClass2.default)(SelectionComponent, [{
      key: "componentDidUpdate",
      value: function componentDidUpdate(prevProps) {
        // Hovered item has changed
        if (this.props.hoveredItemId && this.props.hoveredItemId !== prevProps.hoveredItemId || !this.props.hoveredItemId && prevProps.hoveredItemId) {
          // Remove the old hover item if any
          this.removeItemById(prevProps.hoveredItemId);
        }
      }
    }, {
      key: "removeItemById",
      value: function removeItemById(itemId) {
        if (itemId) {
          var match = _paper.default.project.getItem({
            match: function match(item) {
              return item.id === itemId;
            }
          });

          if (match) {
            match.remove();
          }
        }
      }
    }, {
      key: "render",
      value: function render() {
        var _this$props = this.props,
            hoveredItemId = _this$props.hoveredItemId,
            props = (0, _objectWithoutProperties2.default)(_this$props, _excluded);
        return /*#__PURE__*/_react.default.createElement(WrappedComponent, props);
      }
    }]);
    return SelectionComponent;
  }(_react.default.Component);

  var mapStateToProps = function mapStateToProps(state) {
    return {
      hoveredItemId: state.scratchPaint.hoveredItemId
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps)(SelectionComponent);
};

var _default = SelectionHOC;
exports.default = _default;