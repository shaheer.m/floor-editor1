"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _lodash2 = _interopRequireDefault(require("lodash.omit"));

var _reactRedux = require("react-redux");

var _messages = _interopRequireDefault(require("../lib/messages.js"));

var _selection = require("../helper/selection");

var _bitmap = require("../helper/bitmap");

var _format = _interopRequireWildcard(require("../lib/format"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _selectedItems = require("../reducers/selected-items");

var _clipboard = require("../reducers/clipboard");

var _alertTopError = require("../reducers/alert-top-error");

var CopyPasteHOC = function CopyPasteHOC(WrappedComponent) {
  var CopyPasteWrapper = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(CopyPasteWrapper, _React$Component);

    var _super = (0, _createSuper2.default)(CopyPasteWrapper);

    function CopyPasteWrapper(props) {
      var _this;

      (0, _classCallCheck2.default)(this, CopyPasteWrapper);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleCopy', 'handlePaste']);
      return _this;
    }

    (0, _createClass2.default)(CopyPasteWrapper, [{
      key: "handleCopy",
      value: function handleCopy() {
        var selectedItems = [];

        if (this.props.mode === _modes.default.RESHAPE) {
          var leafItems = (0, _selection.getSelectedLeafItems)(); // Copy root of compound paths

          var _iterator = (0, _createForOfIteratorHelper2.default)(leafItems),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var item = _step.value;

              if (item.parent && item.parent instanceof _paper.default.CompoundPath) {
                selectedItems.push(item.parent);
              } else {
                selectedItems.push(item);
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        } else {
          selectedItems = (0, _selection.getSelectedRootItems)();
        }

        var itemHasName = (0, _selection.checkIfSelectedItemHasName)(selectedItems);

        if (itemHasName) {
          this.props.showAlert(_messages.default.errorDuplicatingAllocatedItem.description);
          return false;
        } else {
          if (selectedItems.length === 0) {
            if ((0, _format.isBitmap)(this.props.format)) {
              var raster = (0, _bitmap.getTrimmedRaster)(false
              /* shouldInsert */
              );
              if (!raster) return;
              selectedItems.push(raster);
            } else {
              selectedItems = (0, _selection.getAllRootItems)();
            }
          }

          var clipboardItems = [];

          for (var i = 0; i < selectedItems.length; i++) {
            var jsonItem = selectedItems[i].exportJSON({
              asString: false
            });
            clipboardItems.push(jsonItem);
          }

          this.props.setClipboardItems(clipboardItems);
          return true;
        }
      }
    }, {
      key: "handlePaste",
      value: function handlePaste() {
        (0, _selection.clearSelection)(this.props.clearSelectedItems);
        if (this.props.clipboardItems.length === 0) return;
        var items = [];

        for (var i = 0; i < this.props.clipboardItems.length; i++) {
          var item = _paper.default.Base.importJSON(this.props.clipboardItems[i]);

          if (item) {
            items.push(item);
          }
        }

        if (!items.length) return; // If pasting a group or non-raster to bitmap, rasterize first

        if ((0, _format.isBitmap)(this.props.format) && !(items.length === 1 && items[0] instanceof _paper.default.Raster)) {
          var group = new _paper.default.Group(items);
          items = [group.rasterize()];
          group.remove();
        }

        for (var _i = 0, _items = items; _i < _items.length; _i++) {
          var _item = _items[_i];

          var placedItem = _paper.default.project.getActiveLayer().addChild(_item);

          placedItem.selected = true;
          placedItem.position.x += 10 * this.props.pasteOffset;
          placedItem.position.y += 10 * this.props.pasteOffset;
        }

        this.props.incrementPasteOffset();
        this.props.setSelectedItems(this.props.format);
        this.props.onUpdateImage();
      }
    }, {
      key: "render",
      value: function render() {
        var componentProps = (0, _lodash2.default)(this.props, ['clearSelectedItems', 'clipboardItems', 'format', 'incrementPasteOffset', 'mode', 'pasteOffset', 'setClipboardItems', 'setSelectedItems']);
        return /*#__PURE__*/_react.default.createElement(WrappedComponent, Object.assign({
          onCopyToClipboard: this.handleCopy,
          onPasteFromClipboard: this.handlePaste
        }, componentProps));
      }
    }]);
    return CopyPasteWrapper;
  }(_react.default.Component);

  var mapStateToProps = function mapStateToProps(state) {
    return {
      clipboardItems: state.scratchPaint.clipboard.items,
      format: state.scratchPaint.format,
      mode: state.scratchPaint.mode,
      pasteOffset: state.scratchPaint.clipboard.pasteOffset
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
      setClipboardItems: function setClipboardItems(items) {
        dispatch((0, _clipboard.setClipboardItems)(items));
      },
      incrementPasteOffset: function incrementPasteOffset() {
        dispatch((0, _clipboard.incrementPasteOffset)());
      },
      clearSelectedItems: function clearSelectedItems() {
        dispatch((0, _selectedItems.clearSelectedItems)());
      },
      setSelectedItems: function setSelectedItems(format) {
        dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
      },
      showAlert: function showAlert(msg) {
        dispatch((0, _alertTopError.setAlertText)(msg));
      }
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(CopyPasteWrapper);
};

var _default = CopyPasteHOC;
exports.default = _default;