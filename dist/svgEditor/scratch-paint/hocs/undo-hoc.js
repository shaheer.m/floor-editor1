"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _lodash2 = _interopRequireDefault(require("lodash.omit"));

var _reactRedux = require("react-redux");

var _selection = require("../helper/selection");

var _selectedItems = require("../reducers/selected-items");

var _undo = require("../helper/undo");

var _undo2 = require("../reducers/undo");

var _format = _interopRequireWildcard(require("../lib/format"));

var UndoHOC = function UndoHOC(WrappedComponent) {
  var UndoWrapper = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(UndoWrapper, _React$Component);

    var _super = (0, _createSuper2.default)(UndoWrapper);

    function UndoWrapper(props) {
      var _this;

      (0, _classCallCheck2.default)(this, UndoWrapper);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleUndo', 'handleRedo', 'handleSetSelectedItems', 'shouldShowUndo', 'shouldShowRedo']);
      return _this;
    }

    (0, _createClass2.default)(UndoWrapper, [{
      key: "handleUndo",
      value: function handleUndo() {
        (0, _undo.performUndo)(this.props.undoState, this.props.onUndo, this.handleSetSelectedItems, this.props.onUpdateImage);
      }
    }, {
      key: "handleRedo",
      value: function handleRedo() {
        (0, _undo.performRedo)(this.props.undoState, this.props.onRedo, this.handleSetSelectedItems, this.props.onUpdateImage);
      }
    }, {
      key: "handleSetSelectedItems",
      value: function handleSetSelectedItems() {
        this.props.setSelectedItems(this.props.format);
      }
    }, {
      key: "shouldShowUndo",
      value: function shouldShowUndo() {
        return (0, _undo.shouldShowUndo)(this.props.undoState);
      }
    }, {
      key: "shouldShowRedo",
      value: function shouldShowRedo() {
        return (0, _undo.shouldShowRedo)(this.props.undoState);
      }
    }, {
      key: "render",
      value: function render() {
        var componentProps = (0, _lodash2.default)(this.props, ['format', 'onUndo', 'onRedo', 'setSelectedItems', 'undoState']);
        return /*#__PURE__*/_react.default.createElement(WrappedComponent, Object.assign({
          shouldShowRedo: this.shouldShowRedo,
          shouldShowUndo: this.shouldShowUndo,
          onRedo: this.handleRedo,
          onUndo: this.handleUndo
        }, componentProps));
      }
    }]);
    return UndoWrapper;
  }(_react.default.Component);

  var mapStateToProps = function mapStateToProps(state) {
    return {
      format: state.scratchPaint.format,
      undoState: state.scratchPaint.undo
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
      setSelectedItems: function setSelectedItems(format) {
        dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
      },
      onUndo: function onUndo(format) {
        dispatch((0, _undo2.undo)(format));
      },
      onRedo: function onRedo(format) {
        dispatch((0, _undo2.redo)(format));
      }
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(UndoWrapper);
};

var _default = UndoHOC;
exports.default = _default;