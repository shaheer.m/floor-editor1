"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../log/log"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _lodash2 = _interopRequireDefault(require("lodash.omit"));

var _reactRedux = require("react-redux");

var _undo = require("../reducers/undo");

var _selectedItems = require("../reducers/selected-items");

var _viewBounds = require("../reducers/view-bounds");

var _selection = require("../helper/selection");

var _layer = require("../helper/layer");

var _bitmap = require("../helper/bitmap");

var _undo2 = require("../helper/undo");

var _math = require("../helper/math");

var _view = require("../helper/view");

var _modes = _interopRequireWildcard(require("../lib/modes"));

var _format = _interopRequireWildcard(require("../lib/format"));

var UpdateImageHOC = function UpdateImageHOC(WrappedComponent) {
  var UpdateImageWrapper = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(UpdateImageWrapper, _React$Component);

    var _super = (0, _createSuper2.default)(UpdateImageWrapper);

    function UpdateImageWrapper(props) {
      var _this;

      (0, _classCallCheck2.default)(this, UpdateImageWrapper);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleUpdateImage', 'handleUpdateBitmap', 'handleUpdateVector']);
      return _this;
    }
    /**
     * @param {?boolean} skipSnapshot True if the call to update image should not trigger saving
     * an undo state. For instance after calling undo.
     * @param {?Formats} formatOverride Normally the mode is used to determine the format of the image,
     * but the format used can be overridden here. In particular when converting between formats,
     * the does not accurately represent the format.
     */


    (0, _createClass2.default)(UpdateImageWrapper, [{
      key: "handleUpdateImage",
      value: function handleUpdateImage(skipSnapshot, formatOverride) {
        // If in the middle of switching formats, rely on the current mode instead of format.
        var actualFormat = formatOverride ? formatOverride : _modes.BitmapModes[this.props.mode] ? _format.default.BITMAP : _format.default.VECTOR;

        if ((0, _format.isBitmap)(actualFormat)) {
          this.handleUpdateBitmap(skipSnapshot);
        } else if ((0, _format.isVector)(actualFormat)) {
          this.handleUpdateVector(skipSnapshot);
        } // Any time an image update is made, recalculate the bounds of the artwork


        (0, _view.setWorkspaceBounds)();
        this.props.updateViewBounds(_paper.default.view.matrix);
      }
    }, {
      key: "handleUpdateBitmap",
      value: function handleUpdateBitmap(skipSnapshot) {
        if (!(0, _layer.getRaster)().loaded) {
          // In general, callers of updateImage should wait for getRaster().loaded = true before
          // calling updateImage.
          // However, this may happen if the user is rapidly undoing/redoing. In this case it's safe
          // to skip the update.
          _log.default.warn('Bitmap layer should be loaded before calling updateImage.');

          return;
        } // Anything that is selected is on the vector layer waiting to be committed to the bitmap layer.
        // Plaster the selection onto the raster layer before exporting, if there is a selection.


        var plasteredRaster = (0, _layer.getRaster)().getSubRaster((0, _layer.getRaster)().bounds); // Clone the raster layer

        plasteredRaster.remove(); // Don't insert

        var selectedItems = (0, _selection.getSelectedLeafItems)();

        if (selectedItems.length === 1) {
          var item = selectedItems[0];

          if (item instanceof _paper.default.Raster) {
            if (!item.loaded || item.data && item.data.expanded && !item.data.expanded.loaded) {
              // This may get logged when rapidly undoing/redoing or changing costumes,
              // in which case the warning is not relevant.
              _log.default.warn('Bitmap layer should be loaded before calling updateImage.');

              return;
            }

            (0, _bitmap.commitSelectionToBitmap)(item, plasteredRaster);
          } else if (item instanceof _paper.default.Shape && item.type === 'rectangle') {
            (0, _bitmap.commitRectToBitmap)(item, plasteredRaster);
          } else if (item instanceof _paper.default.Shape && item.type === 'ellipse') {
            (0, _bitmap.commitOvalToBitmap)(item, plasteredRaster);
          } else if (item instanceof _paper.default.PointText) {
            var bounds = item.drawnBounds;
            var textRaster = item.rasterize(72, false
            /* insert */
            , bounds);
            plasteredRaster.drawImage(textRaster.canvas, new _paper.default.Point(Math.floor(bounds.x), Math.floor(bounds.y)));
          }
        }

        var rect = (0, _bitmap.getHitBounds)(plasteredRaster); // Use 1x1 instead of 0x0 for getting imageData since paper.js automagically
        // returns the full artboard in the case of getImageData(0x0).
        // Bitmaps need a non-zero width/height in order to be saved as PNG.

        if (rect.width === 0 || rect.height === 0) {
          rect.width = rect.height = 1;
        }

        var imageData = plasteredRaster.getImageData(rect);
        this.props.onUpdateImage(false
        /* isVector */
        , imageData, _view.ART_BOARD_WIDTH / 2 - rect.x, _view.ART_BOARD_HEIGHT / 2 - rect.y);

        if (!skipSnapshot) {
          (0, _undo2.performSnapshot)(this.props.undoSnapshot, _format.default.BITMAP);
        }
      }
    }, {
      key: "handleUpdateVector",
      value: function handleUpdateVector(skipSnapshot) {
        // Remove viewbox (this would make it export at MAX_WORKSPACE_BOUNDS)
        var workspaceMask;

        if (_paper.default.project.activeLayer.clipped) {
          var _iterator = (0, _createForOfIteratorHelper2.default)(_paper.default.project.activeLayer.children),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var child = _step.value;

              if (child.isClipMask()) {
                workspaceMask = child;
                break;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          _paper.default.project.activeLayer.clipped = false;
          workspaceMask.remove();
        }

        var guideLayers = (0, _layer.hideGuideLayers)(true
        /* includeRaster */
        ); // Export at 0.5x

        (0, _math.scaleWithStrokes)(_paper.default.project.activeLayer, .5, new _paper.default.Point());
        var bounds = _paper.default.project.activeLayer.drawnBounds; // `bounds.x` and `bounds.y` are relative to the top left corner,
        // but if there is no content in the active layer, they default to 0,
        // making the "Scratch space" rotation center ((SVG_ART_BOARD_WIDTH / 2), (SVG_ART_BOARD_HEIGHT / 2)),
        // aka the upper left corner. Special-case this to be (0, 0), which is the center of the art board.

        var centerX = bounds.width === 0 ? 0 : _view.SVG_ART_BOARD_WIDTH / 2 - bounds.x;
        var centerY = bounds.height === 0 ? 0 : _view.SVG_ART_BOARD_HEIGHT / 2 - bounds.y;
        this.props.onUpdateImage(true
        /* isVector */
        , _paper.default.project.exportSVG({
          asString: true,
          bounds: 'content',
          matrix: new _paper.default.Matrix().translate(-bounds.x, -bounds.y)
        }), centerX, centerY);
        (0, _math.scaleWithStrokes)(_paper.default.project.activeLayer, 2, new _paper.default.Point());
        _paper.default.project.activeLayer.applyMatrix = true;
        (0, _layer.showGuideLayers)(guideLayers); // Add back viewbox

        if (workspaceMask) {
          _paper.default.project.activeLayer.addChild(workspaceMask);

          workspaceMask.clipMask = true;
        }

        if (!skipSnapshot) {
          (0, _undo2.performSnapshot)(this.props.undoSnapshot, _format.default.VECTOR);
        }
      }
    }, {
      key: "render",
      value: function render() {
        var componentProps = (0, _lodash2.default)(this.props, ['format', 'onUpdateImage', 'undoSnapshot']);
        return /*#__PURE__*/_react.default.createElement(WrappedComponent, Object.assign({
          onUpdateImage: this.handleUpdateImage
        }, componentProps));
      }
    }]);
    return UpdateImageWrapper;
  }(_react.default.Component);

  var mapStateToProps = function mapStateToProps(state) {
    return {
      format: state.scratchPaint.format,
      mode: state.scratchPaint.mode,
      undoState: state.scratchPaint.undo
    };
  };

  var mapDispatchToProps = function mapDispatchToProps(dispatch) {
    return {
      setSelectedItems: function setSelectedItems(format) {
        dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
      },
      undoSnapshot: function undoSnapshot(snapshot) {
        dispatch((0, _undo.undoSnapshot)(snapshot));
      },
      updateViewBounds: function updateViewBounds(matrix) {
        dispatch((0, _viewBounds.updateViewBounds)(matrix));
      }
    };
  };

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(UpdateImageWrapper);
};

var _default = UpdateImageHOC;
exports.default = _default;