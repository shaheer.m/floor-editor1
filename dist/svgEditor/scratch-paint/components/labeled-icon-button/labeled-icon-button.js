"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _button = _interopRequireDefault(require("../button/button"));

var _labeledIconButtonModule = _interopRequireDefault(require("./labeled-icon-button.module.css"));

var _excluded = ["className", "hideLabel", "imgAlt", "imgSrc", "onClick", "title"];

var LabeledIconButton = function LabeledIconButton(_ref) {
  var className = _ref.className,
      hideLabel = _ref.hideLabel,
      imgAlt = _ref.imgAlt,
      imgSrc = _ref.imgSrc,
      onClick = _ref.onClick,
      title = _ref.title,
      props = (0, _objectWithoutProperties2.default)(_ref, _excluded);
  return /*#__PURE__*/_react.default.createElement(_button.default, Object.assign({
    className: (0, _classnames.default)(className, _labeledIconButtonModule.default.modEditField),
    onClick: onClick
  }, props), /*#__PURE__*/_react.default.createElement("img", {
    alt: imgAlt || title,
    className: _labeledIconButtonModule.default.editFieldIcon,
    draggable: false,
    src: imgSrc,
    title: title
  }), !hideLabel && /*#__PURE__*/_react.default.createElement("span", {
    className: _labeledIconButtonModule.default.editFieldTitle
  }, title));
};

var _default = LabeledIconButton;
exports.default = _default;