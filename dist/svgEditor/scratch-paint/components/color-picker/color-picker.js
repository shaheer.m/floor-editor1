"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactIntl = require("react-intl");

var _classnames = _interopRequireDefault(require("classnames"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _slider = _interopRequireWildcard(require("../forms/slider"));

var _labeledIconButton = _interopRequireDefault(require("../labeled-icon-button/labeled-icon-button"));

var _colorPickerModule = _interopRequireDefault(require("./color-picker.module.css"));

var _gradientTypes = _interopRequireDefault(require("../../lib/gradient-types"));

var _stylePath = require("../../helper/style-path");

var _eyeDropper = _interopRequireDefault(require("./icons/eye-dropper.svg"));

var _noFill = _interopRequireDefault(require("../color-button/no-fill.svg"));

var _mixedFill = _interopRequireDefault(require("../color-button/mixed-fill.svg"));

var _fillHorzGradientEnabled = _interopRequireDefault(require("./icons/fill-horz-gradient-enabled.svg"));

var _fillRadialEnabled = _interopRequireDefault(require("./icons/fill-radial-enabled.svg"));

var _fillSolidEnabled = _interopRequireDefault(require("./icons/fill-solid-enabled.svg"));

var _fillVertGradientEnabled = _interopRequireDefault(require("./icons/fill-vert-gradient-enabled.svg"));

var _swap = _interopRequireDefault(require("./icons/swap.svg"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var hsvToHex = function hsvToHex(h, s, v) {
  return (// Scale hue back up to [0, 360] from [0, 100]
    (0, _parseColor.default)("hsv(".concat(3.6 * h, ", ").concat(s, ", ").concat(v, ")")).hex
  );
};

var messages = (0, _reactIntl.defineMessages)({
  swap: {
    defaultMessage: 'Swap',
    description: 'Label for button that swaps the two colors in a gradient',
    id: 'paint.colorPicker.swap'
  }
});

var ColorPickerComponent = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ColorPickerComponent, _React$Component);

  var _super = (0, _createSuper2.default)(ColorPickerComponent);

  function ColorPickerComponent() {
    (0, _classCallCheck2.default)(this, ColorPickerComponent);
    return _super.apply(this, arguments);
  }

  (0, _createClass2.default)(ColorPickerComponent, [{
    key: "_makeBackground",
    value: function _makeBackground(channel) {
      var stops = []; // Generate the color slider background CSS gradients by adding
      // color stops depending on the slider.

      for (var n = 100; n >= 0; n -= 10) {
        switch (channel) {
          case 'hue':
            stops.push(hsvToHex(n, this.props.saturation, this.props.brightness));
            break;

          case 'saturation':
            stops.push(hsvToHex(this.props.hue, n, this.props.brightness));
            break;

          case 'brightness':
            stops.push(hsvToHex(this.props.hue, this.props.saturation, n));
            break;

          default:
            throw new Error("Unknown channel for color sliders: ".concat(channel));
        }
      } // The sliders are a rounded capsule shape, and the slider handles are circles. As a consequence, when the
      // slider handle is fully to one side, its center is actually moved away from the start/end of the slider by
      // the slider handle's radius, meaning that the effective range of the slider excludes the rounded caps.
      // To compensate for this, position the first stop to where the rounded cap ends, and position the last stop
      // to where the rounded cap begins.


      var halfHandleWidth = _slider.HANDLE_WIDTH / 2;
      stops[0] += " 0 ".concat(halfHandleWidth, "px");
      stops[stops.length - 1] += " ".concat(_slider.CONTAINER_WIDTH - halfHandleWidth, "px 100%");
      return "linear-gradient(to left, ".concat(stops.join(','), ")");
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames, _classNames2, _classNames3, _classNames4, _classNames5, _classNames6, _classNames7, _classNames8;

      return /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.colorPickerContainer,
        dir: this.props.rtl ? 'rtl' : 'ltr'
      }, this.props.shouldShowGradientTools ? /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.row
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.gradientPickerRow
      }, /*#__PURE__*/_react.default.createElement("img", {
        className: (0, _classnames.default)((_classNames = {}, (0, _defineProperty2.default)(_classNames, _colorPickerModule.default.inactiveGradient, this.props.gradientType !== _gradientTypes.default.SOLID), (0, _defineProperty2.default)(_classNames, _colorPickerModule.default.clickable, true), _classNames)),
        draggable: false,
        src: _fillSolidEnabled.default,
        onClick: this.props.onChangeGradientTypeSolid
      }), /*#__PURE__*/_react.default.createElement("img", {
        className: (0, _classnames.default)((_classNames2 = {}, (0, _defineProperty2.default)(_classNames2, _colorPickerModule.default.inactiveGradient, this.props.gradientType !== _gradientTypes.default.HORIZONTAL), (0, _defineProperty2.default)(_classNames2, _colorPickerModule.default.clickable, true), _classNames2)),
        draggable: false,
        src: _fillHorzGradientEnabled.default,
        onClick: this.props.onChangeGradientTypeHorizontal
      }), /*#__PURE__*/_react.default.createElement("img", {
        className: (0, _classnames.default)((_classNames3 = {}, (0, _defineProperty2.default)(_classNames3, _colorPickerModule.default.inactiveGradient, this.props.gradientType !== _gradientTypes.default.VERTICAL), (0, _defineProperty2.default)(_classNames3, _colorPickerModule.default.clickable, true), _classNames3)),
        draggable: false,
        src: _fillVertGradientEnabled.default,
        onClick: this.props.onChangeGradientTypeVertical
      }), /*#__PURE__*/_react.default.createElement("img", {
        className: (0, _classnames.default)((_classNames4 = {}, (0, _defineProperty2.default)(_classNames4, _colorPickerModule.default.inactiveGradient, this.props.gradientType !== _gradientTypes.default.RADIAL), (0, _defineProperty2.default)(_classNames4, _colorPickerModule.default.clickable, true), _classNames4)),
        draggable: false,
        src: _fillRadialEnabled.default,
        onClick: this.props.onChangeGradientTypeRadial
      }))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.divider
      }), this.props.gradientType === _gradientTypes.default.SOLID ? null : /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.row
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(_colorPickerModule.default.gradientPickerRow, _colorPickerModule.default.gradientSwatchesRow)
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)((_classNames5 = {}, (0, _defineProperty2.default)(_classNames5, _colorPickerModule.default.clickable, true), (0, _defineProperty2.default)(_classNames5, _colorPickerModule.default.swatch, true), (0, _defineProperty2.default)(_classNames5, _colorPickerModule.default.largeSwatch, true), (0, _defineProperty2.default)(_classNames5, _colorPickerModule.default.activeSwatch, this.props.colorIndex === 0), _classNames5)),
        style: {
          backgroundColor: this.props.color === null || this.props.color === _stylePath.MIXED ? 'white' : this.props.color
        },
        onClick: this.props.onSelectColor
      }, this.props.color === null ? /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.largeSwatchIcon,
        draggable: false,
        src: _noFill.default
      }) : this.props.color === _stylePath.MIXED ? /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.largeSwatchIcon,
        draggable: false,
        src: _mixedFill.default
      }) : null), /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        className: _colorPickerModule.default.swapButton,
        imgSrc: _swap.default,
        title: this.props.intl.formatMessage(messages.swap),
        onClick: this.props.onSwap
      }), /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)((_classNames6 = {}, (0, _defineProperty2.default)(_classNames6, _colorPickerModule.default.clickable, true), (0, _defineProperty2.default)(_classNames6, _colorPickerModule.default.swatch, true), (0, _defineProperty2.default)(_classNames6, _colorPickerModule.default.largeSwatch, true), (0, _defineProperty2.default)(_classNames6, _colorPickerModule.default.activeSwatch, this.props.colorIndex === 1), _classNames6)),
        style: {
          backgroundColor: this.props.color2 === null || this.props.color2 === _stylePath.MIXED ? 'white' : this.props.color2
        },
        onClick: this.props.onSelectColor2
      }, this.props.color2 === null ? /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.largeSwatchIcon,
        draggable: false,
        src: _noFill.default
      }) : this.props.color2 === _stylePath.MIXED ? /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.largeSwatchIcon,
        draggable: false,
        src: _mixedFill.default
      }) : null)))) : null, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.row
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowHeader
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelName
      }, /*#__PURE__*/_react.default.createElement(_reactIntl.FormattedMessage, {
        defaultMessage: "Color",
        description: "Label for the hue component in the color picker",
        id: "paint.paintEditor.hue"
      })), /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelReadout
      }, Math.round(this.props.hue))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowSlider
      }, /*#__PURE__*/_react.default.createElement(_slider.default, {
        background: this._makeBackground('hue'),
        value: this.props.hue,
        onChange: this.props.onHueChange
      }))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.row
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowHeader
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelName
      }, /*#__PURE__*/_react.default.createElement(_reactIntl.FormattedMessage, {
        defaultMessage: "Saturation",
        description: "Label for the saturation component in the color picker",
        id: "paint.paintEditor.saturation"
      })), /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelReadout
      }, Math.round(this.props.saturation))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowSlider
      }, /*#__PURE__*/_react.default.createElement(_slider.default, {
        background: this._makeBackground('saturation'),
        value: this.props.saturation,
        onChange: this.props.onSaturationChange
      }))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.row
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowHeader
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelName
      }, /*#__PURE__*/_react.default.createElement(_reactIntl.FormattedMessage, {
        defaultMessage: "Brightness",
        description: "Label for the brightness component in the color picker",
        id: "paint.paintEditor.brightness"
      })), /*#__PURE__*/_react.default.createElement("span", {
        className: _colorPickerModule.default.labelReadout
      }, Math.round(this.props.brightness))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.rowSlider
      }, /*#__PURE__*/_react.default.createElement(_slider.default, {
        lastSlider: true,
        background: this._makeBackground('brightness'),
        value: this.props.brightness,
        onChange: this.props.onBrightnessChange
      }))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.swatchRow
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.swatches
      }, this.props.mode === _modes.default.BIT_LINE || this.props.mode === _modes.default.BIT_RECT || this.props.mode === _modes.default.BIT_OVAL || this.props.mode === _modes.default.BIT_TEXT ? null : /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)((_classNames7 = {}, (0, _defineProperty2.default)(_classNames7, _colorPickerModule.default.clickable, true), (0, _defineProperty2.default)(_classNames7, _colorPickerModule.default.swatch, true), (0, _defineProperty2.default)(_classNames7, _colorPickerModule.default.activeSwatch, this.props.colorIndex === 0 && this.props.color === null || this.props.colorIndex === 1 && this.props.color2 === null), _classNames7)),
        onClick: this.props.onTransparent
      }, /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.swatchIcon,
        draggable: false,
        src: _noFill.default
      }))), /*#__PURE__*/_react.default.createElement("div", {
        className: _colorPickerModule.default.swatches
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)((_classNames8 = {}, (0, _defineProperty2.default)(_classNames8, _colorPickerModule.default.clickable, true), (0, _defineProperty2.default)(_classNames8, _colorPickerModule.default.swatch, true), (0, _defineProperty2.default)(_classNames8, _colorPickerModule.default.activeSwatch, this.props.isEyeDropping), _classNames8)),
        onClick: this.props.onActivateEyeDropper
      }, /*#__PURE__*/_react.default.createElement("img", {
        className: _colorPickerModule.default.swatchIcon,
        draggable: false,
        src: _eyeDropper.default
      })))));
    }
  }]);
  return ColorPickerComponent;
}(_react.default.Component);

var _default = (0, _reactIntl.injectIntl)(ColorPickerComponent);

exports.default = _default;