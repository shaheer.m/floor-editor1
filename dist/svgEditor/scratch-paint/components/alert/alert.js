"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _Alert = _interopRequireDefault(require("@mui/material/Alert"));

var _alertModule = _interopRequireDefault(require("./alert.module.css"));

var _excluded = ["className", "children"];

var AlertComponent = function AlertComponent(_ref) {
  var className = _ref.className,
      children = _ref.children,
      props = (0, _objectWithoutProperties2.default)(_ref, _excluded);
  return /*#__PURE__*/_react.default.createElement("div", {
    className: _alertModule.default.yellowAlertWrp
  }, /*#__PURE__*/_react.default.createElement(_Alert.default, Object.assign({}, props, {
    className: className
  }), children)) // <span
  //     className={classNames(
  //         styles.alert,
  //         className,
  //         {
  //             [styles.modDisabled]: disabled,
  //             [styles.highlighted]: highlighted
  //         }
  //     )}
  //     {...props}
  // >
  //     {children}
  // </span>
  ;
};

var _default = AlertComponent;
exports.default = _default;