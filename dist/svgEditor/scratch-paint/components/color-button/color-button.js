"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _stylePath = require("../../helper/style-path");

var _noFill = _interopRequireDefault(require("./no-fill.svg"));

var _mixedFill = _interopRequireDefault(require("./mixed-fill.svg"));

var _colorButtonModule = _interopRequireDefault(require("./color-button.module.css"));

var _gradientTypes = _interopRequireDefault(require("../../lib/gradient-types"));

var _log = _interopRequireDefault(require("../../log/log"));

var colorToBackground = function colorToBackground(color, color2, gradientType) {
  if (color === _stylePath.MIXED || gradientType !== _gradientTypes.default.SOLID && color2 === _stylePath.MIXED) return 'white';
  if (color === null) color = 'white';
  if (color2 === null) color2 = 'white';

  switch (gradientType) {
    case _gradientTypes.default.SOLID:
      return color;

    case _gradientTypes.default.HORIZONTAL:
      return "linear-gradient(to right, ".concat(color, ", ").concat(color2, ")");

    case _gradientTypes.default.VERTICAL:
      return "linear-gradient(".concat(color, ", ").concat(color2, ")");

    case _gradientTypes.default.RADIAL:
      return "radial-gradient(".concat(color, ", ").concat(color2, ")");

    default:
      _log.default.error("Unrecognized gradient type: ".concat(gradientType));

  }
};

var ColorButtonComponent = function ColorButtonComponent(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: _colorButtonModule.default.colorButton,
    onClick: props.onClick
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(_colorButtonModule.default.colorButtonSwatch, (0, _defineProperty2.default)({}, _colorButtonModule.default.outlineSwatch, props.outline && !(props.color === _stylePath.MIXED))),
    style: {
      background: colorToBackground(props.color, props.color2, props.gradientType)
    }
  }, props.color === null && (props.gradientType === _gradientTypes.default.SOLID || props.color2 === null) ? /*#__PURE__*/_react.default.createElement("img", {
    className: _colorButtonModule.default.swatchIcon,
    draggable: false,
    src: _noFill.default
  }) : props.color === _stylePath.MIXED || props.gradientType !== _gradientTypes.default.SOLID && props.color2 === _stylePath.MIXED ? /*#__PURE__*/_react.default.createElement("img", {
    className: _colorButtonModule.default.swatchIcon,
    draggable: false,
    src: _mixedFill.default
  }) : null), /*#__PURE__*/_react.default.createElement("div", {
    className: _colorButtonModule.default.colorButtonArrow
  }, "\u25BE"));
};

ColorButtonComponent.defaultProps = {
  outline: false
};
var _default = ColorButtonComponent;
exports.default = _default;