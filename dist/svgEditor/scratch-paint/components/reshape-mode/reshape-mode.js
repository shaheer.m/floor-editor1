"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var _toolSelectBase = _interopRequireDefault(require("../tool-select-base/tool-select-base"));

var _reshape = _interopRequireDefault(require("./reshape.svg"));

var ReshapeModeComponent = function ReshapeModeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_toolSelectBase.default, {
    imgDescriptor: _messages.default.reshape,
    imgSrc: _reshape.default,
    isSelected: props.isSelected,
    onMouseDown: props.onMouseDown
  });
};

var _default = ReshapeModeComponent;
exports.default = _default;