"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classnames = _interopRequireDefault(require("classnames"));

var _reactRedux = require("react-redux");

var _react = _interopRequireDefault(require("react"));

var _brushMode = require("../../reducers/brush-mode");

var _eraserMode = require("../../reducers/eraser-mode");

var _bitBrushSize = require("../../reducers/bit-brush-size");

var _bitEraserSize = require("../../reducers/bit-eraser-size");

var _fillBitmapShapes = require("../../reducers/fill-bitmap-shapes");

var _fontDropdown = _interopRequireDefault(require("../../containers/font-dropdown"));

var _liveInputHoc = _interopRequireDefault(require("../forms/live-input-hoc"));

var _label = _interopRequireDefault(require("../forms/label"));

var _reactIntl = require("react-intl");

var _input = _interopRequireDefault(require("../forms/input"));

var _inputGroup = _interopRequireDefault(require("../input-group/input-group"));

var _labeledIconButton = _interopRequireDefault(require("../labeled-icon-button/labeled-icon-button"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _format = _interopRequireWildcard(require("../../lib/format"));

var _hideLabel = require("../../lib/hide-label");

var _modeToolsModule = _interopRequireDefault(require("./mode-tools.module.css"));

var _copy = _interopRequireDefault(require("./icons/copy.svg"));

var _paste = _interopRequireDefault(require("./icons/paste.svg"));

var _delete = _interopRequireDefault(require("./icons/delete.svg"));

var _brush = _interopRequireDefault(require("../bit-brush-mode/brush.svg"));

var _eraser = _interopRequireDefault(require("../bit-eraser-mode/eraser.svg"));

var _line = _interopRequireDefault(require("../bit-line-mode/line.svg"));

var _brush2 = _interopRequireDefault(require("../brush-mode/brush.svg"));

var _curvedPoint = _interopRequireDefault(require("./icons/curved-point.svg"));

var _eraser2 = _interopRequireDefault(require("../eraser-mode/eraser.svg"));

var _flipHorizontal = _interopRequireDefault(require("./icons/flip-horizontal.svg"));

var _flipVertical = _interopRequireDefault(require("./icons/flip-vertical.svg"));

var _straightPoint = _interopRequireDefault(require("./icons/straight-point.svg"));

var _oval = _interopRequireDefault(require("../bit-oval-mode/oval.svg"));

var _rectangle = _interopRequireDefault(require("../bit-rect-mode/rectangle.svg"));

var _ovalOutlined = _interopRequireDefault(require("../bit-oval-mode/oval-outlined.svg"));

var _rectangleOutlined = _interopRequireDefault(require("../bit-rect-mode/rectangle-outlined.svg"));

var _strokeWidth = require("../../reducers/stroke-width");

var _group = require("../../helper/group");

var LiveInput = (0, _liveInputHoc.default)(_input.default);

var ModeToolsComponent = function ModeToolsComponent(props) {
  var messages = (0, _reactIntl.defineMessages)({
    brushSize: {
      defaultMessage: 'Size',
      description: 'Label for the brush size input',
      id: 'paint.modeTools.brushSize'
    },
    eraserSize: {
      defaultMessage: 'Eraser size',
      description: 'Label for the eraser size input',
      id: 'paint.modeTools.eraserSize'
    },
    copy: {
      defaultMessage: 'Duplicate',
      description: 'Label for the copy button',
      id: 'paint.modeTools.copy'
    },
    paste: {
      defaultMessage: 'Paste',
      description: 'Label for the paste button',
      id: 'paint.modeTools.paste'
    },
    delete: {
      defaultMessage: 'Delete',
      description: 'Label for the delete button',
      id: 'paint.modeTools.delete'
    },
    curved: {
      defaultMessage: 'Curved',
      description: 'Label for the button that converts selected points to curves',
      id: 'paint.modeTools.curved'
    },
    pointed: {
      defaultMessage: 'Pointed',
      description: 'Label for the button that converts selected points to sharp points',
      id: 'paint.modeTools.pointed'
    },
    thickness: {
      defaultMessage: 'Thickness',
      description: 'Label for the number input to choose the line thickness',
      id: 'paint.modeTools.thickness'
    },
    flipHorizontal: {
      defaultMessage: 'Flip Horizontal',
      description: 'Label for the button to flip the image horizontally',
      id: 'paint.modeTools.flipHorizontal'
    },
    flipVertical: {
      defaultMessage: 'Flip Vertical',
      description: 'Label for the button to flip the image vertically',
      id: 'paint.modeTools.flipVertical'
    },
    filled: {
      defaultMessage: 'Filled',
      description: 'Label for the button that sets the bitmap rectangle/oval mode to draw outlines',
      id: 'paint.modeTools.filled'
    },
    outlined: {
      defaultMessage: 'Outlined',
      description: 'Label for the button that sets the bitmap rectangle/oval mode to draw filled-in shapes',
      id: 'paint.modeTools.outlined'
    }
  });

  switch (props.mode) {
    case _modes.default.BRUSH:
    /* falls through */

    case _modes.default.BIT_BRUSH:
    /* falls through */

    case _modes.default.BIT_LINE:
      {
        var currentIcon = (0, _format.isVector)(props.format) ? _brush2.default : props.mode === _modes.default.BIT_LINE ? _line.default : _brush.default;
        var currentBrushValue = (0, _format.isBitmap)(props.format) ? props.bitBrushSize : props.brushValue;
        var changeFunction = (0, _format.isBitmap)(props.format) ? props.onBitBrushSliderChange : props.onBrushSliderChange;
        var currentMessage = props.mode === _modes.default.BIT_LINE ? messages.thickness : messages.brushSize;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
        }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("img", {
          alt: props.intl.formatMessage(currentMessage),
          className: _modeToolsModule.default.modeToolsIcon,
          draggable: false,
          src: currentIcon
        })), /*#__PURE__*/_react.default.createElement(LiveInput, {
          range: true,
          small: true,
          max: _strokeWidth.MAX_STROKE_WIDTH,
          min: "1",
          type: "number",
          value: currentBrushValue,
          onSubmit: changeFunction
        }));
      }

    case _modes.default.BIT_ERASER:
    /* falls through */

    case _modes.default.ERASER:
      {
        var _currentIcon = (0, _format.isVector)(props.format) ? _eraser2.default : _eraser.default;

        var currentEraserValue = (0, _format.isBitmap)(props.format) ? props.bitEraserSize : props.eraserValue;

        var _changeFunction = (0, _format.isBitmap)(props.format) ? props.onBitEraserSliderChange : props.onEraserSliderChange;

        return /*#__PURE__*/_react.default.createElement("div", {
          className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
        }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("img", {
          alt: props.intl.formatMessage(messages.eraserSize),
          className: _modeToolsModule.default.modeToolsIcon,
          draggable: false,
          src: _currentIcon
        })), /*#__PURE__*/_react.default.createElement(LiveInput, {
          range: true,
          small: true,
          max: _strokeWidth.MAX_STROKE_WIDTH,
          min: "1",
          type: "number",
          value: currentEraserValue,
          onSubmit: _changeFunction
        }));
      }

    case _modes.default.RESHAPE:
      return /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
      }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
        className: (0, _classnames.default)(_modeToolsModule.default.modDashedBorder, _modeToolsModule.default.modLabeledIconHeight)
      }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        disabled: !props.hasSelectedUncurvedPoints,
        hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
        imgSrc: _curvedPoint.default,
        title: props.intl.formatMessage(messages.curved),
        onClick: props.onCurvePoints
      }), /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        disabled: !props.hasSelectedUnpointedPoints,
        hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
        imgSrc: _straightPoint.default,
        title: props.intl.formatMessage(messages.pointed),
        onClick: props.onPointPoints
      })), /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
        className: (0, _classnames.default)(_modeToolsModule.default.modLabeledIconHeight)
      }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
        imgSrc: _delete.default,
        title: props.intl.formatMessage(messages.delete),
        onClick: props.onDelete
      })));

    case _modes.default.BIT_SELECT:
    /* falls through */

    case _modes.default.SELECT:
      return /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
      }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
        className: (0, _classnames.default)(_modeToolsModule.default.modDashedBorder, _modeToolsModule.default.modLabeledIconHeight)
      }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        disabled: !(0, _group.selectedAtleastOne)(),
        hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
        imgSrc: _copy.default,
        title: props.intl.formatMessage(messages.copy),
        onClick: props.onCopyToClipboard
      })), /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
        className: (0, _classnames.default)(_modeToolsModule.default.modDashedBorder, _modeToolsModule.default.modLabeledIconHeight)
      }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        disabled: !(0, _group.selectedAtleastOne)(),
        hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
        imgSrc: _delete.default,
        title: props.intl.formatMessage(messages.delete),
        onClick: props.onDelete
      })));

    case _modes.default.BIT_TEXT:
    /* falls through */

    case _modes.default.TEXT:
      return /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
      });

    case _modes.default.BIT_RECT:
    /* falls through */

    case _modes.default.BIT_OVAL:
      {
        var fillIcon = props.mode === _modes.default.BIT_RECT ? _rectangle.default : _oval.default;
        var outlineIcon = props.mode === _modes.default.BIT_RECT ? _rectangleOutlined.default : _ovalOutlined.default;
        return /*#__PURE__*/_react.default.createElement("div", {
          className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
        }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, null, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
          highlighted: props.fillBitmapShapes,
          imgSrc: fillIcon,
          title: props.intl.formatMessage(messages.filled),
          onClick: props.onFillShapes
        })), /*#__PURE__*/_react.default.createElement(_inputGroup.default, null, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
          highlighted: !props.fillBitmapShapes,
          imgSrc: outlineIcon,
          title: props.intl.formatMessage(messages.outlined),
          onClick: props.onOutlineShapes
        })), props.fillBitmapShapes ? null : /*#__PURE__*/_react.default.createElement(_inputGroup.default, null, /*#__PURE__*/_react.default.createElement(_label.default, {
          text: props.intl.formatMessage(messages.thickness)
        }, /*#__PURE__*/_react.default.createElement(LiveInput, {
          range: true,
          small: true,
          max: _strokeWidth.MAX_STROKE_WIDTH,
          min: "1",
          type: "number",
          value: props.bitBrushSize,
          onSubmit: props.onBitBrushSliderChange
        }))));
      }

    default:
      // Leave empty for now, if mode not supported
      return /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(props.className, _modeToolsModule.default.modeTools)
      });
  }
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    mode: state.scratchPaint.mode,
    format: state.scratchPaint.format,
    fillBitmapShapes: state.scratchPaint.fillBitmapShapes,
    bitBrushSize: state.scratchPaint.bitBrushSize,
    bitEraserSize: state.scratchPaint.bitEraserSize,
    brushValue: state.scratchPaint.brushMode.brushSize,
    clipboardItems: state.scratchPaint.clipboard.items,
    eraserValue: state.scratchPaint.eraserMode.brushSize,
    selectedItems: state.scratchPaint.selectedItems
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onBrushSliderChange: function onBrushSliderChange(brushSize) {
      dispatch((0, _brushMode.changeBrushSize)(brushSize));
    },
    onBitBrushSliderChange: function onBitBrushSliderChange(bitBrushSize) {
      dispatch((0, _bitBrushSize.changeBitBrushSize)(bitBrushSize));
    },
    onBitEraserSliderChange: function onBitEraserSliderChange(eraserSize) {
      dispatch((0, _bitEraserSize.changeBitEraserSize)(eraserSize));
    },
    onEraserSliderChange: function onEraserSliderChange(eraserSize) {
      dispatch((0, _eraserMode.changeBrushSize)(eraserSize));
    },
    onFillShapes: function onFillShapes() {
      dispatch((0, _fillBitmapShapes.setShapesFilled)(true));
    },
    onOutlineShapes: function onOutlineShapes() {
      dispatch((0, _fillBitmapShapes.setShapesFilled)(false));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _reactIntl.injectIntl)(ModeToolsComponent));

exports.default = _default;