"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _toolSelectBase = _interopRequireDefault(require("../tool-select-base/tool-select-base"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var _oval = _interopRequireDefault(require("./oval.svg"));

var OvalModeComponent = function OvalModeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_toolSelectBase.default, {
    imgDescriptor: _messages.default.oval,
    imgSrc: _oval.default,
    isSelected: props.isSelected,
    onMouseDown: props.onMouseDown
  });
};

var _default = OvalModeComponent;
exports.default = _default;