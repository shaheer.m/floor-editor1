"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

/* eslint-disable import/no-anonymous-default-export */

/**
 * Higher Order Component to manage inputs that submit on change and <enter>
 * @param {React.Component} Input text input that consumes onChange, onBlur, onKeyPress
 * @returns {React.Component} Live input that calls onSubmit on change and <enter>
 */
function _default(Input) {
  var LiveInput = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(LiveInput, _React$Component);

    var _super = (0, _createSuper2.default)(LiveInput);

    function LiveInput(props) {
      var _this;

      (0, _classCallCheck2.default)(this, LiveInput);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChange', 'handleKeyPress', 'handleFlush']);
      _this.state = {
        value: null
      };
      return _this;
    }

    (0, _createClass2.default)(LiveInput, [{
      key: "handleKeyPress",
      value: function handleKeyPress(e) {
        if (e.key === 'Enter') {
          this.handleChange(e);
          e.target.blur();
        }
      }
    }, {
      key: "handleFlush",
      value: function handleFlush() {
        this.setState({
          value: null
        });
      }
    }, {
      key: "handleChange",
      value: function handleChange(e) {
        var isNumeric = typeof this.props.value === 'number';
        var validatesNumeric = isNumeric ? !isNaN(e.target.value) : true;

        if (e.target.value !== null && validatesNumeric) {
          var val = Number(e.target.value);

          if (typeof this.props.max !== 'undefined' && val > Number(this.props.max)) {
            val = this.props.max;
          }

          if (typeof this.props.min !== 'undefined' && val < Number(this.props.min)) {
            val = this.props.min;
          }

          this.props.onSubmit(val);
        }

        this.setState({
          value: e.target.value
        });
      }
    }, {
      key: "render",
      value: function render() {
        var liveValue = this.state.value === null ? this.props.value : this.state.value;
        return /*#__PURE__*/_react.default.createElement(Input, Object.assign({}, this.props, {
          value: liveValue,
          onBlur: this.handleFlush,
          onChange: this.handleChange,
          onKeyPress: this.handleKeyPress
        }));
      }
    }]);
    return LiveInput;
  }(_react.default.Component);

  return LiveInput;
}