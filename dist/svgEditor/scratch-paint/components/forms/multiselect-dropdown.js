"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireWildcard(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactSelect = _interopRequireWildcard(require("react-select"));

var _inputModule = _interopRequireDefault(require("./input.module.css"));

var options = [{
  value: "clickable",
  label: "Set as seat"
}, {
  value: 'monitortype',
  label: 'MonitorType'
}];

var Option = function Option(props) {
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_reactSelect.components.Option, props, /*#__PURE__*/_react.default.createElement("div", {
    className: _inputModule.default.checkSelect
  }, /*#__PURE__*/_react.default.createElement("input", {
    type: "checkbox",
    checked: props.isSelected,
    onChange: function onChange() {
      return null;
    }
  }), " ", /*#__PURE__*/_react.default.createElement("label", null, props.label))));
};

var MultiSelectDropdown = /*#__PURE__*/function (_Component) {
  (0, _inherits2.default)(MultiSelectDropdown, _Component);

  var _super = (0, _createSuper2.default)(MultiSelectDropdown);

  function MultiSelectDropdown(props) {
    var _this;

    (0, _classCallCheck2.default)(this, MultiSelectDropdown);
    _this = _super.call(this, props);

    _this.handleChange = function (selected) {
      _this.setState({
        optionSelected: selected
      });

      _this.props.handleAttributeChange(selected);
    };

    _this.state = {
      optionSelected: null
    };
    return _this;
  }

  (0, _createClass2.default)(MultiSelectDropdown, [{
    key: "render",
    value: function render() {
      var liveValue = this.state.optionSelected === null ? this.props.attributeValues : this.state.optionSelected;
      var disabled = this.props.itemName ? false : true;
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("label", null, "Attributes (optional)"), /*#__PURE__*/_react.default.createElement(_reactSelect.default, {
        isDisabled: disabled,
        options: options,
        isMulti: true,
        closeMenuOnSelect: false,
        hideSelectedOptions: false,
        components: {
          Option: Option
        },
        onChange: this.handleChange,
        allowSelectAll: true,
        value: liveValue,
        className: "popover-select-container",
        classNamePrefix: "popover-select",
        placeholder: 'Select',
        menuPlacement: "auto",
        isClearable: false
      }));
    }
  }]);
  return MultiSelectDropdown;
}(_react.Component);

exports.default = MultiSelectDropdown;