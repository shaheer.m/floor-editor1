"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireWildcard(require("react"));

var _inputModule = _interopRequireDefault(require("./input.module.css"));

var _classnames = _interopRequireDefault(require("classnames"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

/* eslint-disable import/no-anonymous-default-export */

/**
 * Higher Order Component to manage inputs that submit on change and <enter>
 * @param {React.Component} Input text input that consumes onChange, onBlur, onKeyPress
 * @returns {React.Component} Live input that calls onSubmit on change and <enter>
 */
function _default(Input) {
  var LiveInput = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(LiveInput, _React$Component);

    var _super = (0, _createSuper2.default)(LiveInput);

    function LiveInput(props) {
      var _this;

      (0, _classCallCheck2.default)(this, LiveInput);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChange', 'handleKeyPress', 'handleFlush', 'containsSpecialChars']);
      _this.state = {
        value: null,
        errorLimit: false,
        errorRest: false,
        errorSpecialChar: false
      };
      return _this;
    }

    (0, _createClass2.default)(LiveInput, [{
      key: "handleKeyPress",
      value: function handleKeyPress(e) {
        if (e.key === 'Enter') {
          this.handleChange(e);
          e.target.blur();
        }
      }
    }, {
      key: "handleFlush",
      value: function handleFlush() {
        this.setState({
          value: null
        });
      }
    }, {
      key: "containsSpecialChars",
      value: function containsSpecialChars(str) {
        var specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        return specialChars.test(str);
      }
    }, {
      key: "handleChange",
      value: function handleChange(e) {
        var errorLimit = false;
        var errorRest = false;
        var errorSpecialChar = false;

        if (e.target.value.toLowerCase().includes('unavailable') || e.target.value.toLowerCase().includes('clickable')) {
          this.setState({
            errorRest: true
          });
          errorRest = true; // this.setState({errorLimit: false});
          // this.setState({errorSpecialChar: false});

          this.setState({
            value: e.target.value
          });
          this.props.setBtnError(true);
        } else {
          this.setState({
            errorRest: false
          });
        }

        if (this.containsSpecialChars(e.target.value)) {
          this.setState({
            errorSpecialChar: true
          });
          errorSpecialChar = true; // this.setState({errorLimit: false});
          // this.setState({errorRest: false});

          this.setState({
            value: e.target.value
          });
          this.props.setBtnError(true);
        } else {
          this.setState({
            errorSpecialChar: false
          });
        }

        if (e.target.value.length > 15) {
          this.setState({
            errorLimit: true
          });
          errorLimit = true; // this.setState({errorRest: false});
          // this.setState({errorSpecialChar: false});

          this.setState({
            value: e.target.value
          });
          this.props.setBtnError(true);
        } else {
          this.setState({
            errorLimit: false
          });
        }

        if (errorRest || errorLimit || errorSpecialChar) {
          return null;
        } else {
          this.props.setBtnError(false);
        }

        var val = e.target.value;
        this.props.onSubmit(val);
        this.setState({
          value: e.target.value
        });
      }
    }, {
      key: "render",
      value: function render() {
        var liveValue = this.state.value === null ? this.props.value : this.state.value;
        return /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(Input, Object.assign({}, this.props, {
          value: liveValue,
          onBlur: this.handleFlush,
          onChange: this.handleChange,
          onKeyPress: this.handleKeyPress,
          className: (0, _classnames.default)(_inputModule.default.spaceBottom, this.state.errorLimit && _inputModule.default.redBorder || this.state.errorRest && _inputModule.default.redBorder || this.state.errorSpecialChar && _inputModule.default.redBorder)
        })), this.state.errorLimit ? /*#__PURE__*/_react.default.createElement("span", {
          className: _inputModule.default.error
        }, _messages.default.errorMaxChar.description) : null, this.state.errorRest ? /*#__PURE__*/_react.default.createElement("span", {
          className: _inputModule.default.error
        }, _messages.default.errorRestChar.description) : null, this.state.errorSpecialChar ? /*#__PURE__*/_react.default.createElement("span", {
          className: _inputModule.default.error
        }, _messages.default.errorSpecialChar.description) : null);
      }
    }]);
    return LiveInput;
  }(_react.default.Component);

  return LiveInput;
}