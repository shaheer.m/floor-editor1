"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireWildcard(require("react"));

var _creatable = _interopRequireDefault(require("react-select/creatable"));

var _selection = require("../../helper/selection");

var GroupSelect = /*#__PURE__*/function (_Component) {
  (0, _inherits2.default)(GroupSelect, _Component);

  var _super = (0, _createSuper2.default)(GroupSelect);

  function GroupSelect(props) {
    var _this;

    (0, _classCallCheck2.default)(this, GroupSelect);
    _this = _super.call(this, props);

    _this.handleChange = // useCallback(
    function (inputValue) {
      _this.setState({
        value: inputValue
      });

      _this.props.onGroupNameChange(inputValue);
    };

    _this.handleCreate = //    useCallback(
    function (inputValue) {
      var newValue = {
        value: inputValue.toLowerCase(),
        label: inputValue
      };

      _this.setState(function (prevState) {
        return {
          options: [].concat((0, _toConsumableArray2.default)(prevState.options), [newValue])
        };
      });

      _this.setState({
        value: newValue
      });

      _this.props.onGroupNameChange(newValue);
    };

    _this.state = {
      value: null,
      options: []
    };
    return _this;
  }

  (0, _createClass2.default)(GroupSelect, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var values = (0, _selection.groupNameStackOfAllItems)();
      var uniqueValues = [];
      var uniqueObjects = [];
      var unique = values.filter(function (element) {
        var isDuplicate = uniqueValues.includes(element.value);

        if (!isDuplicate) {
          uniqueValues.push(element.value);
          uniqueObjects.push(element);
          return true;
        }

        return false;
      });
      this.setState({
        options: uniqueObjects
      });
    }
  }, {
    key: "render",
    value: //     ,[this.state.options]
    // );
    function render() {
      var liveValue = this.state.value === null ? this.props.groupName : this.state.value;
      var disabled = this.props.itemName ? false : true;
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("label", {
        htmlFor: ""
      }, "Select ODC"), /*#__PURE__*/_react.default.createElement(_creatable.default, {
        isClearable: true,
        value: liveValue,
        options: this.state.options,
        onChange: this.handleChange,
        onCreateOption: this.handleCreate,
        isDisabled: disabled,
        className: "popover-select-container",
        classNamePrefix: "popover-select",
        placeholder: 'Select'
      }));
    }
  }]);
  return GroupSelect;
}(_react.Component);

exports.default = GroupSelect;