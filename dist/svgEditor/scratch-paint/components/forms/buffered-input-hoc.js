"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

/* DO NOT EDIT
@todo This file is copied from GUI and should be pulled out into a shared library.
See https://github.com/LLK/scratch-paint/issues/13 */

/**
 * Higher Order Component to manage inputs that submit on blur and <enter>
 * @param {React.Component} Input text input that consumes onChange, onBlur, onKeyPress
 * @returns {React.Component} Buffered input that calls onSubmit on blur and <enter>
 */
function _default(Input) {
  var BufferedInput = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(BufferedInput, _React$Component);

    var _super = (0, _createSuper2.default)(BufferedInput);

    function BufferedInput(props) {
      var _this;

      (0, _classCallCheck2.default)(this, BufferedInput);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChange', 'handleKeyPress', 'handleFlush']);
      _this.state = {
        value: null
      };
      return _this;
    }

    (0, _createClass2.default)(BufferedInput, [{
      key: "handleKeyPress",
      value: function handleKeyPress(e) {
        if (e.key === 'Enter') {
          this.handleFlush();
          e.target.blur();
        }
      }
    }, {
      key: "handleFlush",
      value: function handleFlush() {
        var isNumeric = typeof this.props.value === 'number';
        var validatesNumeric = isNumeric ? !isNaN(this.state.value) : true;

        if (this.state.value !== null && validatesNumeric) {
          this.props.onSubmit(isNumeric ? Number(this.state.value) : this.state.value);
        }

        if (this.state.value == null) {
          this.props.onSubmit(this.props.value);
        }

        this.setState({
          value: null
        });
      }
    }, {
      key: "handleChange",
      value: function handleChange(e) {
        this.setState({
          value: e.target.value
        });
      }
    }, {
      key: "render",
      value: function render() {
        var bufferedValue = this.state.value === null ? this.props.value : this.state.value;
        return /*#__PURE__*/_react.default.createElement(Input, Object.assign({}, this.props, {
          value: bufferedValue,
          onBlur: this.handleFlush,
          onChange: this.handleChange,
          onKeyPress: this.handleKeyPress
        }));
      }
    }]);
    return BufferedInput;
  }(_react.default.Component);

  return BufferedInput;
}