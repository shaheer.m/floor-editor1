"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireWildcard(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _setAvailableModule = _interopRequireDefault(require("./set-available.module.css"));

var _classnames = _interopRequireDefault(require("classnames"));

var SetAvailable = /*#__PURE__*/function (_Component) {
  (0, _inherits2.default)(SetAvailable, _Component);

  var _super = (0, _createSuper2.default)(SetAvailable);

  function SetAvailable(props) {
    var _this;

    (0, _classCallCheck2.default)(this, SetAvailable);
    _this = _super.call(this, props);
    _this.state = {
      selected: null
    };
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChangeChk']);
    return _this;
  }

  (0, _createClass2.default)(SetAvailable, [{
    key: "handleChangeChk",
    value: function handleChangeChk(e) {
      this.setState(function (prevState) {
        return {
          selected: !prevState.selected
        };
      });
      this.props.onAvailableChange(e.target.checked);
    }
  }, {
    key: "render",
    value: function render() {
      var liveValue = this.state.selected === null ? this.props.available : this.state.selected;
      var disabled = this.props.itemName ? false : true;
      return /*#__PURE__*/_react.default.createElement("label", {
        className: (0, _classnames.default)(_setAvailableModule.default.spaceBottom, _setAvailableModule.default.checkboxContainer)
      }, /*#__PURE__*/_react.default.createElement("input", {
        className: _setAvailableModule.default.checkbox,
        type: "checkbox",
        disabled: disabled,
        defaultChecked: liveValue,
        onChange: this.handleChangeChk
      }), /*#__PURE__*/_react.default.createElement("span", null, "Mark as Unavailable"), /*#__PURE__*/_react.default.createElement("span", {
        className: _setAvailableModule.default.checkmark
      }));
    }
  }]);
  return SetAvailable;
}(_react.Component);

exports.default = SetAvailable;