"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _labelModule = _interopRequireDefault(require("./label.module.css"));

/* DO NOT EDIT
@todo This file is copied from GUI and should be pulled out into a shared library.
See https://github.com/LLK/scratch-paint/issues/13 */
var Label = function Label(props) {
  return /*#__PURE__*/_react.default.createElement("label", {
    className: _labelModule.default.inputGroup
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: props.secondary ? _labelModule.default.inputLabelSecondary : _labelModule.default.inputLabel
  }, props.text), props.children);
};

Label.defaultProps = {
  secondary: false
};
var _default = Label;
exports.default = _default;