"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _inputModule = _interopRequireDefault(require("./input.module.css"));

var _excluded = ["small", "range"];

var Input = function Input(props) {
  var _classNames;

  var small = props.small,
      range = props.range,
      componentProps = (0, _objectWithoutProperties2.default)(props, _excluded);
  return /*#__PURE__*/_react.default.createElement("input", Object.assign({}, componentProps, {
    className: (0, _classnames.default)(_inputModule.default.inputForm, props.className, (_classNames = {}, (0, _defineProperty2.default)(_classNames, _inputModule.default.inputSmall, small && !range), (0, _defineProperty2.default)(_classNames, _inputModule.default.inputSmallRange, small && range), _classNames))
  }));
};

Input.defaultProps = {
  range: false,
  small: false
};
var _default = Input;
exports.default = _default;