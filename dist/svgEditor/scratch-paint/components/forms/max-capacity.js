"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireWildcard(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _inputModule = _interopRequireDefault(require("./input.module.css"));

var _classnames = _interopRequireDefault(require("classnames"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var SetCapacity = /*#__PURE__*/function (_Component) {
  (0, _inherits2.default)(SetCapacity, _Component);

  var _super = (0, _createSuper2.default)(SetCapacity);

  function SetCapacity(props) {
    var _this;

    (0, _classCallCheck2.default)(this, SetCapacity);
    _this = _super.call(this, props);
    _this.state = {
      capacity: null,
      capacityError: false
    };
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChangeChk']);
    return _this;
  }

  (0, _createClass2.default)(SetCapacity, [{
    key: "handleChangeChk",
    value: function handleChangeChk(e) {
      var value = e.target.value.replace(/^0+/, '');

      if (!value.includes('.')) {
        if (value >= 0) {
          this.setState({
            capacity: value
          });
          this.props.onCapacityChange(value);
        }

        if (value < 1) {
          this.props.setBtnError(true);
          this.setState({
            capacityError: true
          });
        } else {
          this.props.setBtnError(false);
          this.setState({
            capacityError: false
          });
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var liveValue = this.state.capacity === null ? this.props.capacity : this.state.capacity;
      var disabled = this.props.itemName ? false : true;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: _inputModule.default.spaceBottom
      }, /*#__PURE__*/_react.default.createElement("label", null, "Capacity"), /*#__PURE__*/_react.default.createElement("input", {
        disabled: disabled,
        type: "number",
        min: "1",
        className: (0, _classnames.default)(_inputModule.default.inputForm, this.state.capacityError && _inputModule.default.redBorder),
        value: liveValue,
        onChange: this.handleChangeChk
      }), this.state.capacityError ? /*#__PURE__*/_react.default.createElement("span", {
        className: _inputModule.default.error
      }, _messages.default.errorCapacity.description) : null);
    }
  }]);
  return SetCapacity;
}(_react.Component);

exports.default = SetCapacity;