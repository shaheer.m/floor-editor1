"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.HANDLE_WIDTH = exports.CONTAINER_WIDTH = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _classnames = _interopRequireDefault(require("classnames"));

var _touchUtils = require("../../lib/touch-utils");

var _sliderModule = _interopRequireDefault(require("./slider.module.css"));

var CONTAINER_WIDTH = 150;
exports.CONTAINER_WIDTH = CONTAINER_WIDTH;
var HANDLE_WIDTH = 26;
exports.HANDLE_WIDTH = HANDLE_WIDTH;

var SliderComponent = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(SliderComponent, _React$Component);

  var _super = (0, _createSuper2.default)(SliderComponent);

  function SliderComponent(props) {
    var _this;

    (0, _classCallCheck2.default)(this, SliderComponent);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleMouseDown', 'handleMouseUp', 'handleMouseMove', 'handleClickBackground', 'setBackground', 'setHandle']); // Distance from the left edge of the slider handle to the mouse down/click event

    _this.handleClickOffset = 0;
    return _this;
  }

  (0, _createClass2.default)(SliderComponent, [{
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      document.addEventListener('mousemove', this.handleMouseMove);
      document.addEventListener('mouseup', this.handleMouseUp);
      document.addEventListener('touchmove', this.handleMouseMove, {
        passive: false
      });
      document.addEventListener('touchend', this.handleMouseUp);
      this.handleClickOffset = (0, _touchUtils.getEventXY)(event).x - this.handle.getBoundingClientRect().left;
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp() {
      document.removeEventListener('mousemove', this.handleMouseMove);
      document.removeEventListener('mouseup', this.handleMouseUp);
      document.removeEventListener('touchmove', this.handleMouseMove, {
        passive: false
      });
      document.removeEventListener('touchend', this.handleMouseUp);
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      event.preventDefault();
      this.props.onChange(this.scaleMouseToSliderPosition(event));
    }
  }, {
    key: "handleClickBackground",
    value: function handleClickBackground(event) {
      // Because the slider handle is a child of the "background" element this handler is registered to, it calls this
      // when clicked as well. We only want to change the slider value if the user clicked on the background itself.
      if (event.target !== this.background) return; // Move slider handle's center to the cursor

      this.handleClickOffset = HANDLE_WIDTH / 2;
      this.props.onChange(this.scaleMouseToSliderPosition(event));
    }
  }, {
    key: "scaleMouseToSliderPosition",
    value: function scaleMouseToSliderPosition(event) {
      var _getEventXY = (0, _touchUtils.getEventXY)(event),
          x = _getEventXY.x;

      var backgroundBBox = this.background.getBoundingClientRect();
      var scaledX = x - backgroundBBox.left - this.handleClickOffset;
      return Math.max(0, Math.min(100, 100 * scaledX / (backgroundBBox.width - HANDLE_WIDTH)));
    }
  }, {
    key: "setBackground",
    value: function setBackground(ref) {
      this.background = ref;
    }
  }, {
    key: "setHandle",
    value: function setHandle(ref) {
      this.handle = ref;
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames;

      var halfHandleWidth = HANDLE_WIDTH / 2;
      var pixelMin = halfHandleWidth;
      var pixelMax = CONTAINER_WIDTH - halfHandleWidth;
      var handleOffset = pixelMin + (pixelMax - pixelMin) * (this.props.value / 100) - halfHandleWidth;
      return /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)((_classNames = {}, (0, _defineProperty2.default)(_classNames, _sliderModule.default.container, true), (0, _defineProperty2.default)(_classNames, _sliderModule.default.last, this.props.lastSlider), _classNames)),
        ref: this.setBackground,
        style: {
          backgroundImage: this.props.background
        },
        onClick: this.handleClickBackground
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _sliderModule.default.handle,
        ref: this.setHandle,
        style: {
          left: "".concat(handleOffset, "px")
        },
        onMouseDown: this.handleMouseDown,
        onTouchStart: this.handleMouseDown
      }));
    }
  }]);
  return SliderComponent;
}(_react.default.Component);

SliderComponent.defaultProps = {
  background: 'yellow'
};
var _default = SliderComponent;
exports.default = _default;