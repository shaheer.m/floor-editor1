"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _buttonModule = _interopRequireDefault(require("./button.module.css"));

var _excluded = ["className", "highlighted", "onClick", "children"];

var ButtonComponent = function ButtonComponent(_ref) {
  var _classNames;

  var className = _ref.className,
      highlighted = _ref.highlighted,
      onClick = _ref.onClick,
      children = _ref.children,
      props = (0, _objectWithoutProperties2.default)(_ref, _excluded);
  var disabled = props.disabled || false;

  if (disabled === false) {
    // if not disabled, add `onClick()` to be applied
    // in props. If disabled, don't add `onClick()`
    props.onClick = onClick;
  }

  return /*#__PURE__*/_react.default.createElement("span", Object.assign({
    className: (0, _classnames.default)(_buttonModule.default.button, className, (_classNames = {}, (0, _defineProperty2.default)(_classNames, _buttonModule.default.modDisabled, disabled), (0, _defineProperty2.default)(_classNames, _buttonModule.default.highlighted, highlighted), _classNames)),
    role: "button"
  }, props), children);
};

var _default = ButtonComponent;
exports.default = _default;