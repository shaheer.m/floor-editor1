"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _input = _interopRequireDefault(require("../forms/input"));

var _inputGroup = _interopRequireDefault(require("../input-group/input-group"));

var _liveInputStringHoc = _interopRequireDefault(require("../forms/live-input-string-hoc"));

var _strokeWidth = require("../../reducers/stroke-width");

var LiveInput = (0, _liveInputStringHoc.default)(_input.default);

var ItemNameIndicatorComponent = function ItemNameIndicatorComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    disabled: props.disabled
  }, /*#__PURE__*/_react.default.createElement("label", null, "Element Name"), /*#__PURE__*/_react.default.createElement(LiveInput, {
    disabled: props.disabled,
    value: props.itemName,
    onSubmit: props.onChangeItemName,
    setBtnError: props.setBtnError
  }));
};

var _default = ItemNameIndicatorComponent;
exports.default = _default;