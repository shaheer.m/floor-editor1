"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _toolSelectBase = _interopRequireDefault(require("../tool-select-base/tool-select-base"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var _eraser = _interopRequireDefault(require("./eraser.svg"));

var EraserModeComponent = function EraserModeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_toolSelectBase.default, {
    imgDescriptor: _messages.default.eraser,
    imgSrc: _eraser.default,
    isSelected: props.isSelected,
    onMouseDown: props.onMouseDown
  });
};

var _default = EraserModeComponent;
exports.default = _default;