"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _toolSelectBase = _interopRequireDefault(require("../tool-select-base/tool-select-base"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var _fill = _interopRequireDefault(require("./fill.svg"));

var FillModeComponent = function FillModeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_toolSelectBase.default, {
    imgDescriptor: _messages.default.fill,
    imgSrc: _fill.default,
    isSelected: props.isSelected,
    onMouseDown: props.onMouseDown
  });
};

var _default = FillModeComponent;
exports.default = _default;