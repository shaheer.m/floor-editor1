"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _reactStyleProptype = _interopRequireDefault(require("react-style-proptype"));

var _excluded = ["alignContent", "alignItems", "alignSelf", "basis", "children", "className", "componentRef", "direction", "element", "grow", "height", "justifyContent", "width", "wrap", "shrink", "style"];

var getRandomColor = function () {
  // In "DEBUG" mode this is used to output a random background color for each
  // box. The function gives the same "random" set for each seed, allowing re-
  // renders of the same content to give the same random display.
  var random = function (seed) {
    var mW = seed;
    var mZ = 987654321;
    var mask = 0xffffffff;
    return function () {
      mZ = 36969 * (mZ & 65535) + (mZ >> 16) & mask;
      mW = 18000 * (mW & 65535) + (mW >> 16) & mask;
      var result = (mZ << 16) + mW & mask;
      result /= 4294967296;
      return result + 1;
    };
  }(601);

  return function () {
    var r = Math.max(parseInt(random() * 100, 10) % 256, 1);
    var g = Math.max(parseInt(random() * 100, 10) % 256, 1);
    var b = Math.max(parseInt(random() * 100, 10) % 256, 1);
    return "rgb(".concat(r, ",").concat(g, ",").concat(b, ")");
  };
}();

var Box = function Box(props) {
  var alignContent = props.alignContent,
      alignItems = props.alignItems,
      alignSelf = props.alignSelf,
      basis = props.basis,
      children = props.children,
      className = props.className,
      componentRef = props.componentRef,
      direction = props.direction,
      element = props.element,
      grow = props.grow,
      height = props.height,
      justifyContent = props.justifyContent,
      width = props.width,
      wrap = props.wrap,
      shrink = props.shrink,
      style = props.style,
      componentProps = (0, _objectWithoutProperties2.default)(props, _excluded);
  return /*#__PURE__*/_react.default.createElement(element, (0, _objectSpread2.default)({
    className: className,
    ref: componentRef,
    style: Object.assign({
      alignContent: alignContent,
      alignItems: alignItems,
      alignSelf: alignSelf,
      flexBasis: basis,
      flexDirection: direction,
      flexGrow: grow,
      flexShrink: shrink,
      flexWrap: wrap,
      justifyContent: justifyContent,
      width: width,
      height: height
    }, process.env.DEBUG ? {
      // eslint-disable-line no-undef
      backgroundColor: getRandomColor(),
      outline: "1px solid black"
    } : {}, style)
  }, componentProps), children);
};

Box.defaultProps = {
  element: 'div',
  style: {}
};
var _default = Box;
exports.default = _default;