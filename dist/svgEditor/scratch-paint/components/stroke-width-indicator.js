"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _input = _interopRequireDefault(require("./forms/input"));

var _inputGroup = _interopRequireDefault(require("./input-group/input-group"));

var _liveInputHoc = _interopRequireDefault(require("./forms/live-input-hoc"));

var _strokeWidth = require("../reducers/stroke-width");

var _labeledIconButton = _interopRequireDefault(require("./labeled-icon-button/labeled-icon-button"));

var _paintEditorModule = _interopRequireDefault(require("./paint-editor/paint-editor.module.css"));

var LiveInput = (0, _liveInputHoc.default)(_input.default);

var StrokeWidthIndicatorComponent = function StrokeWidthIndicatorComponent(props) {
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    disabled: props.disabled
  }, /*#__PURE__*/_react.default.createElement(LiveInput, {
    range: true,
    small: true,
    disabled: props.disabled,
    max: _strokeWidth.MAX_STROKE_WIDTH,
    min: "0",
    type: "number",
    value: props.strokeWidth ? props.strokeWidth : 0,
    onSubmit: props.onChangeStrokeWidth
  })), /*#__PURE__*/_react.default.createElement("span", {
    className: props.disabled ? _paintEditorModule.default.bottomTextDisabled : _paintEditorModule.default.bottomText
  }, "Outline"));
};

var _default = StrokeWidthIndicatorComponent;
exports.default = _default;