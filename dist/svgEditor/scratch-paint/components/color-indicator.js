"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactPopover = _interopRequireDefault(require("react-popover"));

var _colorButton = _interopRequireDefault(require("./color-button/color-button"));

var _colorPicker = _interopRequireDefault(require("../containers/color-picker"));

var _inputGroup = _interopRequireDefault(require("./input-group/input-group"));

var _label = _interopRequireDefault(require("./forms/label"));

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _chairIcon = _interopRequireDefault(require("../../../assets/chair-icon-2.svg"));

var colorOptions = [{
  color: '#44339a',
  icon: _chairIcon.default
}, {
  color: '#9a144e',
  icon: _chairIcon.default
}, {
  color: '#B0BF1A',
  icon: _chairIcon.default
}, {
  color: '#CC5500',
  icon: _chairIcon.default
}];

var ColorIndicatorComponent = function ColorIndicatorComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: props.className,
    disabled: props.disabled
  }, /*#__PURE__*/_react.default.createElement(_reactPopover.default, {
    body: /*#__PURE__*/_react.default.createElement(_react.Fragment, null, colorOptions.map(function (el) {
      return /*#__PURE__*/_react.default.createElement("button", {
        onClick: function onClick() {
          return props.onChangeColor(el.color);
        }
      }, /*#__PURE__*/_react.default.createElement("img", {
        src: el.icon
      }));
    })) // <ColorPicker
    //     color={props.color}
    //     color2={props.color2}
    //     gradientType={props.gradientType}
    //     shouldShowGradientTools={props.shouldShowGradientTools}
    //     onChangeColor={props.onChangeColor}
    //     onChangeGradientType={props.onChangeGradientType}
    //     onSwap={props.onSwap}
    // />
    ,
    isOpen: props.colorModalVisible,
    preferPlace: "below",
    onOuterAction: props.onCloseColor
  }, /*#__PURE__*/_react.default.createElement(_label.default, {
    text: props.label
  }, /*#__PURE__*/_react.default.createElement(_colorButton.default, {
    color: props.color,
    color2: props.color2,
    gradientType: props.gradientType,
    onClick: props.onOpenColor,
    outline: props.outline
  }))));
};

var _default = ColorIndicatorComponent;
exports.default = _default;