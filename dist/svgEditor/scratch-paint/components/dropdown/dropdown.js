"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactPopover = _interopRequireDefault(require("react-popover"));

var _react = _interopRequireDefault(require("react"));

var _dropdownModule = _interopRequireDefault(require("./dropdown.module.css"));

var _dropdownCaret = _interopRequireDefault(require("./dropdown-caret.svg"));

var Dropdown = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(Dropdown, _React$Component);

  var _super = (0, _createSuper2.default)(Dropdown);

  function Dropdown(props) {
    var _this;

    (0, _classCallCheck2.default)(this, Dropdown);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleClosePopover', 'handleToggleOpenState', 'isOpen']);
    _this.state = {
      isOpen: false
    };
    return _this;
  }

  (0, _createClass2.default)(Dropdown, [{
    key: "handleClosePopover",
    value: function handleClosePopover() {
      this.setState({
        isOpen: false
      });
    }
  }, {
    key: "handleToggleOpenState",
    value: function handleToggleOpenState() {
      var newState = !this.state.isOpen;
      this.setState({
        isOpen: newState
      });

      if (newState && this.props.onOpen) {
        this.props.onOpen();
      }
    }
  }, {
    key: "isOpen",
    value: function isOpen() {
      return this.state.isOpen;
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames;

      return /*#__PURE__*/_react.default.createElement(_reactPopover.default, Object.assign({
        body: this.props.popoverContent,
        isOpen: this.state.isOpen,
        preferPlace: "below",
        onOuterAction: this.props.onOuterAction ? this.props.onOuterAction : this.handleClosePopover
      }, this.props), /*#__PURE__*/_react.default.createElement("div", {
        className: (0, _classnames.default)(_dropdownModule.default.dropdown, this.props.className, (_classNames = {}, (0, _defineProperty2.default)(_classNames, _dropdownModule.default.modOpen, this.state.isOpen), (0, _defineProperty2.default)(_classNames, _dropdownModule.default.modClosed, !this.state.isOpen), _classNames)),
        onClick: this.handleToggleOpenState
      }, this.props.children, /*#__PURE__*/_react.default.createElement("img", {
        className: (0, _classnames.default)(_dropdownModule.default.dropdownIcon, (0, _defineProperty2.default)({}, _dropdownModule.default.modCaretUp, this.state.isOpen)),
        draggable: false,
        src: _dropdownCaret.default,
        alt: ""
      })));
    }
  }]);
  return Dropdown;
}(_react.default.Component);

var _default = Dropdown;
exports.default = _default;