"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _scrollableCanvasModule = _interopRequireDefault(require("./scrollable-canvas.module.css"));

var ScrollableCanvasComponent = function ScrollableCanvasComponent(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: props.style
  }, props.children, /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.horizontalScrollbarWrapper,
    style: {
      pointerEvents: 'none'
    }
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.horizontalScrollbarHitbox,
    style: {
      width: "".concat(props.horizontalScrollLengthPercent, "%"),
      left: "".concat(props.horizontalScrollStartPercent, "%"),
      pointerEvents: 'auto',
      display: "".concat(props.hideScrollbars || Math.abs(props.horizontalScrollLengthPercent - 100) < 1e-8 ? 'none' : 'block')
    },
    onMouseDown: props.onHorizontalScrollbarMouseDown,
    onTouchStart: props.onHorizontalScrollbarMouseDown
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.horizontalScrollbar
  }))), /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.verticalScrollbarWrapper,
    style: {
      pointerEvents: 'none'
    }
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.verticalScrollbarHitbox,
    style: {
      height: "".concat(props.verticalScrollLengthPercent, "%"),
      top: "".concat(props.verticalScrollStartPercent, "%"),
      pointerEvents: 'auto',
      display: "".concat(props.hideScrollbars || Math.abs(props.verticalScrollLengthPercent - 100) < 1e-8 ? 'none' : 'block')
    },
    onMouseDown: props.onVerticalScrollbarMouseDown,
    onTouchStart: props.onVerticalScrollbarMouseDown
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _scrollableCanvasModule.default.verticalScrollbar
  }))));
};

var _default = ScrollableCanvasComponent;
exports.default = _default;