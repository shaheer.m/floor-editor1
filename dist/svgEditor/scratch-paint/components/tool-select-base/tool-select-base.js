"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _reactIntl = require("react-intl");

var _button = _interopRequireDefault(require("../button/button"));

var _toolSelectBaseModule = _interopRequireDefault(require("./tool-select-base.module.css"));

var ToolSelectComponent = function ToolSelectComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_button.default, {
    className: (0, _classnames.default)(props.className, _toolSelectBaseModule.default.modToolSelect, (0, _defineProperty2.default)({}, _toolSelectBaseModule.default.isSelected, props.isSelected)),
    disabled: props.disabled,
    title: props.intl.formatMessage(props.imgDescriptor),
    onClick: props.onMouseDown
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: props.intl.formatMessage(props.imgDescriptor),
    className: _toolSelectBaseModule.default.toolSelectIcon,
    draggable: true,
    src: props.imgSrc
  }));
};

var _default = (0, _reactIntl.injectIntl)(ToolSelectComponent);

exports.default = _default;