"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _buttonGroupModule = _interopRequireDefault(require("./button-group.module.css"));

var ButtonGroup = function ButtonGroup(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(props.className, _buttonGroupModule.default.buttonGroup)
  }, props.children);
};

var _default = ButtonGroup;
exports.default = _default;