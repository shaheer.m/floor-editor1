"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactRedux = require("react-redux");

var _react = _interopRequireWildcard(require("react"));

var _reactResponsive = _interopRequireDefault(require("react-responsive"));

var _group = require("../../helper/group");

var _order = require("../../helper/order");

var _bufferedInputHoc = _interopRequireDefault(require("../forms/buffered-input-hoc"));

var _button = _interopRequireDefault(require("../button/button"));

var _buttonGroup = _interopRequireDefault(require("../button-group/button-group"));

var _dropdown = _interopRequireDefault(require("../dropdown/dropdown"));

var _reactIntl = require("react-intl");

var _format = _interopRequireWildcard(require("../../lib/format"));

var _input = _interopRequireDefault(require("../forms/input"));

var _inputGroup = _interopRequireDefault(require("../input-group/input-group"));

var _label = _interopRequireDefault(require("../forms/label"));

var _labeledIconButton = _interopRequireDefault(require("../labeled-icon-button/labeled-icon-button"));

var _layoutConstants = _interopRequireDefault(require("../../lib/layout-constants"));

var _hideLabel = require("../../lib/hide-label");

var _fixedToolsModule = _interopRequireDefault(require("./fixed-tools.module.css"));

var _group2 = _interopRequireDefault(require("./icons/group.svg"));

var _redo = _interopRequireDefault(require("./icons/redo.png"));

var _sendBack = _interopRequireDefault(require("./icons/send-back.svg"));

var _sendBackward = _interopRequireDefault(require("./icons/send-backward.svg"));

var _sendForward = _interopRequireDefault(require("./icons/send-forward.svg"));

var _sendFront = _interopRequireDefault(require("./icons/send-front.svg"));

var _undo = _interopRequireDefault(require("./icons/undo.png"));

var _ungroup = _interopRequireDefault(require("./icons/ungroup.svg"));

var _svgInfo = _interopRequireDefault(require("./svg-info"));

var BufferedInput = (0, _bufferedInputHoc.default)(_input.default);
var messages = (0, _reactIntl.defineMessages)({
  costume: {
    id: 'paint.paintEditor.costume',
    description: 'Label for the name of a costume',
    defaultMessage: 'Name'
  },
  group: {
    defaultMessage: 'Group',
    description: 'Label for the button to group shapes',
    id: 'paint.paintEditor.group'
  },
  ungroup: {
    defaultMessage: 'Ungroup',
    description: 'Label for the button to ungroup shapes',
    id: 'paint.paintEditor.ungroup'
  },
  undo: {
    defaultMessage: 'Undo',
    description: 'Alt to image for the button to undo an action',
    id: 'paint.paintEditor.undo'
  },
  redo: {
    defaultMessage: 'Redo',
    description: 'Alt to image for the button to redo an action',
    id: 'paint.paintEditor.redo'
  },
  forward: {
    defaultMessage: 'Forward',
    description: 'Label for the `Send forward on canvas` button',
    id: 'paint.paintEditor.forward'
  },
  backward: {
    defaultMessage: 'Backward',
    description: 'Label for the `Send backward on canvas` button',
    id: 'paint.paintEditor.backward'
  },
  front: {
    defaultMessage: 'Front',
    description: 'Label for the `Send to front of canvas` button',
    id: 'paint.paintEditor.front'
  },
  back: {
    defaultMessage: 'Back',
    description: 'Label for the `Send to back of canvas` button',
    id: 'paint.paintEditor.back'
  },
  more: {
    defaultMessage: 'More',
    description: 'Label for dropdown to access more action buttons',
    id: 'paint.paintEditor.more'
  }
});

var FixedToolsComponent = function FixedToolsComponent(props) {
  var redoDisabled = !props.canRedo();
  var undoDisabled = !props.canUndo(); // const [editName, setEditName] = useState(false);
  // const onSubmit = (name) => {
  //     props.onUpdateName(name)
  //     setEditName(false)
  // }

  return /*#__PURE__*/_react.default.createElement("div", {
    className: _fixedToolsModule.default.row
  }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, null, /*#__PURE__*/_react.default.createElement(_buttonGroup.default, null, /*#__PURE__*/_react.default.createElement(_button.default, {
    className: (0, _classnames.default)(_fixedToolsModule.default.buttonGroupButton, (0, _defineProperty2.default)({}, _fixedToolsModule.default.modNoEndBorder, !redoDisabled)),
    disabled: undoDisabled,
    onClick: props.onUndo
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: props.intl.formatMessage(messages.undo),
    className: (0, _classnames.default)(_fixedToolsModule.default.buttonGroupButtonIcon, _fixedToolsModule.default.undoIcon),
    draggable: false,
    src: _undo.default
  })), /*#__PURE__*/_react.default.createElement(_button.default, {
    className: (0, _classnames.default)(_fixedToolsModule.default.buttonGroupButton, (0, _defineProperty2.default)({}, _fixedToolsModule.default.modStartBorder, !redoDisabled)),
    disabled: redoDisabled,
    onClick: props.onRedo
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: props.intl.formatMessage(messages.redo),
    className: _fixedToolsModule.default.buttonGroupButtonIcon,
    draggable: false,
    src: _redo.default
  })))), (0, _format.isVector)(props.format) ? /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: _fixedToolsModule.default.modDashedBorder
  }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
    disabled: !(0, _group.shouldShowGroup)(),
    hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
    imgSrc: _group2.default,
    title: props.intl.formatMessage(messages.group),
    onClick: props.onGroup
  }), /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
    disabled: !(0, _group.shouldShowUngroup)() || !(0, _group.selectedSingleItem)(),
    hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
    imgSrc: _ungroup.default,
    title: props.intl.formatMessage(messages.ungroup),
    onClick: props.onUngroup
  })) : null, (0, _format.isVector)(props.format) ? /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: _fixedToolsModule.default.modDashedBorder
  }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
    disabled: !(0, _order.shouldShowBringForward)(),
    hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
    imgSrc: _sendForward.default,
    title: props.intl.formatMessage(messages.forward),
    onClick: props.onSendToFront
  }), /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
    disabled: !(0, _order.shouldShowSendBackward)(),
    hideLabel: (0, _hideLabel.hideLabel)(props.intl.locale),
    imgSrc: _sendBackward.default,
    title: props.intl.formatMessage(messages.backward),
    onClick: props.onSendToBack
  })) : null, (0, _format.isVector)(props.format) ? /*#__PURE__*/_react.default.createElement(_reactResponsive.default, {
    minWidth: _layoutConstants.default.fullSizeEditorMinWidth
  }) : null);
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    format: state.scratchPaint.format,
    rtl: state.scratchPaint.layout.rtl,
    selectedItems: state.scratchPaint.selectedItems,
    undoState: state.scratchPaint.undo
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps)((0, _reactIntl.injectIntl)(FixedToolsComponent));

exports.default = _default;