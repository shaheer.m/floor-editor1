"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/slicedToArray"));

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactRedux = require("react-redux");

var _selection = require("../../helper/selection");

var _fixedToolsModule = _interopRequireDefault(require("./fixed-tools.module.css"));

var SvgInfo = function SvgInfo() {
  var _useState = (0, _react.useState)(),
      _useState2 = (0, _slicedToArray2.default)(_useState, 2),
      seatNumber = _useState2[0],
      setSeatNumber = _useState2[1];

  var _useState3 = (0, _react.useState)(),
      _useState4 = (0, _slicedToArray2.default)(_useState3, 2),
      odcNumber = _useState4[0],
      setOdcNumber = _useState4[1]; // useEffect(() => {
  //     let seatCount = fetchSvgInfo()
  //     setSeatNumber(seatCount) 
  // }, [])


  (0, _react.useEffect)(function () {
    var seatInfo = fetchSvgSeatInfo();
    setSeatNumber(seatInfo[0]);
    setOdcNumber(seatInfo[1].length);
  }, [(0, _selection.getAllSelectableRootItems)()]); // in the case where the item is grouped and does not satisfy the criteria to become a seat
  //ie an id and 'clickable' attribute, then check inside as well.

  var fetchSvgSeatInfo = function fetchSvgSeatInfo() {
    var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _selection.getAllSelectableRootItems)();
    var seatNumber = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var groupNames = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

    var _iterator = (0, _createForOfIteratorHelper2.default)(items),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var item = _step.value;

        if (item._data && item._data.id && item._data.customAttributes) {
          (function () {
            var attr = item._data.customAttributes;
            var unavailable = item._data.unavailable;
            var capacity = parseInt(item._data.capacity);
            var capacityValue = capacity ? capacity : 1;

            if (attr.some(function (obj) {
              return obj.value === "clickable" && !unavailable;
            })) {
              seatNumber = seatNumber + capacityValue;

              if (item._data.groupName && !groupNames.includes(item._data.groupName.value)) {
                groupNames.push(item._data.groupName.value);
              }
            } else {
              //has an id but no clickable attribute
              if (item.className && item.className === 'Group') {
                var _fetchSvgSeatInfo = fetchSvgSeatInfo(item._children, seatNumber, groupNames);

                var _fetchSvgSeatInfo2 = (0, _slicedToArray2.default)(_fetchSvgSeatInfo, 2);

                seatNumber = _fetchSvgSeatInfo2[0];
                groupNames = _fetchSvgSeatInfo2[1];
              }
            }
          })();
        } // has no id
        else if (!item._data || item._data && !item._data.id) {
          if (item.className && item.className === 'Group') {
            var _fetchSvgSeatInfo3 = fetchSvgSeatInfo(item._children, seatNumber, groupNames);

            var _fetchSvgSeatInfo4 = (0, _slicedToArray2.default)(_fetchSvgSeatInfo3, 2);

            seatNumber = _fetchSvgSeatInfo4[0];
            groupNames = _fetchSvgSeatInfo4[1];
          }
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    return [seatNumber, groupNames];
  }; // const fetchSvgOdcCount = () => {
  //     const values = groupNameStackOfAllItems();
  //     const uniqueValues = [];
  //     const uniqueObjects = [];
  //     const unique = values.filter(element => {
  //       const isDuplicate = uniqueValues.includes(element.value);
  //       if (!isDuplicate) {
  //         uniqueValues.push(element.value);
  //         uniqueObjects.push(element)        
  //         return true;
  //       }
  //       return false;
  //     });
  //     return uniqueObjects.length
  // }


  return /*#__PURE__*/_react.default.createElement("div", {
    className: _fixedToolsModule.default.mt10
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: "colonTitle"
  }, "Seats: "), /*#__PURE__*/_react.default.createElement("span", {
    className: "colonPara"
  }, seatNumber));
};

var _default = SvgInfo;
exports.default = _default;