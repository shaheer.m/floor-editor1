"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/slicedToArray"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _react = _interopRequireWildcard(require("react"));

var _paperCanvas = _interopRequireDefault(require("../../containers/paper-canvas"));

var _scrollableCanvas = _interopRequireDefault(require("../../containers/scrollable-canvas"));

var _bitBrushMode = _interopRequireDefault(require("../../containers/bit-brush-mode"));

var _bitLineMode = _interopRequireDefault(require("../../containers/bit-line-mode"));

var _bitOvalMode = _interopRequireDefault(require("../../containers/bit-oval-mode"));

var _bitRectMode = _interopRequireDefault(require("../../containers/bit-rect-mode"));

var _bitFillMode = _interopRequireDefault(require("../../containers/bit-fill-mode"));

var _bitEraserMode = _interopRequireDefault(require("../../containers/bit-eraser-mode"));

var _bitSelectMode = _interopRequireDefault(require("../../containers/bit-select-mode"));

var _box = _interopRequireDefault(require("../box/box"));

var _button = _interopRequireDefault(require("../button/button"));

var _buttonGroup = _interopRequireDefault(require("../button-group/button-group"));

var _brushMode = _interopRequireDefault(require("../../containers/brush-mode"));

var _eraserMode = _interopRequireDefault(require("../../containers/eraser-mode"));

var _fillColorIndicator = _interopRequireDefault(require("../../containers/fill-color-indicator"));

var _fillMode = _interopRequireDefault(require("../../containers/fill-mode"));

var _inputGroup = _interopRequireDefault(require("../input-group/input-group"));

var _lineMode = _interopRequireDefault(require("../../containers/line-mode"));

var _loupe = _interopRequireDefault(require("../loupe/loupe"));

var _fixedTools = _interopRequireDefault(require("../../containers/fixed-tools"));

var _modeTools = _interopRequireDefault(require("../../containers/mode-tools"));

var _ovalMode = _interopRequireDefault(require("../../containers/oval-mode"));

var _rectMode = _interopRequireDefault(require("../../containers/rect-mode"));

var _reshapeMode = _interopRequireDefault(require("../../containers/reshape-mode"));

var _selectMode = _interopRequireDefault(require("../../containers/select-mode"));

var _customShapeChair = _interopRequireDefault(require("../../containers/custom-shape-chair"));

var _strokeColorIndicator = _interopRequireDefault(require("../../containers/stroke-color-indicator"));

var _strokeWidthIndicator = _interopRequireDefault(require("../../containers/stroke-width-indicator"));

var _itemNameIndicator = _interopRequireDefault(require("../../containers/item-name-indicator"));

var _groupNameIndicator = _interopRequireDefault(require("../../containers/group-name-indicator"));

var _textMode = _interopRequireDefault(require("../../containers/text-mode"));

var _format = _interopRequireWildcard(require("../../lib/format"));

var _paintEditorModule = _interopRequireDefault(require("./paint-editor.module.css"));

var _bitmap = _interopRequireDefault(require("./icons/bitmap.svg"));

var _zoomIn = _interopRequireDefault(require("./icons/zoom-in.svg"));

var _zoomOut = _interopRequireDefault(require("./icons/zoom-out.svg"));

var _zoomReset = _interopRequireDefault(require("./icons/zoom-reset.svg"));

var _backButton = _interopRequireDefault(require("./icons/back-button.svg"));

var _svgInfo = _interopRequireDefault(require("../fixed-tools/svg-info"));

var _edit_icon = _interopRequireDefault(require("./icons/edit_icon.svg"));

var _reactResponsive = _interopRequireDefault(require("react-responsive"));

var _label = _interopRequireDefault(require("../forms/label"));

var _layoutConstants = _interopRequireDefault(require("../../lib/layout-constants"));

var _bufferedInputHoc = _interopRequireDefault(require("../forms/buffered-input-hoc"));

var _input = _interopRequireDefault(require("../forms/input"));

var _alert = _interopRequireDefault(require("../alert/alert"));

var _alertTopError = _interopRequireDefault(require("../../containers/alert-top-error"));

var BufferedInput = (0, _bufferedInputHoc.default)(_input.default);
var messages = (0, _reactIntl.defineMessages)({
  costume: {
    id: 'paint.paintEditor.costume',
    description: 'Label for the name of a costume',
    defaultMessage: 'Name'
  },
  bitmap: {
    defaultMessage: "Convert to Bitmap",
    description: "Label for button that converts the paint editor to bitmap mode",
    id: "paint.paintEditor.bitmap"
  },
  vector: {
    defaultMessage: "Convert to Vector",
    description: "Label for button that converts the paint editor to vector mode",
    id: "paint.paintEditor.vector"
  }
});

var PaintEditorComponent = function PaintEditorComponent(props) {
  (0, _react.useEffect)(function () {
    if ((0, _format.isBitmap)(props.format)) {
      setTimeout(function () {
        props.onSwitchToVector();
      }, 100);
    }
  }, [props.format]);

  var _useState = (0, _react.useState)(false),
      _useState2 = (0, _slicedToArray2.default)(_useState, 2),
      editName = _useState2[0],
      setEditName = _useState2[1];

  var onSubmit = function onSubmit(name) {
    props.onUpdateName(name);
    setEditName(false);
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.editorContainer,
    dir: props.rtl ? "rtl" : "ltr"
  }, props.alertOnTop.status && /*#__PURE__*/_react.default.createElement(_alertTopError.default, null), props.canvas !== null ?
  /*#__PURE__*/
  // eslint-disable-line no-negated-condition
  _react.default.createElement("div", {
    className: _paintEditorModule.default.editorContainerTop
  }, /*#__PURE__*/_react.default.createElement("button", {
    onClick: props.onBackButton,
    className: _paintEditorModule.default.paintEditorBackButton
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _backButton.default,
    alt: "Back button"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.editorTopTools
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.row
  }, /*#__PURE__*/_react.default.createElement(_fixedTools.default, {
    canRedo: props.canRedo,
    canUndo: props.canUndo,
    name: props.name,
    onRedo: props.onRedo,
    onUndo: props.onUndo,
    onUpdateImage: props.onUpdateImage,
    onUpdateName: props.onUpdateName
  })), (0, _format.isVector)(props.format) ? /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.row
  }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: _paintEditorModule.default.modModeTools
  }, /*#__PURE__*/_react.default.createElement(_modeTools.default, {
    onUpdateImage: props.onUpdateImage
  })), /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: (0, _classnames.default)(_paintEditorModule.default.row, _paintEditorModule.default.modDashedBorder, _paintEditorModule.default.modLabeledIconHeight)
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.verticalLineRight
  }), /*#__PURE__*/_react.default.createElement(_strokeWidthIndicator.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.verticalLine
  }), /*#__PURE__*/_react.default.createElement(_itemNameIndicator.default, {
    onUpdateImage: props.onUpdateImage,
    sendAlertMessage: props.sendAlertMessage
  }))) : (0, _format.isBitmap)(props.format) ? /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.row
  }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: (0, _classnames.default)(_paintEditorModule.default.row, _paintEditorModule.default.modDashedBorder, _paintEditorModule.default.modLabeledIconHeight)
  }, /*#__PURE__*/_react.default.createElement(_fillColorIndicator.default, {
    className: _paintEditorModule.default.modMarginAfter,
    onUpdateImage: props.onUpdateImage
  })), /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: _paintEditorModule.default.modModeTools
  }, /*#__PURE__*/_react.default.createElement(_modeTools.default, {
    onUpdateImage: props.onUpdateImage
  }))) : null), /*#__PURE__*/_react.default.createElement("button", {
    onClick: props.onSave,
    className: _paintEditorModule.default.paintEditorGradBtn
  }, "Save")) : null, /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.topAlignRow
  }, props.canvas !== null && (0, _format.isVector)(props.format) ?
  /*#__PURE__*/
  // eslint-disable-line no-negated-condition
  _react.default.createElement("div", {
    className: _paintEditorModule.default.modeSelector
  }, /*#__PURE__*/_react.default.createElement(_selectMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_customShapeChair.default, {
    addShape: props.addShape,
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_textMode.default, {
    textArea: props.textArea,
    onUpdateImage: props.onUpdateImage
  })) : null, props.canvas !== null && (0, _format.isBitmap)(props.format) ?
  /*#__PURE__*/
  // eslint-disable-line no-negated-condition
  _react.default.createElement("div", {
    className: _paintEditorModule.default.modeSelector
  }, /*#__PURE__*/_react.default.createElement(_bitBrushMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitLineMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitOvalMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitRectMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_textMode.default, {
    isBitmap: true,
    textArea: props.textArea,
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitFillMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitEraserMode.default, {
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement(_bitSelectMode.default, {
    onUpdateImage: props.onUpdateImage
  })) : null, /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.controlsContainer
  }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("span", {
    className: "colonTitle"
  }, "Name:"), editName && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(BufferedInput, {
    maxlength: 80,
    className: _paintEditorModule.default.costumeInput,
    type: "text",
    value: props.name,
    onSubmit: onSubmit
  })), !editName && /*#__PURE__*/_react.default.createElement(_react.Fragment, null, " ", /*#__PURE__*/_react.default.createElement("span", {
    className: "colonPara"
  }, props.name), /*#__PURE__*/_react.default.createElement("button", {
    className: (0, _classnames.default)(_paintEditorModule.default.imageButton, _paintEditorModule.default.ml10),
    onClick: function onClick() {
      setEditName(true);
    }
  }, /*#__PURE__*/_react.default.createElement("img", {
    src: _edit_icon.default,
    className: _paintEditorModule.default.editIcon,
    alt: "Edit button"
  })))), props.canvas !== null ? /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_svgInfo.default, null)) : null, /*#__PURE__*/_react.default.createElement(_scrollableCanvas.default, {
    canvas: props.canvas,
    hideScrollbars: props.isEyeDropping,
    style: _paintEditorModule.default.canvasContainer
  }, /*#__PURE__*/_react.default.createElement(_paperCanvas.default, {
    canvasRef: props.setCanvas,
    image: props.image,
    imageFormat: props.imageFormat,
    imageId: props.imageId,
    rotationCenterX: props.rotationCenterX,
    rotationCenterY: props.rotationCenterY,
    zoomLevelId: props.zoomLevelId,
    onUpdateImage: props.onUpdateImage
  }), /*#__PURE__*/_react.default.createElement("textarea", {
    className: _paintEditorModule.default.textArea,
    ref: props.setTextArea,
    spellCheck: false
  }), props.isEyeDropping && props.colorInfo !== null && !props.colorInfo.hideLoupe ? /*#__PURE__*/_react.default.createElement(_box.default, {
    className: _paintEditorModule.default.colorPickerWrapper
  }, /*#__PURE__*/_react.default.createElement(_loupe.default, {
    colorInfo: props.colorInfo,
    pixelRatio: _paper.default.project.view.pixelRatio
  })) : null), /*#__PURE__*/_react.default.createElement("div", {
    className: _paintEditorModule.default.canvasControls
  }, /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
    className: _paintEditorModule.default.zoomControls
  }, /*#__PURE__*/_react.default.createElement(_buttonGroup.default, null, /*#__PURE__*/_react.default.createElement(_button.default, {
    className: _paintEditorModule.default.buttonGroupButton,
    onClick: props.onZoomOut
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: "Zoom Out",
    className: _paintEditorModule.default.buttonGroupButtonIcon,
    draggable: false,
    src: _zoomOut.default
  })), /*#__PURE__*/_react.default.createElement(_button.default, {
    className: _paintEditorModule.default.buttonGroupButton,
    onClick: props.onZoomReset
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: "Zoom Reset",
    className: _paintEditorModule.default.buttonGroupButtonIcon,
    draggable: false,
    src: _zoomReset.default
  })), /*#__PURE__*/_react.default.createElement(_button.default, {
    className: _paintEditorModule.default.buttonGroupButton,
    onClick: props.onZoomIn
  }, /*#__PURE__*/_react.default.createElement("img", {
    alt: "Zoom In",
    className: _paintEditorModule.default.buttonGroupButtonIcon,
    draggable: false,
    src: _zoomIn.default
  }))))))));
};

var _default = (0, _reactIntl.injectIntl)(PaintEditorComponent);

exports.default = _default;