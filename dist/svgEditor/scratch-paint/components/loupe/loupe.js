"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectWithoutProperties"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _box = _interopRequireDefault(require("../box/box"));

var _eyeDropper = require("../../helper/tools/eye-dropper");

var _loupeModule = _interopRequireDefault(require("./loupe.module.css"));

var _excluded = ["colorInfo", "pixelRatio"];

var LoupeComponent = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(LoupeComponent, _React$Component);

  var _super = (0, _createSuper2.default)(LoupeComponent);

  function LoupeComponent(props) {
    var _this;

    (0, _classCallCheck2.default)(this, LoupeComponent);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['setCanvas']);
    return _this;
  }

  (0, _createClass2.default)(LoupeComponent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.draw();
    }
  }, {
    key: "draw",
    value: function draw() {
      var boxSize = 5;
      var boxLineWidth = 1;
      var colorRingWidth = 15;
      var loupeRadius = _eyeDropper.ZOOM_SCALE * _eyeDropper.LOUPE_RADIUS;
      var loupeDiameter = loupeRadius * 2;
      var color = this.props.colorInfo.color;
      var ctx = this.canvas.getContext('2d');
      this.canvas.width = loupeDiameter;
      this.canvas.height = loupeDiameter;
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, loupeDiameter, loupeDiameter); // In order to scale the image data, must draw to a tmp canvas first

      var tmpCanvas = document.createElement('canvas');
      tmpCanvas.width = loupeDiameter;
      tmpCanvas.height = loupeDiameter;
      var tmpCtx = tmpCanvas.getContext('2d');
      var imageData = tmpCtx.createImageData(loupeDiameter, loupeDiameter); // Since the color info comes from elsewhere there is no guarantee
      // about the size. Make sure it matches to prevent data.set from throwing.
      // See issue #966 for example of how that can happen.

      if (this.props.colorInfo.data.length === imageData.data.length) {
        imageData.data.set(this.props.colorInfo.data);
      } else {
        console.warn('Image data size mismatch drawing loupe'); // eslint-disable-line no-console
      }

      tmpCtx.putImageData(imageData, 0, 0); // Scale the loupe canvas and draw the zoomed image

      ctx.drawImage(tmpCanvas, 0, 0); // Draw an outlined square at the cursor position (cursor is hidden)

      ctx.lineWidth = boxLineWidth;
      ctx.strokeStyle = 'black';
      ctx.fillStyle = "rgba(".concat(color[0], ", ").concat(color[1], ", ").concat(color[2], ", ").concat(color[3], ")");
      ctx.beginPath();
      ctx.rect(loupeRadius - boxSize / 2, loupeRadius - boxSize / 2, boxSize, boxSize);
      ctx.fill();
      ctx.stroke(); // Draw a thick ring around the loupe showing the current color

      ctx.strokeStyle = "rgba(".concat(color[0], ", ").concat(color[1], ", ").concat(color[2], ", ").concat(color[3], ")");
      ctx.lineWidth = colorRingWidth;
      ctx.beginPath();
      ctx.moveTo(loupeDiameter, loupeDiameter);
      ctx.arc(loupeRadius, loupeRadius, loupeRadius, 0, 2 * Math.PI);
      ctx.stroke();
    }
  }, {
    key: "setCanvas",
    value: function setCanvas(element) {
      this.canvas = element; // Make sure to draw a frame when this component is first mounted
      // Check for null ref because refs are called with null when unmounted

      if (this.canvas) {
        this.draw();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          colorInfo = _this$props.colorInfo,
          pixelRatio = _this$props.pixelRatio,
          boxProps = (0, _objectWithoutProperties2.default)(_this$props, _excluded);
      var loupeDiameter = _eyeDropper.ZOOM_SCALE * _eyeDropper.LOUPE_RADIUS * 2;
      return /*#__PURE__*/_react.default.createElement(_box.default, Object.assign({}, boxProps, {
        className: _loupeModule.default.eyeDropper,
        componentRef: this.setCanvas,
        element: "canvas",
        height: _eyeDropper.LOUPE_RADIUS * 2,
        style: {
          top: colorInfo.y / pixelRatio - loupeDiameter / 2,
          left: colorInfo.x / pixelRatio - loupeDiameter / 2,
          width: loupeDiameter,
          height: loupeDiameter
        },
        width: _eyeDropper.LOUPE_RADIUS * 2
      }));
    }
  }]);
  return LoupeComponent;
}(_react.default.Component);

var _default = LoupeComponent;
exports.default = _default;