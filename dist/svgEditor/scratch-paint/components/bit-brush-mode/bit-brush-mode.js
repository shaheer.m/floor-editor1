"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _toolSelectBase = _interopRequireDefault(require("../tool-select-base/tool-select-base"));

var _messages = _interopRequireDefault(require("../../lib/messages.js"));

var _brush = _interopRequireDefault(require("./brush.svg"));

var BitBrushModeComponent = function BitBrushModeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_toolSelectBase.default, {
    imgDescriptor: _messages.default.brush,
    imgSrc: _brush.default,
    isSelected: props.isSelected,
    onMouseDown: props.onMouseDown
  });
};

var _default = BitBrushModeComponent;
exports.default = _default;