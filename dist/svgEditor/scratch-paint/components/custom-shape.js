"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireWildcard(require("react"));

var _button = _interopRequireDefault(require("./button/button"));

var _toolSelectBaseModule = _interopRequireDefault(require("./tool-select-base/tool-select-base.module.css"));

var _reactPopover = _interopRequireDefault(require("react-popover"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _classnames = _interopRequireDefault(require("classnames"));

var CustomShapeComponent = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(CustomShapeComponent, _React$Component);

  var _super = (0, _createSuper2.default)(CustomShapeComponent);

  function CustomShapeComponent(props) {
    var _this;

    (0, _classCallCheck2.default)(this, CustomShapeComponent);
    _this = _super.call(this, props);
    _this.state = {
      modalStatus: false
    };
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleAttributeModal']);
    return _this;
  }

  (0, _createClass2.default)(CustomShapeComponent, [{
    key: "handleAttributeModal",
    value: function handleAttributeModal() {
      this.setState(function (prevState) {
        return {
          modalStatus: !prevState.modalStatus
        };
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/_react.default.createElement(_reactPopover.default, {
        body: /*#__PURE__*/_react.default.createElement(_react.Fragment, null, this.props.imgSrc.map(function (el, i) {
          return /*#__PURE__*/_react.default.createElement("img", {
            title: _this2.props.title[i].defaultMessage,
            onClick: function onClick() {
              return _this2.props.addShape(i);
            },
            src: el
          });
        })),
        isOpen: this.state.modalStatus,
        preferPlace: "right",
        onOuterAction: this.handleAttributeModal,
        className: "sidebarPopover"
      }, /*#__PURE__*/_react.default.createElement(_button.default, {
        title: this.props.imgDescriptor.defaultMessage,
        className: (0, _classnames.default)(_toolSelectBaseModule.default.modToolDropdown, _toolSelectBaseModule.default.modToolSelect),
        onClick: this.handleAttributeModal
      }, /*#__PURE__*/_react.default.createElement("img", {
        // alt={''}
        className: _toolSelectBaseModule.default.toolSelectIcon,
        draggable: true,
        src: this.props.imgSrc[1]
      })));
    }
  }]);
  return CustomShapeComponent;
}(_react.default.Component); // CustomShapeComponent.propTypes = {
//     className: PropTypes.string,
//     disabled: PropTypes.bool.isRequired,
//     color: PropTypes.string,
//     color2: PropTypes.string,
//     colorModalVisible: PropTypes.bool.isRequired,
//     onChangeGradientType: PropTypes.func.isRequired,
//     onOpenColor: PropTypes.func.isRequired,
//     onSwap: PropTypes.func.isRequired,
// };
// <Button className={styles.modToolSelect} onClick={() => this.props.addShape(this.props.customShape)}>
//     <img
//         // alt={''}
//         className={styles.toolSelectIcon}
//         draggable={true}
//         src={this.props.imgSrc}
//     />
// </Button>


var _default = CustomShapeComponent;
exports.default = _default;