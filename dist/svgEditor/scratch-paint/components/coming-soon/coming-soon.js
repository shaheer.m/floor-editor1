"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComingSoonTooltip = exports.ComingSoonComponent = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactIntl = require("react-intl");

var _react = _interopRequireDefault(require("react"));

var _reactTooltip = _interopRequireDefault(require("react-tooltip"));

var _comingSoonModule = _interopRequireDefault(require("./coming-soon.module.css"));

var _awwCat = _interopRequireDefault(require("./aww-cat.png"));

var _coolCat = _interopRequireDefault(require("./cool-cat.png"));

/* DO NOT EDIT
@todo This file is copied from GUI and should be pulled out into a shared library.
See #13 */
var messages = (0, _reactIntl.defineMessages)({
  message1: {
    defaultMessage: 'Don\'t worry, we\'re on it {emoji}',
    description: 'One of the "coming soon" random messages for yet-to-be-done features',
    id: 'gui.comingSoon.message1'
  },
  message2: {
    defaultMessage: 'Coming Soon...',
    description: 'One of the "coming soon" random messages for yet-to-be-done features',
    id: 'gui.comingSoon.message2'
  },
  message3: {
    defaultMessage: 'We\'re working on it {emoji}',
    description: 'One of the "coming soon" random messages for yet-to-be-done features',
    id: 'gui.comingSoon.message3'
  }
});

var ComingSoonContent = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ComingSoonContent, _React$Component);

  var _super = (0, _createSuper2.default)(ComingSoonContent);

  function ComingSoonContent(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ComingSoonContent);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['setHide', 'setShow', 'getRandomMessage']);
    _this.state = {
      isShowing: false
    };
    return _this;
  }

  (0, _createClass2.default)(ComingSoonContent, [{
    key: "setShow",
    value: function setShow() {
      // needed to set the opacity to 1, since the default is .9 on show
      this.setState({
        isShowing: true
      });
    }
  }, {
    key: "setHide",
    value: function setHide() {
      this.setState({
        isShowing: false
      });
    }
  }, {
    key: "getRandomMessage",
    value: function getRandomMessage() {
      // randomly chooses a messages from `messages` to display in the tooltip.
      var images = [_awwCat.default, _coolCat.default];
      var messageNumber = Math.floor(Math.random() * Object.keys(messages).length) + 1;
      var imageNumber = Math.floor(Math.random() * Object.keys(images).length);
      return /*#__PURE__*/_react.default.createElement(_reactIntl.FormattedMessage, Object.assign({}, messages["message".concat(messageNumber)], {
        values: {
          emoji: /*#__PURE__*/_react.default.createElement("img", {
            className: _comingSoonModule.default.comingSoonImage,
            src: images[imageNumber]
          })
        }
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames;

      return /*#__PURE__*/_react.default.createElement(_reactTooltip.default, {
        afterHide: this.setHide,
        afterShow: this.setShow,
        className: (0, _classnames.default)(_comingSoonModule.default.comingSoon, this.props.className, (_classNames = {}, (0, _defineProperty2.default)(_classNames, _comingSoonModule.default.show, this.state.isShowing), (0, _defineProperty2.default)(_classNames, _comingSoonModule.default.left, this.props.place === 'left'), (0, _defineProperty2.default)(_classNames, _comingSoonModule.default.right, this.props.place === 'right'), (0, _defineProperty2.default)(_classNames, _comingSoonModule.default.top, this.props.place === 'top'), (0, _defineProperty2.default)(_classNames, _comingSoonModule.default.bottom, this.props.place === 'bottom'), _classNames)),
        getContent: this.getRandomMessage,
        id: this.props.tooltipId
      });
    }
  }]);
  return ComingSoonContent;
}(_react.default.Component);

ComingSoonContent.defaultProps = {
  place: 'bottom'
};
var ComingSoon = (0, _reactIntl.injectIntl)(ComingSoonContent);
exports.ComingSoonComponent = ComingSoon;

var ComingSoonTooltip = function ComingSoonTooltip(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: props.className
  }, /*#__PURE__*/_react.default.createElement("div", {
    "data-delay-hide": props.delayHide,
    "data-delay-show": props.delayShow,
    "data-effect": "solid",
    "data-for": props.tooltipId,
    "data-place": props.place,
    "data-tip": "tooltip"
  }, props.children), /*#__PURE__*/_react.default.createElement(ComingSoon, {
    className: props.tooltipClassName,
    place: props.place,
    tooltipId: props.tooltipId
  }));
};

exports.ComingSoonTooltip = ComingSoonTooltip;
ComingSoonTooltip.defaultProps = {
  delayHide: 0,
  delayShow: 0
};