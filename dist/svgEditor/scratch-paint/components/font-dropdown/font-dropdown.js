"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _button = _interopRequireDefault(require("../button/button"));

var _dropdown = _interopRequireDefault(require("../dropdown/dropdown"));

var _inputGroup = _interopRequireDefault(require("../input-group/input-group"));

var _fonts = _interopRequireDefault(require("../../lib/fonts"));

var _fontDropdownModule = _interopRequireDefault(require("./font-dropdown.module.css"));

var ModeToolsComponent = function ModeToolsComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_dropdown.default, {
    className: (0, _classnames.default)(_fontDropdownModule.default.modUnselect, _fontDropdownModule.default.fontDropdown),
    enterExitTransitionDurationMs: 60,
    popoverContent: /*#__PURE__*/_react.default.createElement(_inputGroup.default, {
      className: _fontDropdownModule.default.modContextMenu
    }, /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverSansSerif
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.sansSerif
    }, props.getFontName(_fonts.default.SANS_SERIF))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverSerif
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.serif
    }, props.getFontName(_fonts.default.SERIF))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverHandwriting
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.handwriting
    }, props.getFontName(_fonts.default.HANDWRITING))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverMarker
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.marker
    }, props.getFontName(_fonts.default.MARKER))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverCurly
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.curly
    }, props.getFontName(_fonts.default.CURLY))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverPixel
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.pixel
    }, props.getFontName(_fonts.default.PIXEL))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverChinese
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.chinese
    }, props.getFontName(_fonts.default.CHINESE))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverJapanese
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.japanese
    }, props.getFontName(_fonts.default.JAPANESE))), /*#__PURE__*/_react.default.createElement(_button.default, {
      className: (0, _classnames.default)(_fontDropdownModule.default.modMenuItem),
      onClick: props.onChoose,
      onMouseOver: props.onHoverKorean
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: _fontDropdownModule.default.korean
    }, props.getFontName(_fonts.default.KOREAN)))),
    ref: props.componentRef,
    tipSize: .01,
    onOpen: props.onOpenDropdown,
    onOuterAction: props.onClickOutsideDropdown
  }, /*#__PURE__*/_react.default.createElement("span", {
    className: (0, _classnames.default)(props.getFontStyle(props.font), _fontDropdownModule.default.displayedFontName)
  }, props.getFontName(props.font)));
};

var _default = ModeToolsComponent;
exports.default = _default;