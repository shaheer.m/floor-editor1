"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/defineProperty"));

var _classnames = _interopRequireDefault(require("classnames"));

var _react = _interopRequireDefault(require("react"));

var _inputGroupModule = _interopRequireDefault(require("./input-group.module.css"));

var InputGroup = function InputGroup(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(props.className, _inputGroupModule.default.inputGroup, (0, _defineProperty2.default)({}, _inputGroupModule.default.disabled, props.disabled)),
    dir: props.rtl ? 'rtl' : ''
  }, props.children);
};

var _default = InputGroup;
exports.default = _default;