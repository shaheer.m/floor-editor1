"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "IntlProvider", {
  enumerable: true,
  get: function get() {
    return _reactIntlRedux.IntlProvider;
  }
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _reactIntlRedux.intlReducer;
  }
});
exports.updateIntl = exports.intlInitialState = void 0;

var _reactIntl = require("react-intl");

var _reactIntlRedux = require("react-intl-redux");

var _scratchL10n = _interopRequireDefault(require("scratch-l10n"));

var _paintEditorMsgs = _interopRequireDefault(require("scratch-l10n/locales/paint-editor-msgs"));

Object.keys(_scratchL10n.default).forEach(function (locale) {
  // TODO: will need to handle locales not in the default intl - see www/custom-locales
  (0, _reactIntl.addLocaleData)(_scratchL10n.default[locale].localeData);
});
var intlInitialState = {
  intl: {
    defaultLocale: 'en',
    locale: 'en',
    messages: _paintEditorMsgs.default.en.messages
  }
};
exports.intlInitialState = intlInitialState;

var updateIntl = function updateIntl(locale) {
  return (0, _reactIntlRedux.updateIntl)({
    locale: locale,
    messages: _paintEditorMsgs.default[locale].messages || _paintEditorMsgs.default.en.messages
  });
};

exports.updateIntl = updateIntl;