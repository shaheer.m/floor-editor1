"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _intl = _interopRequireDefault(require("./intl"));

var _ = require("../..");

var _default = (0, _redux.combineReducers)({
  intl: _intl.default,
  scratchPaint: _.ScratchPaintReducer
});

exports.default = _default;