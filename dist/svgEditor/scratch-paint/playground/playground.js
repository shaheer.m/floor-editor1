"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _ = _interopRequireDefault(require(".."));

var _reactRedux = require("react-redux");

var _redux = require("redux");

var _combineReducers = _interopRequireDefault(require("./reducers/combine-reducers"));

var _intl = require("./reducers/intl.js");

var _playgroundModule = _interopRequireDefault(require("./playground.module.css"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _undo = require("../helper/undo");

var _undo2 = require("../reducers/undo");

var _math = require("../helper/math");

var _group = require("../helper/group");

var _scratchRenderFonts = require("scratch-render-fonts");

var _log = _interopRequireDefault(require("../log/log"));

var _format = _interopRequireDefault(require("../lib/format"));

var _view = require("../helper/view");

var _rasterImg = require("../../scratch-paint/helper/tools/raster-img");

/* eslint-disable no-restricted-globals */
// scratch-render-fonts is a playground-only dep. Fonts are expected to be imported
// as a peer dependency, otherwise there will be two copies of them.
// import svgImage from '../../../logo.svg'
// import {performSnapshot} from '../helper/undo';
// import { Point } from 'paper/dist/paper-core';
var appTarget = document.createElement('div');
appTarget.setAttribute('class', _playgroundModule.default.playgroundContainer);
document.body.appendChild(appTarget);
var store = (0, _redux.createStore)(_combineReducers.default, _intl.intlInitialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
var svgString = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"' + ' x="0px" y="0px" width="32px" height="32px" viewBox="0.5 384.5 32 32"' + ' enable-background="new 0.5 384.5 32 32" xml:space="preserve">' + '<path fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="10" d="M7.5,392.241h7.269' + 'c4.571,0,8.231,5.555,8.231,10.123v7.377"/>' + '<polyline points="10.689,399.492 3.193,391.997 10.689,384.5 "/>' + '<polyline points="30.185,405.995 22.689,413.491 15.192,405.995 "/>' + '</svg>';

var Playground = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(Playground, _React$Component);

  var _super = (0, _createSuper2.default)(Playground);

  function Playground(props) {
    var _this;

    (0, _classCallCheck2.default)(this, Playground);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['downloadImage', 'handleUpdateName', 'handleUpdateImage', 'onUploadImage', 'setCustomImage', 'recalibrateSize' // 'importSvg'
    ]); // Append ?dir=rtl to URL to get RTL layout

    var match = location.search.match(/dir=([^&]+)/);
    var rtl = match && match[1] == 'rtl';
    _this.id = 0;
    _this.state = {
      name: 'meow',
      rotationCenterX: 20,
      rotationCenterY: 400,
      imageFormat: 'svg',
      // 'svg', 'png', or 'jpg'
      image: svgString,
      // svg string or data URI
      imageId: _this.id,
      // If this changes, the paint editor will reload
      rtl: rtl
    };
    _this.reusableCanvas = document.createElement('canvas');
    return _this;
  }

  (0, _createClass2.default)(Playground, [{
    key: "handleUpdateName",
    value: function handleUpdateName(name) {
      this.setState({
        name: name
      });
    }
  }, {
    key: "handleUpdateImage",
    value: function handleUpdateImage(isVector, image, rotationCenterX, rotationCenterY) {
      this.setState({
        imageFormat: isVector ? 'svg' : 'png'
      });

      if (!isVector) {}

      if (isVector) {
        this.setState({
          image: image,
          rotationCenterX: rotationCenterX,
          rotationCenterY: rotationCenterY
        });
      } else {
        // is Bitmap
        // image parameter has type ImageData
        // paint editor takes dataURI as input
        this.reusableCanvas.width = image.width;
        this.reusableCanvas.height = image.height;
        var context = this.reusableCanvas.getContext('2d');
        context.putImageData(image, 0, 0);
        this.setState({
          image: this.reusableCanvas.toDataURL('image/png'),
          rotationCenterX: rotationCenterX,
          rotationCenterY: rotationCenterY
        });
      }
    }
  }, {
    key: "recalibrateSize",
    value: function recalibrateSize(callback) {
      // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
      // When these are out of sync, the mouse events in the paint editor don't line up correctly.
      return window.setTimeout(function () {
        // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
        // This could also be called before paper.view exists.
        // In either case, return early if so without running the callback.
        if (!_paper.default.view) return; // Prevent blurriness caused if the "CSS size" of the element is a float--
        // setting canvas dimensions to floats floors them, but we need to round instead

        var elemSize = _paper.default.DomElement.getSize(_paper.default.view.element);

        elemSize.width = Math.round(elemSize.width);
        elemSize.height = Math.round(elemSize.height);

        _paper.default.view.setViewSize(elemSize);

        if (callback) callback();
      }, 0);
    }
  }, {
    key: "setCustomImage",
    value: function setCustomImage(asset) {
      var paperCanvas = this;
      (0, _rasterImg.setRasterImage)(paperCanvas, asset);
    }
  }, {
    key: "downloadImage",
    value: function downloadImage() {
      var downloadLink = document.createElement('a');
      document.body.appendChild(downloadLink);
      var format = this.state.imageFormat;
      var data = this.state.image;

      if (format === 'png' || format === 'jpg') {
        data = this.b64toByteArray(data);
      } else {
        data = [data];
      }

      var blob = new Blob(data, {
        type: format
      });
      var filename = "".concat(this.state.name, ".").concat(format);

      if ('download' in HTMLAnchorElement.prototype) {
        var url = window.URL.createObjectURL(blob);
        downloadLink.href = url;
        downloadLink.download = filename;
        downloadLink.type = blob.type;
        downloadLink.click();
        window.URL.revokeObjectURL(url);
      } else {
        // iOS Safari, open a new page and set href to data-uri
        var popup = window.open('', '_blank');
        var reader = new FileReader();

        reader.onloadend = function () {
          popup.location.href = reader.result;
          popup = null;
        };

        reader.readAsDataURL(blob);
      }

      document.body.removeChild(downloadLink);
    }
  }, {
    key: "b64toByteArray",
    value: function b64toByteArray(b64Data) {
      var sliceSize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 512;
      // Remove header
      b64Data = b64Data.substring(b64Data.indexOf('base64,') + 7);
      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);

        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      return byteArrays;
    }
  }, {
    key: "uploadImage",
    value: function uploadImage() {
      document.getElementById(_playgroundModule.default.fileInput).click();
    }
  }, {
    key: "onUploadImage",
    value: function onUploadImage(event) {
      var file = event.target.files[0];
      var type = file.type === 'image/svg+xml' ? 'svg' : file.type === 'image/png' ? 'png' : file.type === 'image/jpg' ? 'jpg' : file.type === 'image/jpeg' ? 'jpg' : null;
      var reader = new FileReader();

      if (type === 'svg') {
        reader.readAsText(file, 'UTF-8');
      } else if (type === 'png' || type === 'jpg') {
        reader.readAsDataURL(file);
      } else {
        alert("Couldn't read file type: " + file.type);
      }

      var that = this;

      reader.onload = function (readerEvent) {
        var content = readerEvent.target.result; // this is the content!

        that.setState({
          image: content,
          name: file.name.split('.').slice(0, -1).join('.'),
          imageId: ++that.id,
          imageFormat: type,
          rotationCenterX: undefined,
          rotationCenterY: undefined
        });
      };
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: _playgroundModule.default.wrapper
      }, /*#__PURE__*/_react.default.createElement(_.default, Object.assign({}, this.state, {
        onUpdateName: this.handleUpdateName,
        onUpdateImage: this.handleUpdateImage,
        addShape: this.setCustomImage
      })), /*#__PURE__*/_react.default.createElement("button", {
        className: _playgroundModule.default.playgroundButton,
        onClick: this.uploadImage
      }, "Upload"), /*#__PURE__*/_react.default.createElement("input", {
        id: _playgroundModule.default.fileInput,
        type: "file",
        name: "name",
        onChange: this.onUploadImage
      }), /*#__PURE__*/_react.default.createElement("button", {
        className: _playgroundModule.default.playgroundButton,
        onClick: this.downloadImage
      }, "Download"));
    }
  }]);
  return Playground;
}(_react.default.Component);

exports.default = Playground;

_reactDom.default.render( /*#__PURE__*/_react.default.createElement(_reactRedux.Provider, {
  store: store
}, /*#__PURE__*/_react.default.createElement(_intl.IntlProvider, null, /*#__PURE__*/_react.default.createElement(Playground, null))), appTarget);