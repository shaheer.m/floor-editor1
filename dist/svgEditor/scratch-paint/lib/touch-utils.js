"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getEventXY = void 0;

/* DO NOT EDIT
@todo This file is copied from GUI and should be pulled out into a shared library.
See https://github.com/LLK/scratch-paint/issues/13 */
var getEventXY = function getEventXY(e) {
  if (e.touches && e.touches[0]) {
    return {
      x: e.touches[0].clientX,
      y: e.touches[0].clientY
    };
  } else if (e.changedTouches && e.changedTouches[0]) {
    return {
      x: e.changedTouches[0].clientX,
      y: e.changedTouches[0].clientY
    };
  }

  return {
    x: e.clientX,
    y: e.clientY
  };
};

exports.getEventXY = getEventXY;