"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var Fonts = {
  SANS_SERIF: 'Sans Serif',
  SERIF: 'Serif',
  HANDWRITING: 'Handwriting',
  MARKER: 'Marker',
  CURLY: 'Curly',
  PIXEL: 'Pixel',
  CHINESE: '"Microsoft YaHei", "微软雅黑", STXihei, "华文细黑"',
  JAPANESE: '"ヒラギノ角ゴ Pro W3", "Hiragino Kaku Gothic Pro", Osaka, "メイリオ", Meiryo, "ＭＳ Ｐゴシック", "MS PGothic"',
  KOREAN: 'Malgun Gothic'
};
var _default = Fonts;
exports.default = _default;