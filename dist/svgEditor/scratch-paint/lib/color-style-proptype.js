"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = require("prop-types");

var _gradientTypes = _interopRequireDefault(require("./gradient-types"));

var _default = _propTypes.PropTypes.shape({
  primary: _propTypes.PropTypes.string,
  secondary: _propTypes.PropTypes.string,
  gradientType: _propTypes.PropTypes.oneOf(Object.keys(_gradientTypes.default)).isRequired
});

exports.default = _default;