"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactIntl = require("react-intl");

var messages = (0, _reactIntl.defineMessages)({
  brush: {
    defaultMessage: 'Brush',
    description: 'Label for the brush tool',
    id: 'paint.brushMode.brush'
  },
  eraser: {
    defaultMessage: 'Eraser',
    description: 'Label for the eraser tool',
    id: 'paint.eraserMode.eraser'
  },
  fill: {
    defaultMessage: 'Fill',
    description: 'Label for the fill tool',
    id: 'paint.fillMode.fill'
  },
  line: {
    defaultMessage: 'Line',
    description: 'Label for the line tool',
    id: 'paint.lineMode.line'
  },
  oval: {
    defaultMessage: 'Circle',
    description: 'Label for the oval-drawing tool',
    id: 'paint.ovalMode.oval'
  },
  rect: {
    defaultMessage: 'Rectangle',
    description: 'Label for the rectangle tool',
    id: 'paint.rectMode.rect'
  },
  reshape: {
    defaultMessage: 'Reshape',
    description: 'Label for the reshape tool, which allows changing the points in the lines of the vectors',
    id: 'paint.reshapeMode.reshape'
  },
  roundedRect: {
    defaultMessage: 'Rounded Rectangle',
    description: 'Label for the rounded rectangle tool',
    id: 'paint.roundedRectMode.roundedRect'
  },
  select: {
    defaultMessage: 'Select',
    description: 'Label for the select tool, which allows selecting, moving, and resizing shapes',
    id: 'paint.selectMode.select'
  },
  text: {
    defaultMessage: 'Text',
    description: 'Label for the text tool',
    id: 'paint.textMode.text'
  },
  seat: {
    defaultMessage: 'Seat',
    description: 'Label for the Seat tool',
    id: 'paint.textMode.seat'
  },
  front: {
    defaultMessage: 'Front View',
    description: 'Label for the Front View',
    id: 'paint.textMode.seat'
  },
  top: {
    defaultMessage: 'Top View',
    description: 'Label for the Top View',
    id: 'paint.textMode.seat'
  },
  side: {
    defaultMessage: 'Side View',
    description: 'Label for the Side View',
    id: 'paint.textMode.seat'
  },
  errorMaxChar: {
    defaultMessage: 'errorMaxChar',
    description: 'Element name should not exceed 15 characters.',
    id: 'error.MaxChar'
  },
  errorRestChar: {
    defaultMessage: 'errorRestChar',
    description: "The words 'unavailable' and 'clickable' shouldn't be used in the element name text.",
    id: 'error.RestChar'
  },
  errorSpecialChar: {
    defaultMessage: 'errorSpecialChar',
    description: "Element name should not contain special characters.",
    id: 'error.SpecialChar'
  },
  errorCapacity: {
    defaultMessage: 'errorCapacity',
    description: "Please provide a number larger than 0.",
    id: 'error.Capacity'
  },
  errorDuplicateName: {
    defaultMessage: 'errorDuplicateName',
    description: "The Seat Name already exists. Please enter a unique Seat Name.",
    id: 'error.DuplicateName'
  },
  errorDuplicatingAllocatedItem: {
    defaultMessage: 'errorDuplicatingAllocatedItem',
    description: "It is not permitted to duplicate the allocated elements or groups.",
    id: 'error.DuplicatingAllocatedItem'
  }
});
var _default = messages;
exports.default = _default;