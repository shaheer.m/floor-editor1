"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _keymirror = _interopRequireDefault(require("keymirror"));

var GradientTypes = (0, _keymirror.default)({
  SOLID: null,
  HORIZONTAL: null,
  VERTICAL: null,
  RADIAL: null
});
var _default = GradientTypes;
exports.default = _default;