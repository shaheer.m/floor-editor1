"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var Cursors = {
  DEFAULT: 'default',
  GRAB: 'grab',
  GRABBING: 'grabbing',
  NONE: 'none',
  RESIZE_EW: 'ew-resize',
  RESIZE_NS: 'ns-resize',
  RESIZE_NESW: 'nesw-resize',
  RESIZE_NWSE: 'nwse-resize'
};
var _default = Cursors;
exports.default = _default;