"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isVector = exports.isBitmap = exports.default = void 0;

var _keymirror = _interopRequireDefault(require("keymirror"));

var Formats = (0, _keymirror.default)({
  BITMAP: null,
  VECTOR: null,
  // Format changes which should not trigger conversions, for instance undo
  BITMAP_SKIP_CONVERT: null,
  VECTOR_SKIP_CONVERT: null
});
exports.default = Formats;

var isVector = function isVector(format) {
  return format === Formats.VECTOR || format === Formats.VECTOR_SKIP_CONVERT;
};

exports.isVector = isVector;

var isBitmap = function isBitmap(format) {
  return format === Formats.BITMAP || format === Formats.BITMAP_SKIP_CONVERT;
};

exports.isBitmap = isBitmap;