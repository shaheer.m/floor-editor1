"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.VectorModes = exports.GradientToolsModes = exports.BitmapModes = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _keymirror = _interopRequireDefault(require("keymirror"));

var vectorModesObj = {
  BRUSH: null,
  ERASER: null,
  LINE: null,
  FILL: null,
  SELECT: null,
  RESHAPE: null,
  OVAL: null,
  RECT: null,
  ROUNDED_RECT: null,
  TEXT: null
};
var bitmapModesObj = {
  BIT_BRUSH: null,
  BIT_LINE: null,
  BIT_OVAL: null,
  BIT_RECT: null,
  BIT_TEXT: null,
  BIT_FILL: null,
  BIT_ERASER: null,
  BIT_SELECT: null
};
var VectorModes = (0, _keymirror.default)(vectorModesObj);
exports.VectorModes = VectorModes;
var BitmapModes = (0, _keymirror.default)(bitmapModesObj);
exports.BitmapModes = BitmapModes;
var Modes = (0, _keymirror.default)((0, _objectSpread2.default)((0, _objectSpread2.default)({}, vectorModesObj), bitmapModesObj));
exports.default = Modes;
var GradientToolsModes = (0, _keymirror.default)({
  FILL: null,
  SELECT: null,
  RESHAPE: null,
  OVAL: null,
  RECT: null,
  LINE: null,
  BIT_OVAL: null,
  BIT_RECT: null,
  BIT_SELECT: null,
  BIT_FILL: null
});
exports.GradientToolsModes = GradientToolsModes;