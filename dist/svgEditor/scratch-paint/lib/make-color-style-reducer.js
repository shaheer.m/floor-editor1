"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _log = _interopRequireDefault(require("../log/log"));

var _selectedItems = require("../reducers/selected-items");

var _stylePath = require("../helper/style-path");

var _gradientTypes = _interopRequireDefault(require("./gradient-types"));

// Matches hex colors
var hexRegex = /^#([0-9a-f]{3}){1,2}$/i;

var isValidHexColor = function isValidHexColor(color) {
  if (!hexRegex.test(color) && color !== null && color !== _stylePath.MIXED) {
    _log.default.warn("Invalid hex color code: ".concat(color));

    return false;
  }

  return true;
};

var makeColorStyleReducer = function makeColorStyleReducer(_ref) {
  var changePrimaryColorAction = _ref.changePrimaryColorAction,
      changeSecondaryColorAction = _ref.changeSecondaryColorAction,
      changeGradientTypeAction = _ref.changeGradientTypeAction,
      clearGradientAction = _ref.clearGradientAction,
      defaultColor = _ref.defaultColor,
      selectionPrimaryColorKey = _ref.selectionPrimaryColorKey,
      selectionSecondaryColorKey = _ref.selectionSecondaryColorKey,
      selectionGradientTypeKey = _ref.selectionGradientTypeKey;
  return function colorReducer(state, action) {
    if (typeof state === 'undefined') {
      state = {
        primary: defaultColor,
        secondary: null,
        gradientType: _gradientTypes.default.SOLID
      };
    }

    switch (action.type) {
      case changePrimaryColorAction:
        if (!isValidHexColor(action.color)) return state;
        return (0, _objectSpread2.default)((0, _objectSpread2.default)({}, state), {}, {
          primary: action.color
        });

      case changeSecondaryColorAction:
        if (!isValidHexColor(action.color)) return state;
        return (0, _objectSpread2.default)((0, _objectSpread2.default)({}, state), {}, {
          secondary: action.color
        });

      case _selectedItems.CHANGE_SELECTED_ITEMS:
        {
          // Don't change state if no selection
          if (!action.selectedItems || !action.selectedItems.length) {
            return state;
          }

          var colors = (0, _stylePath.getColorsFromSelection)(action.selectedItems, action.bitmapMode); // Only set the primary color + gradient type if they exist in what getColorsFromSelection gave us.
          // E.g. in bitmap mode, getColorsFromSelection will not return stroke color/gradient type. This allows us to
          // preserve stroke swatch state across bitmap mode-- if getColorsFromSelection set them to null, then selecting
          // anything in bitmap mode would overwrite the stroke state.

          var newState = (0, _objectSpread2.default)({}, state);

          if (selectionPrimaryColorKey in colors) {
            newState.primary = colors[selectionPrimaryColorKey];
          }

          if (selectionGradientTypeKey in colors) {
            newState.gradientType = colors[selectionGradientTypeKey];
          } // Gradient type may be solid when multiple gradient types are selected.
          // In this case, changing the first color should not change the second color.


          if (selectionSecondaryColorKey in colors && (colors[selectionGradientTypeKey] !== _gradientTypes.default.SOLID || colors[selectionSecondaryColorKey] === _stylePath.MIXED)) {
            newState.secondary = colors[selectionSecondaryColorKey];
          }

          return newState;
        }

      case changeGradientTypeAction:
        if (action.gradientType in _gradientTypes.default) {
          return (0, _objectSpread2.default)((0, _objectSpread2.default)({}, state), {}, {
            gradientType: action.gradientType
          });
        }

        _log.default.warn("Gradient type does not exist: ".concat(action.gradientType));

        return state;

      case clearGradientAction:
        return (0, _objectSpread2.default)((0, _objectSpread2.default)({}, state), {}, {
          secondary: null,
          gradientType: _gradientTypes.default.SOLID
        });

      default:
        return state;
    }
  };
};

var _default = makeColorStyleReducer;
exports.default = _default;