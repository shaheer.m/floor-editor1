"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hideLabel = void 0;
var localeTooBig = ['ab', 'ca', 'cy', 'de', 'et', 'el', 'ga', 'gd', 'gl', 'mi', 'nl', 'ja', 'ja-Hira', 'nb', 'nn', 'rap', 'th', 'sr', 'sk', 'sl', 'fi', 'sv', 'sw', 'vi', 'tr', 'uk'];

var hideLabel = function hideLabel(locale) {
  return localeTooBig.includes(locale);
};

exports.hideLabel = hideLabel;