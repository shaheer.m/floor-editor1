"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _reactIntl = require("react-intl");

var _selection = require("../helper/selection");

var _format = _interopRequireWildcard(require("../lib/format"));

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _colorIndicator = _interopRequireDefault(require("../components/color-indicator"));

var _stylePath = require("../helper/style-path");

var makeColorIndicator = function makeColorIndicator(label, isStroke) {
  var ColorIndicator = /*#__PURE__*/function (_React$Component) {
    (0, _inherits2.default)(ColorIndicator, _React$Component);

    var _super = (0, _createSuper2.default)(ColorIndicator);

    function ColorIndicator(props) {
      var _this;

      (0, _classCallCheck2.default)(this, ColorIndicator);
      _this = _super.call(this, props);
      (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChangeColor', 'handleChangeGradientType', 'handleCloseColor', 'handleSwap']); // Flag to track whether an svg-update-worthy change has been made

      _this._hasChanged = false;
      return _this;
    }

    (0, _createClass2.default)(ColorIndicator, [{
      key: "componentWillReceiveProps",
      value: function componentWillReceiveProps(newProps) {
        var _this$props = this.props,
            colorModalVisible = _this$props.colorModalVisible,
            onUpdateImage = _this$props.onUpdateImage;

        if (colorModalVisible && !newProps.colorModalVisible) {
          // Submit the new SVG, which also stores a single undo/redo action.
          if (this._hasChanged) onUpdateImage();
          this._hasChanged = false;
        }
      }
    }, {
      key: "handleChangeColor",
      value: function handleChangeColor(newColor) {
        // Stroke-selector-specific logic: if we change the stroke color from "none" to something visible, ensure
        // there's a nonzero stroke width. If we change the stroke color to "none", set the stroke width to zero.
        if (isStroke) {
          // Whether the old color style in this color indicator was null (completely transparent).
          // If it's a solid color, this means that the first color is null.
          // If it's a gradient, this means both colors are null.
          var oldStyleWasNull = this.props.gradientType === _gradientTypes.default.SOLID ? this.props.color === null : this.props.color === null && this.props.color2 === null;
          var otherColor = this.props.colorIndex === 1 ? this.props.color : this.props.color2; // Whether the new color style in this color indicator is null.

          var newStyleIsNull = this.props.gradientType === _gradientTypes.default.SOLID ? newColor === null : newColor === null && otherColor === null;

          if (oldStyleWasNull && !newStyleIsNull) {
            this._hasChanged = (0, _stylePath.applyStrokeWidthToSelection)(1, this.props.textEditTarget) || this._hasChanged;
            this.props.onChangeStrokeWidth(1);
          } else if (!oldStyleWasNull && newStyleIsNull) {
            this._hasChanged = (0, _stylePath.applyStrokeWidthToSelection)(0, this.props.textEditTarget) || this._hasChanged;
            this.props.onChangeStrokeWidth(0);
          }
        }

        var formatIsBitmap = (0, _format.isBitmap)(this.props.format); // Apply color and update redux, but do not update svg until picker closes.

        var isDifferent = (0, _stylePath.applyColorToSelection)(newColor, this.props.colorIndex, this.props.gradientType === _gradientTypes.default.SOLID, // In bitmap mode, only the fill color selector is used, but it applies to stroke if fillBitmapShapes
        // is set to true via the "Fill"/"Outline" selector button
        isStroke || formatIsBitmap && !this.props.fillBitmapShapes, this.props.textEditTarget);
        this._hasChanged = this._hasChanged || isDifferent;
        this.props.onChangeColor(newColor, this.props.colorIndex);
      }
    }, {
      key: "handleChangeGradientType",
      value: function handleChangeGradientType(gradientType) {
        var formatIsBitmap = (0, _format.isBitmap)(this.props.format); // Apply color and update redux, but do not update svg until picker closes.

        var isDifferent = (0, _stylePath.applyGradientTypeToSelection)(gradientType, isStroke || formatIsBitmap && !this.props.fillBitmapShapes, this.props.textEditTarget);
        this._hasChanged = this._hasChanged || isDifferent;
        var hasSelectedItems = (0, _selection.getSelectedLeafItems)().length > 0;

        if (hasSelectedItems) {
          if (isDifferent) {
            // Recalculates the swatch colors
            this.props.setSelectedItems(this.props.format);
          }
        }

        if (this.props.gradientType === _gradientTypes.default.SOLID && gradientType !== _gradientTypes.default.SOLID) {
          // Generate color 2 and change to the 2nd swatch when switching from solid to gradient
          if (!hasSelectedItems) {
            this.props.onChangeColor((0, _stylePath.generateSecondaryColor)(this.props.color), 1);
          }

          this.props.onChangeColorIndex(1);
        }

        if (this.props.onChangeGradientType) this.props.onChangeGradientType(gradientType);
      }
    }, {
      key: "handleCloseColor",
      value: function handleCloseColor() {
        // If the eyedropper is currently being used, don't
        // close the color menu.
        if (this.props.isEyeDropping) return; // Otherwise, close the color menu and
        // also reset the color index to indicate
        // that `color1` is selected.

        this.props.onCloseColor();
        this.props.onChangeColorIndex(0);
      }
    }, {
      key: "handleSwap",
      value: function handleSwap() {
        if ((0, _selection.getSelectedLeafItems)().length) {
          var formatIsBitmap = (0, _format.isBitmap)(this.props.format);
          var isDifferent = (0, _stylePath.swapColorsInSelection)(isStroke || formatIsBitmap && !this.props.fillBitmapShapes, this.props.textEditTarget);
          this.props.setSelectedItems(this.props.format);
          this._hasChanged = this._hasChanged || isDifferent;
        } else {
          var color1 = this.props.color;
          var color2 = this.props.color2;
          color1 = color1 === null || color1 === _stylePath.MIXED ? color1 : (0, _parseColor.default)(color1).hex;
          color2 = color2 === null || color2 === _stylePath.MIXED ? color2 : (0, _parseColor.default)(color2).hex;
          this.props.onChangeColor(color1, 1);
          this.props.onChangeColor(color2, 0);
        }
      }
    }, {
      key: "render",
      value: function render() {
        return /*#__PURE__*/_react.default.createElement(_colorIndicator.default, Object.assign({}, this.props, {
          label: this.props.intl.formatMessage(label),
          outline: isStroke,
          onChangeColor: this.handleChangeColor,
          onChangeGradientType: this.handleChangeGradientType,
          onCloseColor: this.handleCloseColor,
          onSwap: this.handleSwap
        }));
      }
    }]);
    return ColorIndicator;
  }(_react.default.Component);

  return (0, _reactIntl.injectIntl)(ColorIndicator);
};

var _default = makeColorIndicator;
exports.default = _default;