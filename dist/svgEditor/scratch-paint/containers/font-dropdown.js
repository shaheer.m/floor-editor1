"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _fontDropdown = _interopRequireDefault(require("../components/font-dropdown/font-dropdown"));

var _fonts = _interopRequireDefault(require("../lib/fonts"));

var _font = require("../reducers/font");

var _selection = require("../helper/selection");

var _fontDropdownModule = _interopRequireDefault(require("../components/font-dropdown/font-dropdown.module.css"));

var FontDropdown = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(FontDropdown, _React$Component);

  var _super = (0, _createSuper2.default)(FontDropdown);

  function FontDropdown(props) {
    var _this;

    (0, _classCallCheck2.default)(this, FontDropdown);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['getFontStyle', 'getFontName', 'handleChangeFontSerif', 'handleChangeFontSansSerif', 'handleChangeFontHandwriting', 'handleChangeFontMarker', 'handleChangeFontCurly', 'handleChangeFontPixel', 'handleChangeFontChinese', 'handleChangeFontJapanese', 'handleChangeFontKorean', 'handleOpenDropdown', 'handleClickOutsideDropdown', 'setDropdown', 'handleChoose']);
    return _this;
  }

  (0, _createClass2.default)(FontDropdown, [{
    key: "getFontStyle",
    value: function getFontStyle(font) {
      switch (font) {
        case _fonts.default.SERIF:
          return _fontDropdownModule.default.serif;

        case _fonts.default.SANS_SERIF:
          return _fontDropdownModule.default.sansSerif;

        case _fonts.default.HANDWRITING:
          return _fontDropdownModule.default.handwriting;

        case _fonts.default.MARKER:
          return _fontDropdownModule.default.marker;

        case _fonts.default.CURLY:
          return _fontDropdownModule.default.curly;

        case _fonts.default.PIXEL:
          return _fontDropdownModule.default.pixel;

        case _fonts.default.CHINESE:
          return _fontDropdownModule.default.chinese;

        case _fonts.default.JAPANESE:
          return _fontDropdownModule.default.japanese;

        case _fonts.default.KOREAN:
          return _fontDropdownModule.default.korean;

        default:
          return '';
      }
    }
  }, {
    key: "getFontName",
    value: function getFontName(font) {
      switch (font) {
        case _fonts.default.CHINESE:
          return '中文';

        case _fonts.default.KOREAN:
          return '한국어';

        case _fonts.default.JAPANESE:
          return '日本語';

        default:
          return font;
      }
    }
  }, {
    key: "handleChangeFontSansSerif",
    value: function handleChangeFontSansSerif() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.SANS_SERIF);
      }
    }
  }, {
    key: "handleChangeFontSerif",
    value: function handleChangeFontSerif() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.SERIF);
      }
    }
  }, {
    key: "handleChangeFontHandwriting",
    value: function handleChangeFontHandwriting() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.HANDWRITING);
      }
    }
  }, {
    key: "handleChangeFontMarker",
    value: function handleChangeFontMarker() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.MARKER);
      }
    }
  }, {
    key: "handleChangeFontCurly",
    value: function handleChangeFontCurly() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.CURLY);
      }
    }
  }, {
    key: "handleChangeFontPixel",
    value: function handleChangeFontPixel() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.PIXEL);
      }
    }
  }, {
    key: "handleChangeFontChinese",
    value: function handleChangeFontChinese() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.CHINESE);
      }
    }
  }, {
    key: "handleChangeFontJapanese",
    value: function handleChangeFontJapanese() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.JAPANESE);
      }
    }
  }, {
    key: "handleChangeFontKorean",
    value: function handleChangeFontKorean() {
      if (this.dropDown.isOpen()) {
        this.props.changeFont(_fonts.default.KOREAN);
      }
    }
  }, {
    key: "handleChoose",
    value: function handleChoose() {
      if (this.dropDown.isOpen()) {
        this.dropDown.handleClosePopover();
        this.props.onUpdateImage();
      }
    }
  }, {
    key: "handleOpenDropdown",
    value: function handleOpenDropdown() {
      this.savedFont = this.props.font;
      this.savedSelection = (0, _selection.getSelectedLeafItems)();
    }
  }, {
    key: "handleClickOutsideDropdown",
    value: function handleClickOutsideDropdown(e) {
      e.stopPropagation();
      this.dropDown.handleClosePopover(); // Cancel font change

      var _iterator = (0, _createForOfIteratorHelper2.default)(this.savedSelection),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;

          if (item instanceof _paper.default.PointText) {
            item.font = this.savedFont;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this.props.changeFont(this.savedFont);
      this.savedFont = null;
      this.savedSelection = null;
    }
  }, {
    key: "setDropdown",
    value: function setDropdown(element) {
      this.dropDown = element;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_fontDropdown.default, {
        componentRef: this.setDropdown,
        font: this.props.font,
        getFontName: this.getFontName,
        getFontStyle: this.getFontStyle,
        onChoose: this.handleChoose,
        onClickOutsideDropdown: this.handleClickOutsideDropdown,
        onHoverChinese: this.handleChangeFontChinese,
        onHoverCurly: this.handleChangeFontCurly,
        onHoverHandwriting: this.handleChangeFontHandwriting,
        onHoverJapanese: this.handleChangeFontJapanese,
        onHoverKorean: this.handleChangeFontKorean,
        onHoverMarker: this.handleChangeFontMarker,
        onHoverPixel: this.handleChangeFontPixel,
        onHoverSansSerif: this.handleChangeFontSansSerif,
        onHoverSerif: this.handleChangeFontSerif,
        onOpenDropdown: this.handleOpenDropdown
      });
    }
  }]);
  return FontDropdown;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    font: state.scratchPaint.font
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    changeFont: function changeFont(font) {
      dispatch((0, _font.changeFont)(font));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FontDropdown);

exports.default = _default;