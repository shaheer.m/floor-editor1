"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _fillTool = _interopRequireDefault(require("../helper/tools/fill-tool"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _hover = require("../reducers/hover");

var _fillModeGradientType = require("../reducers/fill-mode-gradient-type");

var _fillMode = _interopRequireDefault(require("../components/fill-mode/fill-mode"));

var FillMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(FillMode, _React$Component);

  var _super = (0, _createSuper2.default)(FillMode);

  function FillMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, FillMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(FillMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isFillModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool) {
        if (nextProps.fillColor !== this.props.fillColor) {
          this.tool.setFillColor(nextProps.fillColor);
        }

        if (nextProps.fillColor2 !== this.props.fillColor2) {
          this.tool.setFillColor2(nextProps.fillColor2);
        }

        if (nextProps.hoveredItemId !== this.props.hoveredItemId) {
          this.tool.setPrevHoveredItemId(nextProps.hoveredItemId);
        }

        if (nextProps.fillModeGradientType !== this.props.fillModeGradientType) {
          this.tool.setGradientType(nextProps.fillModeGradientType);
        }
      }

      if (nextProps.isFillModeActive && !this.props.isFillModeActive) {
        this.activateTool();
      } else if (!nextProps.isFillModeActive && this.props.isFillModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isFillModeActive !== this.props.isFillModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems); // Force the default fill color if fill is MIXED

      var fillColor = this.props.fillColor;

      if (this.props.fillColor === _stylePath.MIXED) {
        fillColor = _fillStyle.DEFAULT_COLOR;
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR, 0);
      }

      var gradientType = this.props.fillModeGradientType ? this.props.fillModeGradientType : this.props.fillStyleGradientType;
      var fillColor2 = this.props.fillColor2;

      if (gradientType !== this.props.fillStyleGradientType) {
        if (this.props.fillStyleGradientType === _gradientTypes.default.SOLID) {
          fillColor2 = (0, _stylePath.generateSecondaryColor)(fillColor);
          this.props.onChangeFillColor(fillColor2, 1);
        }

        this.props.changeGradientType(gradientType);
      }

      if (this.props.fillColor2 === _stylePath.MIXED) {
        fillColor2 = (0, _stylePath.generateSecondaryColor)(fillColor);
        this.props.onChangeFillColor(fillColor2, 1);
      }

      this.tool = new _fillTool.default(this.props.setHoveredItem, this.props.clearHoveredItem, this.props.onUpdateImage);
      this.tool.setFillColor(fillColor);
      this.tool.setFillColor2(fillColor2);
      this.tool.setGradientType(gradientType);
      this.tool.setPrevHoveredItemId(this.props.hoveredItemId);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_fillMode.default, {
        isSelected: this.props.isFillModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return FillMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    fillModeGradientType: state.scratchPaint.fillMode.gradientType,
    // Last user-selected gradient type
    fillColor: state.scratchPaint.color.fillColor.primary,
    fillColor2: state.scratchPaint.color.fillColor.secondary,
    fillStyleGradientType: state.scratchPaint.color.fillColor.gradientType,
    // Selected item(s)' gradient type
    hoveredItemId: state.scratchPaint.hoveredItemId,
    isFillModeActive: state.scratchPaint.mode === _modes.default.FILL
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    setHoveredItem: function setHoveredItem(hoveredItemId) {
      dispatch((0, _hover.setHoveredItem)(hoveredItemId));
    },
    clearHoveredItem: function clearHoveredItem() {
      dispatch((0, _hover.clearHoveredItem)());
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    changeGradientType: function changeGradientType(gradientType) {
      dispatch((0, _fillModeGradientType.changeGradientType)(gradientType));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.FILL));
    },
    onChangeFillColor: function onChangeFillColor(fillColor, index) {
      if (index === 0) {
        dispatch((0, _fillStyle.changeFillColor)(fillColor));
      } else if (index === 1) {
        dispatch((0, _fillStyle.changeFillColor2)(fillColor));
      }
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FillMode);

exports.default = _default;