"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _bitLineMode = _interopRequireDefault(require("../components/bit-line-mode/bit-line-mode"));

var _lineTool = _interopRequireDefault(require("../helper/bit-tools/line-tool"));

var BitLineMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitLineMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitLineMode);

  function BitLineMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitLineMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitLineMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isBitLineModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.color !== this.props.color) {
        this.tool.setColor(nextProps.color);
      }

      if (this.tool && nextProps.bitBrushSize !== this.props.bitBrushSize) {
        this.tool.setLineSize(nextProps.bitBrushSize);
      }

      if (nextProps.isBitLineModeActive && !this.props.isBitLineModeActive) {
        this.activateTool();
      } else if (!nextProps.isBitLineModeActive && this.props.isBitLineModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isBitLineModeActive !== this.props.isBitLineModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.props.clearGradient(); // Force the default line color if fill is MIXED or transparent

      var color = this.props.color;

      if (!color || color === _stylePath.MIXED) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
        color = _fillStyle.DEFAULT_COLOR;
      }

      this.tool = new _lineTool.default(this.props.onUpdateImage);
      this.tool.setColor(color);
      this.tool.setLineSize(this.props.bitBrushSize);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitLineMode.default, {
        isSelected: this.props.isBitLineModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitLineMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    bitBrushSize: state.scratchPaint.bitBrushSize,
    color: state.scratchPaint.color.fillColor.primary,
    isBitLineModeActive: state.scratchPaint.mode === _modes.default.BIT_LINE
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearGradient: function clearGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_LINE));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitLineMode);

exports.default = _default;