"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _selection = require("../helper/selection");

var _snapping = require("../helper/snapping");

var _guides = require("../helper/guides");

var _stylePath = require("../helper/style-path");

var _strokeStyle = require("../reducers/stroke-style");

var _strokeWidth = require("../reducers/stroke-width");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _math = require("../helper/math");

var _lineMode = _interopRequireDefault(require("../components/line-mode/line-mode"));

var LineMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(LineMode, _React$Component);

  var _super = (0, _createSuper2.default)(LineMode);

  function LineMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, LineMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool', 'drawHitPoint', 'onMouseDown', 'onMouseMove', 'onMouseDrag', 'onMouseUp']);
    return _this;
  }

  (0, _createClass2.default)(LineMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isLineModeActive) {
        this.activateTool();
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.isLineModeActive && !this.props.isLineModeActive) {
        this.activateTool();
      } else if (!nextProps.isLineModeActive && this.props.isLineModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isLineModeActive !== this.props.isLineModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems); // Force the default line color if stroke is MIXED or transparent

      var strokeColor1 = this.props.colorState.strokeColor.primary;
      var strokeColor2 = this.props.colorState.strokeColor.secondary;

      if (strokeColor1 === _stylePath.MIXED || strokeColor1 === null && (strokeColor2 === null || strokeColor2 === _stylePath.MIXED)) {
        this.props.onChangeStrokeColor(LineMode.DEFAULT_COLOR);
      }

      if (strokeColor2 === _stylePath.MIXED) {
        this.props.clearStrokeGradient();
      } // Force a minimum stroke width


      if (!this.props.colorState.strokeWidth) {
        this.props.onChangeStrokeWidth(1);
      }

      this.tool = new _paper.default.Tool();
      this.active = false;
      this.path = null;
      this.hitResult = null;
      var lineMode = this;

      this.tool.onMouseDown = function (event) {
        if (event.event.button > 0) return; // only first mouse button

        lineMode.onMouseDown(event);
      };

      this.tool.onMouseMove = function (event) {
        lineMode.onMouseMove(event);
      };

      this.tool.onMouseDrag = function (event) {
        if (event.event.button > 0) return; // only first mouse button

        lineMode.onMouseDrag(event);
      };

      this.tool.onMouseUp = function (event) {
        if (event.event.button > 0) return; // only first mouse button

        lineMode.onMouseUp(event);
      };

      this.tool.activate();
    }
  }, {
    key: "onMouseDown",
    value: function onMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true; // If you click near a point, continue that line instead of making a new line

      this.hitResult = (0, _snapping.endPointHit)(event.point, LineMode.SNAP_TOLERANCE);

      if (this.hitResult) {
        this.path = this.hitResult.path;
        (0, _stylePath.styleShape)(this.path, {
          fillColor: null,
          strokeColor: this.props.colorState.strokeColor,
          strokeWidth: this.props.colorState.strokeWidth
        });

        if (this.hitResult.isFirst) {
          this.path.reverse();
        }

        this.path.lastSegment.handleOut = null; // Make sure added line isn't made curvy

        this.path.add(this.hitResult.segment.point); // Add second point, which is what will move when dragged
      } // If not near other path, start a new path


      if (!this.path) {
        this.path = new _paper.default.Path();
        this.path.strokeCap = 'round';
        (0, _stylePath.styleShape)(this.path, {
          fillColor: null,
          strokeColor: this.props.colorState.strokeColor,
          strokeWidth: this.props.colorState.strokeWidth
        });
        this.path.add(event.point);
        this.path.add(event.point); // Add second point, which is what will move when dragged
      }
    }
  }, {
    key: "drawHitPoint",
    value: function drawHitPoint(hitResult) {
      // If near another path's endpoint, draw hit point to indicate that paths would merge
      if (hitResult) {
        var hitPath = hitResult.path;

        if (hitResult.isFirst) {
          (0, _guides.drawHitPoint)(hitPath.firstSegment.point);
        } else {
          (0, _guides.drawHitPoint)(hitPath.lastSegment.point);
        }
      }
    }
  }, {
    key: "onMouseMove",
    value: function onMouseMove(event) {
      if (this.hitResult) {
        (0, _guides.removeHitPoint)();
      }

      this.hitResult = (0, _snapping.endPointHit)(event.point, LineMode.SNAP_TOLERANCE);
      this.drawHitPoint(this.hitResult);
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button
      // Clear the last hit result

      if (this.hitResult) {
        (0, _guides.removeHitPoint)();
        this.hitResult = null;
      } // If shift is held, act like event.point always lies on a straight or 45 degree line from the last point


      var endPoint = event.point;

      if (event.modifiers.shift) {
        var line = event.point.subtract(this.path.lastSegment.previous.point);
        endPoint = this.path.lastSegment.previous.point.add((0, _math.snapDeltaToAngle)(line, Math.PI / 4));
      } // Find an end point that endPoint is close to (to snap lines together)


      if (this.path && !this.path.closed && this.path.segments.length > 3 && (0, _snapping.touching)(this.path.firstSegment.point, endPoint, LineMode.SNAP_TOLERANCE)) {
        this.hitResult = {
          path: this.path,
          segment: this.path.firstSegment,
          isFirst: true
        };
      } else {
        this.hitResult = (0, _snapping.endPointHit)(endPoint, LineMode.SNAP_TOLERANCE, this.path);
      } // If shift is being held, we shouldn't snap to end points that change the slope by too much.
      // In that case, clear the hit result.


      if (this.hitResult && event.modifiers.shift) {
        var lineToSnap = this.hitResult.segment.point.subtract(this.path.lastSegment.previous.point);
        var lineToEndPoint = endPoint.subtract(this.path.lastSegment.previous.point);

        if (lineToSnap.normalize().getDistance(lineToEndPoint.normalize()) > 1e-2) {
          this.hitResult = null;
        }
      } // If near another path's endpoint, or this path's beginpoint, clip to it to suggest
      // joining/closing the paths.


      if (this.hitResult) {
        this.drawHitPoint(this.hitResult);
        this.path.lastSegment.point = this.hitResult.segment.point;
      } else {
        this.path.lastSegment.point = endPoint;
      }

      (0, _stylePath.styleShape)(this.path, {
        fillColor: null,
        strokeColor: this.props.colorState.strokeColor,
        strokeWidth: this.props.colorState.strokeWidth
      });
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button
      // If I single clicked, don't do anything

      if (this.path.segments.length < 2 || this.path.segments.length === 2 && (0, _snapping.touching)(this.path.firstSegment.point, event.point, LineMode.SNAP_TOLERANCE) && !this.hitResult) {
        // Let lines be short if you're connecting them
        this.path.remove();
        this.path = null;
        return;
      } else if (!this.hitResult && (0, _snapping.touching)(this.path.lastSegment.point, this.path.segments[this.path.segments.length - 2].point, LineMode.SNAP_TOLERANCE)) {
        // Single click or short drag on an existing path end point
        this.path.removeSegment(this.path.segments.length - 1);
        this.path = null;
        return;
      } // If I intersect other line end points, join or close


      if (this.hitResult) {
        this.path.removeSegment(this.path.segments.length - 1);

        if (this.path.firstSegment.point.equals(this.hitResult.segment.point)) {
          this.path.firstSegment.handleIn = null; // Make sure added line isn't made curvy
          // close path

          this.path.closed = true;
        } else {
          // joining two paths
          if (!this.hitResult.isFirst) {
            this.hitResult.path.reverse();
          }

          this.hitResult.path.firstSegment.handleIn = null; // Make sure added line isn't made curvy

          this.path.join(this.hitResult.path);
        }

        (0, _guides.removeHitPoint)();
        this.hitResult = null;
      }

      (0, _stylePath.styleShape)(this.path, {
        fillColor: null,
        strokeColor: this.props.colorState.strokeColor,
        strokeWidth: this.props.colorState.strokeWidth
      });

      if (this.path) {
        this.props.onUpdateImage();
        this.path = null;
      }

      this.active = false;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.remove();
      this.tool = null;

      if (this.hitResult) {
        (0, _guides.removeHitPoint)();
        this.hitResult = null;
      }

      if (this.path) {
        this.path = null;
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_lineMode.default, {
        isSelected: this.props.isLineModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }], [{
    key: "SNAP_TOLERANCE",
    get: function get() {
      return 6;
    }
  }, {
    key: "DEFAULT_COLOR",
    get: function get() {
      return '#000000';
    }
  }]);
  return LineMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    colorState: state.scratchPaint.color,
    isLineModeActive: state.scratchPaint.mode === _modes.default.LINE
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearStrokeGradient: function clearStrokeGradient() {
      dispatch((0, _strokeStyle.clearStrokeGradient)());
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.LINE));
    },
    onChangeStrokeColor: function onChangeStrokeColor(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeColor)(strokeColor));
    },
    onChangeStrokeWidth: function onChangeStrokeWidth(strokeWidth) {
      dispatch((0, _strokeWidth.changeStrokeWidth)(strokeWidth));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(LineMode);

exports.default = _default;