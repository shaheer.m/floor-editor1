"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactRedux = require("react-redux");

var _reactIntl = require("react-intl");

var _colorIndex = require("../reducers/color-index");

var _fillStyle = require("../reducers/fill-style");

var _fillModeGradientType = require("../reducers/fill-mode-gradient-type");

var _modals = require("../reducers/modals");

var _selection = require("../helper/selection");

var _selectedItems = require("../reducers/selected-items");

var _modes = _interopRequireWildcard(require("../lib/modes"));

var _format = require("../lib/format");

var _colorIndicator = _interopRequireDefault(require("./color-indicator"));

var messages = (0, _reactIntl.defineMessages)({
  label: {
    id: 'paint.paintEditor.fill',
    description: 'Label for the color picker for the fill color',
    defaultMessage: 'Fill'
  }
});
var FillColorIndicator = (0, _colorIndicator.default)(messages.label, false);

var mapStateToProps = function mapStateToProps(state) {
  return {
    colorIndex: state.scratchPaint.fillMode.colorIndex,
    disabled: state.scratchPaint.mode === _modes.default.LINE,
    color: state.scratchPaint.color.fillColor.primary,
    color2: state.scratchPaint.color.fillColor.secondary,
    colorModalVisible: state.scratchPaint.modals.fillColor,
    fillBitmapShapes: state.scratchPaint.fillBitmapShapes,
    format: state.scratchPaint.format,
    gradientType: state.scratchPaint.color.fillColor.gradientType,
    isEyeDropping: state.scratchPaint.color.eyeDropper.active,
    mode: state.scratchPaint.mode,
    shouldShowGradientTools: state.scratchPaint.mode in _modes.GradientToolsModes,
    textEditTarget: state.scratchPaint.textEditTarget
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChangeColorIndex: function onChangeColorIndex(index) {
      dispatch((0, _colorIndex.changeColorIndex)(index));
    },
    onChangeColor: function onChangeColor(fillColor, index) {
      if (index === 0) {
        dispatch((0, _fillStyle.changeFillColor)(fillColor));
      } else if (index === 1) {
        dispatch((0, _fillStyle.changeFillColor2)(fillColor));
      }
    },
    onOpenColor: function onOpenColor() {
      dispatch((0, _modals.openFillColor)());
    },
    onCloseColor: function onCloseColor() {
      dispatch((0, _modals.closeFillColor)());
    },
    onChangeGradientType: function onChangeGradientType(gradientType) {
      dispatch((0, _fillModeGradientType.changeGradientType)(gradientType));
    },
    setSelectedItems: function setSelectedItems(format) {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FillColorIndicator);

exports.default = _default;