"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _ovalTool = _interopRequireDefault(require("../helper/bit-tools/oval-tool"));

var _bitOvalMode = _interopRequireDefault(require("../components/bit-oval-mode/bit-oval-mode"));

var BitOvalMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitOvalMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitOvalMode);

  function BitOvalMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitOvalMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitOvalMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isOvalModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool) {
        if (nextProps.color !== this.props.color) {
          this.tool.setColor(nextProps.color);
        }

        if (nextProps.selectedItems !== this.props.selectedItems) {
          this.tool.onSelectionChanged(nextProps.selectedItems);
        }

        if (nextProps.filled !== this.props.filled) {
          this.tool.setFilled(nextProps.filled);
        }

        if (nextProps.thickness !== this.props.thickness || nextProps.zoom !== this.props.zoom) {
          this.tool.setThickness(nextProps.thickness);
        }
      }

      if (nextProps.isOvalModeActive && !this.props.isOvalModeActive) {
        this.activateTool();
      } else if (!nextProps.isOvalModeActive && this.props.isOvalModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isOvalModeActive !== this.props.isOvalModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems); // Force the default brush color if fill is MIXED or transparent

      var fillColorPresent = this.props.color.primary !== _stylePath.MIXED && this.props.color.primary !== null;

      if (!fillColorPresent) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
      }

      this.tool = new _ovalTool.default(this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage);
      this.tool.setColor(this.props.color);
      this.tool.setFilled(this.props.filled);
      this.tool.setThickness(this.props.thickness);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitOvalMode.default, {
        isSelected: this.props.isOvalModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitOvalMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    color: state.scratchPaint.color.fillColor,
    filled: state.scratchPaint.fillBitmapShapes,
    isOvalModeActive: state.scratchPaint.mode === _modes.default.BIT_OVAL,
    selectedItems: state.scratchPaint.selectedItems,
    thickness: state.scratchPaint.bitBrushSize,
    zoom: state.scratchPaint.viewBounds.scaling.x
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    setCursor: function setCursor(cursorString) {
      dispatch((0, _cursor.setCursor)(cursorString));
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), true
      /* bitmapMode */
      ));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_OVAL));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitOvalMode);

exports.default = _default;