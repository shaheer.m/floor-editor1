"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../log/log"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _paintEditor = _interopRequireDefault(require("../components/paint-editor/paint-editor"));

var _keyboardShortcutsHoc = _interopRequireDefault(require("../hocs/keyboard-shortcuts-hoc"));

var _selectionHoc = _interopRequireDefault(require("../hocs/selection-hoc"));

var _undoHoc = _interopRequireDefault(require("../hocs/undo-hoc"));

var _updateImageHoc = _interopRequireDefault(require("../hocs/update-image-hoc"));

var _modes = require("../reducers/modes");

var _format = require("../reducers/format");

var _selectedItems = require("../reducers/selected-items");

var _eyeDropper = require("../reducers/eye-dropper");

var _textEditTarget = require("../reducers/text-edit-target");

var _viewBounds = require("../reducers/view-bounds");

var _layout = require("../reducers/layout");

var _selection = require("../helper/selection");

var _bitmap = require("../helper/bitmap");

var _view = require("../helper/view");

var _eyeDropper2 = _interopRequireDefault(require("../helper/tools/eye-dropper"));

var _modes2 = _interopRequireWildcard(require("../lib/modes"));

var _format2 = _interopRequireWildcard(require("../lib/format"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _rasterImg = require("../../scratch-paint/helper/tools/raster-img");

/**
 * The top-level paint editor component. See README for more details on usage.
 *
 * <PaintEditor
 *     image={optionalImage}
 *     imageId={optionalId}
 *     imageFormat='svg'
 *     rotationCenterX={optionalCenterPointX}
 *     rotationCenterY={optionalCenterPointY}
 *     rtl={true|false}
 *     onUpdateImage={handleUpdateImageFunction}
 *     zoomLevelId={optionalZoomLevelId}
 * />
 *
 * `image`: may either be nothing, an SVG string or a base64 data URI)
 * SVGs of up to size 480 x 360 will fit into the view window of the paint editor,
 * while bitmaps of size up to 960 x 720 will fit into the paint editor. One unit
 * of an SVG will appear twice as tall and wide as one unit of a bitmap. This quirky
 * import behavior comes from needing to support legacy projects in Scratch.
 *
 * `imageId`: If this parameter changes, then the paint editor will be cleared, the
 * undo stack reset, and the image re-imported.
 *
 * `imageFormat`: 'svg', 'png', or 'jpg'. Other formats are currently not supported.
 *
 * `rotationCenterX`: x coordinate relative to the top left corner of the sprite of
 * the point that should be centered.
 *
 * `rotationCenterY`: y coordinate relative to the top left corner of the sprite of
 * the point that should be centered.
 *
 * `rtl`: True if the paint editor should be laid out right to left (meant for right
 * to left languages)
 *
 * `onUpdateImage`: A handler called with the new image (either an SVG string or an
 * ImageData) each time the drawing is edited.
 *
 * `zoomLevelId`: All costumes with the same zoom level ID will share the same saved
 * zoom level. When a new zoom level ID is encountered, the paint editor will zoom to
 * fit the current costume comfortably. Leave undefined to perform no zoom to fit.
 */
var PaintEditor = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(PaintEditor, _React$Component);

  var _super = (0, _createSuper2.default)(PaintEditor);

  function PaintEditor(props) {
    var _this;

    (0, _classCallCheck2.default)(this, PaintEditor);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['switchModeForFormat', 'onMouseDown', 'onMouseUp', 'setCanvas', 'setTextArea', 'startEyeDroppingLoop', 'stopEyeDroppingLoop', 'handleSetSelectedItems', 'handleZoomIn', 'handleZoomOut', 'handleZoomReset', 'recalibrateSize', 'setCustomImage']);
    _this.state = {
      canvas: null,
      colorInfo: null
    };

    _this.props.setLayout(_this.props.rtl ? 'rtl' : 'ltr');

    return _this;
  }

  (0, _createClass2.default)(PaintEditor, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      document.addEventListener('keydown', this.props.onKeyPress); // document listeners used to detect if a mouse is down outside of the
      // canvas, and should therefore stop the eye dropper

      document.addEventListener('mousedown', this.onMouseDown);
      document.addEventListener('touchstart', this.onMouseDown);
      document.addEventListener('mouseup', this.onMouseUp);
      document.addEventListener('touchend', this.onMouseUp);
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      if (!(0, _format2.isBitmap)(this.props.format) && (0, _format2.isBitmap)(newProps.format)) {
        this.switchModeForFormat(_format2.default.BITMAP);
      } else if (!(0, _format2.isVector)(this.props.format) && (0, _format2.isVector)(newProps.format)) {
        this.switchModeForFormat(_format2.default.VECTOR);
      }

      if (newProps.rtl !== this.props.rtl) {
        this.props.setLayout(newProps.rtl ? 'rtl' : 'ltr');
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.isEyeDropping && !prevProps.isEyeDropping) {
        this.startEyeDroppingLoop();
      } else if (!this.props.isEyeDropping && prevProps.isEyeDropping) {
        this.stopEyeDroppingLoop();
      } else if (this.props.isEyeDropping && this.props.viewBounds !== prevProps.viewBounds) {
        if (this.props.previousTool) this.props.previousTool.activate();
        this.props.onDeactivateEyeDropper();
        this.stopEyeDroppingLoop();
      }

      if (this.props.format === _format2.default.VECTOR && (0, _format2.isBitmap)(prevProps.format)) {
        (0, _bitmap.convertToVector)(this.props.clearSelectedItems, this.props.onUpdateImage);
      } else if ((0, _format2.isVector)(prevProps.format) && this.props.format === _format2.default.BITMAP) {
        (0, _bitmap.convertToBitmap)(this.props.clearSelectedItems, this.props.onUpdateImage, this.props.fontInlineFn);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('keydown', this.props.onKeyPress);
      this.stopEyeDroppingLoop();
      document.removeEventListener('mousedown', this.onMouseDown);
      document.removeEventListener('touchstart', this.onMouseDown);
      document.removeEventListener('mouseup', this.onMouseUp);
      document.removeEventListener('touchend', this.onMouseUp);
    }
  }, {
    key: "recalibrateSize",
    value: function recalibrateSize(callback) {
      // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
      // When these are out of sync, the mouse events in the paint editor don't line up correctly.
      return window.setTimeout(function () {
        // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
        // This could also be called before paper.view exists.
        // In either case, return early if so without running the callback.
        if (!_paper.default.view) return; // Prevent blurriness caused if the "CSS size" of the element is a float--
        // setting canvas dimensions to floats floors them, but we need to round instead

        var elemSize = _paper.default.DomElement.getSize(_paper.default.view.element);

        elemSize.width = Math.round(elemSize.width);
        elemSize.height = Math.round(elemSize.height);

        _paper.default.view.setViewSize(elemSize);

        if (callback) callback();
      }, 0);
    }
  }, {
    key: "setCustomImage",
    value: function setCustomImage(asset) {
      var _this2 = this;

      var paperCanvas = this;
      (0, _rasterImg.setRasterImage)(paperCanvas, asset);
      setTimeout(function () {
        _this2.props.onUpdateImage();
      }, 100);
    }
  }, {
    key: "switchModeForFormat",
    value: function switchModeForFormat(newFormat) {
      if ((0, _format2.isVector)(newFormat) && this.props.mode in _modes2.VectorModes || (0, _format2.isBitmap)(newFormat) && this.props.mode in _modes2.BitmapModes) {
        // Format didn't change; no mode change needed
        return;
      }

      if ((0, _format2.isVector)(newFormat)) {
        switch (this.props.mode) {
          case _modes2.default.BIT_BRUSH:
            this.props.changeMode(_modes2.default.BRUSH);
            break;

          case _modes2.default.BIT_LINE:
            this.props.changeMode(_modes2.default.LINE);
            break;

          case _modes2.default.BIT_OVAL:
            this.props.changeMode(_modes2.default.OVAL);
            break;

          case _modes2.default.BIT_RECT:
            this.props.changeMode(_modes2.default.RECT);
            break;

          case _modes2.default.BIT_TEXT:
            this.props.changeMode(_modes2.default.TEXT);
            break;

          case _modes2.default.BIT_FILL:
            this.props.changeMode(_modes2.default.FILL);
            break;

          case _modes2.default.BIT_ERASER:
            this.props.changeMode(_modes2.default.ERASER);
            break;

          case _modes2.default.BIT_SELECT:
            this.props.changeMode(_modes2.default.SELECT);
            break;

          default:
            _log.default.error("Mode not handled: ".concat(this.props.mode));

            this.props.changeMode(_modes2.default.BRUSH);
        }
      } else if ((0, _format2.isBitmap)(newFormat)) {
        switch (this.props.mode) {
          case _modes2.default.BRUSH:
            this.props.changeMode(_modes2.default.BIT_BRUSH);
            break;

          case _modes2.default.LINE:
            this.props.changeMode(_modes2.default.BIT_LINE);
            break;

          case _modes2.default.OVAL:
            this.props.changeMode(_modes2.default.BIT_OVAL);
            break;

          case _modes2.default.RECT:
            this.props.changeMode(_modes2.default.BIT_RECT);
            break;

          case _modes2.default.TEXT:
            this.props.changeMode(_modes2.default.BIT_TEXT);
            break;

          case _modes2.default.FILL:
            this.props.changeMode(_modes2.default.BIT_FILL);
            break;

          case _modes2.default.ERASER:
            this.props.changeMode(_modes2.default.BIT_ERASER);
            break;

          case _modes2.default.RESHAPE:
          /* falls through */

          case _modes2.default.SELECT:
            this.props.changeMode(_modes2.default.BIT_SELECT);
            break;

          default:
            _log.default.error("Mode not handled: ".concat(this.props.mode));

            this.props.changeMode(_modes2.default.BIT_BRUSH);
        }
      }
    }
  }, {
    key: "handleZoomIn",
    value: function handleZoomIn() {
      // Make the "next step" after the outermost zoom level be the default
      // zoom level (0.5)
      var zoomIncrement = PaintEditor.ZOOM_INCREMENT;

      if (_paper.default.view.zoom === _view.OUTERMOST_ZOOM_LEVEL) {
        zoomIncrement = 0.5 - _view.OUTERMOST_ZOOM_LEVEL;
      }

      (0, _view.zoomOnSelection)(zoomIncrement);
      this.props.updateViewBounds(_paper.default.view.matrix);
      this.handleSetSelectedItems();
    }
  }, {
    key: "handleZoomOut",
    value: function handleZoomOut() {
      (0, _view.zoomOnSelection)(-PaintEditor.ZOOM_INCREMENT);
      this.props.updateViewBounds(_paper.default.view.matrix);
      this.handleSetSelectedItems();
    }
  }, {
    key: "handleZoomReset",
    value: function handleZoomReset() {
      (0, _view.resetZoom)();
      this.props.updateViewBounds(_paper.default.view.matrix);
      this.handleSetSelectedItems();
    }
  }, {
    key: "handleSetSelectedItems",
    value: function handleSetSelectedItems() {
      this.props.setSelectedItems(this.props.format);
    }
  }, {
    key: "setCanvas",
    value: function setCanvas(canvas) {
      this.setState({
        canvas: canvas
      });
      this.canvas = canvas;
    }
  }, {
    key: "setTextArea",
    value: function setTextArea(element) {
      this.setState({
        textArea: element
      });
    }
  }, {
    key: "onMouseDown",
    value: function onMouseDown(event) {
      if (event.target === _paper.default.view.element && document.activeElement instanceof HTMLInputElement) {
        document.activeElement.blur();
      }

      if (event.target !== _paper.default.view.element && event.target !== this.state.textArea) {
        // Exit text edit mode if you click anywhere outside of canvas
        this.props.removeTextEditTarget();
      }
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp() {
      if (this.props.isEyeDropping) {
        var colorString = this.eyeDropper.colorString;
        var callback = this.props.changeColorToEyeDropper;
        this.eyeDropper.remove();

        if (!this.eyeDropper.hideLoupe) {
          // If not hide loupe, that means the click is inside the canvas,
          // so apply the new color
          callback(colorString);
        }

        if (this.props.previousTool) this.props.previousTool.activate();
        this.props.onDeactivateEyeDropper();
        this.stopEyeDroppingLoop();
      }
    }
  }, {
    key: "startEyeDroppingLoop",
    value: function startEyeDroppingLoop() {
      var _this3 = this;

      this.eyeDropper = new _eyeDropper2.default(this.canvas, _paper.default.project.view.bounds.width, _paper.default.project.view.bounds.height, _paper.default.project.view.pixelRatio, _paper.default.view.zoom, _paper.default.project.view.bounds.x, _paper.default.project.view.bounds.y, (0, _format2.isBitmap)(this.props.format));
      this.eyeDropper.pickX = -1;
      this.eyeDropper.pickY = -1;
      this.eyeDropper.activate();
      this.intervalId = setInterval(function () {
        var colorInfo = _this3.eyeDropper.getColorInfo(_this3.eyeDropper.pickX, _this3.eyeDropper.pickY, _this3.eyeDropper.hideLoupe);

        if (!colorInfo) return;

        if (_this3.state.colorInfo === null || _this3.state.colorInfo.x !== colorInfo.x || _this3.state.colorInfo.y !== colorInfo.y) {
          _this3.setState({
            colorInfo: colorInfo
          });
        }
      }, 30);
    }
  }, {
    key: "stopEyeDroppingLoop",
    value: function stopEyeDroppingLoop() {
      clearInterval(this.intervalId);
      this.setState({
        colorInfo: null
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_paintEditor.default, {
        alertOnTop: this.props.showAlertOnTop,
        onBackButton: this.props.onBackButton,
        sendAlertMessage: this.props.sendAlertMessage,
        onSave: this.props.onSave,
        canRedo: this.props.shouldShowRedo,
        canUndo: this.props.shouldShowUndo,
        canvas: this.state.canvas,
        colorInfo: this.state.colorInfo,
        format: this.props.format,
        image: this.props.image,
        imageFormat: this.props.imageFormat,
        imageId: this.props.imageId,
        isEyeDropping: this.props.isEyeDropping,
        name: this.props.name,
        rotationCenterX: this.props.rotationCenterX,
        rotationCenterY: this.props.rotationCenterY,
        rtl: this.props.rtl,
        setCanvas: this.setCanvas,
        setTextArea: this.setTextArea,
        textArea: this.state.textArea,
        zoomLevelId: this.props.zoomLevelId,
        onRedo: this.props.onRedo,
        onSwitchToBitmap: this.props.handleSwitchToBitmap,
        onSwitchToVector: this.props.handleSwitchToVector,
        onUndo: this.props.onUndo,
        onUpdateImage: this.props.onUpdateImage,
        onUpdateName: this.props.onUpdateName,
        onZoomIn: this.handleZoomIn,
        onZoomOut: this.handleZoomOut,
        onZoomReset: this.handleZoomReset,
        addShape: this.setCustomImage
      });
    }
  }], [{
    key: "ZOOM_INCREMENT",
    get: function get() {
      return 0.5;
    }
  }]);
  return PaintEditor;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    changeColorToEyeDropper: state.scratchPaint.color.eyeDropper.callback,
    format: state.scratchPaint.format,
    isEyeDropping: state.scratchPaint.color.eyeDropper.active,
    mode: state.scratchPaint.mode,
    previousTool: state.scratchPaint.color.eyeDropper.previousTool,
    viewBounds: state.scratchPaint.viewBounds,
    showAlertOnTop: state.scratchPaint.showAlertOnTop
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    changeMode: function changeMode(mode) {
      dispatch((0, _modes.changeMode)(mode));
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    handleSwitchToBitmap: function handleSwitchToBitmap() {
      dispatch((0, _format.changeFormat)(_format2.default.BITMAP));
    },
    handleSwitchToVector: function handleSwitchToVector() {
      dispatch((0, _format.changeFormat)(_format2.default.VECTOR));
    },
    removeTextEditTarget: function removeTextEditTarget() {
      dispatch((0, _textEditTarget.setTextEditTarget)());
    },
    setLayout: function setLayout(layout) {
      dispatch((0, _layout.setLayout)(layout));
    },
    setSelectedItems: function setSelectedItems(format) {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format2.isBitmap)(format)));
    },
    onDeactivateEyeDropper: function onDeactivateEyeDropper() {
      // set redux values to default for eye dropper reducer
      dispatch((0, _eyeDropper.deactivateEyeDropper)());
    },
    updateViewBounds: function updateViewBounds(matrix) {
      dispatch((0, _viewBounds.updateViewBounds)(matrix));
    }
  };
};

var _default = (0, _updateImageHoc.default)((0, _selectionHoc.default)((0, _undoHoc.default)((0, _keyboardShortcutsHoc.default)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PaintEditor)))));

exports.default = _default;