"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _bitEraserMode = _interopRequireDefault(require("../components/bit-eraser-mode/bit-eraser-mode"));

var _brushTool = _interopRequireDefault(require("../helper/bit-tools/brush-tool"));

var BitEraserMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitEraserMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitEraserMode);

  function BitEraserMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitEraserMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitEraserMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isBitEraserModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.bitEraserSize !== this.props.bitEraserSize) {
        this.tool.setBrushSize(nextProps.bitEraserSize);
      }

      if (nextProps.isBitEraserModeActive && !this.props.isBitEraserModeActive) {
        this.activateTool();
      } else if (!nextProps.isBitEraserModeActive && this.props.isBitEraserModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isBitEraserModeActive !== this.props.isBitEraserModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.tool = new _brushTool.default(this.props.onUpdateImage, true
      /* isEraser */
      );
      this.tool.setBrushSize(this.props.bitEraserSize);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitEraserMode.default, {
        isSelected: this.props.isBitEraserModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitEraserMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    bitEraserSize: state.scratchPaint.bitEraserSize,
    isBitEraserModeActive: state.scratchPaint.mode === _modes.default.BIT_ERASER
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_ERASER));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitEraserMode);

exports.default = _default;