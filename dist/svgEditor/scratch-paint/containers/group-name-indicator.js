"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _reactRedux = require("react-redux");

var _react = _interopRequireWildcard(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _strokeStyle = require("../reducers/stroke-style");

var _strokeWidth = require("../reducers/stroke-width");

var _itemNameIndicator = _interopRequireDefault(require("../components/item-name-indicator/item-name-indicator"));

var _selection = require("../helper/selection");

var _stylePath = require("../helper/style-path");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _format = _interopRequireWildcard(require("../lib/format"));

var _reactPopover = _interopRequireDefault(require("react-popover"));

var _group = require("../helper/group");

var _labeledIconButton = _interopRequireDefault(require("../components/labeled-icon-button/labeled-icon-button"));

var _group2 = _interopRequireDefault(require("../components/fixed-tools/icons/group.svg"));

var _multiselectDropdown = _interopRequireDefault(require("../components/forms/multiselect-dropdown"));

var _groupNameSelect = _interopRequireDefault(require("../components/forms/group-name-select"));

var ItemNameIndicator = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ItemNameIndicator, _React$Component);

  var _super = (0, _createSuper2.default)(ItemNameIndicator);

  function ItemNameIndicator(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ItemNameIndicator);
    _this = _super.call(this, props);
    _this.state = {
      modalStatus: false,
      groupName: null
    };
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleAttributeModal', 'handleSubmit', 'groupNameChange']);
    return _this;
  }

  (0, _createClass2.default)(ItemNameIndicator, [{
    key: "groupNameChange",
    value: function groupNameChange(value) {
      this.setState({
        groupName: value
      });
    }
  }, {
    key: "handleAttributeModal",
    value: function handleAttributeModal() {
      this.setState(function (prevState) {
        return {
          modalStatus: !prevState.modalStatus
        };
      });
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit() {
      var groupName = this.state.groupName ? this.state.groupName : null;
      var changedGroupName = (0, _stylePath.applyGroupNameToSelection)(groupName, this.props.textEditTarget);
      if (changedGroupName) this.props.onUpdateImage();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_reactPopover.default, {
        body: /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement(_groupNameSelect.default, {
          groupName: this.props.groupName,
          onGroupNameChange: this.groupNameChange
        }), /*#__PURE__*/_react.default.createElement("button", {
          onClick: this.handleSubmit
        }, "Update group name")),
        isOpen: this.state.modalStatus,
        preferPlace: "below",
        onOuterAction: this.handleAttributeModal
      }, /*#__PURE__*/_react.default.createElement(_labeledIconButton.default, {
        disabled: !(0, _group.selectedAtleastOne)(),
        hideLabel: false,
        imgSrc: _group2.default,
        title: 'change group name',
        onClick: this.handleAttributeModal
      }));
    }
  }]);
  return ItemNameIndicator;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  var groupName;
  var itemGroupNames;

  if (state.scratchPaint.selectedItems && state.scratchPaint.selectedItems[0]) {
    // show only if all the selected items are of same groupName
    itemGroupNames = (0, _selection.groupNameStackOfAllItems)(state.scratchPaint.selectedItems);
    groupName = state.scratchPaint.selectedItems[0]._data.groupName ? state.scratchPaint.selectedItems[0]._data.groupName : null;
  }

  return {
    disabled: state.scratchPaint.mode === _modes.default.BRUSH || state.scratchPaint.mode === _modes.default.TEXT || state.scratchPaint.mode === _modes.default.FILL,
    textEditTarget: state.scratchPaint.textEditTarget,
    groupName: groupName
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {};
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ItemNameIndicator);

exports.default = _default;