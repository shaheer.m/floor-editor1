"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _bitBrushMode = _interopRequireDefault(require("../components/bit-brush-mode/bit-brush-mode"));

var _brushTool = _interopRequireDefault(require("../helper/bit-tools/brush-tool"));

var BitBrushMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitBrushMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitBrushMode);

  function BitBrushMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitBrushMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitBrushMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isBitBrushModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.color !== this.props.color) {
        this.tool.setColor(nextProps.color);
      }

      if (this.tool && nextProps.bitBrushSize !== this.props.bitBrushSize) {
        this.tool.setBrushSize(nextProps.bitBrushSize);
      }

      if (nextProps.isBitBrushModeActive && !this.props.isBitBrushModeActive) {
        this.activateTool();
      } else if (!nextProps.isBitBrushModeActive && this.props.isBitBrushModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isBitBrushModeActive !== this.props.isBitBrushModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.props.clearGradient(); // Force the default brush color if fill is MIXED or transparent

      var color = this.props.color;

      if (!color || color === _stylePath.MIXED) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
        color = _fillStyle.DEFAULT_COLOR;
      }

      this.tool = new _brushTool.default(this.props.onUpdateImage);
      this.tool.setColor(color);
      this.tool.setBrushSize(this.props.bitBrushSize);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitBrushMode.default, {
        isSelected: this.props.isBitBrushModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitBrushMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    bitBrushSize: state.scratchPaint.bitBrushSize,
    color: state.scratchPaint.color.fillColor.primary,
    isBitBrushModeActive: state.scratchPaint.mode === _modes.default.BIT_BRUSH
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearGradient: function clearGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_BRUSH));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitBrushMode);

exports.default = _default;