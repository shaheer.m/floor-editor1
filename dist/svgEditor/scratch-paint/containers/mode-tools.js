"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _copyPasteHoc = _interopRequireDefault(require("../hocs/copy-paste-hoc"));

var _modeTools = _interopRequireDefault(require("../components/mode-tools/mode-tools"));

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _math = require("../helper/math");

var _layer = require("../helper/layer");

var _bitmap = require("../helper/bitmap");

var _format = _interopRequireWildcard(require("../lib/format"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var ModeTools = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ModeTools, _React$Component);

  var _super = (0, _createSuper2.default)(ModeTools);

  function ModeTools(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ModeTools);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['_getSelectedUncurvedPoints', '_getSelectedUnpointedPoints', 'hasSelectedUncurvedPoints', 'hasSelectedUnpointedPoints', 'handleCurvePoints', 'handleFlipHorizontal', 'handleFlipVertical', 'handleDelete', 'handlePasteFromClipboard', 'handlePointPoints', 'copyAndPaste']);
    return _this;
  }

  (0, _createClass2.default)(ModeTools, [{
    key: "_getSelectedUncurvedPoints",
    value: function _getSelectedUncurvedPoints() {
      var items = [];
      var selectedItems = (0, _selection.getSelectedLeafItems)();

      var _iterator = (0, _createForOfIteratorHelper2.default)(selectedItems),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;
          if (!item.segments) continue;

          var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.segments),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var seg = _step2.value;

              if (seg.selected) {
                var prev = seg.getPrevious();
                var next = seg.getNext();
                var isCurved = (!prev || seg.handleIn.length > 0) && (!next || seg.handleOut.length > 0) && (prev && next ? seg.handleOut.isColinear(seg.handleIn) : true);
                if (!isCurved) items.push(seg);
              }
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return items;
    }
  }, {
    key: "_getSelectedUnpointedPoints",
    value: function _getSelectedUnpointedPoints() {
      var points = [];
      var selectedItems = (0, _selection.getSelectedLeafItems)();

      var _iterator3 = (0, _createForOfIteratorHelper2.default)(selectedItems),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var item = _step3.value;
          if (!item.segments) continue;

          var _iterator4 = (0, _createForOfIteratorHelper2.default)(item.segments),
              _step4;

          try {
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              var seg = _step4.value;

              if (seg.selected) {
                if (seg.handleIn.length > 0 || seg.handleOut.length > 0) {
                  points.push(seg);
                }
              }
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      return points;
    }
  }, {
    key: "hasSelectedUncurvedPoints",
    value: function hasSelectedUncurvedPoints() {
      var points = this._getSelectedUncurvedPoints();

      return points.length > 0;
    }
  }, {
    key: "hasSelectedUnpointedPoints",
    value: function hasSelectedUnpointedPoints() {
      var points = this._getSelectedUnpointedPoints();

      return points.length > 0;
    }
  }, {
    key: "handleCurvePoints",
    value: function handleCurvePoints() {
      var changed;

      var points = this._getSelectedUncurvedPoints();

      var _iterator5 = (0, _createForOfIteratorHelper2.default)(points),
          _step5;

      try {
        for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
          var point = _step5.value;
          var prev = point.getPrevious();
          var next = point.getNext();
          var noHandles = point.handleIn.length === 0 && point.handleOut.length === 0;

          if (!prev && !next) {
            continue;
          } else if (prev && next && noHandles) {
            // Handles are parallel to the line from prev to next
            point.handleIn = prev.point.subtract(next.point).normalize().multiply(prev.getCurve().length * _math.HANDLE_RATIO);
          } else if (prev && !next && point.handleIn.length === 0) {
            // Point is end point
            // Direction is average of normal at the point and direction to prev point, using the
            // normal that points out from the convex side
            // Lenth is curve length * HANDLE_RATIO
            var convexity = prev.getCurve().getCurvatureAtTime(.5) < 0 ? -1 : 1;
            point.handleIn = prev.getCurve().getNormalAtTime(1).multiply(convexity).add(prev.point.subtract(point.point).normalize()).normalize().multiply(prev.getCurve().length * _math.HANDLE_RATIO);
          } else if (next && !prev && point.handleOut.length === 0) {
            // Point is start point
            // Direction is average of normal at the point and direction to prev point, using the
            // normal that points out from the convex side
            // Lenth is curve length * HANDLE_RATIO
            var _convexity = point.getCurve().getCurvatureAtTime(.5) < 0 ? -1 : 1;

            point.handleOut = point.getCurve().getNormalAtTime(0).multiply(_convexity).add(next.point.subtract(point.point).normalize()).normalize().multiply(point.getCurve().length * _math.HANDLE_RATIO);
          } // Point guaranteed to have a handle now. Make the second handle match the length and direction of first.
          // This defines a curved point.


          if (point.handleIn.length > 0 && next) {
            point.handleOut = point.handleIn.multiply(-1);
          } else if (point.handleOut.length > 0 && prev) {
            point.handleIn = point.handleOut.multiply(-1);
          }

          changed = true;
        }
      } catch (err) {
        _iterator5.e(err);
      } finally {
        _iterator5.f();
      }

      if (changed) {
        this.props.setSelectedItems(this.props.format);
        this.props.onUpdateImage();
      }
    }
  }, {
    key: "handlePointPoints",
    value: function handlePointPoints() {
      var changed;

      var points = this._getSelectedUnpointedPoints();

      var _iterator6 = (0, _createForOfIteratorHelper2.default)(points),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var point = _step6.value;
          var noHandles = point.handleIn.length === 0 && point.handleOut.length === 0;

          if (!noHandles) {
            point.handleIn = null;
            point.handleOut = null;
            changed = true;
          }
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }

      if (changed) {
        this.props.setSelectedItems(this.props.format);
        this.props.onUpdateImage();
      }
    }
  }, {
    key: "_handleFlip",
    value: function _handleFlip(horizontalScale, verticalScale, selectedItems) {
      if (selectedItems.length === 0) {
        // If nothing is selected, select everything
        selectedItems = (0, _selection.getAllRootItems)();
      } // Record old indices


      var _iterator7 = (0, _createForOfIteratorHelper2.default)(selectedItems),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var item = _step7.value;
          item.data.index = item.index;
        } // Group items so that they flip as a unit

      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }

      var itemGroup = new _paper.default.Group(selectedItems); // Flip

      itemGroup.scale(horizontalScale, verticalScale);
      (0, _math.ensureClockwise)(itemGroup); // Remove flipped item from group and insert at old index. Must insert from bottom index up.

      for (var i = 0; i < selectedItems.length; i++) {
        itemGroup.layer.insertChild(selectedItems[i].data.index, selectedItems[i]);
        selectedItems[i].data.index = null;
      }

      itemGroup.remove();
      this.props.onUpdateImage();
    }
  }, {
    key: "handleFlipHorizontal",
    value: function handleFlipHorizontal() {
      var selectedItems = (0, _selection.getSelectedRootItems)();

      if ((0, _format.isBitmap)(this.props.format) && selectedItems.length === 0) {
        (0, _layer.getRaster)().canvas = (0, _bitmap.flipBitmapHorizontal)((0, _layer.getRaster)().canvas);
        this.props.onUpdateImage();
      } else {
        this._handleFlip(-1, 1, selectedItems);
      }
    }
  }, {
    key: "handleFlipVertical",
    value: function handleFlipVertical() {
      var selectedItems = (0, _selection.getSelectedRootItems)();

      if ((0, _format.isBitmap)(this.props.format) && selectedItems.length === 0) {
        (0, _layer.getRaster)().canvas = (0, _bitmap.flipBitmapVertical)((0, _layer.getRaster)().canvas);
        this.props.onUpdateImage();
      } else {
        this._handleFlip(1, -1, selectedItems);
      }
    }
  }, {
    key: "handlePasteFromClipboard",
    value: function handlePasteFromClipboard() {
      if (this.props.onPasteFromClipboard()) {
        this.props.onUpdateImage();
      }
    }
  }, {
    key: "handleDelete",
    value: function handleDelete() {
      if (!this.props.selectedItems.length) {
        if ((0, _format.isBitmap)(this.props.format)) {
          (0, _bitmap.selectAllBitmap)(this.props.clearSelectedItems);
        } else if (this.props.mode === _modes.default.RESHAPE) {
          (0, _selection.selectAllSegments)();
        } else {
          (0, _selection.selectAllItems)();
        }
      }

      if ((0, _selection.deleteSelection)(this.props.mode, this.props.onUpdateImage)) {
        this.props.setSelectedItems(this.props.format);
      }
    }
  }, {
    key: "copyAndPaste",
    value: function copyAndPaste() {
      var _this2 = this;

      var copied = this.props.onCopyToClipboard();
      setTimeout(function () {
        if (copied) {
          _this2.handlePasteFromClipboard();
        }
      }, 100);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_modeTools.default, {
        hasSelectedUncurvedPoints: this.hasSelectedUncurvedPoints(),
        hasSelectedUnpointedPoints: this.hasSelectedUnpointedPoints(),
        onCopyToClipboard: this.copyAndPaste,
        onCurvePoints: this.handleCurvePoints,
        onDelete: this.handleDelete,
        onFlipHorizontal: this.handleFlipHorizontal,
        onFlipVertical: this.handleFlipVertical,
        onPasteFromClipboard: this.handlePasteFromClipboard,
        onPointPoints: this.handlePointPoints,
        onUpdateImage: this.props.onUpdateImage
      });
    }
  }]);
  return ModeTools;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    format: state.scratchPaint.format,
    mode: state.scratchPaint.mode,
    selectedItems: state.scratchPaint.selectedItems
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    setSelectedItems: function setSelectedItems(format) {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
    }
  };
};

var _default = (0, _copyPasteHoc.default)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ModeTools));

exports.default = _default;