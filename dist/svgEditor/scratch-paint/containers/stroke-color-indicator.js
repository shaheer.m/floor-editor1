"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactRedux = require("react-redux");

var _reactIntl = require("react-intl");

var _colorIndex = require("../reducers/color-index");

var _strokeStyle = require("../reducers/stroke-style");

var _strokeWidth = require("../reducers/stroke-width");

var _modals = require("../reducers/modals");

var _selection = require("../helper/selection");

var _selectedItems = require("../reducers/selected-items");

var _modes = _interopRequireWildcard(require("../lib/modes"));

var _format = require("../lib/format");

var _colorIndicator = _interopRequireDefault(require("./color-indicator"));

var messages = (0, _reactIntl.defineMessages)({
  label: {
    id: 'paint.paintEditor.stroke',
    description: 'Label for the color picker for the outline color',
    defaultMessage: 'Outline'
  }
});
var StrokeColorIndicator = (0, _colorIndicator.default)(messages.label, true);

var mapStateToProps = function mapStateToProps(state) {
  return {
    colorIndex: state.scratchPaint.fillMode.colorIndex,
    disabled: state.scratchPaint.mode === _modes.default.BRUSH || state.scratchPaint.mode === _modes.default.TEXT || state.scratchPaint.mode === _modes.default.FILL,
    color: state.scratchPaint.color.strokeColor.primary,
    color2: state.scratchPaint.color.strokeColor.secondary,
    fillBitmapShapes: state.scratchPaint.fillBitmapShapes,
    colorModalVisible: state.scratchPaint.modals.strokeColor,
    format: state.scratchPaint.format,
    gradientType: state.scratchPaint.color.strokeColor.gradientType,
    isEyeDropping: state.scratchPaint.color.eyeDropper.active,
    mode: state.scratchPaint.mode,
    shouldShowGradientTools: state.scratchPaint.mode in _modes.GradientToolsModes,
    textEditTarget: state.scratchPaint.textEditTarget
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChangeColorIndex: function onChangeColorIndex(index) {
      dispatch((0, _colorIndex.changeColorIndex)(index));
    },
    onChangeColor: function onChangeColor(strokeColor, index) {
      if (index === 0) {
        dispatch((0, _strokeStyle.changeStrokeColor)(strokeColor));
      } else if (index === 1) {
        dispatch((0, _strokeStyle.changeStrokeColor2)(strokeColor));
      }
    },
    onChangeStrokeWidth: function onChangeStrokeWidth(strokeWidth) {
      dispatch((0, _strokeWidth.changeStrokeWidth)(strokeWidth));
    },
    onOpenColor: function onOpenColor() {
      dispatch((0, _modals.openStrokeColor)());
    },
    onCloseColor: function onCloseColor() {
      dispatch((0, _modals.closeStrokeColor)());
    },
    onChangeGradientType: function onChangeGradientType(gradientType) {
      dispatch((0, _strokeStyle.changeStrokeGradientType)(gradientType));
    },
    setSelectedItems: function setSelectedItems(format) {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(format)));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(StrokeColorIndicator);

exports.default = _default;