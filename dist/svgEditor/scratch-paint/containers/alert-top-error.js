"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _bitEraserMode = _interopRequireDefault(require("../components/bit-eraser-mode/bit-eraser-mode"));

var _brushTool = _interopRequireDefault(require("../helper/bit-tools/brush-tool"));

var _alert = _interopRequireDefault(require("../components/alert/alert"));

var _alertTopError = require("../reducers/alert-top-error");

var AlertTopError = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(AlertTopError, _React$Component);

  var _super = (0, _createSuper2.default)(AlertTopError);

  function AlertTopError(props) {
    var _this;

    (0, _classCallCheck2.default)(this, AlertTopError);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['showAlert']);
    return _this;
  }

  (0, _createClass2.default)(AlertTopError, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      setTimeout(function () {
        _this2.props.removeAlert();
      }, 5000);
    }
  }, {
    key: "showAlert",
    value: function showAlert() {}
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_alert.default, {
        className: "yellowAlert",
        severity: "error"
      }, this.props.alertMessage);
    }
  }]);
  return AlertTopError;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    alertMessage: state.scratchPaint.showAlertOnTop.msg
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    removeAlert: function removeAlert() {
      dispatch((0, _alertTopError.hideAlertText)());
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(AlertTopError);

exports.default = _default;