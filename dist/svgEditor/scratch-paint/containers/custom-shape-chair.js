"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _chairIcon = _interopRequireDefault(require("../../../assets/chair-icon-1.svg"));

var _chairIcon2 = _interopRequireDefault(require("../../../assets/chair-icon-2.svg"));

var _chairIcon3 = _interopRequireDefault(require("../../../assets/chair-icon-3.svg"));

var _customShape = _interopRequireDefault(require("../components/custom-shape"));

var _fill = _interopRequireDefault(require("../components/fill-mode/fill.svg"));

var _toolSelectBase = _interopRequireDefault(require("../components/tool-select-base/tool-select-base"));

var _reactRedux = require("react-redux");

var _messages = _interopRequireDefault(require("../lib/messages.js"));

// import chairIcon from '../../../assets/chair.svg';
var CustomShapeComponent = function CustomShapeComponent(props) {
  return /*#__PURE__*/_react.default.createElement(_customShape.default, {
    imgDescriptor: _messages.default.seat,
    imgSrc: [_chairIcon.default, _chairIcon2.default, _chairIcon3.default],
    title: [_messages.default.front, _messages.default.top, _messages.default.side],
    customShape: 'chair',
    addShape: props.addShape // isSelected={props.isSelected}
    //  onMouseDown={props.onMouseDown}

  });
}; //check comment
// const mapStateToProps = state => ({
//     colorModalVisible: state.scratchPaint.modals.fillColor,
// });


var _default = CustomShapeComponent; //check comment
// export default connect(
//     mapStateToProps
// )(CustomShapeComponent);

exports.default = _default;