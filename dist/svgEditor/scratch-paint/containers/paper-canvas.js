"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _format = _interopRequireDefault(require("../lib/format"));

var _log = _interopRequireDefault(require("../log/log"));

var _undo = require("../helper/undo");

var _undo2 = require("../reducers/undo");

var _group = require("../helper/group");

var _layer = require("../helper/layer");

var _selectedItems = require("../reducers/selected-items");

var _view = require("../helper/view");

var _math = require("../helper/math");

var _hover = require("../reducers/hover");

var _clipboard = require("../reducers/clipboard");

var _format2 = require("../reducers/format");

var _viewBounds = require("../reducers/view-bounds");

var _zoomLevels = require("../reducers/zoom-levels");

var _paperCanvasModule = _interopRequireDefault(require("./paper-canvas.module.css"));

var PaperCanvas = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(PaperCanvas, _React$Component);

  var _super = (0, _createSuper2.default)(PaperCanvas);

  function PaperCanvas(props) {
    var _this;

    (0, _classCallCheck2.default)(this, PaperCanvas);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['clearQueuedImport', 'setCanvas', 'importSvg', 'initializeSvg', 'maybeZoomToFit', 'switchCostume', 'onViewResize', 'recalibrateSize']);
    return _this;
  }

  (0, _createClass2.default)(PaperCanvas, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _paper.default.setup(this.canvas);

      _paper.default.view.on('resize', this.onViewResize);

      (0, _view.resetZoom)();

      if (this.props.zoomLevelId) {
        this.props.setZoomLevelId(this.props.zoomLevelId);

        if (this.props.zoomLevels[this.props.zoomLevelId]) {
          // This is the matrix that the view should be zoomed to after image import
          this.shouldZoomToFit = this.props.zoomLevels[this.props.zoomLevelId];
        } else {
          // Zoom to fit true means find a comfortable zoom level for viewing the costume
          this.shouldZoomToFit = true;
        }
      } else {
        this.props.updateViewBounds(_paper.default.view.matrix);
      }

      var context = this.canvas.getContext('2d');
      context.webkitImageSmoothingEnabled = false;
      context.imageSmoothingEnabled = false; // Don't show handles by default

      _paper.default.settings.handleSize = 0; // Make layers.

      (0, _layer.setupLayers)(this.props.format);
      this.importImage(this.props.imageFormat, this.props.image, this.props.rotationCenterX, this.props.rotationCenterY);
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      if (this.props.imageId !== newProps.imageId) {
        this.switchCostume(newProps.imageFormat, newProps.image, newProps.rotationCenterX, newProps.rotationCenterY, this.props.zoomLevelId, newProps.zoomLevelId);
      }

      if (this.props.format !== newProps.format) {
        this.recalibrateSize();
        (0, _layer.convertBackgroundGuideLayer)(newProps.format);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.clearQueuedImport(); // shouldZoomToFit means the zoom level hasn't been initialized yet

      if (!this.shouldZoomToFit) {
        this.props.saveZoomLevel();
      }

      _paper.default.remove();
    }
  }, {
    key: "clearQueuedImport",
    value: function clearQueuedImport() {
      if (this.queuedImport) {
        window.clearTimeout(this.queuedImport);
        this.queuedImport = null;
      }

      if (this.queuedImageToLoad) {
        this.queuedImageToLoad.src = '';
        this.queuedImageToLoad.onload = null;
        this.queuedImageToLoad = null;
      }
    }
  }, {
    key: "switchCostume",
    value: function switchCostume(format, image, rotationCenterX, rotationCenterY, oldZoomLevelId, newZoomLevelId) {
      if (oldZoomLevelId && oldZoomLevelId !== newZoomLevelId) {
        this.props.saveZoomLevel();
      }

      if (newZoomLevelId && oldZoomLevelId !== newZoomLevelId) {
        if (this.props.zoomLevels[newZoomLevelId]) {
          this.shouldZoomToFit = this.props.zoomLevels[newZoomLevelId];
        } else {
          this.shouldZoomToFit = true;
        }

        this.props.setZoomLevelId(newZoomLevelId);
      }

      var _iterator = (0, _createForOfIteratorHelper2.default)(_paper.default.project.layers),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var layer = _step.value;

          if (layer.data.isRasterLayer) {
            (0, _layer.clearRaster)();
          } else if (!layer.data.isBackgroundGuideLayer && !layer.data.isDragCrosshairLayer && !layer.data.isOutlineLayer) {
            layer.removeChildren();
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this.props.clearUndo();
      this.props.clearSelectedItems();
      this.props.clearHoveredItem();
      this.props.clearPasteOffset();
      this.importImage(format, image, rotationCenterX, rotationCenterY);
    }
  }, {
    key: "importImage",
    value: function importImage(format, image, rotationCenterX, rotationCenterY) {
      var _this2 = this;

      // Stop any in-progress imports
      this.clearQueuedImport();

      if (!image) {
        this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
        (0, _undo.performSnapshot)(this.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
        this.recalibrateSize();
        return;
      }

      if (format === 'jpg' || format === 'png') {
        // import bitmap
        this.props.changeFormat(_format.default.BITMAP_SKIP_CONVERT);
        var mask = new _paper.default.Shape.Rectangle((0, _layer.getRaster)().getBounds());
        mask.guide = true;
        mask.locked = true;
        mask.setPosition(_view.CENTER);
        mask.clipMask = true;
        var imgElement = new Image();
        this.queuedImageToLoad = imgElement;

        imgElement.onload = function () {
          if (!_this2.queuedImageToLoad) return;
          _this2.queuedImageToLoad = null;

          if (typeof rotationCenterX === 'undefined') {
            rotationCenterX = imgElement.width / 2;
          }

          if (typeof rotationCenterY === 'undefined') {
            rotationCenterY = imgElement.height / 2;
          }

          (0, _layer.getRaster)().drawImage(imgElement, _view.ART_BOARD_WIDTH / 2 - rotationCenterX, _view.ART_BOARD_HEIGHT / 2 - rotationCenterY);
          (0, _layer.getRaster)().drawImage(imgElement, _view.ART_BOARD_WIDTH / 2 - rotationCenterX, _view.ART_BOARD_HEIGHT / 2 - rotationCenterY);

          _this2.maybeZoomToFit(true
          /* isBitmap */
          );

          (0, _undo.performSnapshot)(_this2.props.undoSnapshot, _format.default.BITMAP_SKIP_CONVERT);

          _this2.recalibrateSize();
        };

        imgElement.src = image;
      } else if (format === 'svg') {
        this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
        this.importSvg(image, rotationCenterX, rotationCenterY);
      } else {
        _log.default.error("Didn't recognize format: ".concat(format, ". Use 'jpg', 'png' or 'svg'."));

        this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
        (0, _undo.performSnapshot)(this.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
        this.recalibrateSize();
      }
    }
  }, {
    key: "maybeZoomToFit",
    value: function maybeZoomToFit(isBitmapMode) {
      if (this.shouldZoomToFit instanceof _paper.default.Matrix) {
        _paper.default.view.matrix = this.shouldZoomToFit;
        this.props.updateViewBounds(_paper.default.view.matrix);
        (0, _view.resizeCrosshair)();
      } else if (this.shouldZoomToFit === true) {
        (0, _view.zoomToFit)(isBitmapMode);
      }

      this.shouldZoomToFit = false;
      (0, _view.setWorkspaceBounds)();
      this.props.updateViewBounds(_paper.default.view.matrix);
    }
  }, {
    key: "importSvg",
    value: function importSvg(svg, rotationCenterX, rotationCenterY) {
      var paperCanvas = this; // Pre-process SVG to prevent parsing errors (discussion from #213)
      // 1. Remove svg: namespace on elements.
      // TODO: remove

      svg = svg.split(/<\s*svg:/).join('<');
      svg = svg.split(/<\/\s*svg:/).join('</'); // 2. Add root svg namespace if it does not exist.

      var svgAttrs = svg.match(/<svg [^>]*>/);

      if (svgAttrs && svgAttrs[0].indexOf('xmlns=') === -1) {
        svg = svg.replace('<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ');
      } // Get the origin which the viewBox is defined relative to. During import, Paper will translate
      // the viewBox to start at (0, 0), and we need to translate it back for some costumes to render
      // correctly.


      var parser = new DOMParser();
      var svgDom = parser.parseFromString(svg, 'text/xml');
      var viewBox = svgDom.documentElement.attributes.viewBox ? svgDom.documentElement.attributes.viewBox.value.match(/\S+/g) : null;

      if (viewBox) {
        for (var i = 0; i < viewBox.length; i++) {
          viewBox[i] = parseFloat(viewBox[i]);
        }
      }

      _paper.default.project.importSVG(svg, {
        expandShapes: true,
        onLoad: function onLoad(item) {
          if (!item) {
            _log.default.error('SVG import failed:');

            _log.default.info(svg);

            this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
            (0, _undo.performSnapshot)(paperCanvas.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
            return;
          }

          item.remove(); // Without the callback, rasters' load function has not been called yet, and they are
          // positioned incorrectly

          paperCanvas.queuedImport = paperCanvas.recalibrateSize(function () {
            paperCanvas.props.updateViewBounds(_paper.default.view.matrix);
            paperCanvas.initializeSvg(item, rotationCenterX, rotationCenterY, viewBox);
          });
        }
      });
    }
  }, {
    key: "initializeSvg",
    value: function initializeSvg(item, rotationCenterX, rotationCenterY, viewBox) {
      if (this.queuedImport) this.queuedImport = null;
      var itemWidth = item.bounds.width;
      var itemHeight = item.bounds.height; // Get reference to viewbox

      var mask;

      if (item.clipped) {
        var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.children),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var child = _step2.value;

            if (child.isClipMask()) {
              mask = child;
              break;
            }
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }

        mask.clipMask = false;
      } else {
        mask = new _paper.default.Shape.Rectangle(item.bounds);
      }

      mask.guide = true;
      mask.locked = true;
      mask.matrix = new _paper.default.Matrix(); // Identity
      // Set the artwork to get clipped at the max costume size

      mask.size.height = _view.MAX_WORKSPACE_BOUNDS.height;
      mask.size.width = _view.MAX_WORKSPACE_BOUNDS.width;
      mask.setPosition(_view.CENTER);

      _paper.default.project.activeLayer.addChild(mask);

      mask.clipMask = true; // Reduce single item nested in groups

      if (item instanceof _paper.default.Group && item.children.length === 1) {
        item = item.reduce();
      }

      (0, _math.ensureClockwise)(item);
      (0, _math.scaleWithStrokes)(item, 2, new _paper.default.Point()); // Import at 2x
      // Apply rotation center

      if (typeof rotationCenterX !== 'undefined' && typeof rotationCenterY !== 'undefined') {
        var rotationPoint = new _paper.default.Point(rotationCenterX, rotationCenterY);

        if (viewBox && viewBox.length >= 2 && !isNaN(viewBox[0]) && !isNaN(viewBox[1])) {
          rotationPoint = rotationPoint.subtract(viewBox[0], viewBox[1]);
        }

        item.translate(_view.CENTER.subtract(rotationPoint.multiply(2)));
      } else {
        // Center
        item.translate(_view.CENTER.subtract(itemWidth, itemHeight));
      }

      _paper.default.project.activeLayer.insertChild(0, item);

      if ((0, _group.isGroup)(item)) {
        // Fixes an issue where we may export empty groups
        var _iterator3 = (0, _createForOfIteratorHelper2.default)(item.children),
            _step3;

        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var _child = _step3.value;

            if ((0, _group.isGroup)(_child) && _child.children.length === 0) {
              _child.remove();
            }
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }

        (0, _group.ungroupItems)([item]);
      }

      (0, _undo.performSnapshot)(this.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
      this.maybeZoomToFit();
    }
  }, {
    key: "onViewResize",
    value: function onViewResize() {
      (0, _view.setWorkspaceBounds)(true
      /* clipEmpty */
      );
      (0, _view.clampViewBounds)(); // Fix incorrect paper canvas scale on browser zoom reset

      this.recalibrateSize();
      this.props.updateViewBounds(_paper.default.view.matrix);
    }
  }, {
    key: "recalibrateSize",
    value: function recalibrateSize(callback) {
      // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
      // When these are out of sync, the mouse events in the paint editor don't line up correctly.
      return window.setTimeout(function () {
        // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
        // This could also be called before paper.view exists.
        // In either case, return early if so without running the callback.
        if (!_paper.default.view) return; // Prevent blurriness caused if the "CSS size" of the element is a float--
        // setting canvas dimensions to floats floors them, but we need to round instead

        var elemSize = _paper.default.DomElement.getSize(_paper.default.view.element);

        elemSize.width = Math.round(elemSize.width);
        elemSize.height = Math.round(elemSize.height);

        _paper.default.view.setViewSize(elemSize);

        if (callback) callback();
      }, 0);
    }
  }, {
    key: "setCanvas",
    value: function setCanvas(canvas) {
      this.canvas = canvas;

      if (this.props.canvasRef) {
        this.props.canvasRef(canvas);
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("canvas", {
        className: _paperCanvasModule.default.paperCanvas,
        ref: this.setCanvas,
        style: {
          cursor: this.props.cursor
        },
        resize: "true"
      });
    }
  }]);
  return PaperCanvas;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    mode: state.scratchPaint.mode,
    cursor: state.scratchPaint.cursor,
    format: state.scratchPaint.format,
    zoomLevels: state.scratchPaint.zoomLevels
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    undoSnapshot: function undoSnapshot(snapshot) {
      dispatch((0, _undo2.undoSnapshot)(snapshot));
    },
    clearUndo: function clearUndo() {
      dispatch((0, _undo2.clearUndoState)());
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearHoveredItem: function clearHoveredItem() {
      dispatch((0, _hover.clearHoveredItem)());
    },
    clearPasteOffset: function clearPasteOffset() {
      dispatch((0, _clipboard.clearPasteOffset)());
    },
    changeFormat: function changeFormat(format) {
      dispatch((0, _format2.changeFormat)(format));
    },
    saveZoomLevel: function saveZoomLevel() {
      dispatch((0, _zoomLevels.saveZoomLevel)(_paper.default.view.matrix));
    },
    setZoomLevelId: function setZoomLevelId(zoomLevelId) {
      dispatch((0, _zoomLevels.setZoomLevelId)(zoomLevelId));
    },
    updateViewBounds: function updateViewBounds(matrix) {
      dispatch((0, _viewBounds.updateViewBounds)(matrix));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PaperCanvas);

exports.default = _default;