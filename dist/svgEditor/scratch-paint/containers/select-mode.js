"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _modes2 = require("../reducers/modes");

var _hover = require("../reducers/hover");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _selectTool = _interopRequireDefault(require("../helper/selection-tools/select-tool"));

var _selectMode = _interopRequireDefault(require("../components/select-mode/select-mode"));

var SelectMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(SelectMode, _React$Component);

  var _super = (0, _createSuper2.default)(SelectMode);

  function SelectMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, SelectMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(SelectMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isSelectModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.hoveredItemId !== this.props.hoveredItemId) {
        this.tool.setPrevHoveredItemId(nextProps.hoveredItemId);
      }

      if (this.tool && nextProps.selectedItems !== this.props.selectedItems) {
        this.tool.onSelectionChanged(nextProps.selectedItems);
      }

      if (nextProps.isSelectModeActive && !this.props.isSelectModeActive) {
        this.activateTool();
      } else if (!nextProps.isSelectModeActive && this.props.isSelectModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isSelectModeActive !== this.props.isSelectModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      this.tool = new _selectTool.default(this.props.setHoveredItem, this.props.clearHoveredItem, this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage, this.props.switchToTextTool);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_selectMode.default, {
        isSelected: this.props.isSelectModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return SelectMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    isSelectModeActive: state.scratchPaint.mode === _modes.default.SELECT,
    hoveredItemId: state.scratchPaint.hoveredItemId,
    selectedItems: state.scratchPaint.selectedItems
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    setHoveredItem: function setHoveredItem(hoveredItemId) {
      dispatch((0, _hover.setHoveredItem)(hoveredItemId));
    },
    clearHoveredItem: function clearHoveredItem() {
      dispatch((0, _hover.clearHoveredItem)());
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), false
      /* bitmapMode */
      ));
    },
    setCursor: function setCursor(cursorString) {
      dispatch((0, _cursor.setCursor)(cursorString));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.SELECT));
    },
    switchToTextTool: function switchToTextTool() {
      dispatch((0, _modes2.changeMode)(_modes.default.TEXT));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(SelectMode);

exports.default = _default;