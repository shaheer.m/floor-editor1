"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _scrollableCanvas = _interopRequireDefault(require("../components/scrollable-canvas/scrollable-canvas"));

var _view = require("../helper/view");

var _viewBounds = require("../reducers/view-bounds");

var _selectedItems = require("../reducers/selected-items");

var _touchUtils = require("../lib/touch-utils");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

/* eslint-disable no-restricted-globals */
var ScrollableCanvas = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ScrollableCanvas, _React$Component);

  var _super = (0, _createSuper2.default)(ScrollableCanvas);

  function ScrollableCanvas(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ScrollableCanvas);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleHorizontalScrollbarMouseDown', 'handleHorizontalScrollbarMouseMove', 'handleHorizontalScrollbarMouseUp', 'handleVerticalScrollbarMouseDown', 'handleVerticalScrollbarMouseMove', 'handleVerticalScrollbarMouseUp', 'handleWheel']);
    return _this;
  }

  (0, _createClass2.default)(ScrollableCanvas, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.canvas) {
        this.props.canvas.addEventListener('wheel', this.handleWheel);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.canvas) {
        if (this.props.canvas) {
          this.props.canvas.removeEventListener('wheel', this.handleWheel);
        }

        nextProps.canvas.addEventListener('wheel', this.handleWheel);
      }
    }
  }, {
    key: "handleHorizontalScrollbarMouseDown",
    value: function handleHorizontalScrollbarMouseDown(event) {
      this.initialMouseX = (0, _touchUtils.getEventXY)(event).x;
      this.initialScreenX = _paper.default.view.matrix.tx;
      window.addEventListener('mousemove', this.handleHorizontalScrollbarMouseMove);
      window.addEventListener('touchmove', this.handleHorizontalScrollbarMouseMove, {
        passive: false
      });
      window.addEventListener('mouseup', this.handleHorizontalScrollbarMouseUp);
      window.addEventListener('touchend', this.handleHorizontalScrollbarMouseUp);
      event.preventDefault();
    }
  }, {
    key: "handleHorizontalScrollbarMouseMove",
    value: function handleHorizontalScrollbarMouseMove(event) {
      var dx = this.initialMouseX - (0, _touchUtils.getEventXY)(event).x;
      _paper.default.view.matrix.tx = this.initialScreenX + dx * _paper.default.view.zoom * 2;
      (0, _view.clampViewBounds)();
      this.props.updateViewBounds(_paper.default.view.matrix);
      event.preventDefault();
    }
  }, {
    key: "handleHorizontalScrollbarMouseUp",
    value: function handleHorizontalScrollbarMouseUp() {
      window.removeEventListener('mousemove', this.handleHorizontalScrollbarMouseMove);
      window.removeEventListener('touchmove', this.handleHorizontalScrollbarMouseMove, {
        passive: false
      });
      window.removeEventListener('mouseup', this.handleHorizontalScrollbarMouseUp);
      window.removeEventListener('touchend', this.handleHorizontalScrollbarMouseUp);
      this.initialMouseX = null;
      this.initialScreenX = null;
      event.preventDefault();
    }
  }, {
    key: "handleVerticalScrollbarMouseDown",
    value: function handleVerticalScrollbarMouseDown(event) {
      this.initialMouseY = (0, _touchUtils.getEventXY)(event).y;
      this.initialScreenY = _paper.default.view.matrix.ty;
      window.addEventListener('mousemove', this.handleVerticalScrollbarMouseMove);
      window.addEventListener('touchmove', this.handleVerticalScrollbarMouseMove, {
        passive: false
      });
      window.addEventListener('mouseup', this.handleVerticalScrollbarMouseUp);
      window.addEventListener('touchend', this.handleVerticalScrollbarMouseUp);
      event.preventDefault();
    }
  }, {
    key: "handleVerticalScrollbarMouseMove",
    value: function handleVerticalScrollbarMouseMove(event) {
      var dy = this.initialMouseY - (0, _touchUtils.getEventXY)(event).y;
      _paper.default.view.matrix.ty = this.initialScreenY + dy * _paper.default.view.zoom * 2;
      (0, _view.clampViewBounds)();
      this.props.updateViewBounds(_paper.default.view.matrix);
      event.preventDefault();
    }
  }, {
    key: "handleVerticalScrollbarMouseUp",
    value: function handleVerticalScrollbarMouseUp(event) {
      window.removeEventListener('mousemove', this.handleVerticalScrollbarMouseMove);
      window.removeEventListener('touchmove', this.handleVerticalScrollbarMouseMove, {
        passive: false
      });
      window.removeEventListener('mouseup', this.handleVerticalScrollbarMouseUp);
      window.removeEventListener('touchend', this.handleVerticalScrollbarMouseUp);
      this.initialMouseY = null;
      this.initialScreenY = null;
      event.preventDefault();
    }
  }, {
    key: "handleWheel",
    value: function handleWheel(event) {
      // Multiplier variable, so that non-pixel-deltaModes are supported. Needed for Firefox.
      // See #529 (or LLK/scratch-blocks#1190).
      var multiplier = event.deltaMode === 0x1 ? 15 : 1;
      var deltaX = event.deltaX * multiplier;
      var deltaY = event.deltaY * multiplier;
      var canvasRect = this.props.canvas.getBoundingClientRect();
      var offsetX = event.clientX - canvasRect.left;
      var offsetY = event.clientY - canvasRect.top;

      var fixedPoint = _paper.default.view.viewToProject(new _paper.default.Point(offsetX, offsetY));

      if (event.metaKey || event.ctrlKey) {
        // Zoom keeping mouse location fixed
        (0, _view.zoomOnFixedPoint)(-deltaY / 1000, fixedPoint);
        this.props.updateViewBounds(_paper.default.view.matrix);
        this.props.redrawSelectionBox(); // Selection handles need to be resized after zoom
      } else if (event.shiftKey && event.deltaX === 0) {
        // Scroll horizontally (based on vertical scroll delta)
        // This is needed as for some browser/system combinations which do not set deltaX.
        // See #156.
        var dx = deltaY / _paper.default.view.zoom;
        (0, _view.pan)(dx, 0);
        this.props.updateViewBounds(_paper.default.view.matrix);
      } else {
        var _dx = deltaX / _paper.default.view.zoom;

        var dy = deltaY / _paper.default.view.zoom;
        (0, _view.pan)(_dx, dy);
        this.props.updateViewBounds(_paper.default.view.matrix);

        if (_paper.default.tool) {
          _paper.default.tool.view._handleMouseEvent('mousemove', event, fixedPoint);
        }
      }

      event.preventDefault();
    }
  }, {
    key: "render",
    value: function render() {
      var widthPercent = 0;
      var heightPercent = 0;
      var topPercent = 0;
      var leftPercent = 0;

      if (_paper.default.project) {
        var bounds = (0, _view.getWorkspaceBounds)();
        var _paper$view$bounds = _paper.default.view.bounds,
            x = _paper$view$bounds.x,
            y = _paper$view$bounds.y,
            width = _paper$view$bounds.width,
            height = _paper$view$bounds.height;
        widthPercent = Math.min(100, 100 * width / bounds.width);
        heightPercent = Math.min(100, 100 * height / bounds.height);
        var centerX = (x + width / 2 - bounds.x) / bounds.width;
        var centerY = (y + height / 2 - bounds.y) / bounds.height;
        topPercent = Math.max(0, 100 * centerY - heightPercent / 2);
        leftPercent = Math.max(0, 100 * centerX - widthPercent / 2);
      }

      return /*#__PURE__*/_react.default.createElement(_scrollableCanvas.default, {
        hideScrollbars: this.props.hideScrollbars,
        horizontalScrollLengthPercent: widthPercent,
        horizontalScrollStartPercent: leftPercent,
        style: this.props.style,
        verticalScrollLengthPercent: heightPercent,
        verticalScrollStartPercent: topPercent,
        onHorizontalScrollbarMouseDown: this.handleHorizontalScrollbarMouseDown,
        onVerticalScrollbarMouseDown: this.handleVerticalScrollbarMouseDown
      }, this.props.children);
    }
  }], [{
    key: "ZOOM_INCREMENT",
    get: function get() {
      return 0.5;
    }
  }]);
  return ScrollableCanvas;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    viewBounds: state.scratchPaint.viewBounds
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    redrawSelectionBox: function redrawSelectionBox() {
      dispatch((0, _selectedItems.redrawSelectionBox)());
    },
    updateViewBounds: function updateViewBounds(matrix) {
      dispatch((0, _viewBounds.updateViewBounds)(matrix));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ScrollableCanvas);

exports.default = _default;