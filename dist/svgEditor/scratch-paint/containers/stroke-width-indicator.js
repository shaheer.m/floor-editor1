"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _reactRedux = require("react-redux");

var _react = _interopRequireDefault(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _strokeStyle = require("../reducers/stroke-style");

var _strokeWidth = require("../reducers/stroke-width");

var _strokeWidthIndicator = _interopRequireDefault(require("../components/stroke-width-indicator"));

var _selection = require("../helper/selection");

var _stylePath = require("../helper/style-path");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _format = _interopRequireWildcard(require("../lib/format"));

var StrokeWidthIndicator = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(StrokeWidthIndicator, _React$Component);

  var _super = (0, _createSuper2.default)(StrokeWidthIndicator);

  function StrokeWidthIndicator(props) {
    var _this;

    (0, _classCallCheck2.default)(this, StrokeWidthIndicator);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleChangeStrokeWidth']);
    return _this;
  }

  (0, _createClass2.default)(StrokeWidthIndicator, [{
    key: "handleChangeStrokeWidth",
    value: function handleChangeStrokeWidth(newWidth) {
      var changed = (0, _stylePath.applyStrokeWidthToSelection)(newWidth, this.props.textEditTarget);

      if ((!this.props.strokeWidth || this.props.strokeWidth === 0) && newWidth > 0) {
        var currentColorState = (0, _stylePath.getColorsFromSelection)((0, _selection.getSelectedLeafItems)(), (0, _format.isBitmap)(this.props.format)); // Color counts as null if either both colors are null or the primary color is null and it's solid
        // TODO: consolidate this check in one place

        var wasNull = currentColorState.strokeColor === null && (currentColorState.strokeColor2 === null || currentColorState.strokeGradientType === _gradientTypes.default.SOLID);

        if (wasNull) {
          changed = (0, _stylePath.applyColorToSelection)('#000', 0, // colorIndex,
          true, // isSolidGradient
          true, // applyToStroke
          this.props.textEditTarget) || changed; // If there's no previous stroke color, default to solid black

          this.props.onChangeStrokeGradientType(_gradientTypes.default.SOLID);
          this.props.onChangeStrokeColor('#000');
        } else if (currentColorState.strokeColor !== _stylePath.MIXED) {
          // Set color state from the selected item's stroke color
          this.props.onChangeStrokeGradientType(currentColorState.strokeGradientType);
          this.props.onChangeStrokeColor((0, _parseColor.default)(currentColorState.strokeColor).hex);
          this.props.onChangeStrokeColor2((0, _parseColor.default)(currentColorState.strokeColor2).hex);
        }
      }

      this.props.onChangeStrokeWidth(newWidth);
      if (changed) this.props.onUpdateImage();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_strokeWidthIndicator.default, {
        disabled: this.props.disabled,
        strokeWidth: this.props.strokeWidth,
        onChangeStrokeWidth: this.handleChangeStrokeWidth
      });
    }
  }]);
  return StrokeWidthIndicator;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    disabled: state.scratchPaint.mode === _modes.default.BRUSH || state.scratchPaint.mode === _modes.default.TEXT || state.scratchPaint.mode === _modes.default.FILL,
    format: state.scratchPaint.format,
    strokeWidth: state.scratchPaint.color.strokeWidth,
    textEditTarget: state.scratchPaint.textEditTarget
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChangeStrokeColor: function onChangeStrokeColor(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeColor)(strokeColor));
    },
    onChangeStrokeColor2: function onChangeStrokeColor2(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeColor2)(strokeColor));
    },
    onChangeStrokeGradientType: function onChangeStrokeGradientType(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeGradientType)(strokeColor));
    },
    onChangeStrokeWidth: function onChangeStrokeWidth(strokeWidth) {
      dispatch((0, _strokeWidth.changeStrokeWidth)(strokeWidth));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(StrokeWidthIndicator);

exports.default = _default;