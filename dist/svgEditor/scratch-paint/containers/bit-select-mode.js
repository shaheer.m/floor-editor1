"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _selectTool = _interopRequireDefault(require("../helper/bit-tools/select-tool"));

var _bitSelectMode = _interopRequireDefault(require("../components/bit-select-mode/bit-select-mode"));

var BitSelectMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitSelectMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitSelectMode);

  function BitSelectMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitSelectMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitSelectMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isSelectModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.selectedItems !== this.props.selectedItems) {
        this.tool.onSelectionChanged(nextProps.selectedItems);
      }

      if (nextProps.isSelectModeActive && !this.props.isSelectModeActive) {
        this.activateTool();
      } else if (!nextProps.isSelectModeActive && this.props.isSelectModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isSelectModeActive !== this.props.isSelectModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      this.props.clearGradient();
      this.tool = new _selectTool.default(this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitSelectMode.default, {
        isSelected: this.props.isSelectModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitSelectMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    isSelectModeActive: state.scratchPaint.mode === _modes.default.BIT_SELECT,
    selectedItems: state.scratchPaint.selectedItems
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearGradient: function clearGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    setCursor: function setCursor(cursorType) {
      dispatch((0, _cursor.setCursor)(cursorType));
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)()));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_SELECT));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitSelectMode);

exports.default = _default;