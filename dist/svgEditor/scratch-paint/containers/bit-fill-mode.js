"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _bitFillMode = _interopRequireDefault(require("../components/bit-fill-mode/bit-fill-mode"));

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _fillModeGradientType = require("../reducers/fill-mode-gradient-type");

var _selection = require("../helper/selection");

var _fillTool = _interopRequireDefault(require("../helper/bit-tools/fill-tool"));

var _stylePath = require("../helper/style-path");

var BitFillMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitFillMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitFillMode);

  function BitFillMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitFillMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitFillMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isFillModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool) {
        if (nextProps.color !== this.props.color) {
          this.tool.setColor(nextProps.color);
        }

        if (nextProps.color2 !== this.props.color2) {
          this.tool.setColor2(nextProps.color2);
        }

        if (nextProps.fillModeGradientType !== this.props.fillModeGradientType) {
          this.tool.setGradientType(nextProps.fillModeGradientType);
        }
      }

      if (nextProps.isFillModeActive && !this.props.isFillModeActive) {
        this.activateTool();
      } else if (!nextProps.isFillModeActive && this.props.isFillModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isFillModeActive !== this.props.isFillModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems); // Force the default brush color if fill is MIXED or transparent

      var color = this.props.color;

      if (this.props.color === _stylePath.MIXED) {
        color = _fillStyle.DEFAULT_COLOR;
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR, 0);
      }

      var gradientType = this.props.fillModeGradientType ? this.props.fillModeGradientType : this.props.styleGradientType;
      var color2 = this.props.color2;

      if (gradientType !== this.props.styleGradientType) {
        if (this.props.styleGradientType === _gradientTypes.default.SOLID) {
          color2 = (0, _stylePath.generateSecondaryColor)(color);
          this.props.onChangeFillColor(color2, 1);
        }

        this.props.changeGradientType(gradientType);
      }

      if (this.props.color2 === _stylePath.MIXED) {
        color2 = (0, _stylePath.generateSecondaryColor)();
        this.props.onChangeFillColor(color2, 1);
      }

      this.tool = new _fillTool.default(this.props.onUpdateImage);
      this.tool.setColor(color);
      this.tool.setColor2(color2);
      this.tool.setGradientType(gradientType);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitFillMode.default, {
        isSelected: this.props.isFillModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitFillMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    fillModeGradientType: state.scratchPaint.fillMode.gradientType,
    // Last user-selected gradient type
    color: state.scratchPaint.color.fillColor.primary,
    color2: state.scratchPaint.color.fillColor.secondary,
    styleGradientType: state.scratchPaint.color.fillColor.gradientType,
    isFillModeActive: state.scratchPaint.mode === _modes.default.BIT_FILL
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    changeGradientType: function changeGradientType(gradientType) {
      dispatch((0, _fillModeGradientType.changeGradientType)(gradientType));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_FILL));
    },
    onChangeFillColor: function onChangeFillColor(fillColor, index) {
      if (index === 0) {
        dispatch((0, _fillStyle.changeFillColor)(fillColor));
      } else if (index === 1) {
        dispatch((0, _fillStyle.changeFillColor2)(fillColor));
      }
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitFillMode);

exports.default = _default;