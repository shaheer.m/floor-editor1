"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _fixedTools = _interopRequireDefault(require("../components/fixed-tools/fixed-tools"));

var _modes = require("../reducers/modes");

var _format = require("../reducers/format");

var _selectedItems = require("../reducers/selected-items");

var _eyeDropper = require("../reducers/eye-dropper");

var _textEditTarget = require("../reducers/text-edit-target");

var _layout = require("../reducers/layout");

var _selection = require("../helper/selection");

var _order = require("../helper/order");

var _group = require("../helper/group");

var _format2 = _interopRequireWildcard(require("../lib/format"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _stylePath = require("../helper/style-path");

var FixedTools = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(FixedTools, _React$Component);

  var _super = (0, _createSuper2.default)(FixedTools);

  function FixedTools(props) {
    var _this;

    (0, _classCallCheck2.default)(this, FixedTools);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleSendBackward', 'handleSendForward', 'handleSendToBack', 'handleSendToFront', 'handleSetSelectedItems', 'handleGroup', 'handleUngroup']);
    return _this;
  }

  (0, _createClass2.default)(FixedTools, [{
    key: "handleGroup",
    value: function handleGroup() {
      (0, _group.groupSelection)(this.props.clearSelectedItems, this.handleSetSelectedItems, this.props.onUpdateImage);
    }
  }, {
    key: "handleUngroup",
    value: function handleUngroup() {
      var activeItems = (0, _stylePath._getColorStateListeners)(this.props.textEditTargetId, 1); // check if the greatest parent has a clickable attribute

      var item = (0, _selection.returnGreatestParent)(activeItems);

      if (item && item.className && item.className === 'Group' && item._children && item._data && item._data.customAttributes) {
        var attributes = item._data.customAttributes;

        if (attributes.some(function (obj) {
          return obj.value === "clickable";
        })) {
          (0, _stylePath.removeParentAttributesFromChildren)(item._children);
        }
      }

      (0, _group.ungroupSelection)(this.props.clearSelectedItems, this.handleSetSelectedItems, this.props.onUpdateImage);
    }
  }, {
    key: "handleSendBackward",
    value: function handleSendBackward() {
      (0, _order.sendBackward)(this.props.onUpdateImage);
    }
  }, {
    key: "handleSendForward",
    value: function handleSendForward() {
      (0, _order.bringForward)(this.props.onUpdateImage);
    }
  }, {
    key: "handleSendToBack",
    value: function handleSendToBack() {
      (0, _order.sendToBack)(this.props.onUpdateImage);
    }
  }, {
    key: "handleSendToFront",
    value: function handleSendToFront() {
      (0, _order.bringToFront)(this.props.onUpdateImage);
    }
  }, {
    key: "handleSetSelectedItems",
    value: function handleSetSelectedItems() {
      this.props.setSelectedItems(this.props.format);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_fixedTools.default, {
        canRedo: this.props.canRedo,
        canUndo: this.props.canUndo,
        name: this.props.name,
        onGroup: this.handleGroup,
        onRedo: this.props.onRedo,
        onSendBackward: this.handleSendBackward,
        onSendForward: this.handleSendForward,
        onSendToBack: this.handleSendToBack,
        onSendToFront: this.handleSendToFront,
        onUndo: this.props.onUndo,
        onUngroup: this.handleUngroup,
        onUpdateImage: this.props.onUpdateImage,
        onUpdateName: this.props.onUpdateName
      });
    }
  }]);
  return FixedTools;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    changeColorToEyeDropper: state.scratchPaint.color.eyeDropper.callback,
    format: state.scratchPaint.format,
    isEyeDropping: state.scratchPaint.color.eyeDropper.active,
    mode: state.scratchPaint.mode,
    pasteOffset: state.scratchPaint.clipboard.pasteOffset,
    previousTool: state.scratchPaint.color.eyeDropper.previousTool,
    selectedItems: state.scratchPaint.selectedItems,
    viewBounds: state.scratchPaint.viewBounds
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    changeMode: function changeMode(mode) {
      dispatch((0, _modes.changeMode)(mode));
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    handleSwitchToBitmap: function handleSwitchToBitmap() {
      dispatch((0, _format.changeFormat)(_format2.default.BITMAP));
    },
    handleSwitchToVector: function handleSwitchToVector() {
      dispatch((0, _format.changeFormat)(_format2.default.VECTOR));
    },
    removeTextEditTarget: function removeTextEditTarget() {
      dispatch((0, _textEditTarget.setTextEditTarget)());
    },
    setLayout: function setLayout(layout) {
      dispatch((0, _layout.setLayout)(layout));
    },
    setSelectedItems: function setSelectedItems(format) {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), (0, _format2.isBitmap)(format)));
    },
    onDeactivateEyeDropper: function onDeactivateEyeDropper() {
      // set redux values to default for eye dropper reducer
      dispatch((0, _eyeDropper.deactivateEyeDropper)());
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FixedTools);

exports.default = _default;