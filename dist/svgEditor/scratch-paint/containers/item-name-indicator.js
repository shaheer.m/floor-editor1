"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _reactRedux = require("react-redux");

var _react = _interopRequireWildcard(require("react"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _strokeStyle = require("../reducers/stroke-style");

var _strokeWidth = require("../reducers/stroke-width");

var _itemNameIndicator = _interopRequireDefault(require("../components/item-name-indicator/item-name-indicator"));

var _selection = require("../helper/selection");

var _stylePath = require("../helper/style-path");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _format = _interopRequireWildcard(require("../lib/format"));

var _reactPopover = _interopRequireDefault(require("react-popover"));

var _group = require("../helper/group");

var _labeledIconButton = _interopRequireDefault(require("../components/labeled-icon-button/labeled-icon-button"));

var _assignDropdown = _interopRequireDefault(require("../components/fixed-tools/icons/assign-dropdown.svg"));

var _multiselectDropdown = _interopRequireDefault(require("../components/forms/multiselect-dropdown"));

var _groupNameSelect = _interopRequireDefault(require("../components/forms/group-name-select"));

var _setAvailable = _interopRequireDefault(require("../components/forms/set-available"));

var _maxCapacity = _interopRequireDefault(require("../components/forms/max-capacity"));

var _classnames = _interopRequireDefault(require("classnames"));

var _inputModule = _interopRequireDefault(require("../components/forms/input.module.css"));

var _alertTopError = require("../reducers/alert-top-error");

var ItemNameIndicator = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ItemNameIndicator, _React$Component);

  var _super = (0, _createSuper2.default)(ItemNameIndicator);

  function ItemNameIndicator(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ItemNameIndicator);
    _this = _super.call(this, props);
    _this.state = {
      modalStatus: false,
      appliedName: null,
      attributes: [],
      groupName: null,
      available: null,
      capacityVal: null,
      btnDisabled: false,
      showCapacity: false
    };
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleNameChange', 'handleAttributeModal', 'handleSubmit', 'handleAttributeChange', 'groupNameChange', 'handleAvailableChange', 'handleCapacityChange', 'handleBtn']);
    return _this;
  }

  (0, _createClass2.default)(ItemNameIndicator, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.handleNameChange(nextProps.selectedName);
      this.handleAttributeChange(nextProps.attributeValues);
      this.groupNameChange(nextProps.groupName);
      this.handleAvailableChange(nextProps.available);
      this.handleCapacityChange(nextProps.capacityMax);
    }
  }, {
    key: "handleNameChange",
    value: function handleNameChange(name) {
      this.setState({
        appliedName: name
      });
    }
  }, {
    key: "handleAttributeChange",
    value: function handleAttributeChange(value) {
      this.setState({
        attributes: value
      });

      if (value && value.find(function (e) {
        return e.value === 'clickable';
      })) {
        this.setState({
          showCapacity: true
        });
      } else {
        this.setState({
          showCapacity: false
        });
      }
    }
  }, {
    key: "groupNameChange",
    value: function groupNameChange(value) {
      this.setState({
        groupName: value
      });
    }
  }, {
    key: "handleAvailableChange",
    value: function handleAvailableChange(value) {
      this.setState({
        available: value
      });
    }
  }, {
    key: "handleCapacityChange",
    value: function handleCapacityChange(value) {
      this.setState({
        capacityVal: value
      });
    }
  }, {
    key: "handleBtn",
    value: function handleBtn(value) {
      this.setState({
        btnDisabled: value
      });
    }
  }, {
    key: "handleAttributeModal",
    value: function handleAttributeModal() {
      this.setState(function (prevState) {
        return {
          modalStatus: !prevState.modalStatus
        };
      });
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit() {
      var name = this.state.appliedName ? this.state.appliedName : null;
      var attributes, groupName, available, capacity;
      attributes = [];
      groupName = available = capacity = null;

      if (name) {
        attributes = this.state.attributes ? this.state.attributes : [];
        groupName = this.state.groupName ? this.state.groupName : null;
        available = this.state.available ? this.state.available : null;

        if (attributes && attributes.find(function (e) {
          return e.value === 'clickable';
        })) {
          capacity = this.state.capacityVal ? this.state.capacityVal : null;
        } else {
          capacity = null;
        }
      }

      var changed = (0, _stylePath.applyNameAttributesToSelection)(name, attributes, available, capacity, groupName, this.props.textEditTarget);
      if (changed[0]) this.props.onUpdateImage();

      if (changed[1]) {
        this.props.showAlert(changed[1]);
        this.props.sendAlertMessage(changed[1]);
        this.handleNameChange(this.props.selectedName ? this.props.selectedName : '');
      } else {
        this.handleAttributeModal();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_reactPopover.default, {
        body: /*#__PURE__*/_react.default.createElement(_react.Fragment, null, /*#__PURE__*/_react.default.createElement("h4", {
          className: _inputModule.default.spaceBottom
        }, "Assign Seat"), /*#__PURE__*/_react.default.createElement(_itemNameIndicator.default, {
          disabled: this.props.disabled,
          itemName: this.state.appliedName,
          onChangeItemName: this.handleNameChange,
          setBtnError: this.handleBtn
        }), /*#__PURE__*/_react.default.createElement(_multiselectDropdown.default, {
          itemName: this.state.appliedName,
          attributeValues: this.props.attributeValues,
          handleAttributeChange: this.handleAttributeChange
        }), this.state.showCapacity && /*#__PURE__*/_react.default.createElement(_maxCapacity.default, {
          itemName: this.state.appliedName,
          setBtnError: this.handleBtn,
          capacity: this.props.capacityMax,
          onCapacityChange: this.handleCapacityChange
        }), /*#__PURE__*/_react.default.createElement(_setAvailable.default, {
          available: this.props.available,
          itemName: this.state.appliedName,
          onAvailableChange: this.handleAvailableChange
        }), /*#__PURE__*/_react.default.createElement("div", {
          className: _inputModule.default.twoBtnWrp
        }, /*#__PURE__*/_react.default.createElement("button", {
          className: (0, _classnames.default)(_inputModule.default.btn, _inputModule.default.gradBtn),
          disabled: this.state.btnDisabled || !this.state.appliedName,
          onClick: this.handleSubmit
        }, "Assign"), /*#__PURE__*/_react.default.createElement("button", {
          onClick: this.handleAttributeModal,
          className: (0, _classnames.default)(_inputModule.default.btn, _inputModule.default.whiteBtn)
        }, "Cancel"))),
        isOpen: this.state.modalStatus,
        preferPlace: "below",
        onOuterAction: this.handleAttributeModal,
        className: "itemNamePopover"
      }, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("button", {
        className: (0, _classnames.default)(_inputModule.default.redBorderBtn),
        disabled: !(0, _group.selectedSingleItem)(),
        onClick: this.handleAttributeModal
      }, this.props.selectedName ? /*#__PURE__*/_react.default.createElement("span", null, "Edit Attributes") : /*#__PURE__*/_react.default.createElement("span", null, "Assign"), /*#__PURE__*/_react.default.createElement("img", {
        src: _assignDropdown.default,
        alt: "arrow"
      }))));
    }
  }]);
  return ItemNameIndicator;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  var name;
  var attribute;
  var groupName;
  var availableAttribute;
  var capacity;

  if (state.scratchPaint.selectedItems && state.scratchPaint.selectedItems[0]) {
    var parents = [];
    parents.push(state.scratchPaint.selectedItems[0]);
    var parentStack = (0, _selection.findGreaterParent)(state.scratchPaint.selectedItems[0], parents);
    var greaterParent = parentStack[parentStack.length - 2];
    var greaterParentName = greaterParent._data.id;
    var greaterParentAttributes = greaterParent._data.customAttributes;
    var greaterParentAvailable = greaterParent._data.unavailable;
    var greaterParentCapacity = greaterParent._data.capacity;
    var greaterParentGroupName = greaterParent._data.groupName;
    attribute = greaterParentAttributes;
    name = greaterParentName ? greaterParentName : null;
    availableAttribute = greaterParentAvailable ? greaterParentAvailable : null;
    capacity = greaterParentCapacity ? greaterParentCapacity : 1;
    groupName = greaterParentGroupName ? greaterParentGroupName : null;
  }

  return {
    disabled: state.scratchPaint.mode === _modes.default.BRUSH || state.scratchPaint.mode === _modes.default.TEXT || state.scratchPaint.mode === _modes.default.FILL,
    textEditTarget: state.scratchPaint.textEditTarget,
    selectedName: name,
    attributeValues: attribute,
    groupName: groupName,
    selectedItems: state.scratchPaint.selectedItems,
    available: availableAttribute,
    capacityMax: capacity
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    showAlert: function showAlert(msg) {
      dispatch((0, _alertTopError.setAlertText)(msg));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ItemNameIndicator);

exports.default = _default;