"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _rectTool = _interopRequireDefault(require("../helper/bit-tools/rect-tool"));

var _bitRectMode = _interopRequireDefault(require("../components/bit-rect-mode/bit-rect-mode"));

var BitRectMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BitRectMode, _React$Component);

  var _super = (0, _createSuper2.default)(BitRectMode);

  function BitRectMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BitRectMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(BitRectMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isRectModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool) {
        if (nextProps.color !== this.props.color) {
          this.tool.setColor(nextProps.color);
        }

        if (nextProps.selectedItems !== this.props.selectedItems) {
          this.tool.onSelectionChanged(nextProps.selectedItems);
        }

        if (nextProps.filled !== this.props.filled) {
          this.tool.setFilled(nextProps.filled);
        }

        if (nextProps.thickness !== this.props.thickness || nextProps.zoom !== this.props.zoom) {
          this.tool.setThickness(nextProps.thickness);
        }
      }

      if (nextProps.isRectModeActive && !this.props.isRectModeActive) {
        this.activateTool();
      } else if (!nextProps.isRectModeActive && this.props.isRectModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isRectModeActive !== this.props.isRectModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems); // Force the default brush color if fill is MIXED or transparent

      var fillColorPresent = this.props.color.primary !== _stylePath.MIXED && this.props.color.primary !== null;

      if (!fillColorPresent) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
      }

      this.tool = new _rectTool.default(this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage);
      this.tool.setColor(this.props.color);
      this.tool.setFilled(this.props.filled);
      this.tool.setThickness(this.props.thickness);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_bitRectMode.default, {
        isSelected: this.props.isRectModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BitRectMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    color: state.scratchPaint.color.fillColor,
    filled: state.scratchPaint.fillBitmapShapes,
    isRectModeActive: state.scratchPaint.mode === _modes.default.BIT_RECT,
    selectedItems: state.scratchPaint.selectedItems,
    thickness: state.scratchPaint.bitBrushSize,
    zoom: state.scratchPaint.viewBounds.scaling.x
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    setCursor: function setCursor(cursorString) {
      dispatch((0, _cursor.setCursor)(cursorString));
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), true
      /* bitmapMode */
      ));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_RECT));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BitRectMode);

exports.default = _default;