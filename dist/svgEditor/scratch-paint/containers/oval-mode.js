"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _stylePath = require("../helper/style-path");

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _fillStyle = require("../reducers/fill-style");

var _strokeStyle = require("../reducers/stroke-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _ovalTool = _interopRequireDefault(require("../helper/tools/oval-tool"));

var _ovalMode = _interopRequireDefault(require("../components/oval-mode/oval-mode"));

var OvalMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(OvalMode, _React$Component);

  var _super = (0, _createSuper2.default)(OvalMode);

  function OvalMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, OvalMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool', 'validateColorState']);
    return _this;
  }

  (0, _createClass2.default)(OvalMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isOvalModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool && nextProps.colorState !== this.props.colorState) {
        this.tool.setColorState(nextProps.colorState);
      }

      if (this.tool && nextProps.selectedItems !== this.props.selectedItems) {
        this.tool.onSelectionChanged(nextProps.selectedItems);
      }

      if (nextProps.isOvalModeActive && !this.props.isOvalModeActive) {
        this.activateTool();
      } else if (!nextProps.isOvalModeActive && this.props.isOvalModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isOvalModeActive !== this.props.isOvalModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.validateColorState();
      this.tool = new _ovalTool.default(this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage);
      this.tool.setColorState(this.props.colorState);
      this.tool.activate();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "validateColorState",
    value: function validateColorState() {
      // Make sure that at least one of fill/stroke is set, and that MIXED is not one of the colors.
      // If fill and stroke color are both missing, set fill to default and stroke to transparent.
      // If exactly one of fill or stroke color is set, set the other one to transparent.
      var strokeWidth = this.props.colorState.strokeWidth;
      var fillColor1 = this.props.colorState.fillColor.primary;
      var fillColor2 = this.props.colorState.fillColor.secondary;
      var fillGradient = this.props.colorState.fillColor.gradientType;
      var strokeColor1 = this.props.colorState.strokeColor.primary;
      var strokeColor2 = this.props.colorState.strokeColor.secondary;
      var strokeGradient = this.props.colorState.strokeColor.gradientType;

      if (fillColor2 === _stylePath.MIXED) {
        this.props.clearFillGradient();
        fillColor2 = null;
        fillGradient = _gradientTypes.default.SOLID;
      }

      if (strokeColor2 === _stylePath.MIXED) {
        this.props.clearStrokeGradient();
        strokeColor2 = null;
        strokeGradient = _gradientTypes.default.SOLID;
      }

      var fillColorMissing = fillColor1 === _stylePath.MIXED || fillGradient === _gradientTypes.default.SOLID && fillColor1 === null || fillGradient !== _gradientTypes.default.SOLID && fillColor1 === null && fillColor2 === null;
      var strokeColorMissing = strokeColor1 === _stylePath.MIXED || strokeWidth === null || strokeWidth === 0 || strokeGradient === _gradientTypes.default.SOLID && strokeColor1 === null || strokeGradient !== _gradientTypes.default.SOLID && strokeColor1 === null && strokeColor2 === null;

      if (fillColorMissing && strokeColorMissing) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
        this.props.clearFillGradient();
        this.props.onChangeStrokeColor(null);
        this.props.clearStrokeGradient();
      } else if (fillColorMissing && !strokeColorMissing) {
        this.props.onChangeFillColor(null);
        this.props.clearFillGradient();
      } else if (!fillColorMissing && strokeColorMissing) {
        this.props.onChangeStrokeColor(null);
        this.props.clearStrokeGradient();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_ovalMode.default, {
        isSelected: this.props.isOvalModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return OvalMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    colorState: state.scratchPaint.color,
    isOvalModeActive: state.scratchPaint.mode === _modes.default.OVAL,
    selectedItems: state.scratchPaint.selectedItems
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearFillGradient: function clearFillGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    clearStrokeGradient: function clearStrokeGradient() {
      dispatch((0, _strokeStyle.clearStrokeGradient)());
    },
    setCursor: function setCursor(cursorString) {
      dispatch((0, _cursor.setCursor)(cursorString));
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), false
      /* bitmapMode */
      ));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.OVAL));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    },
    onChangeStrokeColor: function onChangeStrokeColor(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeColor)(strokeColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(OvalMode);

exports.default = _default;