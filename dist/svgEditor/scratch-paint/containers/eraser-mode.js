"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _blob = _interopRequireDefault(require("../helper/blob-tools/blob"));

var _eraserMode = require("../reducers/eraser-mode");

var _selectedItems = require("../reducers/selected-items");

var _eraserMode2 = _interopRequireDefault(require("../components/eraser-mode/eraser-mode"));

var _modes2 = require("../reducers/modes");

var EraserMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(EraserMode, _React$Component);

  var _super = (0, _createSuper2.default)(EraserMode);

  function EraserMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, EraserMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    _this.blob = new _blob.default(_this.props.onUpdateImage, _this.props.clearSelectedItems);
    return _this;
  }

  (0, _createClass2.default)(EraserMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isEraserModeActive) {
        this.activateTool();
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.isEraserModeActive && !this.props.isEraserModeActive) {
        this.activateTool();
      } else if (!nextProps.isEraserModeActive && this.props.isEraserModeActive) {
        this.deactivateTool();
      } else if (nextProps.isEraserModeActive && this.props.isEraserModeActive) {
        this.blob.setOptions((0, _objectSpread2.default)({
          isEraser: true
        }, nextProps.eraserModeState));
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isEraserModeActive !== this.props.isEraserModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.blob.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      this.blob.activateTool((0, _objectSpread2.default)({
        isEraser: true
      }, this.props.eraserModeState));
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.blob.deactivateTool();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_eraserMode2.default, {
        isSelected: this.props.isEraserModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return EraserMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    eraserModeState: state.scratchPaint.eraserMode,
    isEraserModeActive: state.scratchPaint.mode === _modes.default.ERASER
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    changeBrushSize: function changeBrushSize(brushSize) {
      dispatch((0, _eraserMode.changeBrushSize)(brushSize));
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.ERASER));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(EraserMode);

exports.default = _default;