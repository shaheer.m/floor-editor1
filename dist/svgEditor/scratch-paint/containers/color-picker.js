"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _reactRedux = require("react-redux");

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _parseColor = _interopRequireDefault(require("parse-color"));

var _react = _interopRequireDefault(require("react"));

var _colorIndex = require("../reducers/color-index");

var _selectedItems = require("../reducers/selected-items");

var _eyeDropper = require("../reducers/eye-dropper");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _colorPicker = _interopRequireDefault(require("../components/color-picker/color-picker"));

var _stylePath = require("../helper/style-path");

var _modes = _interopRequireDefault(require("../lib/modes"));

var colorStringToHsv = function colorStringToHsv(hexString) {
  var hsv = (0, _parseColor.default)(hexString).hsv; // Hue comes out in [0, 360], limit to [0, 100]

  hsv[0] = hsv[0] / 3.6; // Black is parsed as {0, 0, 0}, but turn saturation up to 100
  // to make it easier to see slider values.

  if (hsv[1] === 0 && hsv[2] === 0) {
    hsv[1] = 100;
  }

  return hsv;
};

var hsvToHex = function hsvToHex(h, s, v) {
  return (// Scale hue back up to [0, 360] from [0, 100]
    (0, _parseColor.default)("hsv(".concat(3.6 * h, ", ").concat(s, ", ").concat(v, ")")).hex
  );
}; // Important! This component ignores new color props except when isEyeDropping
// This is to make the HSV <=> RGB conversion stable. The sliders manage their
// own changes until unmounted or color changes with props.isEyeDropping = true.


var ColorPicker = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(ColorPicker, _React$Component);

  var _super = (0, _createSuper2.default)(ColorPicker);

  function ColorPicker(props) {
    var _this;

    (0, _classCallCheck2.default)(this, ColorPicker);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['getHsv', 'handleChangeGradientTypeHorizontal', 'handleChangeGradientTypeRadial', 'handleChangeGradientTypeSolid', 'handleChangeGradientTypeVertical', 'handleHueChange', 'handleSaturationChange', 'handleBrightnessChange', 'handleTransparent', 'handleActivateEyeDropper']);
    var color = props.colorIndex === 0 ? props.color : props.color2;

    var hsv = _this.getHsv(color);

    _this.state = {
      hue: hsv[0],
      saturation: hsv[1],
      brightness: hsv[2]
    };
    return _this;
  }

  (0, _createClass2.default)(ColorPicker, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(newProps) {
      var color = newProps.colorIndex === 0 ? this.props.color : this.props.color2;
      var newColor = newProps.colorIndex === 0 ? newProps.color : newProps.color2;
      var colorSetByEyedropper = this.props.isEyeDropping && color !== newColor;

      if (colorSetByEyedropper || this.props.colorIndex !== newProps.colorIndex) {
        var hsv = this.getHsv(newColor);
        this.setState({
          hue: hsv[0],
          saturation: hsv[1],
          brightness: hsv[2]
        });
      }
    }
  }, {
    key: "getHsv",
    value: function getHsv(color) {
      var isTransparent = color === null;
      var isMixed = color === _stylePath.MIXED;
      return isTransparent || isMixed ? [50, 100, 100] : colorStringToHsv(color);
    }
  }, {
    key: "handleHueChange",
    value: function handleHueChange(hue) {
      var _this2 = this;

      this.setState({
        hue: hue
      }, function () {
        _this2.handleColorChange();
      });
    }
  }, {
    key: "handleSaturationChange",
    value: function handleSaturationChange(saturation) {
      var _this3 = this;

      this.setState({
        saturation: saturation
      }, function () {
        _this3.handleColorChange();
      });
    }
  }, {
    key: "handleBrightnessChange",
    value: function handleBrightnessChange(brightness) {
      var _this4 = this;

      this.setState({
        brightness: brightness
      }, function () {
        _this4.handleColorChange();
      });
    }
  }, {
    key: "handleColorChange",
    value: function handleColorChange() {
      this.props.onChangeColor(hsvToHex(this.state.hue, this.state.saturation, this.state.brightness));
    }
  }, {
    key: "handleTransparent",
    value: function handleTransparent() {
      this.props.onChangeColor(null);
    }
  }, {
    key: "handleActivateEyeDropper",
    value: function handleActivateEyeDropper() {
      this.props.onActivateEyeDropper(_paper.default.tool, // get the currently active tool from paper
      this.props.onChangeColor);
    }
  }, {
    key: "handleChangeGradientTypeHorizontal",
    value: function handleChangeGradientTypeHorizontal() {
      this.props.onChangeGradientType(_gradientTypes.default.HORIZONTAL);
    }
  }, {
    key: "handleChangeGradientTypeRadial",
    value: function handleChangeGradientTypeRadial() {
      this.props.onChangeGradientType(_gradientTypes.default.RADIAL);
    }
  }, {
    key: "handleChangeGradientTypeSolid",
    value: function handleChangeGradientTypeSolid() {
      this.props.onChangeGradientType(_gradientTypes.default.SOLID);
    }
  }, {
    key: "handleChangeGradientTypeVertical",
    value: function handleChangeGradientTypeVertical() {
      this.props.onChangeGradientType(_gradientTypes.default.VERTICAL);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_colorPicker.default, {
        brightness: this.state.brightness,
        color: this.props.color,
        color2: this.props.color2,
        colorIndex: this.props.colorIndex,
        gradientType: this.props.gradientType,
        hue: this.state.hue,
        isEyeDropping: this.props.isEyeDropping,
        mode: this.props.mode,
        rtl: this.props.rtl,
        saturation: this.state.saturation,
        shouldShowGradientTools: this.props.shouldShowGradientTools,
        onActivateEyeDropper: this.handleActivateEyeDropper,
        onBrightnessChange: this.handleBrightnessChange,
        onChangeGradientTypeHorizontal: this.handleChangeGradientTypeHorizontal,
        onChangeGradientTypeRadial: this.handleChangeGradientTypeRadial,
        onChangeGradientTypeSolid: this.handleChangeGradientTypeSolid,
        onChangeGradientTypeVertical: this.handleChangeGradientTypeVertical,
        onHueChange: this.handleHueChange,
        onSaturationChange: this.handleSaturationChange,
        onSelectColor: this.props.onSelectColor,
        onSelectColor2: this.props.onSelectColor2,
        onSwap: this.props.onSwap,
        onTransparent: this.handleTransparent
      });
    }
  }]);
  return ColorPicker;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    colorIndex: state.scratchPaint.fillMode.colorIndex,
    isEyeDropping: state.scratchPaint.color.eyeDropper.active,
    mode: state.scratchPaint.mode,
    rtl: state.scratchPaint.layout.rtl
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    onActivateEyeDropper: function onActivateEyeDropper(currentTool, callback) {
      dispatch((0, _eyeDropper.activateEyeDropper)(currentTool, callback));
    },
    onSelectColor: function onSelectColor() {
      dispatch((0, _colorIndex.changeColorIndex)(0));
    },
    onSelectColor2: function onSelectColor2() {
      dispatch((0, _colorIndex.changeColorIndex)(1));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ColorPicker);

exports.default = _default;