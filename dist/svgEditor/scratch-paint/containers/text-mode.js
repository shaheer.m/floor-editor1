"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _fonts = _interopRequireDefault(require("../lib/fonts"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _stylePath = require("../helper/style-path");

var _font = require("../reducers/font");

var _fillStyle = require("../reducers/fill-style");

var _strokeStyle = require("../reducers/stroke-style");

var _modes2 = require("../reducers/modes");

var _textEditTarget = require("../reducers/text-edit-target");

var _selectedItems = require("../reducers/selected-items");

var _cursor = require("../reducers/cursor");

var _selection = require("../helper/selection");

var _textTool = _interopRequireDefault(require("../helper/tools/text-tool"));

var _textMode = _interopRequireDefault(require("../components/text-mode/text-mode"));

var _bitTextMode = _interopRequireDefault(require("../components/bit-text-mode/bit-text-mode"));

var TextMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(TextMode, _React$Component);

  var _super = (0, _createSuper2.default)(TextMode);

  function TextMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, TextMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    return _this;
  }

  (0, _createClass2.default)(TextMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isTextModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.tool) {
        if (nextProps.colorState !== this.props.colorState) {
          this.tool.setColorState(nextProps.colorState);
        }

        if (nextProps.selectedItems !== this.props.selectedItems) {
          this.tool.onSelectionChanged(nextProps.selectedItems);
        }

        if (!nextProps.textEditTarget && this.props.textEditTarget) {
          this.tool.onTextEditCancelled();
        }

        if (!nextProps.viewBounds.equals(this.props.viewBounds)) {
          this.tool.onViewBoundsChanged(nextProps.viewBounds);
        }

        if (nextProps.font !== this.props.font) {
          this.tool.setFont(nextProps.font);
        }

        if (nextProps.rtl !== this.props.rtl) {
          this.tool.setRtl(nextProps.rtl);
        }
      }

      if (nextProps.isTextModeActive && !this.props.isTextModeActive) {
        this.activateTool(nextProps);
      } else if (!nextProps.isTextModeActive && this.props.isTextModeActive) {
        this.deactivateTool();
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isTextModeActive !== this.props.isTextModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool(nextProps) {
      var selected = (0, _selection.getSelectedLeafItems)();
      var textBoxToStartEditing = null;

      if (selected.length === 1 && selected[0] instanceof _paper.default.PointText) {
        textBoxToStartEditing = selected[0];
      }

      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.props.clearGradient(); // If fill and stroke color are both mixed/transparent/absent, set fill to default and stroke to transparent.
      // If exactly one of fill or stroke color is set, set the other one to transparent.
      // This way the tool won't draw an invisible state, or be unclear about what will be drawn.

      var strokeWidth = nextProps.colorState.strokeWidth;
      var fillColor = nextProps.colorState.fillColor.primary;
      var strokeColor = nextProps.colorState.strokeColor.primary;
      var fillColorPresent = fillColor !== _stylePath.MIXED && fillColor !== null;
      var strokeColorPresent = nextProps.isBitmap ? false : strokeColor !== _stylePath.MIXED && strokeColor !== null && strokeWidth !== null && strokeWidth !== 0;

      if (!fillColorPresent && !strokeColorPresent) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
        this.props.onChangeStrokeColor(null);
      } else if (!fillColorPresent && strokeColorPresent) {
        this.props.onChangeFillColor(null);
      } else if (fillColorPresent && !strokeColorPresent) {
        this.props.onChangeStrokeColor(null);
      }

      if (!nextProps.font || Object.keys(_fonts.default).map(function (key) {
        return _fonts.default[key];
      }).indexOf(nextProps.font) < 0) {
        this.props.changeFont(_fonts.default.SANS_SERIF);
      }

      this.tool = new _textTool.default(this.props.textArea, this.props.setSelectedItems, this.props.clearSelectedItems, this.props.setCursor, this.props.onUpdateImage, this.props.setTextEditTarget, this.props.changeFont, nextProps.isBitmap);
      this.tool.setRtl(this.props.rtl);
      this.tool.setColorState(nextProps.colorState);
      this.tool.setFont(nextProps.font);
      this.tool.activate();

      if (textBoxToStartEditing) {
        this.tool.beginTextEdit(textBoxToStartEditing);
        this.props.textArea.select();
      }
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.tool.deactivateTool();
      this.tool.remove();
      this.tool = null;
    }
  }, {
    key: "render",
    value: function render() {
      return this.props.isBitmap ? /*#__PURE__*/_react.default.createElement(_bitTextMode.default, {
        isSelected: this.props.isTextModeActive,
        onMouseDown: this.props.handleChangeModeBitText
      }) : /*#__PURE__*/_react.default.createElement(_textMode.default, {
        isSelected: this.props.isTextModeActive,
        onMouseDown: this.props.handleChangeModeText
      });
    }
  }]);
  return TextMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    colorState: state.scratchPaint.color,
    font: state.scratchPaint.font,
    isTextModeActive: ownProps.isBitmap ? state.scratchPaint.mode === _modes.default.BIT_TEXT : state.scratchPaint.mode === _modes.default.TEXT,
    rtl: state.scratchPaint.layout.rtl,
    selectedItems: state.scratchPaint.selectedItems,
    textEditTarget: state.scratchPaint.textEditTarget,
    viewBounds: state.scratchPaint.viewBounds
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
  return {
    changeFont: function changeFont(font) {
      dispatch((0, _font.changeFont)(font));
    },
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearGradient: function clearGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    handleChangeModeBitText: function handleChangeModeBitText() {
      dispatch((0, _modes2.changeMode)(_modes.default.BIT_TEXT));
    },
    handleChangeModeText: function handleChangeModeText() {
      dispatch((0, _modes2.changeMode)(_modes.default.TEXT));
    },
    setCursor: function setCursor(cursorString) {
      dispatch((0, _cursor.setCursor)(cursorString));
    },
    setSelectedItems: function setSelectedItems() {
      dispatch((0, _selectedItems.setSelectedItems)((0, _selection.getSelectedLeafItems)(), ownProps.isBitmap));
    },
    setTextEditTarget: function setTextEditTarget(targetId) {
      dispatch((0, _textEditTarget.setTextEditTarget)(targetId));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    },
    onChangeStrokeColor: function onChangeStrokeColor(strokeColor) {
      dispatch((0, _strokeStyle.changeStrokeColor)(strokeColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TextMode);

exports.default = _default;