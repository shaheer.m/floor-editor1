"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _colorStyleProptype = _interopRequireDefault(require("../lib/color-style-proptype"));

var _blob = _interopRequireDefault(require("../helper/blob-tools/blob"));

var _stylePath = require("../helper/style-path");

var _fillStyle = require("../reducers/fill-style");

var _modes2 = require("../reducers/modes");

var _selectedItems = require("../reducers/selected-items");

var _selection = require("../helper/selection");

var _brushMode = _interopRequireDefault(require("../components/brush-mode/brush-mode"));

var BrushMode = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(BrushMode, _React$Component);

  var _super = (0, _createSuper2.default)(BrushMode);

  function BrushMode(props) {
    var _this;

    (0, _classCallCheck2.default)(this, BrushMode);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['activateTool', 'deactivateTool']);
    _this.blob = new _blob.default(_this.props.onUpdateImage, _this.props.clearSelectedItems);
    return _this;
  }

  (0, _createClass2.default)(BrushMode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.isBrushModeActive) {
        this.activateTool(this.props);
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.isBrushModeActive && !this.props.isBrushModeActive) {
        this.activateTool();
      } else if (!nextProps.isBrushModeActive && this.props.isBrushModeActive) {
        this.deactivateTool();
      } else if (nextProps.isBrushModeActive && this.props.isBrushModeActive) {
        var _nextProps$colorState = nextProps.colorState,
            fillColor = _nextProps$colorState.fillColor,
            strokeColor = _nextProps$colorState.strokeColor,
            strokeWidth = _nextProps$colorState.strokeWidth;
        this.blob.setOptions((0, _objectSpread2.default)({
          isEraser: false,
          fillColor: fillColor.primary,
          strokeColor: strokeColor.primary,
          strokeWidth: strokeWidth
        }, nextProps.brushModeState));
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.isBrushModeActive !== this.props.isBrushModeActive;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.blob.tool) {
        this.deactivateTool();
      }
    }
  }, {
    key: "activateTool",
    value: function activateTool() {
      // TODO: Instead of clearing selection, consider a kind of "draw inside"
      // analogous to how selection works with eraser
      (0, _selection.clearSelection)(this.props.clearSelectedItems);
      this.props.clearGradient(); // Force the default brush color if fill is MIXED or transparent

      var fillColor = this.props.colorState.fillColor.primary;

      if (fillColor === _stylePath.MIXED || fillColor === null) {
        this.props.onChangeFillColor(_fillStyle.DEFAULT_COLOR);
      }

      this.blob.activateTool((0, _objectSpread2.default)((0, _objectSpread2.default)({
        isEraser: false
      }, this.props.colorState), this.props.brushModeState));
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.blob.deactivateTool();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_brushMode.default, {
        isSelected: this.props.isBrushModeActive,
        onMouseDown: this.props.handleMouseDown
      });
    }
  }]);
  return BrushMode;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    brushModeState: state.scratchPaint.brushMode,
    colorState: state.scratchPaint.color,
    isBrushModeActive: state.scratchPaint.mode === _modes.default.BRUSH
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    clearSelectedItems: function clearSelectedItems() {
      dispatch((0, _selectedItems.clearSelectedItems)());
    },
    clearGradient: function clearGradient() {
      dispatch((0, _fillStyle.clearFillGradient)());
    },
    handleMouseDown: function handleMouseDown() {
      dispatch((0, _modes2.changeMode)(_modes.default.BRUSH));
    },
    onChangeFillColor: function onChangeFillColor(fillColor) {
      dispatch((0, _fillStyle.changeFillColor)(fillColor));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BrushMode);

exports.default = _default;