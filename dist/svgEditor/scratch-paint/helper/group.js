"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ungroupSelection = exports.ungroupItems = exports.shouldShowUngroup = exports.shouldShowGroup = exports.selectedSingleItem = exports.selectedAtleastOne = exports.isGroupChild = exports.isGroup = exports.groupSelection = exports.groupItems = exports.getItemsGroup = void 0;

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _item = require("./item");

var _selection = require("./selection");

var isGroup = function isGroup(item) {
  return (0, _item.isGroupItem)(item);
};
/**
 * Groups the given items. Other things are then deselected and the new group is selected.
 * @param {!Array<paper.Item>} items Root level items to group
 * @param {!function} clearSelectedItems Function to clear Redux state's selected items
 * @param {!function} setSelectedItems Function to set Redux state with new list of selected items
 * @param {!function} onUpdateImage Function to let listeners know that SVG has changed.
 * @return {paper.Group} the group if one is created, otherwise false.
 */


exports.isGroup = isGroup;

var groupItems = function groupItems(items, clearSelectedItems, setSelectedItems, onUpdateImage) {
  if (items.length > 0) {
    var group = new _paper.default.Group(items);
    (0, _selection.clearSelection)(clearSelectedItems);
    (0, _selection.setItemSelection)(group, true);

    for (var i = 0; i < group.children.length; i++) {
      group.children[i].selected = true;
    }

    setSelectedItems();
    onUpdateImage();
    return group;
  }

  return false;
};
/**
 * Groups the selected items. Other things are then deselected and the new group is selected.
 * @param {!function} clearSelectedItems Function to clear Redux state's selected items
 * @param {!function} setSelectedItems Function to set Redux state with new list of selected items
 * @param {!function} onUpdateImage Function to let listeners know that SVG has changed.
 * @return {paper.Group} the group if one is created, otherwise false.
 */


exports.groupItems = groupItems;

var groupSelection = function groupSelection(clearSelectedItems, setSelectedItems, onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)();
  return groupItems(items, clearSelectedItems, setSelectedItems, onUpdateImage);
};

exports.groupSelection = groupSelection;

var _ungroupLoop = function _ungroupLoop(group, recursive, setSelectedItems) {
  // Can't ungroup items that are not groups
  if (!group || !group.children || !isGroup(group)) return;
  group.applyMatrix = true; // iterate over group children recursively

  for (var i = 0; i < group.children.length; i++) {
    var groupChild = group.children[i];

    if (groupChild instanceof _paper.default.Group && groupChild.hasChildren()) {
      // recursion (groups can contain groups, ie. from SVG import)
      if (recursive) {
        _ungroupLoop(groupChild, recursive, setSelectedItems);

        continue;
      }

      if (groupChild.children.length === 1) {
        groupChild = groupChild.reduce();
      }
    }

    groupChild.applyMatrix = true; // move items from the group to the activeLayer (ungrouping)

    groupChild.insertBelow(group);

    if (setSelectedItems) {
      groupChild.selected = true;
    }

    i--;
  }
};
/**
 * Ungroups the given items. The new group is selected only if setSelectedItems is passed in.
 * onUpdateImage is called to notify listeners of a change on the SVG only if onUpdateImage is passed in.
 * The reason these arguments are optional on ungroupItems is because ungroupItems is used for parts of
 * SVG import, which shouldn't change the selection or undo state.
 *
 * @param {!Array<paper.Item>} items Items to ungroup if they are groups
 * @param {?function} setSelectedItems Function to set Redux state with new list of selected items
 * @param {?function} onUpdateImage Function to let listeners know that SVG has changed.
 */


var ungroupItems = function ungroupItems(items, setSelectedItems, onUpdateImage) {
  if (items.length === 0) {
    return;
  }

  var emptyGroups = [];

  for (var i = 0; i < items.length; i++) {
    var item = items[i];

    if (isGroup(item) && !item.data.isPGTextItem) {
      _ungroupLoop(item, false
      /* recursive */
      , setSelectedItems);

      if (!item.hasChildren()) {
        emptyGroups.push(item);
      }
    } else if (setSelectedItems) {
      item.selected = true;
    }
  }

  if (setSelectedItems) {
    setSelectedItems();
  } // remove all empty groups after ungrouping


  for (var j = 0; j < emptyGroups.length; j++) {
    emptyGroups[j].remove();
  } // @todo: enable/disable grouping icons


  if (onUpdateImage) {
    onUpdateImage();
  }
};
/**
 * Ungroups the selected items. Other items are deselected and the ungrouped items are selected.
 *
 * @param {!function} clearSelectedItems Function to clear Redux state's selected items
 * @param {!function} setSelectedItems Function to set Redux state with new list of selected items
 * @param {!function} onUpdateImage Function to let listeners know that SVG has changed.
 */


exports.ungroupItems = ungroupItems;

var ungroupSelection = function ungroupSelection(clearSelectedItems, setSelectedItems, onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)();
  (0, _selection.clearSelection)(clearSelectedItems);
  ungroupItems(items, setSelectedItems, onUpdateImage);
};

exports.ungroupSelection = ungroupSelection;

var getItemsGroup = function getItemsGroup(item) {
  var itemParent = item.parent;

  if (isGroup(itemParent)) {
    return itemParent;
  }

  return null;
};

exports.getItemsGroup = getItemsGroup;

var isGroupChild = function isGroupChild(item) {
  var rootItem = (0, _item.getRootItem)(item);
  return isGroup(rootItem);
};

exports.isGroupChild = isGroupChild;

var shouldShowGroup = function shouldShowGroup() {
  var items = (0, _selection.getSelectedRootItems)();
  return items.length > 1;
};

exports.shouldShowGroup = shouldShowGroup;

var selectedAtleastOne = function selectedAtleastOne() {
  var items = (0, _selection.getSelectedRootItems)();
  return items.length > 0;
};

exports.selectedAtleastOne = selectedAtleastOne;

var selectedSingleItem = function selectedSingleItem() {
  var items = (0, _selection.getSelectedRootItems)();

  if (items.length === 1) {
    return true;
  } else {
    return false;
  }
};

exports.selectedSingleItem = selectedSingleItem;

var shouldShowUngroup = function shouldShowUngroup() {
  var items = (0, _selection.getSelectedRootItems)();

  for (var i = 0; i < items.length; i++) {
    var item = items[i];

    if (isGroup(item) && !item.data.isPGTextItem && item.children && item.children.length > 0) {
      return true;
    }
  }

  return false;
};

exports.shouldShowUngroup = shouldShowUngroup;