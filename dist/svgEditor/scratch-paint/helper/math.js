"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortItemsByZIndex = exports.snapDeltaToAngle = exports.scaleWithStrokes = exports.getSquareDimensions = exports.getRandomInt = exports.getRandomBoolean = exports.expandBy = exports.ensureClockwise = exports.checkPointsClose = exports.HANDLE_RATIO = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

/** The ratio of the curve length to use for the handle length to convert squares into approximately circles. */
var HANDLE_RATIO = 0.3902628565;
exports.HANDLE_RATIO = HANDLE_RATIO;

var checkPointsClose = function checkPointsClose(startPos, eventPoint, threshold) {
  var xOff = Math.abs(startPos.x - eventPoint.x);
  var yOff = Math.abs(startPos.y - eventPoint.y);

  if (xOff < threshold && yOff < threshold) {
    return true;
  }

  return false;
};

exports.checkPointsClose = checkPointsClose;

var getRandomInt = function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

exports.getRandomInt = getRandomInt;

var getRandomBoolean = function getRandomBoolean() {
  return getRandomInt(0, 2) === 1;
}; // Thanks Mikko Mononen! https://github.com/memononen/stylii


exports.getRandomBoolean = getRandomBoolean;

var snapDeltaToAngle = function snapDeltaToAngle(delta, snapAngle) {
  var angle = Math.atan2(delta.y, delta.x);
  angle = Math.round(angle / snapAngle) * snapAngle;
  var dirx = Math.cos(angle);
  var diry = Math.sin(angle);
  var d = dirx * delta.x + diry * delta.y;
  return new _paper.default.Point(dirx * d, diry * d);
};

exports.snapDeltaToAngle = snapDeltaToAngle;

var _getDepth = function _getDepth(item) {
  var temp = item;
  var depth = 0;

  while (!(temp instanceof _paper.default.Layer)) {
    depth++;

    if (temp.parent === null) {
      // This item isn't attached to a layer, so it's not on the canvas and can't be compared.
      return null;
    }

    temp = temp.parent;
  }

  return depth;
};

var sortItemsByZIndex = function sortItemsByZIndex(a, b) {
  if (a === null || b === null) {
    return null;
  } // Get to the same depth in the project tree


  var tempA = a;
  var tempB = b;

  var aDepth = _getDepth(a);

  var bDepth = _getDepth(b);

  while (bDepth > aDepth) {
    tempB = tempB.parent;
    bDepth--;
  }

  while (aDepth > bDepth) {
    tempA = tempA.parent;
    aDepth--;
  } // Step up until they share parents. When they share parents, compare indices.


  while (tempA && tempB) {
    if (tempB === tempA) {
      return 0;
    } else if (tempB.parent === tempA.parent) {
      if (tempB.parent instanceof _paper.default.CompoundPath) {
        // Neither is on top of the other in a compound path. Return in order of decreasing size.
        return Math.abs(tempB.area) - Math.abs(tempA.area);
      }

      return parseFloat(tempA.index) - parseFloat(tempB.index);
    }

    tempB = tempB.parent;
    tempA = tempA.parent;
  } // No shared hierarchy


  return null;
}; // Expand the size of the path by amount all around


exports.sortItemsByZIndex = sortItemsByZIndex;

var expandBy = function expandBy(path, amount) {
  var center = path.position;
  var pathArea = path.area;

  var _iterator = (0, _createForOfIteratorHelper2.default)(path.segments),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var seg = _step.value;
      var delta = seg.point.subtract(center).normalize().multiply(amount);
      seg.point = seg.point.add(delta); // If that made the path area smaller, go the other way.

      if (path.area < pathArea) seg.point = seg.point.subtract(delta.multiply(2));
      pathArea = path.area;
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
}; // Do for all nested items in groups


exports.expandBy = expandBy;

var _doRecursively = function _doRecursively(item, func) {
  if (item instanceof _paper.default.Group) {
    var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.children),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var child = _step2.value;

        _doRecursively(child, func);
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  } else {
    func(item);
  }
}; // Make item clockwise. Drill down into groups.


var ensureClockwise = function ensureClockwise(root) {
  _doRecursively(root, function (item) {
    if (item instanceof _paper.default.PathItem) {
      item.clockwise = true;
    }
  });
}; // Scale item and its strokes by factor


exports.ensureClockwise = ensureClockwise;

var scaleWithStrokes = function scaleWithStrokes(root, factor, pivot) {
  _doRecursively(root, function (item) {
    if (item instanceof _paper.default.PointText) {
      // Text outline size is controlled by text transform matrix, thus it's already scaled.
      return;
    }

    if (item.strokeWidth) {
      item.strokeWidth = item.strokeWidth * factor;
    }
  });

  root.scale(factor, pivot);
};
/**
 * Get the size and position of a square, as in if the user were holding the shift key down while drawing the shape,
 * from the point where the drag started and the point where the mouse is currently positioned. (Note: This also works
 * for shapes like circles ("square ovals"), which fill the same dimensions.)
 * @param {!paper.Point} startPos The point where the user started dragging
 * @param {!paper.Point} eventPoint The point where the user has currently dragged to
 * @return {object} Information about the size and position of how the square should be drawn
 */


exports.scaleWithStrokes = scaleWithStrokes;

var getSquareDimensions = function getSquareDimensions(startPos, eventPoint) {
  // These variables are used for determining the relative quadrant that the shape will appear in.
  // So if you drag up and right, it'll show up above and to the right of where you started dragging, etc.
  var offsetX = eventPoint.x - startPos.x;
  var offsetY = eventPoint.y - startPos.y; // If the offset variables are zero, the shape ends up having zero width or height, which is bad.
  // Deal with this by forcing them to be non-zero (we arbitrarily choose 1; any non-zero value would work).

  offsetX = offsetX ? offsetX : 1;
  offsetY = offsetY ? offsetY : 1; // The length of the shape is the greater of the X and Y offsets.

  var offsetDistance = eventPoint.subtract(startPos).abs();
  var length = Math.max(offsetDistance.x, offsetDistance.y);
  var size = new _paper.default.Point(length * offsetX / Math.abs(offsetX), length * offsetY / Math.abs(offsetY));
  var position = startPos.add(size.multiply(0.5));
  return {
    size: size,
    position: position
  };
};

exports.getSquareDimensions = getSquareDimensions;