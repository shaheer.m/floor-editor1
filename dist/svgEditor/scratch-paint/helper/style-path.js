"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.swapColorsInSelection = exports.styleShape = exports.styleCursorPreview = exports.styleBlob = exports.removeParentAttributesFromChildren = exports.getColorsFromSelection = exports.generateSecondaryColor = exports.createGradientObject = exports.applyStrokeWidthToSelection = exports.applyNameAttributesToSelection = exports.applyGroupNameToSelection = exports.applyGradientTypeToSelection = exports.applyColorToSelection = exports._getColorStateListeners = exports.MIXED = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _selection = require("./selection");

var _item = require("./item");

var _group = require("./group");

var _gradientTypes = _interopRequireDefault(require("../lib/gradient-types"));

var _fillStyle = require("../reducers/fill-style");

var _compoundPath = require("./compound-path");

var _log = _interopRequireDefault(require("../log/log"));

var _messages = _interopRequireDefault(require("../lib/messages.js"));

var MIXED = 'scratch-paint/style-path/mixed'; // Check if the item color matches the incoming color. If the item color is a gradient, we assume
// that the incoming color never matches, since we don't support gradients yet.

exports.MIXED = MIXED;

var _colorMatch = function _colorMatch(itemColor, incomingColor) {
  if (itemColor && itemColor.type === 'gradient') return false; // Either both are null or both are the same color when converted to CSS.

  return !itemColor && !incomingColor || itemColor && incomingColor && itemColor.toCSS() === new _paper.default.Color(incomingColor).toCSS();
}; // Selected items and currently active text edit items respond to color changes.


var _getColorStateListeners = function _getColorStateListeners(textEditTargetId) {
  var withGroup = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var selectedItems;

  if (!withGroup) {
    selectedItems = (0, _selection.getSelectedLeafItems)();
  } else {
    selectedItems = (0, _selection.getSelectedLeafItemsWithGroup)();
  }

  var items = selectedItems;

  if (textEditTargetId) {
    var matches = (0, _selection.getItems)({
      match: function match(item) {
        return item.id === textEditTargetId;
      }
    });

    if (matches.length) {
      items.push(matches[0]);
    }
  }

  return items;
};
/**
 * Transparent R, G, B values need to match the other color of the gradient
 * in order to form a smooth gradient, otherwise it fades through black. This
 * function gets the transparent color for a given color string.
 * @param {?string} colorToMatch CSS string of other color of gradient, or null for transparent
 * @return {string} CSS string for matching color of transparent
 */


exports._getColorStateListeners = _getColorStateListeners;

var getColorStringForTransparent = function getColorStringForTransparent(colorToMatch) {
  var color = new _paper.default.Color(colorToMatch);
  color.alpha = 0;
  return color.toCSS();
};
/**
 * Generate a color that contrasts well with the passed-in color.
 * @param {string} firstColor The "primary" color
 * @return {string} CSS string for generated color
 */


var generateSecondaryColor = function generateSecondaryColor(firstColor) {
  if (firstColor === MIXED) return null;
  var color = new _paper.default.Color(firstColor);
  if (!firstColor || color.alpha === 0) return _fillStyle.DEFAULT_COLOR;
  color.type = 'hsb';
  var desaturated = color.saturation <= 0.15; // If the color is desaturated or dark enough that a hue shift would be hard to see, do a brightness shift.

  if (desaturated || color.brightness <= 0.4) {
    // Choose the shade that contrasts the most with the given color.
    // Use a brightness of 0.1 instead of 0 because if the brightness is 0, it's black and we lose the hue.
    color.brightness = color.brightness < 0.55 ? 1 : 0.1;
  } // If the color was desaturated, don't do a hue shift, as it would be hard to see anyway.


  if (!desaturated) {
    color.hue -= 72;
  } // The returned color will be one of three things:
  // 1. If the color was bright and saturated (e.g. colorful), it will be that color, hue-shifted.
  // 2. If the color was dark and saturated, it will be that color, brightened and hue-shifted.
  // 3. If the color was not saturated, it will be that color, brightened or darkened as needed to contrast most.


  return color.toCSS(true
  /* hex */
  );
};
/**
 * Convert params to a paper.Color gradient object
 * @param {?string} color1 CSS string, or null for transparent
 * @param {?string} color2 CSS string, or null for transparent
 * @param {GradientType} gradientType gradient type
 * @param {paper.Rectangle} bounds Bounds of the object
 * @param {?paper.Point} [radialCenter] Where the center of a radial gradient should be, if the gradient is radial.
 * Defaults to center of bounds.
 * @param {number} [minSize] The minimum width/height of the gradient object.
 * @return {paper.Color} Color object with gradient, may be null or color string if the gradient type is solid
 */


exports.generateSecondaryColor = generateSecondaryColor;

var createGradientObject = function createGradientObject(color1, color2, gradientType, bounds, radialCenter, minSize) {
  if (gradientType === _gradientTypes.default.SOLID) return color1;

  if (color1 === null) {
    color1 = getColorStringForTransparent(color2);
  }

  if (color2 === null) {
    color2 = getColorStringForTransparent(color1);
  } // Force gradients to have a minimum length. If the gradient start and end points are the same or very close
  // (e.g. applying a vertical gradient to a perfectly horizontal line or vice versa), the gradient will not appear.


  if (!minSize) minSize = 1e-2;
  var start;
  var end;

  switch (gradientType) {
    case _gradientTypes.default.HORIZONTAL:
      {
        // clone these points so that adding/subtracting doesn't affect actual bounds
        start = bounds.leftCenter.clone();
        end = bounds.rightCenter.clone();
        var gradientSize = Math.abs(end.x - start.x);

        if (gradientSize < minSize) {
          var sizeDiff = (minSize - gradientSize) / 2;
          end.x += sizeDiff;
          start.x -= sizeDiff;
        }

        break;
      }

    case _gradientTypes.default.VERTICAL:
      {
        // clone these points so that adding/subtracting doesn't affect actual bounds
        start = bounds.topCenter.clone();
        end = bounds.bottomCenter.clone();

        var _gradientSize = Math.abs(end.y - start.y);

        if (_gradientSize < minSize) {
          var _sizeDiff = (minSize - _gradientSize) / 2;

          end.y += _sizeDiff;
          start.y -= _sizeDiff;
        }

        break;
      }

    case _gradientTypes.default.RADIAL:
      {
        var halfLongestDimension = Math.max(bounds.width, bounds.height) / 2;
        start = radialCenter || bounds.center;
        end = start.add(new _paper.default.Point(Math.max(halfLongestDimension, minSize / 2), 0));
        break;
      }
  }

  return {
    gradient: {
      stops: [color1, color2],
      radial: gradientType === _gradientTypes.default.RADIAL
    },
    origin: start,
    destination: end
  };
};
/**
 * Called when setting an item's color
 * @param {string} colorString color, css format, or null if completely transparent
 * @param {number} colorIndex index of color being changed
 * @param {boolean} isSolidGradient True if is solid gradient. Sometimes the item has a gradient but the color
 *     picker is set to a solid gradient. This happens when a mix of colors and gradient types is selected.
 *     When changing the color in this case, the solid gradient should override the existing gradient on the item.
 * @param {?boolean} applyToStroke True if changing the selection's stroke, false if changing its fill.
 * @param {?string} textEditTargetId paper.Item.id of text editing target, if any
 * @return {boolean} Whether the color application actually changed visibly.
 */


exports.createGradientObject = createGradientObject;

var applyColorToSelection = function applyColorToSelection(colorString, colorIndex, isSolidGradient, applyToStroke, textEditTargetId) {
  var items = _getColorStateListeners(textEditTargetId);

  var changed = false;

  var _iterator = (0, _createForOfIteratorHelper2.default)(items),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var item = _step.value;

      if (item.parent instanceof _paper.default.CompoundPath) {
        item = item.parent;
      }

      var itemColorProp = applyToStroke ? 'strokeColor' : 'fillColor';
      var itemColor = item[itemColorProp];

      if (isSolidGradient || !itemColor || !itemColor.gradient || !itemColor.gradient.stops.length === 2) {
        // Applying a solid color
        if (!_colorMatch(itemColor, colorString)) {
          changed = true;

          if ((0, _item.isPointTextItem)(item) && !colorString) {
            // Allows transparent text to be hit
            item[itemColorProp] = 'rgba(0,0,0,0)';
          } else {
            item[itemColorProp] = colorString;
          }
        }
      } else if (!_colorMatch(itemColor.gradient.stops[colorIndex].color, colorString)) {
        // Changing one color of an existing gradient
        changed = true;
        var otherIndex = colorIndex === 0 ? 1 : 0;

        if (colorString === null) {
          colorString = getColorStringForTransparent(itemColor.gradient.stops[otherIndex].color.toCSS());
        }

        var colors = [0, 0];
        colors[colorIndex] = colorString; // If the other color is transparent, its RGB values need to be adjusted for the gradient to be smooth

        if (itemColor.gradient.stops[otherIndex].color.alpha === 0) {
          colors[otherIndex] = getColorStringForTransparent(colorString);
        } else {
          colors[otherIndex] = itemColor.gradient.stops[otherIndex].color.toCSS();
        } // There seems to be a bug where setting colors on stops doesn't always update the view, so set gradient.


        itemColor.gradient = {
          stops: colors,
          radial: itemColor.gradient.radial
        };
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return changed;
};
/**
 * Called to swap gradient colors
 * @param {?boolean} applyToStroke True if changing the selection's stroke, false if changing its fill.
 * @param {?string} textEditTargetId paper.Item.id of text editing target, if any
 * @return {boolean} Whether the color application actually changed visibly.
 */


exports.applyColorToSelection = applyColorToSelection;

var swapColorsInSelection = function swapColorsInSelection(applyToStroke, textEditTargetId) {
  var items = _getColorStateListeners(textEditTargetId);

  var changed = false;

  var _iterator2 = (0, _createForOfIteratorHelper2.default)(items),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var item = _step2.value;
      // If an item is a child path, do not swap colors.
      // At some point, we'll iterate over its parent path, and we don't want to swap colors twice--
      // that would leave us right where we started.
      if ((0, _compoundPath.isCompoundPathChild)(item)) continue;
      var itemColor = applyToStroke ? item.strokeColor : item.fillColor;

      if (!itemColor || !itemColor.gradient || !itemColor.gradient.stops.length === 2) {
        // Only one color; nothing to swap
        continue;
      } else if (!itemColor.gradient.stops[0].color.equals(itemColor.gradient.stops[1].color)) {
        // Changing one color of an existing gradient
        changed = true;
        var colors = [itemColor.gradient.stops[1].color.toCSS(), itemColor.gradient.stops[0].color.toCSS()]; // There seems to be a bug where setting colors on stops doesn't always update the view, so set gradient.

        itemColor.gradient = {
          stops: colors,
          radial: itemColor.gradient.radial
        };
      }
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  return changed;
};
/**
 * Called when setting gradient type
 * @param {GradientType} gradientType gradient type
 * @param {?boolean} applyToStroke True if changing the selection's stroke, false if changing its fill.
 * @param {?string} textEditTargetId paper.Item.id of text editing target, if any
 * @return {boolean} Whether the color application actually changed visibly.
 */


exports.swapColorsInSelection = swapColorsInSelection;

var applyGradientTypeToSelection = function applyGradientTypeToSelection(gradientType, applyToStroke, textEditTargetId) {
  var items = _getColorStateListeners(textEditTargetId);

  var changed = false;

  var _iterator3 = (0, _createForOfIteratorHelper2.default)(items),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      var item = _step3.value;

      if (item.parent instanceof _paper.default.CompoundPath) {
        item = item.parent;
      }

      var itemColorProp = applyToStroke ? 'strokeColor' : 'fillColor';
      var itemColor = item[itemColorProp];
      var hasGradient = itemColor && itemColor.gradient;
      var itemColor1 = void 0;

      if (itemColor === null || itemColor.alpha === 0) {
        // Transparent
        itemColor1 = null;
      } else if (!hasGradient) {
        // Solid color
        itemColor1 = itemColor.toCSS();
      } else if (!itemColor.gradient.stops[0] || itemColor.gradient.stops[0].color.alpha === 0) {
        // Gradient where first color is transparent
        itemColor1 = null;
      } else {
        // Gradient where first color is not transparent
        itemColor1 = itemColor.gradient.stops[0].color.toCSS();
      }

      var itemColor2 = void 0;

      if (!hasGradient || !itemColor.gradient.stops[1]) {
        // If item color is solid or a gradient that has no 2nd color, set the 2nd color based on the first color
        itemColor2 = generateSecondaryColor(itemColor1);
      } else if (itemColor.gradient.stops[1].color.alpha === 0) {
        // Gradient has 2nd color which is transparent
        itemColor2 = null;
      } else {
        // Gradient has 2nd color which is not transparent
        itemColor2 = itemColor.gradient.stops[1].color.toCSS();
      }

      if (gradientType === _gradientTypes.default.SOLID) {
        if (itemColor && itemColor.gradient) {
          changed = true;
          item[itemColorProp] = itemColor1;
        }

        continue;
      } // If this is a stroke, we don't display it as having a gradient in the color picker
      // if there's no stroke width. Then treat it as if it doesn't have a gradient.


      var hasDisplayGradient = hasGradient;
      if (applyToStroke) hasDisplayGradient = hasGradient && item.strokeWidth > 0;

      if (!hasDisplayGradient) {
        var noColorOriginally = !itemColor || itemColor.gradient && itemColor.gradient.stops && itemColor.gradient.stops[0].color.alpha === 0;
        var addingStroke = applyToStroke && item.strokeWidth === 0;
        var hasGradientNow = itemColor1 || itemColor2;

        if ((noColorOriginally || addingStroke) && hasGradientNow) {
          if (applyToStroke) {
            // Make outline visible
            item.strokeWidth = 1;
          } // Make the gradient black to white


          itemColor1 = 'black';
          itemColor2 = 'white';
        }
      }

      if (itemColor1 === null) {
        itemColor1 = getColorStringForTransparent(itemColor2);
      }

      if (itemColor2 === null) {
        itemColor2 = getColorStringForTransparent(itemColor1);
      }

      var gradientTypeDiffers = false; // If the item's gradient type differs from the gradient type we want to apply, then we change it

      switch (gradientType) {
        case _gradientTypes.default.RADIAL:
          {
            var hasRadialGradient = hasDisplayGradient && itemColor.gradient.radial;
            gradientTypeDiffers = !hasRadialGradient;
            break;
          }

        case _gradientTypes.default.HORIZONTAL:
          {
            var hasHorizontalGradient = hasDisplayGradient && !itemColor.gradient.radial && Math.abs(itemColor.origin.y - itemColor.destination.y) < 1e-8;
            gradientTypeDiffers = !hasHorizontalGradient;
            break;
          }

        case _gradientTypes.default.VERTICAL:
          {
            var hasVerticalGradient = hasDisplayGradient && !itemColor.gradient.radial && Math.abs(itemColor.origin.x - itemColor.destination.x) < 1e-8;
            gradientTypeDiffers = !hasVerticalGradient;
            break;
          }
      }

      if (gradientTypeDiffers) {
        changed = true;
        item[itemColorProp] = createGradientObject(itemColor1, itemColor2, gradientType, item.bounds, null, // radialCenter
        item.strokeWidth);
      }
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }

  return changed;
};
/**
 * Called when setting stroke width
 * @param {number} value New stroke width
 * @param {?string} textEditTargetId paper.Item.id of text editing target, if any
 * @return {boolean} Whether the color application actually changed visibly.
 */


exports.applyGradientTypeToSelection = applyGradientTypeToSelection;

var applyStrokeWidthToSelection = function applyStrokeWidthToSelection(value, textEditTargetId) {
  var changed = false;

  var items = _getColorStateListeners(textEditTargetId);

  var _iterator4 = (0, _createForOfIteratorHelper2.default)(items),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      var item = _step4.value;

      if (item.parent instanceof _paper.default.CompoundPath) {
        item = item.parent;
      }

      if ((0, _group.isGroup)(item)) {
        continue;
      } else if (item.strokeWidth !== value) {
        item.strokeWidth = value;
        changed = true;
      }
    }
  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }

  return changed;
};

exports.applyStrokeWidthToSelection = applyStrokeWidthToSelection;

var applyGroupNameToSelection = function applyGroupNameToSelection(value, textEditTargetId) {
  var changed = false;

  var items = _getColorStateListeners(textEditTargetId, 1);

  var _iterator5 = (0, _createForOfIteratorHelper2.default)(items),
      _step5;

  try {
    for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
      var item = _step5.value;

      // if (item.parent instanceof paper.CompoundPath) {
      //     item = item.parent;
      // }
      // if (isGroup(item)) {
      //     continue;
      // } else if (item.strokeWidth !== value) {
      //     item.strokeWidth = value;
      //     changed = true;
      // }
      if (value !== null) {
        item._data.groupName = value;
      }

      if (item._children) {
        applyToChildren(item._children, value);
      }
    }
  } catch (err) {
    _iterator5.e(err);
  } finally {
    _iterator5.f();
  }

  return changed;
};

exports.applyGroupNameToSelection = applyGroupNameToSelection;

var applyToChildren = function applyToChildren(items, value) {
  var _iterator6 = (0, _createForOfIteratorHelper2.default)(items),
      _step6;

  try {
    for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
      var item = _step6.value;
      item._data.groupName = value;

      if (item._children) {
        applyToChildren(item._children, value);
      }
    }
  } catch (err) {
    _iterator6.e(err);
  } finally {
    _iterator6.f();
  }
};

var applyNameAttributesToSelection = function applyNameAttributesToSelection(value, attributes, unavailable, capacity, groupName, textEditTargetId) {
  var changed = false;
  var allItems = (0, _selection.getAllSelectableRootItems)();
  var nameStack = (0, _selection.nameStackOfAllItems)(allItems);
  var nameExists = nameStack.indexOf(value);
  var checkIfSingleGroup = (0, _group.selectedSingleItem)();

  var items = _getColorStateListeners(textEditTargetId, 1);

  var msg = '';

  if (items.length === 1) {
    // items[0]._data.customName = value;
    items[0]._data.customAttributes = attributes;
    items[0]._data.unavailable = unavailable;

    if (!unavailable && 'unavailable' in items[0]._data) {
      delete items[0]._data.unavailable;
    }

    items[0]._data.capacity = capacity;
    items[0]._data.groupName = groupName;
    changed = true;

    if (nameExists === -1) {
      //  items[0].name = value;
      items[0]._data.id = value;
    } else if (items[0]._data.id !== value) {
      msg = _messages.default.errorDuplicateName.description;
    }
  } else {
    // const checkIfSingleParent = checkSingleParent(items);
    if (checkIfSingleGroup) {
      var item = (0, _selection.returnGreatestParent)(items);

      if (item) {
        //item._data.customName = value;
        item._data.customAttributes = attributes;
        item._data.unavailable = unavailable;

        if (!unavailable && 'unavailable' in item._data) {
          delete item._data.unavailable;
        }

        item._data.capacity = capacity;
        item._data.groupName = groupName;
        changed = true;

        if (item.className && item.className === 'Group' && item._children && attributes.some(function (obj) {
          return obj.value === "clickable";
        })) {
          removeAttributesFromChildren(item._children); // // created copy of attribute array
          // const tempatr = Object.assign([], attributes);
          // const indexOfClickable = tempatr.findIndex((obj) => obj.value === "clickable");
          // if (indexOfClickable !== -1) { 
          //     tempatr.splice(indexOfClickable, 1);
          // }

          var parentData = {
            id: value,
            customAttributes: attributes,
            capacity: capacity,
            groupName: groupName
          };

          if (unavailable) {
            parentData.unavailable = unavailable;
          }

          applyParentAttributesToChildren(item._children, parentData);
        }

        if (nameExists === -1) {
          //  item.name = value;
          item._data.id = value;
        } else if (item._data.id !== value) {
          msg = _messages.default.errorDuplicateName.description;
        }
      }
    } else {// alert('not a single parent');
    }
  }

  return [changed, msg];
}; //Remove 'clickable' from children if parent is clickable


exports.applyNameAttributesToSelection = applyNameAttributesToSelection;

var removeAttributesFromChildren = function removeAttributesFromChildren(children) {
  var _iterator7 = (0, _createForOfIteratorHelper2.default)(children),
      _step7;

  try {
    for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
      var item = _step7.value;

      if (item && item._data) {
        if (item._data.unavailable) {
          delete item._data.unavailable;
        }
      }

      if (item && item.className === 'Group' && item._children) {
        removeAttributesFromChildren(item._children);
      }
    }
  } catch (err) {
    _iterator7.e(err);
  } finally {
    _iterator7.f();
  }
}; // when a parent is a clickable entity, add its properties(except clickable & unavailable) 
// as a separate field to all its nested children for ease of use in interactive component
// Clickable and unavailable should be only for the parent attribute; else it creates issue in the interactive component


var applyParentAttributesToChildren = function applyParentAttributesToChildren(children, parentData) {
  var _iterator8 = (0, _createForOfIteratorHelper2.default)(children),
      _step8;

  try {
    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
      var item = _step8.value;
      item._data.parentData = parentData;

      if (item.className === 'Group' && item._children) {
        applyParentAttributesToChildren(item._children, parentData);
      }
    }
  } catch (err) {
    _iterator8.e(err);
  } finally {
    _iterator8.f();
  }
};

var removeParentAttributesFromChildren = function removeParentAttributesFromChildren(children) {
  var _iterator9 = (0, _createForOfIteratorHelper2.default)(children),
      _step9;

  try {
    for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
      var item = _step9.value;

      if (item._data && item._data.parentData) {
        delete item._data.parentData;
      }

      if (item.className === 'Group' && item._children) {
        removeParentAttributesFromChildren(item._children);
      }
    }
  } catch (err) {
    _iterator9.e(err);
  } finally {
    _iterator9.f();
  }
};

exports.removeParentAttributesFromChildren = removeParentAttributesFromChildren;

var _colorStateFromGradient = function _colorStateFromGradient(gradient) {
  var colorState = {}; // Scratch only recognizes 2 color gradients

  if (gradient.stops.length === 2) {
    if (gradient.radial) {
      colorState.gradientType = _gradientTypes.default.RADIAL;
    } else {
      // Always use horizontal for linear gradients, since horizontal and vertical gradients
      // are the same with rotation. We don't want to show MIXED just because anything is rotated.
      colorState.gradientType = _gradientTypes.default.HORIZONTAL;
    }

    colorState.primary = gradient.stops[0].color.alpha === 0 ? null : gradient.stops[0].color.toCSS();
    colorState.secondary = gradient.stops[1].color.alpha === 0 ? null : gradient.stops[1].color.toCSS();
  } else {
    if (gradient.stops.length < 2) _log.default.warn("Gradient has ".concat(gradient.stops.length, " stop(s)"));
    colorState.primary = MIXED;
    colorState.secondary = MIXED;
  }

  return colorState;
};
/**
 * Get state of colors and stroke width for selection
 * @param {!Array<paper.Item>} selectedItems Selected paper items
 * @param {?boolean} bitmapMode True if the item is being selected in bitmap mode
 * @return {?object} Object of strokeColor, strokeWidth, fillColor, thickness of the selection.
 *     Gives MIXED when there are mixed values for a color, and null for transparent.
 *     Gives null when there are mixed values for stroke width.
 *     Thickness is line thickness, used in the bitmap editor
 */


var getColorsFromSelection = function getColorsFromSelection(selectedItems, bitmapMode) {
  // TODO: DRY out this code
  var selectionFillColorString;
  var selectionFillColor2String;
  var selectionStrokeColorString;
  var selectionStrokeColor2String;
  var selectionStrokeWidth;
  var selectionThickness;
  var selectionFillGradientType;
  var selectionStrokeGradientType;
  var firstChild = true;

  var _iterator10 = (0, _createForOfIteratorHelper2.default)(selectedItems),
      _step10;

  try {
    for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
      var item = _step10.value;

      if (item.parent instanceof _paper.default.CompoundPath) {
        // Compound path children inherit fill and stroke color from their parent.
        item = item.parent;
      }

      var itemFillColorString = void 0;
      var itemFillColor2String = void 0;
      var itemStrokeColorString = void 0;
      var itemStrokeColor2String = void 0;
      var itemFillGradientType = _gradientTypes.default.SOLID;
      var itemStrokeGradientType = _gradientTypes.default.SOLID;

      if (!(0, _group.isGroup)(item)) {
        if (item.fillColor) {
          // hack bc text items with null fill can't be detected by fill-hitTest anymore
          if ((0, _item.isPointTextItem)(item) && item.fillColor.alpha === 0) {
            itemFillColorString = null;
          } else if (item.fillColor.type === 'gradient') {
            var _colorStateFromGradie = _colorStateFromGradient(item.fillColor.gradient),
                primary = _colorStateFromGradie.primary,
                secondary = _colorStateFromGradie.secondary,
                gradientType = _colorStateFromGradie.gradientType;

            itemFillColorString = primary;
            itemFillColor2String = secondary;
            itemFillGradientType = gradientType;
          } else {
            itemFillColorString = item.fillColor.alpha === 0 ? null : item.fillColor.toCSS();
            itemFillColor2String = null;
          }
        }

        if (item.strokeColor) {
          if (item.strokeColor.type === 'gradient') {
            var _colorStateFromGradie2 = _colorStateFromGradient(item.strokeColor.gradient),
                _primary = _colorStateFromGradie2.primary,
                _secondary = _colorStateFromGradie2.secondary,
                _gradientType = _colorStateFromGradie2.gradientType;

            var strokeColorString = _primary;
            var strokeColor2String = _secondary;
            var strokeGradientType = _gradientType; // If the item's stroke width is 0, pretend the stroke color is null

            if (!item.strokeWidth) {
              strokeColorString = null; // Hide the second color. This way if you choose a second color, remove
              // the gradient, and re-add it, your second color selection is preserved.

              strokeGradientType = _gradientTypes.default.SOLID;
            } // Stroke color is fill color in bitmap


            if (bitmapMode) {
              itemFillColorString = strokeColorString;
              itemFillColor2String = strokeColor2String;
              itemFillGradientType = strokeGradientType;
            } else {
              itemStrokeColorString = strokeColorString;
              itemStrokeColor2String = strokeColor2String;
              itemStrokeGradientType = strokeGradientType;
            }
          } else {
            var _strokeColorString = item.strokeColor.alpha === 0 || !item.strokeWidth ? null : item.strokeColor.toCSS(); // Stroke color is fill color in bitmap


            if (bitmapMode) {
              itemFillColorString = _strokeColorString;
            } else {
              itemStrokeColorString = _strokeColorString;
            }
          }
        } else {
          itemStrokeColorString = null;
          itemStrokeColor2String = null;
        } // check every style against the first of the items


        if (firstChild) {
          firstChild = false;
          selectionFillColorString = itemFillColorString;
          selectionFillColor2String = itemFillColor2String;
          selectionStrokeColorString = itemStrokeColorString;
          selectionStrokeColor2String = itemStrokeColor2String;
          selectionFillGradientType = itemFillGradientType;
          selectionStrokeGradientType = itemStrokeGradientType;
          selectionStrokeWidth = itemStrokeColorString || itemStrokeColor2String ? item.strokeWidth : 0;

          if (item.strokeWidth && item.data && item.data.zoomLevel) {
            selectionThickness = item.strokeWidth / item.data.zoomLevel;
          }
        }

        if (itemFillColorString !== selectionFillColorString) {
          selectionFillColorString = MIXED;
        }

        if (itemFillColor2String !== selectionFillColor2String) {
          selectionFillColor2String = MIXED;
        }

        if (itemFillGradientType !== selectionFillGradientType) {
          selectionFillGradientType = _gradientTypes.default.SOLID;
          selectionFillColorString = MIXED;
          selectionFillColor2String = MIXED;
        }

        if (itemStrokeGradientType !== selectionStrokeGradientType) {
          selectionStrokeGradientType = _gradientTypes.default.SOLID;
          selectionStrokeColorString = MIXED;
          selectionStrokeColor2String = MIXED;
        }

        if (itemStrokeColorString !== selectionStrokeColorString) {
          selectionStrokeColorString = MIXED;
        }

        if (itemStrokeColor2String !== selectionStrokeColor2String) {
          selectionStrokeColor2String = MIXED;
        }

        var itemStrokeWidth = itemStrokeColorString || itemStrokeColor2String ? item.strokeWidth : 0;

        if (selectionStrokeWidth !== itemStrokeWidth) {
          selectionStrokeWidth = null;
        }
      }
    } // Convert selection gradient type from horizontal to vertical if first item is exactly vertical
    // This is because up to this point, we assume all non-radial gradients are horizontal
    // Otherwise, if there were a mix of horizontal/vertical gradient types in the selection, they would show as MIXED
    // whereas we want them to show as horizontal (or vertical if the first item is vertical)

  } catch (err) {
    _iterator10.e(err);
  } finally {
    _iterator10.f();
  }

  if (selectedItems && selectedItems.length) {
    var firstItem = selectedItems[0];
    if (firstItem.parent instanceof _paper.default.CompoundPath) firstItem = firstItem.parent;

    if (selectionFillGradientType !== _gradientTypes.default.SOLID) {
      // Stroke color is fill color in bitmap if fill color is missing
      // TODO: this whole "treat horizontal/vertical gradients specially" logic is janky; refactor at some point
      var firstItemColor = bitmapMode && firstItem.strokeColor ? firstItem.strokeColor : firstItem.fillColor;
      var direction = firstItemColor.destination.subtract(firstItemColor.origin);

      if (Math.abs(direction.angle) === 90) {
        selectionFillGradientType = _gradientTypes.default.VERTICAL;
      }
    }

    if (selectionStrokeGradientType !== _gradientTypes.default.SOLID) {
      var _direction = firstItem.strokeColor.destination.subtract(firstItem.strokeColor.origin);

      if (Math.abs(_direction.angle) === 90) {
        selectionStrokeGradientType = _gradientTypes.default.VERTICAL;
      }
    }
  }

  if (bitmapMode) {
    return {
      fillColor: selectionFillColorString ? selectionFillColorString : null,
      fillColor2: selectionFillColor2String ? selectionFillColor2String : null,
      fillGradientType: selectionFillGradientType,
      thickness: selectionThickness
    };
  }

  return {
    fillColor: selectionFillColorString ? selectionFillColorString : null,
    fillColor2: selectionFillColor2String ? selectionFillColor2String : null,
    fillGradientType: selectionFillGradientType,
    strokeColor: selectionStrokeColorString ? selectionStrokeColorString : null,
    strokeColor2: selectionStrokeColor2String ? selectionStrokeColor2String : null,
    strokeGradientType: selectionStrokeGradientType,
    strokeWidth: selectionStrokeWidth || selectionStrokeWidth === null ? selectionStrokeWidth : 0
  };
};

exports.getColorsFromSelection = getColorsFromSelection;

var styleBlob = function styleBlob(path, options) {
  if (options.isEraser) {
    path.fillColor = 'white';
  } else if (options.fillColor) {
    path.fillColor = options.fillColor;
  } else {
    // Make sure something visible is drawn
    path.fillColor = 'black';
  }
};

exports.styleBlob = styleBlob;

var styleCursorPreview = function styleCursorPreview(path, options) {
  if (options.isEraser) {
    path.fillColor = 'white';
    path.strokeColor = 'cornflowerblue';
    path.strokeWidth = 1;
  } else if (options.fillColor) {
    path.fillColor = options.fillColor;
  } else {
    // Make sure something visible is drawn
    path.fillColor = 'black';
  }
};

exports.styleCursorPreview = styleCursorPreview;

var styleShape = function styleShape(path, options) {
  for (var _i = 0, _arr = ['fillColor', 'strokeColor']; _i < _arr.length; _i++) {
    var colorKey = _arr[_i];

    if (options[colorKey] === null) {
      path[colorKey] = null;
    } else if (options[colorKey].gradientType === _gradientTypes.default.SOLID) {
      path[colorKey] = options[colorKey].primary;
    } else {
      var _options$colorKey = options[colorKey],
          primary = _options$colorKey.primary,
          secondary = _options$colorKey.secondary,
          gradientType = _options$colorKey.gradientType;
      path[colorKey] = createGradientObject(primary, secondary, gradientType, path.bounds, null, // radialCenter
      options.strokeWidth // minimum gradient size is stroke width
      );
    }
  }

  if (options.hasOwnProperty('strokeWidth')) path.strokeWidth = options.strokeWidth;
};

exports.styleShape = styleShape;