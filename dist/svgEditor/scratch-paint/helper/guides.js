"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setDefaultGuideStyle = exports.removeHitPoint = exports.removeBoundsPath = exports.removeBoundsHandles = exports.removeAllGuides = exports.rectSelect = exports.hoverItem = exports.hoverBounds = exports.getGuideColor = exports.drawHitPoint = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("./layer");

var _selection = require("./selection");

var GUIDE_BLUE = '#009dec';
var GUIDE_GREY = '#aaaaaa';

var setDefaultGuideStyle = function setDefaultGuideStyle(item) {
  item.strokeWidth = 1 / _paper.default.view.zoom;
  item.opacity = 1;
  item.blendMode = 'normal';
  item.guide = true;
};

exports.setDefaultGuideStyle = setDefaultGuideStyle;

var hoverItem = function hoverItem(item) {
  var segments = item.segments;
  var clone = new _paper.default.Path(segments);
  setDefaultGuideStyle(clone);

  if (item.closed) {
    clone.closed = true;
  }

  clone.parent = (0, _layer.getGuideLayer)();
  clone.position = item.position;
  clone.strokeColor = GUIDE_BLUE;
  clone.fillColor = null;
  clone.data.isHelperItem = true;
  clone.data.origItem = item;
  clone.bringToFront();
  return clone;
};

exports.hoverItem = hoverItem;

var hoverBounds = function hoverBounds(item, expandBy) {
  var bounds = item.internalBounds;

  if (expandBy) {
    bounds = bounds.expand(expandBy);
  }

  var rect = new _paper.default.Path.Rectangle(bounds);
  rect.matrix = item.matrix;
  setDefaultGuideStyle(rect);
  rect.parent = (0, _layer.getGuideLayer)();
  rect.strokeColor = GUIDE_BLUE;
  rect.fillColor = null;
  rect.data.isHelperItem = true;
  rect.data.origItem = item;
  rect.bringToFront();
  return rect;
};

exports.hoverBounds = hoverBounds;

var rectSelect = function rectSelect(event, color) {
  var half = new _paper.default.Point(0.5 / _paper.default.view.zoom, 0.5 / _paper.default.view.zoom);
  var start = event.downPoint.add(half);
  var end = event.point.add(half);
  var rect = new _paper.default.Path.Rectangle(start, end);
  var zoom = 1.0 / _paper.default.view.zoom;
  setDefaultGuideStyle(rect);
  if (!color) color = GUIDE_GREY;
  rect.parent = (0, _layer.getGuideLayer)();
  rect.strokeColor = color;
  rect.data.isRectSelect = true;
  rect.data.isHelperItem = true;
  rect.dashArray = [3.0 * zoom, 3.0 * zoom];
  return rect;
};

exports.rectSelect = rectSelect;

var getGuideColor = function getGuideColor() {
  return GUIDE_BLUE;
};

exports.getGuideColor = getGuideColor;

var _removePaperItemsByDataTags = function _removePaperItemsByDataTags(tags) {
  var allItems = (0, _selection.getAllRootItems)(true);

  var _iterator = (0, _createForOfIteratorHelper2.default)(allItems),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var item = _step.value;

      var _iterator2 = (0, _createForOfIteratorHelper2.default)(tags),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var tag = _step2.value;

          if (item.data && item.data[tag]) {
            item.remove();
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
};

var _removePaperItemsByTags = function _removePaperItemsByTags(tags) {
  var allItems = (0, _selection.getAllRootItems)(true);

  var _iterator3 = (0, _createForOfIteratorHelper2.default)(allItems),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      var item = _step3.value;

      var _iterator4 = (0, _createForOfIteratorHelper2.default)(tags),
          _step4;

      try {
        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var tag = _step4.value;

          if (item[tag]) {
            item.remove();
          }
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }
};

var removeBoundsPath = function removeBoundsPath() {
  _removePaperItemsByDataTags(['isSelectionBound', 'isRotHandle', 'isScaleHandle']);
};

exports.removeBoundsPath = removeBoundsPath;

var removeBoundsHandles = function removeBoundsHandles() {
  _removePaperItemsByDataTags(['isRotHandle', 'isScaleHandle']);
};

exports.removeBoundsHandles = removeBoundsHandles;

var removeAllGuides = function removeAllGuides() {
  _removePaperItemsByTags(['guide']);
};

exports.removeAllGuides = removeAllGuides;

var removeHitPoint = function removeHitPoint() {
  _removePaperItemsByDataTags(['isHitPoint']);
};

exports.removeHitPoint = removeHitPoint;

var drawHitPoint = function drawHitPoint(point) {
  removeHitPoint();

  if (point) {
    var hitPoint = _paper.default.Path.Circle(point, 4 / _paper.default.view.zoom
    /* radius */
    );

    hitPoint.strokeWidth = 1 / _paper.default.view.zoom;
    hitPoint.strokeColor = GUIDE_BLUE;
    hitPoint.fillColor = new _paper.default.Color(1, 1, 1, 0.5);
    hitPoint.parent = (0, _layer.getGuideLayer)();
    hitPoint.data.isHitPoint = true;
    hitPoint.data.isHelperItem = true;
  }
};

exports.drawHitPoint = drawHitPoint;