"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../../log/log"));

var _broadBrushHelper = _interopRequireDefault(require("./broad-brush-helper"));

var _segmentBrushHelper = _interopRequireDefault(require("./segment-brush-helper"));

var _stylePath = require("../style-path");

var _selection = require("../selection");

var _layer = require("../layer");

var _compoundPath = require("../compound-path");

/**
 * Shared code for the brush and eraser mode. Adds functions on the paper tool object
 * to handle mouse events, which are delegated to broad-brush-helper and segment-brush-helper
 * based on the brushSize in the state.
 */
var Blobbiness = /*#__PURE__*/function () {
  /**
   * @param {function} onUpdateImage call when the drawing has changed to let listeners know
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   */
  function Blobbiness(onUpdateImage, clearSelectedItems) {
    (0, _classCallCheck2.default)(this, Blobbiness);
    this.broadBrushHelper = new _broadBrushHelper.default();
    this.segmentBrushHelper = new _segmentBrushHelper.default();
    this.onUpdateImage = onUpdateImage;
    this.clearSelectedItems = clearSelectedItems; // The following are stored to check whether these have changed and the cursor preview needs to be redrawn.

    this.strokeColor = null;
    this.brushSize = null;
    this.fillColor = null;
  }
  /**
   * Set configuration options for a blob
   * @param {!object} options Configuration
   * @param {!number} options.brushSize Width of blob marking made by mouse
   * @param {!boolean} options.isEraser Whether the stroke should be treated as an erase path. If false,
   *     the stroke is an additive path.
   * @param {?string} options.fillColor Color of the brush stroke.
   * @param {?string} options.strokeColor Color of the brush outline.
   * @param {?number} options.strokeWidth Width of the brush outline.
   */


  (0, _createClass2.default)(Blobbiness, [{
    key: "setOptions",
    value: function setOptions(options) {
      var oldFillColor = this.options ? this.options.fillColor : 'black';
      var oldStrokeColor = this.options ? this.options.strokeColor : null;
      var oldStrokeWidth = this.options ? this.options.strokeWidth : null; // If values are mixed, it means the color was set by a selection contained multiple values.
      // In this case keep drawing with the previous values if any. (For stroke width, null indicates
      // mixed, because stroke width is required to be a number)

      this.options = (0, _objectSpread2.default)((0, _objectSpread2.default)({}, options), {}, {
        fillColor: options.fillColor === _stylePath.MIXED ? oldFillColor : options.fillColor,
        strokeColor: options.strokeColor === _stylePath.MIXED ? oldStrokeColor : options.strokeColor,
        strokeWidth: options.strokeWidth === null ? oldStrokeWidth : options.strokeWidth
      });
      this.resizeCursorIfNeeded();
    }
    /**
     * Adds handlers on the mouse tool to draw blobs. Initialize with configuration options for a blob.
     * @param {!object} options Configuration
     * @param {!number} options.brushSize Width of blob marking made by mouse
     * @param {!boolean} options.isEraser Whether the stroke should be treated as an erase path. If false,
     *     the stroke is an additive path.
     * @param {?string} options.fillColor Color of the brush stroke.
     * @param {?string} options.strokeColor Color of the brush outline.
     * @param {?number} options.strokeWidth Width of the brush outline.
     */

  }, {
    key: "activateTool",
    value: function activateTool(options) {
      this.tool = new _paper.default.Tool();
      this.cursorPreviewLastPoint = new _paper.default.Point(-10000, -10000);
      this.setOptions(options);
      this.tool.active = false;
      this.tool.fixedDistance = 1;
      var blob = this;

      this.tool.onMouseMove = function (event) {
        blob.resizeCursorIfNeeded(event.point);
        (0, _stylePath.styleCursorPreview)(blob.cursorPreview, blob.options);
        blob.cursorPreview.bringToFront();
        blob.cursorPreview.position = event.point;
      };

      this.tool.onMouseDown = function (event) {
        blob.resizeCursorIfNeeded(event.point);
        if (event.event.button > 0) return; // only first mouse button

        this.active = true;

        if (blob.options.brushSize < Blobbiness.THRESHOLD) {
          blob.brush = Blobbiness.BROAD;
          blob.broadBrushHelper.onBroadMouseDown(event, blob.tool, blob.options);
        } else {
          blob.brush = Blobbiness.SEGMENT;
          blob.segmentBrushHelper.onSegmentMouseDown(event, blob.tool, blob.options);
        }

        blob.cursorPreview.bringToFront();
        blob.cursorPreview.position = event.point;
      };

      this.tool.onMouseDrag = function (event) {
        if (event.event.button > 0 || !this.active) return; // only first mouse button

        if (blob.brush === Blobbiness.BROAD) {
          blob.broadBrushHelper.onBroadMouseDrag(event, blob.tool, blob.options);
        } else if (blob.brush === Blobbiness.SEGMENT) {
          blob.segmentBrushHelper.onSegmentMouseDrag(event, blob.tool, blob.options);
        } else {
          _log.default.warn("Brush type does not exist: ".concat(blob.brush));
        }

        blob.cursorPreview.bringToFront();
        blob.cursorPreview.position = event.point;
      };

      this.tool.onMouseUp = function (event) {
        if (event.event.button > 0 || !this.active) return; // only first mouse button

        var lastPath;

        if (blob.brush === Blobbiness.BROAD) {
          lastPath = blob.broadBrushHelper.onBroadMouseUp(event, blob.tool, blob.options);
        } else if (blob.brush === Blobbiness.SEGMENT) {
          lastPath = blob.segmentBrushHelper.onSegmentMouseUp(event, blob.tool, blob.options);
        } else {
          _log.default.warn("Brush type does not exist: ".concat(blob.brush));
        }

        if (blob.options.isEraser) {
          blob.mergeEraser(lastPath);
        } else {
          blob.mergeBrush(lastPath);
        } // Remove cursor preview during snapshot, then bring it back


        blob.cursorPreview.remove();
        blob.onUpdateImage();
        blob.cursorPreview.parent = (0, _layer.getGuideLayer)(); // Reset

        blob.brush = null;
        this.fixedDistance = 1;
        this.active = false;
      };

      this.tool.activate();
    }
  }, {
    key: "resizeCursorIfNeeded",
    value: function resizeCursorIfNeeded(point) {
      if (!this.options) {
        return;
      } // The cursor preview was unattached from the view by an outside process,
      // such as changing costumes or undo.


      if (this.cursorPreview && !this.cursorPreview.parent) {
        this.cursorPreview = null;
      }

      if (this.cursorPreview && this.brushSize === this.options.brushSize && this.fillColor === this.options.fillColor && this.strokeColor === this.options.strokeColor && this.cursorPreviewLastPoint.equals(point)) {
        return;
      }

      if (typeof point !== 'undefined') {
        this.cursorPreviewLastPoint = point;
      }

      if (!this.cursorPreview) {
        this.cursorPreview = new _paper.default.Shape.Ellipse({
          point: this.cursorPreviewLastPoint,
          size: this.options.brushSize / 2
        });
        this.cursorPreview.parent = (0, _layer.getGuideLayer)();
        this.cursorPreview.data.isHelperItem = true;
        (0, _layer.setGuideItem)(this.cursorPreview);
      }

      this.cursorPreview.position = this.cursorPreviewLastPoint;
      this.cursorPreview.radius = this.options.brushSize / 2;
      this.brushSize = this.options.brushSize;
      this.fillColor = this.options.fillColor;
      this.strokeColor = this.options.strokeColor;
      (0, _stylePath.styleCursorPreview)(this.cursorPreview, this.options);
    }
  }, {
    key: "mergeBrush",
    value: function mergeBrush(lastPath) {
      var blob = this; // Get all path items to merge with

      var paths = (0, _selection.getItems)({
        match: function match(item) {
          return blob.isMergeable(lastPath, item) && item.parent instanceof _paper.default.Layer; // don't merge with nested in group
        }
      });
      var mergedPath = lastPath;
      var i; // Move down z order to first overlapping item

      for (i = paths.length - 1; i >= 0 && !this.touches(paths[i], lastPath); i--) {
        continue;
      }

      var mergedPathIndex = i;

      for (; i >= 0; i--) {
        if (!this.touches(paths[i], lastPath)) {
          continue;
        }

        if (!paths[i].getFillColor()) {// Ignore for merge. Paths without fill need to be in paths though,
          // since they can visibly change if z order changes
        } else if (this.colorMatch(paths[i], lastPath)) {
          // Make sure the new shape isn't overlapped by anything that would
          // visibly change if we change its z order
          for (var j = mergedPathIndex; j > i; j--) {
            if (this.touches(paths[j], paths[i])) {
              continue;
            }
          } // Merge same fill color


          var tempPath = mergedPath.unite(paths[i]);
          tempPath.strokeColor = paths[i].strokeColor;
          tempPath.strokeWidth = paths[i].strokeWidth;

          if (mergedPath === lastPath) {
            tempPath.insertAbove(paths[i]); // First intersected path determines z position of the new path
          } else {
            tempPath.insertAbove(mergedPath); // Rest of merges join z index of merged path

            mergedPathIndex--; // Removed an item, so the merged path index decreases
          }

          mergedPath.remove();
          mergedPath = tempPath;
          paths[i].remove();
          paths.splice(i, 1);
        }
      }
    }
  }, {
    key: "mergeEraser",
    value: function mergeEraser(lastPath) {
      var blob = this; // Get all path items to merge with
      // If there are selected items, try to erase from amongst those.

      var items = (0, _selection.getItems)({
        match: function match(item) {
          return item.selected && blob.isMergeable(lastPath, item) && blob.touches(lastPath, item) && // Boolean operations will produce incorrect results if directly applied to compound path children,
          // so exclude those. Their parents are also selected so boolean operations will apply to them.
          !(0, _compoundPath.isCompoundPathChild)(item);
        },
        class: _paper.default.PathItem
      }); // Eraser didn't hit anything selected, so assume they meant to erase from all instead of from subset
      // and deselect the selection

      if (items.length === 0) {
        (0, _selection.clearSelection)(this.clearSelectedItems);
        items = (0, _selection.getItems)({
          match: function match(item) {
            return blob.isMergeable(lastPath, item) && blob.touches(lastPath, item) && !(0, _compoundPath.isCompoundPathChild)(item);
          },
          class: _paper.default.PathItem
        });
      }

      for (var i = items.length - 1; i >= 0; i--) {
        if (items[i] instanceof _paper.default.Path && (!items[i].fillColor || items[i].fillColor._alpha === 0)) {
          // Gather path segments
          var _subpaths = [];
          var firstSeg = items[i];
          var intersections = firstSeg.getIntersections(lastPath);

          for (var j = intersections.length - 1; j >= 0; j--) {
            var split = firstSeg.splitAt(intersections[j]);

            if (split) {
              split.insertAbove(firstSeg);

              _subpaths.push(split);
            }
          }

          _subpaths.push(firstSeg); // Remove the ones that are within the eraser stroke boundary


          for (var k = _subpaths.length - 1; k >= 0; k--) {
            var segMidpoint = _subpaths[k].getLocationAt(_subpaths[k].length / 2).point;

            if (lastPath.contains(segMidpoint)) {
              _subpaths[k].remove();

              _subpaths.splice(k, 1);
            }
          }

          lastPath.remove();
          continue;
        } // Erase


        var newPath = items[i].subtract(lastPath);
        newPath.insertBelow(items[i]); // Gather path segments

        var subpaths = [];

        if (items[i] instanceof _paper.default.Path && !items[i].closed) {
          var _firstSeg = items[i].clone();

          var _intersections = _firstSeg.getIntersections(lastPath); // keep first and last segments


          for (var _j = _intersections.length - 1; _j >= 0; _j--) {
            var _split = _firstSeg.splitAt(_intersections[_j]);

            _split.insertAbove(_firstSeg);

            subpaths.push(_split);
          }

          subpaths.push(_firstSeg);
        } // Remove the ones that are within the eraser stroke boundary, or are already part of new path.
        // This way subpaths only remain if they didn't get turned into a shape by subtract.


        for (var _k = subpaths.length - 1; _k >= 0; _k--) {
          var _segMidpoint = subpaths[_k].getLocationAt(subpaths[_k].length / 2).point;

          if (lastPath.contains(_segMidpoint) || newPath.contains(_segMidpoint)) {
            subpaths[_k].remove();

            subpaths.splice(_k, 1);
          }
        }

        if (newPath.children) {
          this.separateCompoundPath(newPath);
          newPath.remove();
        }

        items[i].remove();
      }

      lastPath.remove();
    }
  }, {
    key: "separateCompoundPath",
    value: function separateCompoundPath(compoundPath) {
      if (!compoundPath.isClockwise()) {
        compoundPath.reverse();
      } // Divide topologically separate shapes into their own compound paths, instead of
      // everything being stuck together.


      var clockwiseChildren = [];
      var ccwChildren = [];

      for (var j = compoundPath.children.length - 1; j >= 0; j--) {
        var child = compoundPath.children[j];

        if (child.isClockwise()) {
          clockwiseChildren.push(child);
        } else {
          ccwChildren.push(child);
        }
      } // Sort by area smallest to largest


      clockwiseChildren.sort(function (a, b) {
        return a.area - b.area;
      });
      ccwChildren.sort(function (a, b) {
        return Math.abs(a.area) - Math.abs(b.area);
      }); // Go smallest to largest non-hole, so larger non-holes don't get the smaller pieces' holes

      for (var _j2 = 0; _j2 < clockwiseChildren.length; _j2++) {
        var cw = clockwiseChildren[_j2];
        cw.copyAttributes(compoundPath);
        cw.fillColor = compoundPath.fillColor;
        cw.strokeColor = compoundPath.strokeColor;
        cw.strokeWidth = compoundPath.strokeWidth;
        cw.insertAbove(compoundPath); // Go backward since we are deleting elements. Backwards is largest to smallest hole.

        var newCw = cw;

        for (var k = ccwChildren.length - 1; k >= 0; k--) {
          var ccw = ccwChildren[k];

          if (this.firstEnclosesSecond(cw, ccw)) {
            var temp = newCw.subtract(ccw);
            temp.insertAbove(compoundPath);
            newCw.remove();
            newCw = temp;
            ccw.remove();
            ccwChildren.splice(k, 1);
          }
        }
      }
    }
  }, {
    key: "colorMatch",
    value: function colorMatch(existingPath, addedPath) {
      // Note: transparent fill colors do notdetect as touching
      return existingPath.getFillColor().equals(addedPath.getFillColor()) && (addedPath.getStrokeColor() === existingPath.getStrokeColor() || // both null
      addedPath.getStrokeColor() && addedPath.getStrokeColor().equals(existingPath.getStrokeColor())) && addedPath.getStrokeWidth() === existingPath.getStrokeWidth() && this.touches(existingPath, addedPath);
    }
  }, {
    key: "touches",
    value: function touches(path1, path2) {
      // Two shapes are touching if their paths intersect
      if (path1 && path2 && path1.intersects(path2)) {
        return true;
      }

      return this.firstEnclosesSecond(path1, path2) || this.firstEnclosesSecond(path2, path1);
    }
  }, {
    key: "firstEnclosesSecond",
    value: function firstEnclosesSecond(path1, path2) {
      // Two shapes are also touching if one is completely inside the other
      if (path1 && path2 && path2.firstSegment && path2.firstSegment.point && path1.hitTest(path2.firstSegment.point)) {
        return true;
      } // TODO: clean up these no point paths


      return false;
    }
  }, {
    key: "matchesAnyChild",
    value: function matchesAnyChild(group, path) {
      var _iterator = (0, _createForOfIteratorHelper2.default)(group.children),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var child = _step.value;

          if (child.children && this.matchesAnyChild(path, child)) {
            return true;
          }

          if (path === child) {
            return true;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return false;
    }
  }, {
    key: "isMergeable",
    value: function isMergeable(newPath, existingPath) {
      // Path or compound path
      if (!(existingPath instanceof _paper.default.PathItem)) {
        return;
      }

      if (newPath.children) {
        if (this.matchesAnyChild(newPath, existingPath)) {
          // Don't merge with children of self
          return false;
        }
      }

      return existingPath !== newPath; // don't merge with self
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      if (this.cursorPreview) {
        this.cursorPreview.remove();
        this.cursorPreview = null;
      }

      this.tool.remove();
      this.tool = null;
    }
  }], [{
    key: "BROAD",
    get: function get() {
      return 'broadbrush';
    }
  }, {
    key: "SEGMENT",
    get: function get() {
      return 'segmentbrush';
    } // If brush size >= threshold use segment brush, else use broadbrush
    // Segment brush has performance issues at low threshold, but broad brush has weird corners
    // which get more obvious the bigger it is

  }, {
    key: "THRESHOLD",
    get: function get() {
      return 30 / _paper.default.view.zoom;
    }
  }]);
  return Blobbiness;
}();

var _default = Blobbiness;
exports.default = _default;