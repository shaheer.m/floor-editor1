"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shouldShowUndo = exports.shouldShowRedo = exports.performUndo = exports.performSnapshot = exports.performRedo = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("./layer");

var _selection = require("./selection");

var _format = _interopRequireWildcard(require("../lib/format"));

var _log = _interopRequireDefault(require("../log/log"));

// undo functionality
// modifed from https://github.com/memononen/stylii

/**
 * Take an undo snapshot
 * @param {function} dispatchPerformSnapshot Callback to dispatch a state update
 * @param {Formats} format Either Formats.BITMAP or Formats.VECTOR
 */
var performSnapshot = function performSnapshot(dispatchPerformSnapshot, format) {
  if (!format) {
    _log.default.error('Format must be specified.');
  }

  var guideLayers = (0, _layer.hideGuideLayers)();
  dispatchPerformSnapshot({
    json: _paper.default.project.exportJSON({
      asString: false
    }),
    paintEditorFormat: format
  });
  (0, _layer.showGuideLayers)(guideLayers);
};

exports.performSnapshot = performSnapshot;

var _restore = function _restore(entry, setSelectedItems, onUpdateImage, isBitmapMode) {
  for (var i = _paper.default.project.layers.length - 1; i >= 0; i--) {
    var layer = _paper.default.project.layers[i];

    if (!layer.data.isBackgroundGuideLayer && !layer.data.isDragCrosshairLayer && !layer.data.isOutlineLayer) {
      layer.removeChildren();
      layer.remove();
    }
  }

  _paper.default.project.importJSON(entry.json);

  setSelectedItems(); // Ensure that all rasters are loaded before updating storage with new image data.

  var rastersThatNeedToLoad = [];

  var onLoad = function onLoad() {
    if (!(0, _layer.getRaster)().loaded) return;

    var _iterator = (0, _createForOfIteratorHelper2.default)(rastersThatNeedToLoad),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var raster = _step.value;
        if (!raster.loaded) return;
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    onUpdateImage(true
    /* skipSnapshot */
    );
  }; // Bitmap mode should have at most 1 selected item


  if (isBitmapMode) {
    var selectedItems = (0, _selection.getSelectedLeafItems)();

    if (selectedItems.length === 1 && selectedItems[0] instanceof _paper.default.Raster) {
      rastersThatNeedToLoad.push(selectedItems[0]);

      if (selectedItems[0].data && selectedItems[0].data.expanded instanceof _paper.default.Raster) {
        rastersThatNeedToLoad.push(selectedItems[0].data.expanded);
      }
    }
  }

  (0, _layer.getRaster)().onLoad = onLoad;

  for (var _i = 0, _rastersThatNeedToLoa = rastersThatNeedToLoad; _i < _rastersThatNeedToLoa.length; _i++) {
    var raster = _rastersThatNeedToLoa[_i];
    raster.onLoad = onLoad;
    if (raster.loaded) raster.onLoad();
  }
};

var performUndo = function performUndo(undoState, dispatchPerformUndo, setSelectedItems, onUpdateImage) {
  if (undoState.pointer > 0) {
    var state = undoState.stack[undoState.pointer - 1];

    _restore(state, setSelectedItems, onUpdateImage, (0, _format.isBitmap)(state.paintEditorFormat));

    var format = (0, _format.isVector)(state.paintEditorFormat) ? _format.default.VECTOR_SKIP_CONVERT : (0, _format.isBitmap)(state.paintEditorFormat) ? _format.default.BITMAP_SKIP_CONVERT : null;
    dispatchPerformUndo(format);
  }
};

exports.performUndo = performUndo;

var performRedo = function performRedo(undoState, dispatchPerformRedo, setSelectedItems, onUpdateImage) {
  if (undoState.pointer >= 0 && undoState.pointer < undoState.stack.length - 1) {
    var state = undoState.stack[undoState.pointer + 1];

    _restore(state, setSelectedItems, onUpdateImage, (0, _format.isBitmap)(state.paintEditorFormat));

    var format = (0, _format.isVector)(state.paintEditorFormat) ? _format.default.VECTOR_SKIP_CONVERT : (0, _format.isBitmap)(state.paintEditorFormat) ? _format.default.BITMAP_SKIP_CONVERT : null;
    dispatchPerformRedo(format);
  }
};

exports.performRedo = performRedo;

var shouldShowUndo = function shouldShowUndo(undoState) {
  return undoState.pointer > 0;
};

exports.shouldShowUndo = shouldShowUndo;

var shouldShowRedo = function shouldShowRedo(undoState) {
  return undoState.pointer > -1 && undoState.pointer !== undoState.stack.length - 1;
};

exports.shouldShowRedo = shouldShowRedo;