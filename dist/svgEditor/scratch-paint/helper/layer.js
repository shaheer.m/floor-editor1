"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showGuideLayers = exports.setupLayers = exports.setGuideItem = exports.hideGuideLayers = exports.getRaster = exports.getGuideLayer = exports.getDragCrosshairLayer = exports.getBackgroundGuideLayer = exports.createCanvas = exports.convertBackgroundGuideLayer = exports.clearRaster = exports.CROSSHAIR_SIZE = exports.CROSSHAIR_FULL_OPACITY = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../log/log"));

var _view = require("./view");

var _item = require("./item");

var _format = require("../lib/format");

var CHECKERBOARD_SIZE = 8;
var CROSSHAIR_SIZE = 16;
exports.CROSSHAIR_SIZE = CROSSHAIR_SIZE;
var CROSSHAIR_FULL_OPACITY = 0.75;
exports.CROSSHAIR_FULL_OPACITY = CROSSHAIR_FULL_OPACITY;

var _getLayer = function _getLayer(layerString) {
  var _iterator = (0, _createForOfIteratorHelper2.default)(_paper.default.project.layers),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var layer = _step.value;

      if (layer.data && layer.data[layerString]) {
        return layer;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
};

var _getPaintingLayer = function _getPaintingLayer() {
  return _getLayer('isPaintingLayer');
};
/**
 * Creates a canvas with width and height matching the art board size.
 * @param {?number} width Width of the canvas. Defaults to ART_BOARD_WIDTH.
 * @param {?number} height Height of the canvas. Defaults to ART_BOARD_HEIGHT.
 * @return {HTMLCanvasElement} the canvas
 */


var createCanvas = function createCanvas(width, height) {
  var canvas = document.createElement('canvas');
  canvas.width = width ? width : _view.ART_BOARD_WIDTH;
  canvas.height = height ? height : _view.ART_BOARD_HEIGHT;
  canvas.getContext('2d').imageSmoothingEnabled = false;
  return canvas;
};

exports.createCanvas = createCanvas;

var clearRaster = function clearRaster() {
  var layer = _getLayer('isRasterLayer');

  layer.removeChildren(); // Generate blank raster

  var raster = new _paper.default.Raster(createCanvas());
  raster.canvas.getContext('2d').imageSmoothingEnabled = false;
  raster.parent = layer;
  raster.guide = true;
  raster.locked = true;
  raster.position = _view.CENTER;
};

exports.clearRaster = clearRaster;

var getRaster = function getRaster() {
  var layer = _getLayer('isRasterLayer'); // Generate blank raster


  if (layer.children.length === 0) {
    clearRaster();
  }

  return _getLayer('isRasterLayer').children[0];
};

exports.getRaster = getRaster;

var getDragCrosshairLayer = function getDragCrosshairLayer() {
  return _getLayer('isDragCrosshairLayer');
};

exports.getDragCrosshairLayer = getDragCrosshairLayer;

var getBackgroundGuideLayer = function getBackgroundGuideLayer() {
  return _getLayer('isBackgroundGuideLayer');
};

exports.getBackgroundGuideLayer = getBackgroundGuideLayer;

var _convertLayer = function _convertLayer(layer, format) {
  layer.bitmapBackground.visible = (0, _format.isBitmap)(format);
  layer.vectorBackground.visible = (0, _format.isVector)(format);
};

var convertBackgroundGuideLayer = function convertBackgroundGuideLayer(format) {
  _convertLayer(getBackgroundGuideLayer(), format);
};

exports.convertBackgroundGuideLayer = convertBackgroundGuideLayer;

var _makeGuideLayer = function _makeGuideLayer() {
  var guideLayer = new _paper.default.Layer();
  guideLayer.data.isGuideLayer = true;
  return guideLayer;
};

var getGuideLayer = function getGuideLayer() {
  var layer = _getLayer('isGuideLayer');

  if (!layer) {
    layer = _makeGuideLayer();

    _getPaintingLayer().activate();
  }

  return layer;
};

exports.getGuideLayer = getGuideLayer;

var setGuideItem = function setGuideItem(item) {
  item.locked = true;
  item.guide = true;

  if ((0, _item.isGroupItem)(item)) {
    for (var i = 0; i < item.children.length; i++) {
      setGuideItem(item.children[i]);
    }
  }
};
/**
 * Removes the guide layers, e.g. for purposes of exporting the image. Must call showGuideLayers to re-add them.
 * @param {boolean} includeRaster true if the raster layer should also be hidden
 * @return {object} an object of the removed layers, which should be passed to showGuideLayers to re-add them.
 */


exports.setGuideItem = setGuideItem;

var hideGuideLayers = function hideGuideLayers(includeRaster) {
  var backgroundGuideLayer = getBackgroundGuideLayer();
  var dragCrosshairLayer = getDragCrosshairLayer();

  var outlineLayer = _getLayer('isOutlineLayer');

  var guideLayer = getGuideLayer();
  dragCrosshairLayer.remove();
  outlineLayer.remove();
  guideLayer.remove();
  backgroundGuideLayer.remove();
  var rasterLayer;

  if (includeRaster) {
    rasterLayer = _getLayer('isRasterLayer');
    rasterLayer.remove();
  }

  return {
    dragCrosshairLayer: dragCrosshairLayer,
    outlineLayer: outlineLayer,
    guideLayer: guideLayer,
    backgroundGuideLayer: backgroundGuideLayer,
    rasterLayer: rasterLayer
  };
};
/**
 * Add back the guide layers removed by calling hideGuideLayers. This must be done before any editing operations are
 * taken in the paint editor.
 * @param {!object} guideLayers object of the removed layers, which was returned by hideGuideLayers
 */


exports.hideGuideLayers = hideGuideLayers;

var showGuideLayers = function showGuideLayers(guideLayers) {
  var backgroundGuideLayer = guideLayers.backgroundGuideLayer;
  var dragCrosshairLayer = guideLayers.dragCrosshairLayer;
  var outlineLayer = guideLayers.outlineLayer;
  var guideLayer = guideLayers.guideLayer;
  var rasterLayer = guideLayers.rasterLayer;

  if (rasterLayer && !rasterLayer.index) {
    _paper.default.project.addLayer(rasterLayer);

    rasterLayer.sendToBack();
  }

  if (!backgroundGuideLayer.index) {
    _paper.default.project.addLayer(backgroundGuideLayer);

    backgroundGuideLayer.sendToBack();
  }

  if (!dragCrosshairLayer.index) {
    _paper.default.project.addLayer(dragCrosshairLayer);

    dragCrosshairLayer.bringToFront();
  }

  if (!outlineLayer.index) {
    _paper.default.project.addLayer(outlineLayer);

    outlineLayer.bringToFront();
  }

  if (!guideLayer.index) {
    _paper.default.project.addLayer(guideLayer);

    guideLayer.bringToFront();
  }

  if (_paper.default.project.activeLayer !== _getPaintingLayer()) {
    _log.default.error("Wrong active layer");

    _log.default.error(_paper.default.project.activeLayer.data);
  }
};

exports.showGuideLayers = showGuideLayers;

var _makePaintingLayer = function _makePaintingLayer() {
  var paintingLayer = new _paper.default.Layer();
  paintingLayer.data.isPaintingLayer = true;
  return paintingLayer;
};

var _makeRasterLayer = function _makeRasterLayer() {
  var rasterLayer = new _paper.default.Layer();
  rasterLayer.data.isRasterLayer = true;
  clearRaster();
  return rasterLayer;
};

var _makeBackgroundPaper = function _makeBackgroundPaper(width, height, color, opacity) {
  // creates a checkerboard path of width * height squares in color on white
  var x = 0;
  var y = 0;
  var pathPoints = [];

  while (x < width) {
    pathPoints.push(new _paper.default.Point(x, y));
    x++;
    pathPoints.push(new _paper.default.Point(x, y));
    y = y === 0 ? height : 0;
  }

  y = height - 1;
  x = width;

  while (y > 0) {
    pathPoints.push(new _paper.default.Point(x, y));
    x = x === 0 ? width : 0;
    pathPoints.push(new _paper.default.Point(x, y));
    y--;
  }

  var vRect = new _paper.default.Shape.Rectangle(new _paper.default.Point(0, 0), new _paper.default.Point(_view.ART_BOARD_WIDTH / CHECKERBOARD_SIZE, _view.ART_BOARD_HEIGHT / CHECKERBOARD_SIZE));
  vRect.fillColor = '#fff';
  vRect.guide = true;
  vRect.locked = true;
  vRect.position = _view.CENTER;
  var vPath = new _paper.default.Path(pathPoints);
  vPath.fillRule = 'evenodd';
  vPath.fillColor = color;
  vPath.opacity = opacity;
  vPath.guide = true;
  vPath.locked = true;
  vPath.position = _view.CENTER;
  var mask = new _paper.default.Shape.Rectangle(_view.MAX_WORKSPACE_BOUNDS);
  mask.position = _view.CENTER;
  mask.guide = true;
  mask.locked = true;
  mask.scale(1 / CHECKERBOARD_SIZE);
  var vGroup = new _paper.default.Group([vRect, vPath, mask]);
  mask.clipMask = true;
  return vGroup;
}; // Helper function for drawing a crosshair


var _makeCrosshair = function _makeCrosshair(opacity, parent) {
  var crosshair = new _paper.default.Group();
  var vLine2 = new _paper.default.Path.Line(new _paper.default.Point(0, -7), new _paper.default.Point(0, 7));
  vLine2.strokeWidth = 6;
  vLine2.strokeColor = 'white';
  vLine2.strokeCap = 'round';
  crosshair.addChild(vLine2);
  var hLine2 = new _paper.default.Path.Line(new _paper.default.Point(-7, 0), new _paper.default.Point(7, 0));
  hLine2.strokeWidth = 6;
  hLine2.strokeColor = 'white';
  hLine2.strokeCap = 'round';
  crosshair.addChild(hLine2);
  var circle2 = new _paper.default.Shape.Circle(new _paper.default.Point(0, 0), 5.5);
  circle2.strokeWidth = 6;
  circle2.strokeColor = 'white';
  crosshair.addChild(circle2);
  var vLine = new _paper.default.Path.Line(new _paper.default.Point(0, -7), new _paper.default.Point(0, 7));
  vLine.strokeWidth = 2;
  vLine.strokeColor = 'black';
  vLine.strokeCap = 'round';
  crosshair.addChild(vLine);
  var hLine = new _paper.default.Path.Line(new _paper.default.Point(-7, 0), new _paper.default.Point(7, 0));
  hLine.strokeWidth = 2;
  hLine.strokeColor = 'black';
  hLine.strokeCap = 'round';
  crosshair.addChild(hLine);
  var circle = new _paper.default.Shape.Circle(new _paper.default.Point(0, 0), 5.5);
  circle.strokeWidth = 2;
  circle.strokeColor = 'black';
  crosshair.addChild(circle);
  setGuideItem(crosshair);
  crosshair.position = _view.CENTER;
  crosshair.opacity = opacity;
  crosshair.parent = parent;
  crosshair.applyMatrix = false;
  parent.dragCrosshair = crosshair;
  crosshair.scale(CROSSHAIR_SIZE / crosshair.bounds.width / _paper.default.view.zoom);
};

var _makeDragCrosshairLayer = function _makeDragCrosshairLayer() {
  var dragCrosshairLayer = new _paper.default.Layer();

  _makeCrosshair(CROSSHAIR_FULL_OPACITY, dragCrosshairLayer);

  dragCrosshairLayer.data.isDragCrosshairLayer = true;
  dragCrosshairLayer.visible = false;
  return dragCrosshairLayer;
};

var _makeOutlineLayer = function _makeOutlineLayer() {
  var outlineLayer = new _paper.default.Layer();
  var whiteRect = new _paper.default.Shape.Rectangle(_view.ART_BOARD_BOUNDS.expand(1));
  whiteRect.strokeWidth = 2;
  whiteRect.strokeColor = 'white';
  setGuideItem(whiteRect);
  var blueRect = new _paper.default.Shape.Rectangle(_view.ART_BOARD_BOUNDS.expand(5));
  blueRect.strokeWidth = 2;
  blueRect.strokeColor = '#4280D7';
  blueRect.opacity = 0.25;
  setGuideItem(blueRect);
  outlineLayer.data.isOutlineLayer = true;
  return outlineLayer;
};

var _makeBackgroundGuideLayer = function _makeBackgroundGuideLayer(format) {
  var guideLayer = new _paper.default.Layer();
  guideLayer.locked = true;
  var vWorkspaceBounds = new _paper.default.Shape.Rectangle(_view.MAX_WORKSPACE_BOUNDS);
  vWorkspaceBounds.fillColor = '#ECF1F9';
  vWorkspaceBounds.position = _view.CENTER; // Add 1 to the height because it's an odd number otherwise, and we want it to be even
  // so the corner of the checkerboard to line up with the center crosshair

  var vBackground = _makeBackgroundPaper(_view.MAX_WORKSPACE_BOUNDS.width / CHECKERBOARD_SIZE, _view.MAX_WORKSPACE_BOUNDS.height / CHECKERBOARD_SIZE + 1, '#D9E3F2', 0.55);

  vBackground.position = _view.CENTER;
  vBackground.scaling = new _paper.default.Point(CHECKERBOARD_SIZE, CHECKERBOARD_SIZE);
  var vectorBackground = new _paper.default.Group();
  vectorBackground.addChild(vWorkspaceBounds);
  vectorBackground.addChild(vBackground);
  setGuideItem(vectorBackground);
  guideLayer.vectorBackground = vectorBackground;

  var bitmapBackground = _makeBackgroundPaper(_view.ART_BOARD_WIDTH / CHECKERBOARD_SIZE, _view.ART_BOARD_HEIGHT / CHECKERBOARD_SIZE, '#D9E3F2', 0.55);

  bitmapBackground.position = _view.CENTER;
  bitmapBackground.scaling = new _paper.default.Point(CHECKERBOARD_SIZE, CHECKERBOARD_SIZE);
  bitmapBackground.guide = true;
  bitmapBackground.locked = true;
  guideLayer.bitmapBackground = bitmapBackground;

  _convertLayer(guideLayer, format);

  _makeCrosshair(0.16, guideLayer);

  guideLayer.data.isBackgroundGuideLayer = true;
  return guideLayer;
};

var setupLayers = function setupLayers(format) {
  var backgroundGuideLayer = _makeBackgroundGuideLayer(format);

  _makeRasterLayer();

  var paintLayer = _makePaintingLayer();

  var dragCrosshairLayer = _makeDragCrosshairLayer();

  var outlineLayer = _makeOutlineLayer();

  var guideLayer = _makeGuideLayer();

  backgroundGuideLayer.sendToBack();
  dragCrosshairLayer.bringToFront();
  outlineLayer.bringToFront();
  guideLayer.bringToFront();
  paintLayer.activate();
};

exports.setupLayers = setupLayers;