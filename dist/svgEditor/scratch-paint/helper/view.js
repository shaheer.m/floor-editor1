"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.zoomToFit = exports.zoomOnSelection = exports.zoomOnFixedPoint = exports.setWorkspaceBounds = exports.resizeCrosshair = exports.resetZoom = exports.pan = exports.getWorkspaceBounds = exports.getActionBounds = exports.clampViewBounds = exports.SVG_ART_BOARD_WIDTH = exports.SVG_ART_BOARD_HEIGHT = exports.OUTERMOST_ZOOM_LEVEL = exports.MAX_WORKSPACE_BOUNDS = exports.CENTER = exports.ART_BOARD_WIDTH = exports.ART_BOARD_HEIGHT = exports.ART_BOARD_BOUNDS = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("./layer");

var _selection = require("./selection");

var _bitmap = require("./bitmap");

var _log = _interopRequireDefault(require("../log/log"));

// Vectors are imported and exported at SVG_ART_BOARD size.
// Once they are imported however, both SVGs and bitmaps are on
// canvases of ART_BOARD size.
// (This is for backwards compatibility, to handle both assets
// designed for 480 x 360, and bitmap resolution 2 bitmaps)
var SVG_ART_BOARD_WIDTH = 480 * 4;
exports.SVG_ART_BOARD_WIDTH = SVG_ART_BOARD_WIDTH;
var SVG_ART_BOARD_HEIGHT = 360 * 3;
exports.SVG_ART_BOARD_HEIGHT = SVG_ART_BOARD_HEIGHT;
var ART_BOARD_WIDTH = SVG_ART_BOARD_WIDTH * 2;
exports.ART_BOARD_WIDTH = ART_BOARD_WIDTH;
var ART_BOARD_HEIGHT = SVG_ART_BOARD_HEIGHT * 2;
exports.ART_BOARD_HEIGHT = ART_BOARD_HEIGHT;
var CENTER = new _paper.default.Point(ART_BOARD_WIDTH / 2, ART_BOARD_HEIGHT / 2);
exports.CENTER = CENTER;
var PADDING_PERCENT = 25; // Padding as a percent of the max of width/height of the sprite

var BUFFER = 50; // Number of pixels of allowance around objects at the edges of the workspace

var MIN_RATIO = .125; // Zoom in to at least 1/8 of the screen. This way you don't end up incredibly
//                         zoomed in for tiny costumes.

var OUTERMOST_ZOOM_LEVEL = 0.333;
exports.OUTERMOST_ZOOM_LEVEL = OUTERMOST_ZOOM_LEVEL;
var ART_BOARD_BOUNDS = new _paper.default.Rectangle(0, 0, ART_BOARD_WIDTH, ART_BOARD_HEIGHT);
exports.ART_BOARD_BOUNDS = ART_BOARD_BOUNDS;
var MAX_WORKSPACE_BOUNDS = new _paper.default.Rectangle(-ART_BOARD_WIDTH / 4, -ART_BOARD_HEIGHT / 4, ART_BOARD_WIDTH * 1.5, ART_BOARD_HEIGHT * 1.5);
exports.MAX_WORKSPACE_BOUNDS = MAX_WORKSPACE_BOUNDS;
var _workspaceBounds = ART_BOARD_BOUNDS;

var getWorkspaceBounds = function getWorkspaceBounds() {
  return _workspaceBounds;
};
/**
* The workspace bounds define the areas that the scroll bars can access.
* They include at minimum the artboard, and extend to a bit beyond the
* farthest item off tne edge in any given direction (so items can't be
* "lost" off the edge)
*
* @param {boolean} clipEmpty Clip empty space from bounds, even if it
* means discontinuously jumping the viewport. This should probably be
* false unless the viewport is going to move discontinuously anyway
* (such as in a zoom button click)
*/


exports.getWorkspaceBounds = getWorkspaceBounds;

var setWorkspaceBounds = function setWorkspaceBounds(clipEmpty) {
  var items = (0, _selection.getAllRootItems)(); // Include the artboard and what's visible in the viewport

  var bounds = ART_BOARD_BOUNDS;

  if (!clipEmpty) {
    bounds = bounds.unite(_paper.default.view.bounds);
  } // Include everything the user has drawn and a buffer around it


  var _iterator = (0, _createForOfIteratorHelper2.default)(items),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var item = _step.value;
      bounds = bounds.unite(item.bounds.expand(BUFFER));
    } // Limit to max workspace bounds

  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  bounds = bounds.intersect(MAX_WORKSPACE_BOUNDS.expand(BUFFER));
  var top = bounds.top;
  var left = bounds.left;
  var bottom = bounds.bottom;
  var right = bounds.right; // Center in view if viewport is larger than workspace

  var hDiff = 0;
  var vDiff = 0;

  if (bounds.width < _paper.default.view.bounds.width) {
    hDiff = (_paper.default.view.bounds.width - bounds.width) / 2;
    left -= hDiff;
    right += hDiff;
  }

  if (bounds.height < _paper.default.view.bounds.height) {
    vDiff = (_paper.default.view.bounds.height - bounds.height) / 2;
    top -= vDiff;
    bottom += vDiff;
  }

  _workspaceBounds = new _paper.default.Rectangle(left, top, right - left, bottom - top);
};

exports.setWorkspaceBounds = setWorkspaceBounds;

var clampViewBounds = function clampViewBounds() {
  var _paper$project$view$b = _paper.default.project.view.bounds,
      left = _paper$project$view$b.left,
      right = _paper$project$view$b.right,
      top = _paper$project$view$b.top,
      bottom = _paper$project$view$b.bottom;

  if (left < _workspaceBounds.left) {
    _paper.default.project.view.scrollBy(new _paper.default.Point(_workspaceBounds.left - left, 0));
  }

  if (top < _workspaceBounds.top) {
    _paper.default.project.view.scrollBy(new _paper.default.Point(0, _workspaceBounds.top - top));
  }

  if (bottom > _workspaceBounds.bottom) {
    _paper.default.project.view.scrollBy(new _paper.default.Point(0, _workspaceBounds.bottom - bottom));
  }

  if (right > _workspaceBounds.right) {
    _paper.default.project.view.scrollBy(new _paper.default.Point(_workspaceBounds.right - right, 0));
  }

  setWorkspaceBounds();
};

exports.clampViewBounds = clampViewBounds;

var resizeCrosshair = function resizeCrosshair() {
  if ((0, _layer.getDragCrosshairLayer)() && (0, _layer.getDragCrosshairLayer)().dragCrosshair) {
    (0, _layer.getDragCrosshairLayer)().dragCrosshair.scale(_layer.CROSSHAIR_SIZE / (0, _layer.getDragCrosshairLayer)().dragCrosshair.bounds.width / _paper.default.view.zoom);
  }

  if ((0, _layer.getBackgroundGuideLayer)() && (0, _layer.getBackgroundGuideLayer)().dragCrosshair) {
    (0, _layer.getBackgroundGuideLayer)().dragCrosshair.scale(_layer.CROSSHAIR_SIZE / (0, _layer.getBackgroundGuideLayer)().dragCrosshair.bounds.width / _paper.default.view.zoom);
  }
}; // Zoom keeping a project-space point fixed.
// This article was helpful http://matthiasberth.com/tech/stable-zoom-and-pan-in-paperjs


exports.resizeCrosshair = resizeCrosshair;

var zoomOnFixedPoint = function zoomOnFixedPoint(deltaZoom, fixedPoint) {
  var view = _paper.default.view;
  var preZoomCenter = view.center;
  var newZoom = Math.max(OUTERMOST_ZOOM_LEVEL, view.zoom + deltaZoom);
  var scaling = view.zoom / newZoom;
  var preZoomOffset = fixedPoint.subtract(preZoomCenter);
  var postZoomOffset = fixedPoint.subtract(preZoomOffset.multiply(scaling)).subtract(preZoomCenter);
  view.zoom = newZoom;
  view.translate(postZoomOffset.multiply(-1));
  setWorkspaceBounds(true
  /* clipEmpty */
  );
  clampViewBounds();
  resizeCrosshair();
}; // Zoom keeping the selection center (if any) fixed.


exports.zoomOnFixedPoint = zoomOnFixedPoint;

var zoomOnSelection = function zoomOnSelection(deltaZoom) {
  var fixedPoint;
  var items = (0, _selection.getSelectedRootItems)();

  if (items.length > 0) {
    var rect = null;

    var _iterator2 = (0, _createForOfIteratorHelper2.default)(items),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var item = _step2.value;

        if (rect) {
          rect = rect.unite(item.bounds);
        } else {
          rect = item.bounds;
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }

    fixedPoint = rect.center;
  } else {
    fixedPoint = _paper.default.project.view.center;
  }

  zoomOnFixedPoint(deltaZoom, fixedPoint);
};

exports.zoomOnSelection = zoomOnSelection;

var resetZoom = function resetZoom() {
  _paper.default.project.view.zoom = .5;
  setWorkspaceBounds(true
  /* clipEmpty */
  );
  resizeCrosshair();
  clampViewBounds();
};

exports.resetZoom = resetZoom;

var pan = function pan(dx, dy) {
  _paper.default.project.view.scrollBy(new _paper.default.Point(dx, dy));

  clampViewBounds();
};
/**
 * Mouse actions are clamped to action bounds
 * @param {boolean} isBitmap True if the editor is in bitmap mode, false if it is in vector mode
 * @returns {paper.Rectangle} the bounds within which mouse events should work in the paint editor
 */


exports.pan = pan;

var getActionBounds = function getActionBounds(isBitmap) {
  if (isBitmap) {
    return ART_BOARD_BOUNDS;
  }

  return _paper.default.view.bounds.unite(ART_BOARD_BOUNDS).intersect(MAX_WORKSPACE_BOUNDS);
};

exports.getActionBounds = getActionBounds;

var zoomToFit = function zoomToFit(isBitmap) {
  resetZoom();
  var bounds;

  if (isBitmap) {
    bounds = (0, _bitmap.getHitBounds)((0, _layer.getRaster)()).expand(BUFFER);
  } else {
    var items = (0, _selection.getAllRootItems)();

    var _iterator3 = (0, _createForOfIteratorHelper2.default)(items),
        _step3;

    try {
      for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
        var item = _step3.value;

        if (bounds) {
          bounds = bounds.unite(item.bounds);
        } else {
          bounds = item.bounds;
        }
      }
    } catch (err) {
      _iterator3.e(err);
    } finally {
      _iterator3.f();
    }
  }

  if (bounds && bounds.width && bounds.height) {
    var canvas = _paper.default.view.element; // Ratio of (sprite length plus padding on all sides) to viewport length.

    var ratio = _paper.default.view.zoom * Math.max(bounds.width * (1 + 2 * PADDING_PERCENT / 100) / canvas.clientWidth, bounds.height * (1 + 2 * PADDING_PERCENT / 100) / canvas.clientHeight); // Clamp ratio

    ratio = Math.max(Math.min(1, ratio), MIN_RATIO);

    if (ratio < 1) {
      _paper.default.view.center = bounds.center;
      _paper.default.view.zoom = _paper.default.view.zoom / ratio;
      resizeCrosshair();
      clampViewBounds();
    }
  } else {
    _log.default.warn('No bounds!');
  }
};

exports.zoomToFit = zoomToFit;