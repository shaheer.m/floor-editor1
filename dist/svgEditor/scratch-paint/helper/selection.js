"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setItemSelection = exports.selectRootItem = exports.selectAllSegments = exports.selectAllItems = exports.returnGreatestParent = exports.processRectangularSelection = exports.nameStackOfAllItems = exports.groupNameStackOfAllItems = exports.getSelectedSegments = exports.getSelectedRootItems = exports.getSelectedLeafItemsWithGroup = exports.getSelectedLeafItems = exports.getItems = exports.getAllSelectableRootItems = exports.getAllRootItems = exports.findGreaterParentName = exports.findGreaterParentAvailable = exports.findGreaterParentAttribute = exports.findGreaterParent = exports.deleteSelection = exports.cloneSelection = exports.clearSelection = exports.checkSingleParent = exports.checkIfSelectedItemHasName = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../lib/modes"));

var _group = require("./group");

var _item = require("./item");

var _compoundPath = require("./compound-path");

var _math = require("./math");

/**
 * Wrapper for paper.project.getItems that excludes our helper items
 * @param {?object} options See paper.js docs for paper.Item.getItems
 * @return {Array<paper.Item>} items that match options
 */
var getItems = function getItems(options) {
  var newMatcher = function newMatcher(item) {
    return !(item instanceof _paper.default.Layer) && item.layer.data && item.layer.data.isPaintingLayer && !item.locked && !item.isClipMask() && !(item.data && item.data.isHelperItem) && (!options.match || options.match(item));
  };

  var newOptions = (0, _objectSpread2.default)((0, _objectSpread2.default)({}, options), {}, {
    match: newMatcher
  });
  return _paper.default.project.getItems(newOptions);
};
/**
 * @param {boolean} includeGuides True if guide layer items like the bounding box should
 *     be included in the returned items.
 * @return {Array<paper.item>} all top-level (direct descendants of a paper.Layer) items
 */


exports.getItems = getItems;

var getAllRootItems = function getAllRootItems(includeGuides) {
  includeGuides = includeGuides || false;
  var allItems = [];

  var _iterator = (0, _createForOfIteratorHelper2.default)(_paper.default.project.layers),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var layer = _step.value;

      var _iterator2 = (0, _createForOfIteratorHelper2.default)(layer.children),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var child = _step2.value;

          // don't give guides back
          if (!includeGuides && child.guide) {
            continue;
          }

          allItems.push(child);
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return allItems;
};
/**
 * @return {Array<paper.item>} all top-level (direct descendants of a paper.Layer) items
 *     that aren't guide items or helper items.
 */


exports.getAllRootItems = getAllRootItems;

var getAllSelectableRootItems = function getAllSelectableRootItems() {
  var allItems = getAllRootItems();
  var selectables = [];

  for (var i = 0; i < allItems.length; i++) {
    if (allItems[i].data && !allItems[i].data.isHelperItem) {
      selectables.push(allItems[i]);
    }
  }

  return selectables;
};

exports.getAllSelectableRootItems = getAllSelectableRootItems;

var selectItemSegments = function selectItemSegments(item, state) {
  if (item.children) {
    for (var i = 0; i < item.children.length; i++) {
      var child = item.children[i];

      if (child.children && child.children.length > 0) {
        selectItemSegments(child, state);
      } else {
        child.fullySelected = state;
      }
    }
  } else {
    for (var _i = 0; _i < item.segments.length; _i++) {
      item.segments[_i].selected = state;
    }
  }
};

var _setGroupSelection = function _setGroupSelection(root, selected, fullySelected) {
  root.fullySelected = fullySelected;
  root.selected = selected; // select children of compound-path or group

  if ((0, _compoundPath.isCompoundPath)(root) || (0, _group.isGroup)(root)) {
    var children = root.children;

    if (children) {
      var _iterator3 = (0, _createForOfIteratorHelper2.default)(children),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var child = _step3.value;

          if ((0, _group.isGroup)(child)) {
            _setGroupSelection(child, selected, fullySelected);
          } else {
            child.fullySelected = fullySelected;
            child.selected = selected;
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }
    }
  }
};

var setItemSelection = function setItemSelection(item, state, fullySelected) {
  var parentGroup = (0, _group.getItemsGroup)(item);
  var itemsCompoundPath = (0, _compoundPath.getItemsCompoundPath)(item); // if selection is in a group, select group

  if (parentGroup) {
    // do it recursive
    setItemSelection(parentGroup, state, fullySelected);
  } else if (itemsCompoundPath) {
    _setGroupSelection(itemsCompoundPath, state, fullySelected);
  } else {
    if (item.data && item.data.noSelect) {
      return;
    }

    _setGroupSelection(item, state, fullySelected);
  }
};
/** @return {boolean} true if anything was selected */


exports.setItemSelection = setItemSelection;

var selectAllItems = function selectAllItems() {
  var items = getAllSelectableRootItems();
  if (items.length === 0) return false;

  for (var i = 0; i < items.length; i++) {
    setItemSelection(items[i], true);
  }

  return true;
};
/** @return {boolean} true if anything was selected */


exports.selectAllItems = selectAllItems;

var selectAllSegments = function selectAllSegments() {
  var items = getAllSelectableRootItems();
  if (items.length === 0) return false;

  for (var i = 0; i < items.length; i++) {
    selectItemSegments(items[i], true);
  }

  return true;
};
/** @param {!function} dispatchClearSelect Function to update the Redux select state */


exports.selectAllSegments = selectAllSegments;

var clearSelection = function clearSelection(dispatchClearSelect) {
  _paper.default.project.deselectAll();

  dispatchClearSelect();
};
/**
 * This gets all selected non-grouped items and groups
 * (alternative to paper.project.selectedItems, which includes
 * group children in addition to the group)
 * @return {Array<paper.Item>} in increasing Z order.
 */


exports.clearSelection = clearSelection;

var getSelectedRootItems = function getSelectedRootItems() {
  var allItems = getAllSelectableRootItems();
  var items = [];

  var _iterator4 = (0, _createForOfIteratorHelper2.default)(allItems),
      _step4;

  try {
    for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
      var item = _step4.value;

      if (item.selected) {
        items.push(item);
      } else if (item instanceof _paper.default.CompoundPath) {
        // Consider a compound path selected if any of its paths are selected
        var _iterator5 = (0, _createForOfIteratorHelper2.default)(item.children),
            _step5;

        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var child = _step5.value;

            if (child.selected) {
              items.push(item);
              break;
            }
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      }
    } // sort items by index (0 at bottom)

  } catch (err) {
    _iterator4.e(err);
  } finally {
    _iterator4.f();
  }

  items.sort(function (a, b) {
    return parseFloat(a.index) - parseFloat(b.index);
  });
  return items;
};
/**
 * This gets all selected items that are as deeply nested as possible. Does not
 * return the parent groups.
 * @return {Array<paper.Item>} in increasing Z order.
 */


exports.getSelectedRootItems = getSelectedRootItems;

var getSelectedLeafItems = function getSelectedLeafItems() {
  var allItems = _paper.default.project.selectedItems;
  var items = [];

  for (var i = 0; i < allItems.length; i++) {
    var item = allItems[i];

    if (!(item instanceof _paper.default.Layer) && !(0, _group.isGroup)(item) && item.data && !item.data.isSelectionBound) {
      items.push(item);
    }
  }

  items.sort(_math.sortItemsByZIndex);
  return items;
}; // retrieve items including both path and group


exports.getSelectedLeafItems = getSelectedLeafItems;

var getSelectedLeafItemsWithGroup = function getSelectedLeafItemsWithGroup() {
  var allItems = _paper.default.project.selectedItems;
  var items = [];

  for (var i = 0; i < allItems.length; i++) {
    var item = allItems[i];

    if (!(item instanceof _paper.default.Layer) && item.data && !item.data.isSelectionBound) {
      items.push(item);
    }
  }

  items.sort(_math.sortItemsByZIndex);
  return items;
};
/**
 * This gets all selected path segments.
 * @return {Array<paper.Segment>} selected segments
 */


exports.getSelectedLeafItemsWithGroup = getSelectedLeafItemsWithGroup;

var getSelectedSegments = function getSelectedSegments() {
  var selected = getSelectedLeafItems();
  var segments = [];

  var _iterator6 = (0, _createForOfIteratorHelper2.default)(selected),
      _step6;

  try {
    for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
      var item = _step6.value;

      if (!item.segments) {
        continue;
      }

      var _iterator7 = (0, _createForOfIteratorHelper2.default)(item.segments),
          _step7;

      try {
        for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
          var seg = _step7.value;

          if (seg.selected) {
            segments.push(seg);
          }
        }
      } catch (err) {
        _iterator7.e(err);
      } finally {
        _iterator7.f();
      }
    }
  } catch (err) {
    _iterator6.e(err);
  } finally {
    _iterator6.f();
  }

  return segments;
};

exports.getSelectedSegments = getSelectedSegments;

var _deleteItemSelection = function _deleteItemSelection(items, onUpdateImage) {
  // @todo: Update toolbar state on change
  if (items.length === 0) {
    return false;
  }

  for (var i = 0; i < items.length; i++) {
    items[i].remove();
  }

  onUpdateImage();
  return true;
}; // Return true if anything was removed


var _removeSelectedSegments = function _removeSelectedSegments(items, onUpdateImage) {
  var segmentsToRemove = [];

  for (var i = 0; i < items.length; i++) {
    if (!items[i].segments) continue;
    var segments = items[i].segments;

    for (var j = 0; j < segments.length; j++) {
      var seg = segments[j];

      if (seg.selected) {
        segmentsToRemove.push(seg);
      }
    }
  }

  var removedSegments = false;

  for (var _i2 = 0; _i2 < segmentsToRemove.length; _i2++) {
    var _seg = segmentsToRemove[_i2];

    _seg.remove();

    removedSegments = true;
  }

  if (removedSegments) {
    onUpdateImage();
  }

  return removedSegments;
}; // Return whether anything was deleted


var deleteSelection = function deleteSelection(mode, onUpdateImage) {
  if (mode === _modes.default.RESHAPE) {
    var _selectedItems = getSelectedLeafItems(); // If there are points selected remove them. If not delete the item selected.


    if (_removeSelectedSegments(_selectedItems, onUpdateImage)) {
      return true;
    }

    return _deleteItemSelection(_selectedItems, onUpdateImage);
  }

  var selectedItems = getSelectedRootItems();
  return _deleteItemSelection(selectedItems, onUpdateImage);
};

exports.deleteSelection = deleteSelection;

var cloneSelection = function cloneSelection(recursive, onUpdateImage) {
  var selectedItems = recursive ? getSelectedLeafItems() : getSelectedRootItems();

  for (var i = 0; i < selectedItems.length; i++) {
    var item = selectedItems[i];
    item.clone();
    item.selected = false;
  }

  onUpdateImage();
};

exports.cloneSelection = cloneSelection;

var _checkBoundsItem = function _checkBoundsItem(selectionRect, item, event) {
  var itemBounds = new _paper.default.Path([item.localToGlobal(item.internalBounds.topLeft), item.localToGlobal(item.internalBounds.topRight), item.localToGlobal(item.internalBounds.bottomRight), item.localToGlobal(item.internalBounds.bottomLeft)]);
  itemBounds.closed = true;
  itemBounds.guide = true;

  for (var i = 0; i < itemBounds.segments.length; i++) {
    var seg = itemBounds.segments[i];

    if (selectionRect.contains(seg.point) || i === 0 && selectionRect.getIntersections(itemBounds).length > 0) {
      if (event.modifiers.shift && item.selected) {
        setItemSelection(item, false);
      } else {
        setItemSelection(item, true);
      }

      itemBounds.remove();
      return true;
    }
  }

  itemBounds.remove();
};

var _handleRectangularSelectionItems = function _handleRectangularSelectionItems(item, event, rect, mode, root) {
  if ((0, _item.isPathItem)(item)) {
    var segmentMode = false; // first round checks for segments inside the selectionRect

    for (var j = 0; j < item.segments.length; j++) {
      var seg = item.segments[j];

      if (rect.contains(seg.point)) {
        if (mode === _modes.default.RESHAPE) {
          if (event.modifiers.shift && seg.selected) {
            seg.selected = false;
          } else {
            seg.selected = true;
          }

          segmentMode = true;
        } else {
          if (event.modifiers.shift && item.selected) {
            setItemSelection(root, false);
          } else {
            setItemSelection(root, true, true
            /* fullySelected */
            );
          }

          return false;
        }
      }
    } // second round checks for path intersections


    var intersections = item.getIntersections(rect);

    if (intersections.length > 0 && !segmentMode) {
      // if in reshape mode, select the curves that intersect
      // with the selectionRect
      if (mode === _modes.default.RESHAPE) {
        for (var k = 0; k < intersections.length; k++) {
          var curve = intersections[k].curve; // intersections contains every curve twice because
          // the selectionRect intersects a circle always at
          // two points. so we skip every other curve

          if (k % 2 === 1) {
            continue;
          }

          if (event.modifiers.shift) {
            curve.selected = !curve.selected;
          } else {
            curve.selected = true;
          }
        }
      } else {
        if (event.modifiers.shift && item.selected) {
          setItemSelection(item, false);
        } else {
          setItemSelection(item, true);
        }

        return false;
      }
    } // @todo: Update toolbar state on change

  } else if ((0, _item.isBoundsItem)(item)) {
    if (_checkBoundsItem(rect, item, event)) {
      return false;
    }
  }

  return true;
}; // if the rectangular selection found a group, drill into it recursively


var _rectangularSelectionGroupLoop = function _rectangularSelectionGroupLoop(group, rect, root, event, mode) {
  for (var i = 0; i < group.children.length; i++) {
    var child = group.children[i];

    if ((0, _group.isGroup)(child) || (0, _item.isCompoundPathItem)(child)) {
      _rectangularSelectionGroupLoop(child, rect, root, event, mode);
    } else {
      _handleRectangularSelectionItems(child, event, rect, mode, root);
    }
  }

  return true;
};
/**
 * Called after drawing a selection rectangle in a select mode. In reshape mode, this
 * selects all control points and curves within the rectangle. In select mode, this
 * selects all items and groups that intersect the rectangle
 * @param {!MouseEvent} event The mouse event to draw the rectangle
 * @param {!paper.Rect} rect The selection rectangle
 * @param {Modes} mode The mode of the paint editor when drawing the rectangle
 */


var processRectangularSelection = function processRectangularSelection(event, rect, mode) {
  var allItems = getAllSelectableRootItems();

  for (var i = 0; i < allItems.length; i++) {
    var item = allItems[i];

    if (mode === _modes.default.RESHAPE && (0, _item.isPGTextItem)((0, _item.getRootItem)(item))) {
      continue;
    }

    if ((0, _group.isGroup)(item) || (0, _item.isCompoundPathItem)(item)) {
      // check for item segment points inside
      _rectangularSelectionGroupLoop(item, rect, item, event, mode);
    } else {
      _handleRectangularSelectionItems(item, event, rect, mode, item);
    }
  }
};
/**
 * When switching to the select tool while having a child object of a
 * compound path selected, deselect the child and select the compound path
 * instead. (otherwise the compound path breaks because of scale-grouping)
 */


exports.processRectangularSelection = processRectangularSelection;

var selectRootItem = function selectRootItem() {
  var items = getSelectedLeafItems();

  var _iterator8 = (0, _createForOfIteratorHelper2.default)(items),
      _step8;

  try {
    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
      var item = _step8.value;

      if ((0, _compoundPath.isCompoundPathChild)(item)) {
        var cp = (0, _compoundPath.getItemsCompoundPath)(item);
        setItemSelection(cp, true, true
        /* fullySelected */
        );
      }

      var rootItem = (0, _item.getRootItem)(item);

      if (item !== rootItem) {
        setItemSelection(rootItem, true, true
        /* fullySelected */
        );
      }
    }
  } catch (err) {
    _iterator8.e(err);
  } finally {
    _iterator8.f();
  }
}; // // returns the item (group) with largest number of children.
// // therefore find the item with largest number of 'group' as children
// const returnGreatestParent = function (items) {
//     let max = 0;
//     let maxChild;
//     for (const item of items) {
//         if (item._children) {
//             let children = item._children;
//             let groupNum = 0;
//             for (let child of children) {
//               //  if (child._children) {
//                 if (isGroup(child)) {
//                     groupNum++;
//                 }
//             }
//             if (groupNum >= max) {
//                 max = groupNum;
//                 maxChild = item;
//             }
//         }
//     }
//     return maxChild;
// };
// returns the item (group) with largest number of children.
// check children of children as well
// recursively find the total number of grand


exports.selectRootItem = selectRootItem;

var returnGreatestParent = function returnGreatestParent(items) {
  var max = 0;
  var maxChild;

  var _iterator9 = (0, _createForOfIteratorHelper2.default)(items),
      _step9;

  try {
    for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
      var item = _step9.value;

      if (item._children) {
        var pathNum = findGrandChildren(item._children);

        if (pathNum >= max) {
          max = pathNum;
          maxChild = item;
        }
      }
    }
  } catch (err) {
    _iterator9.e(err);
  } finally {
    _iterator9.f();
  }

  return maxChild;
};

exports.returnGreatestParent = returnGreatestParent;

var findGrandChildren = function findGrandChildren(children) {
  var pathNumber = 0;

  var _iterator10 = (0, _createForOfIteratorHelper2.default)(children),
      _step10;

  try {
    for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
      var item = _step10.value;

      if (item.className && item.className === 'Path') {
        pathNumber++;
      } else {
        if (item._children) {
          pathNumber += findGrandChildren(item._children);
        }
      }
    }
  } catch (err) {
    _iterator10.e(err);
  } finally {
    _iterator10.f();
  }

  return pathNumber;
}; // check if all items belong to a single parent


var checkSingleParent = function checkSingleParent(items) {
  var greaterParent = null;

  var _iterator11 = (0, _createForOfIteratorHelper2.default)(items),
      _step11;

  try {
    for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
      var item = _step11.value;
      var parentStack = findParentStack(item, [item._id]);

      if (!greaterParent) {
        greaterParent = parentStack[parentStack.length - 2];
      }

      if (greaterParent !== parentStack[parentStack.length - 2]) {
        return false;
      }
    }
  } catch (err) {
    _iterator11.e(err);
  } finally {
    _iterator11.f();
  }

  return true;
}; // returns the stack of parents of a single item


exports.checkSingleParent = checkSingleParent;

var findParentStack = function findParentStack(item) {
  var parentStack = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  if (item._parent && item._parent._id) {
    parentStack.push(item._parent._id);
    return findParentStack(item._parent, parentStack);
  } else {
    return parentStack;
  }
}; // returns the name stack of the parents of a single item


var findGreaterParentName = function findGreaterParentName(item, parentNames) {
  if (item._parent) {
    parentNames.push(item._parent._data.id);
    return findGreaterParentName(item._parent, parentNames);
  } else {
    return parentNames;
  }
}; // returns the attribute stack of the parents of a single item


exports.findGreaterParentName = findGreaterParentName;

var findGreaterParentAttribute = function findGreaterParentAttribute(item, parentAttributes) {
  if (item._parent) {
    parentAttributes.push(item._parent._data.customAttributes);
    return findGreaterParentAttribute(item._parent, parentAttributes);
  } else {
    return parentAttributes;
  }
}; //returns the available stack of parents of a single item


exports.findGreaterParentAttribute = findGreaterParentAttribute;

var findGreaterParentAvailable = function findGreaterParentAvailable(item, parentAttributes) {
  if (item._parent) {
    parentAttributes.push(item._parent._data.unavailable);
    return findGreaterParentAttribute(item._parent, parentAttributes);
  } else {
    return parentAttributes;
  }
}; //returns the stack of parents of a single item


exports.findGreaterParentAvailable = findGreaterParentAvailable;

var findGreaterParent = function findGreaterParent(item, parents) {
  if (item._parent) {
    parents.push(item._parent);
    return findGreaterParent(item._parent, parents);
  } else {
    return parents;
  }
}; // returns the stack of names of all items


exports.findGreaterParent = findGreaterParent;

var nameStackOfAllItems = function nameStackOfAllItems(items) {
  var names = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  var _iterator12 = (0, _createForOfIteratorHelper2.default)(items),
      _step12;

  try {
    for (_iterator12.s(); !(_step12 = _iterator12.n()).done;) {
      var item = _step12.value;

      if (item._data.id) {
        names.push(item._data.id);
      }

      if (item._children) {
        names = nameStackOfAllItems(item._children, names);
      }
    }
  } catch (err) {
    _iterator12.e(err);
  } finally {
    _iterator12.f();
  }

  return names;
}; // returns the stack of group names of all items


exports.nameStackOfAllItems = nameStackOfAllItems;

var groupNameStackOfAllItems = function groupNameStackOfAllItems() {
  var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : getAllSelectableRootItems();
  var names = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  var _iterator13 = (0, _createForOfIteratorHelper2.default)(items),
      _step13;

  try {
    for (_iterator13.s(); !(_step13 = _iterator13.n()).done;) {
      var item = _step13.value;

      if (item._data && item._data.groupName) {
        names.push(item._data.groupName);
      }

      if (item._children) {
        names = groupNameStackOfAllItems(item._children, names);
      }
    }
  } catch (err) {
    _iterator13.e(err);
  } finally {
    _iterator13.f();
  }

  return names;
};

exports.groupNameStackOfAllItems = groupNameStackOfAllItems;

var checkIfSelectedItemHasName = function checkIfSelectedItemHasName(items) {
  var _iterator14 = (0, _createForOfIteratorHelper2.default)(items),
      _step14;

  try {
    for (_iterator14.s(); !(_step14 = _iterator14.n()).done;) {
      var item = _step14.value;

      if (item._data.id) {
        return true;
      }

      if (item._children) {
        if (checkIfSelectedItemHasName(item._children)) {
          return true;
        }
      }
    }
  } catch (err) {
    _iterator14.e(err);
  } finally {
    _iterator14.f();
  }

  return false;
};

exports.checkIfSelectedItemHasName = checkIfSelectedItemHasName;