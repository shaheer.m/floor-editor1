"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _stylePath = require("../style-path");

var _selection = require("../selection");

var _math = require("../math");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

/**
 * Tool for drawing rectangles.
 */
var RectTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(RectTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(RectTool);

  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function RectTool(setSelectedItems, clearSelectedItems, setCursor, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, RectTool);
    _this = _super.call(this);
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.RECT, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    var nudgeTool = new _nudgeTool.default(_modes.default.RECT, _this.boundingBoxTool, onUpdateImage); // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;
    _this.rect = null;
    _this.colorState = null;
    _this.isBoundingBoxMode = null;
    _this.active = false;
    return _this;
  }

  (0, _createClass2.default)(RectTool, [{
    key: "getHitOptions",
    value: function getHitOptions() {
      return {
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item.data && (hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle) || hitResult.item.selected;
        },
        // Allow hits on bounding box and selected only
        tolerance: RectTool.TOLERANCE / _paper.default.view.zoom
      };
    }
    /**
     * Should be called if the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);
    }
  }, {
    key: "setColorState",
    value: function setColorState(colorState) {
      this.colorState = colorState;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;

      if (this.boundingBoxTool.onMouseDown(event, false
      /* clone */
      , false
      /* multiselect */
      , false
      /* doubleClicked */
      , this.getHitOptions())) {
        this.isBoundingBoxMode = true;
      } else {
        this.isBoundingBoxMode = false;
        (0, _selection.clearSelection)(this.clearSelectedItems);
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseDrag(event);
        return;
      }

      if (this.rect) {
        this.rect.remove();
      }

      var rect = new _paper.default.Rectangle(event.downPoint, event.point);
      var squareDimensions = (0, _math.getSquareDimensions)(event.downPoint, event.point);

      if (event.modifiers.shift) {
        rect.size = squareDimensions.size.abs();
      }

      this.rect = new _paper.default.Path.Rectangle(rect);

      if (event.modifiers.alt) {
        this.rect.position = event.downPoint;
      } else if (event.modifiers.shift) {
        this.rect.position = squareDimensions.position;
      } else {
        var dimensions = event.point.subtract(event.downPoint);
        this.rect.position = event.downPoint.add(dimensions.multiply(0.5));
      }

      (0, _stylePath.styleShape)(this.rect, this.colorState);
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseUp(event);
        this.isBoundingBoxMode = null;
        return;
      }

      if (this.rect) {
        if (this.rect.area < RectTool.TOLERANCE / _paper.default.view.zoom) {
          // Tiny rectangle created unintentionally?
          this.rect.remove();
          this.rect = null;
        } else {
          this.rect.selected = true;
          this.setSelectedItems();
          this.onUpdateImage();
          this.rect = null;
        }
      }

      this.active = false;
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.boundingBoxTool.onMouseMove(event, this.getHitOptions());
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.boundingBoxTool.deactivateTool();
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }]);
  return RectTool;
}(_paper.default.Tool);

var _default = RectTool;
exports.default = _default;