"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setRasterImage = setRasterImage;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../../log/log"));

var _format = _interopRequireDefault(require("../../lib/format"));

var _undo = require("../../helper/undo");

var _view = require("../../helper/view");

var _math = require("../../helper/math");

var _group = require("../../helper/group");

var _chairIcon = _interopRequireDefault(require("../../../../assets/chair-icon-1.svg"));

var _chairIcon2 = _interopRequireDefault(require("../../../../assets/chair-icon-2.svg"));

var _chairIcon3 = _interopRequireDefault(require("../../../../assets/chair-icon-3.svg"));

function setRasterImage(paperCanvas, asset) {
  var svg;

  if (asset === 0) {
    svg = _chairIcon.default;
  } else if (asset === 1) {
    svg = _chairIcon2.default;
  } else {
    svg = _chairIcon3.default;
  } //  const paperCanvas = this;
  // Pre-process SVG to prevent parsing errors (discussion from #213)
  // 1. Remove svg: namespace on elements.
  // TODO: remove


  svg = svg.split(/<\s*svg:/).join('<');
  svg = svg.split(/<\/\s*svg:/).join('</'); // 2. Add root svg namespace if it does not exist.

  var svgAttrs = svg.match(/<svg [^>]*>/);

  if (svgAttrs && svgAttrs[0].indexOf('xmlns=') === -1) {
    svg = svg.replace('<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ');
  } // Get the origin which the viewBox is defined relative to. During import, Paper will translate
  // the viewBox to start at (0, 0), and we need to translate it back for some costumes to render
  // correctly.


  var parser = new DOMParser();
  var svgDom = parser.parseFromString(svg, 'text/xml');
  var viewBox = svgDom.documentElement.attributes.viewBox ? svgDom.documentElement.attributes.viewBox.value.match(/\S+/g) : null;

  if (viewBox) {
    for (var i = 0; i < viewBox.length; i++) {
      viewBox[i] = parseFloat(viewBox[i]);
    }
  }

  _paper.default.project.importSVG(svg, {
    expandShapes: true,
    onLoad: function onLoad(item) {
      var _this = this;

      if (!item) {
        _log.default.error('SVG import failed:');

        _log.default.info(svg);

        this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
        (0, _undo.performSnapshot)(paperCanvas.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
        return;
      }

      item.remove(); // Without the callback, rasters' load function has not been called yet, and they are
      // positioned incorrectly

      paperCanvas.queuedImport = recalibrateSize(function () {
        var mask;

        if (item.clipped) {
          var _iterator = (0, _createForOfIteratorHelper2.default)(item.children),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var child = _step.value;

              if (child.isClipMask()) {
                mask = child;
                break;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          mask.clipMask = false;
        } else {
          mask = new _paper.default.Shape.Rectangle(item.bounds);
        }

        mask.guide = true;
        mask.locked = true;
        mask.matrix = new _paper.default.Matrix(); // Identity
        // Set the artwork to get clipped at the max costume size
        // mask.size.height = MAX_WORKSPACE_BOUNDS.height;
        // mask.size.width = MAX_WORKSPACE_BOUNDS.width;
        // mask.setPosition(CENTER);
        // paper.project.activeLayer.addChild(mask);
        // mask.clipMask = true;
        // Reduce single item nested in groups

        if (item instanceof _paper.default.Group && item.children.length === 1) {
          item = item.reduce();
        }

        (0, _math.ensureClockwise)(item);
        (0, _math.scaleWithStrokes)(item, 2, new _paper.default.Point()); // Import at 2x
        // Apply rotation center

        if (typeof rotationCenterX !== 'undefined' && typeof rotationCenterY !== 'undefined') {
          var rotationPoint = new _paper.default.Point(0, 0);

          if (viewBox && viewBox.length >= 2 && !isNaN(viewBox[0]) && !isNaN(viewBox[1])) {
            rotationPoint = rotationPoint.subtract(viewBox[0], viewBox[1]);
          }

          item.translate(_view.CENTER.subtract(rotationPoint.multiply(2)));
        } else {
          // Center
          item.translate(_view.CENTER.subtract(100, 100));
        }

        _paper.default.project.activeLayer.insertChild(0, item);

        setTimeout(function () {
          item.bringToFront();

          if ((0, _group.isGroup)(item)) {
            // Fixes an issue where we may export empty groups
            var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.children),
                _step2;

            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var _child = _step2.value;

                if ((0, _group.isGroup)(_child) && _child.children.length === 0) {
                  _child.remove();
                }
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }

            (0, _group.ungroupItems)([item]);
          }
        }, 100);
        (0, _undo.performSnapshot)(_this.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);

        _this.maybeZoomToFit(); // paperCanvas.props.updateViewBounds(paper.view.matrix);
        // paperCanvas.initializeSvg(item, rotationCenterX, rotationCenterY, viewBox);

      });
    }
  });
}

function recalibrateSize(callback) {
  // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
  // When these are out of sync, the mouse events in the paint editor don't line up correctly.
  return window.setTimeout(function () {
    // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
    // This could also be called before paper.view exists.
    // In either case, return early if so without running the callback.
    if (!_paper.default.view) return; // Prevent blurriness caused if the "CSS size" of the element is a float--
    // setting canvas dimensions to floats floors them, but we need to round instead

    var elemSize = _paper.default.DomElement.getSize(_paper.default.view.element);

    elemSize.width = Math.round(elemSize.width);
    elemSize.height = Math.round(elemSize.height);

    _paper.default.view.setViewSize(elemSize);

    if (callback) callback();
  }, 0);
}