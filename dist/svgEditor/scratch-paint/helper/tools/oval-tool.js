"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _stylePath = require("../style-path");

var _selection = require("../selection");

var _math = require("../math");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

/**
 * Tool for drawing ovals.
 */
var OvalTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(OvalTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(OvalTool);

  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function OvalTool(setSelectedItems, clearSelectedItems, setCursor, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, OvalTool);
    _this = _super.call(this);
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.OVAL, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    var nudgeTool = new _nudgeTool.default(_modes.default.OVAL, _this.boundingBoxTool, onUpdateImage); // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;
    _this.oval = null;
    _this.colorState = null;
    _this.isBoundingBoxMode = null;
    _this.active = false;
    return _this;
  }

  (0, _createClass2.default)(OvalTool, [{
    key: "getHitOptions",
    value: function getHitOptions() {
      return {
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item.data && (hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle) || hitResult.item.selected;
        },
        // Allow hits on bounding box and selected only
        tolerance: OvalTool.TOLERANCE / _paper.default.view.zoom
      };
    }
    /**
     * Should be called if the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);
    }
  }, {
    key: "setColorState",
    value: function setColorState(colorState) {
      this.colorState = colorState;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;

      if (this.boundingBoxTool.onMouseDown(event, false
      /* clone */
      , false
      /* multiselect */
      , false
      /* doubleClicked */
      , this.getHitOptions())) {
        this.isBoundingBoxMode = true;
      } else {
        this.isBoundingBoxMode = false;
        (0, _selection.clearSelection)(this.clearSelectedItems);
        this.oval = new _paper.default.Shape.Ellipse({
          point: event.downPoint,
          size: 0
        });
        (0, _stylePath.styleShape)(this.oval, this.colorState);
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseDrag(event);
        return;
      }

      var downPoint = new _paper.default.Point(event.downPoint.x, event.downPoint.y);
      var point = new _paper.default.Point(event.point.x, event.point.y);
      var squareDimensions = (0, _math.getSquareDimensions)(event.downPoint, event.point);

      if (event.modifiers.shift) {
        this.oval.size = squareDimensions.size.abs();
      } else {
        this.oval.size = downPoint.subtract(point);
      }

      if (event.modifiers.alt) {
        this.oval.position = downPoint;
      } else if (event.modifiers.shift) {
        this.oval.position = squareDimensions.position;
      } else {
        this.oval.position = downPoint.subtract(this.oval.size.multiply(0.5));
      }

      (0, _stylePath.styleShape)(this.oval, this.colorState);
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.boundingBoxTool.onMouseMove(event, this.getHitOptions());
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseUp(event);
        this.isBoundingBoxMode = null;
        return;
      }

      if (this.oval) {
        if (Math.abs(this.oval.size.width * this.oval.size.height) < OvalTool.TOLERANCE / _paper.default.view.zoom) {
          // Tiny oval created unintentionally?
          this.oval.remove();
          this.oval = null;
        } else {
          var ovalPath = this.oval.toPath(true
          /* insert */
          );
          this.oval.remove();
          this.oval = null;
          ovalPath.selected = true;
          this.setSelectedItems();
          this.onUpdateImage();
        }
      }

      this.active = false;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.boundingBoxTool.deactivateTool();
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }]);
  return OvalTool;
}(_paper.default.Tool);

var _default = OvalTool;
exports.default = _default;