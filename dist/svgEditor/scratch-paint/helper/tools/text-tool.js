"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _selection = require("../selection");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

var _guides = require("../guides");

var _layer = require("../layer");

/**
 * Tool for adding text. Text elements have limited editability; they can't be reshaped,
 * drawn on or erased. This way they can preserve their ability to have the text edited.
 */
var TextTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(TextTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(TextTool);

  /**
   * @param {HTMLTextAreaElement} textAreaElement dom element for the editable text field
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {!function} setTextEditTarget Call to set text editing target whenever text editing is active
   * @param {!function} changeFont Call to change the font in the dropdown
   * @param {?boolean} isBitmap True if text should be rasterized once it's deselected
   */
  function TextTool(textAreaElement, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage, setTextEditTarget, changeFont, isBitmap) {
    var _this;

    (0, _classCallCheck2.default)(this, TextTool);
    _this = _super.call(this);
    _this.element = textAreaElement;
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.setTextEditTarget = setTextEditTarget;
    _this.changeFont = changeFont;
    var paintMode = isBitmap ? _modes.default.BIT_TEXT : _modes.default.TEXT;
    _this.boundingBoxTool = new _boundingBoxTool.default(paintMode, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    _this.nudgeTool = new _nudgeTool.default(paintMode, _this.boundingBoxTool, onUpdateImage);
    _this.isBitmap = isBitmap; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onKeyUp = _this.handleKeyUp;
    _this.onKeyDown = _this.handleKeyDown;
    _this.textBox = null;
    _this.guide = null;
    _this.colorState = null;
    _this.mode = null;
    _this.active = false;
    _this.lastTypeEvent = null;
    _this.lastEvent = null; // If text selected and then activate this tool, switch to text edit mode for that text
    // If double click on text while in select mode, does mode change to text mode? Text fully selected by default

    return _this;
  }

  (0, _createClass2.default)(TextTool, [{
    key: "getBoundingBoxHitOptions",
    value: function getBoundingBoxHitOptions() {
      return {
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item.data && (hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle) || hitResult.item.selected;
        },
        // Allow hits on bounding box and selected only
        tolerance: TextTool.TOLERANCE / _paper.default.view.zoom
      };
    }
  }, {
    key: "getTextEditHitOptions",
    value: function getTextEditHitOptions() {
      return {
        class: _paper.default.PointText,
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item && !(hitResult.item.data && hitResult.item.data.isHelperItem) && !hitResult.item.selected;
        },
        // Unselected only
        tolerance: TextTool.TOLERANCE / _paper.default.view.zoom
      };
    }
    /**
     * Called when the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);

      if ((!this.textBox || !this.textBox.parent) && selectedItems && selectedItems.length === 1 && selectedItems[0] instanceof _paper.default.PointText) {
        // Infer that an undo occurred and get back the active text
        this.textBox = selectedItems[0];
        this.mode = TextTool.SELECT_MODE;
      }
    }
  }, {
    key: "setFont",
    value: function setFont(font) {
      this.font = font;

      if (this.textBox) {
        this.textBox.font = font;
      }

      var selected = (0, _selection.getSelectedLeafItems)();

      var _iterator = (0, _createForOfIteratorHelper2.default)(selected),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;

          if (item instanceof _paper.default.PointText) {
            item.font = font;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this.element.style.fontFamily = font;
      this.setSelectedItems();
    } // Allow other tools to cancel text edit mode

  }, {
    key: "onTextEditCancelled",
    value: function onTextEditCancelled() {
      if (this.mode !== TextTool.TEXT_EDIT_MODE) {
        return;
      }

      this.endTextEdit();
      this.beginSelect();
    }
    /**
     * Called when the view matrix changes
     * @param {paper.Matrix} viewMtx applied to paper.view
     */

  }, {
    key: "onViewBoundsChanged",
    value: function onViewBoundsChanged(viewMtx) {
      if (this.mode !== TextTool.TEXT_EDIT_MODE) {
        return;
      }

      this.calculateMatrix(viewMtx);
    }
  }, {
    key: "calculateMatrix",
    value: function calculateMatrix(viewMtx) {
      var textBoxMtx = this.textBox.matrix;
      var calculated = new _paper.default.Matrix(); // In RTL, the element is moved relative to its parent's right edge instead of its left
      // edge. We need to correct for this in order for the element to overlap the object in paper.

      var tx = 0;

      if (this.rtl && this.element.parentElement) {
        tx = -this.element.parentElement.clientWidth;
      } // The transform origin in paper is x at justification side, y at the baseline of the text.
      // The offset from (0, 0) to the upper left corner is recorded by internalBounds
      // (so this.textBox.internalBounds.y is negative).
      // Move the transform origin down to the text baseline to match paper


      this.element.style.transformOrigin = "".concat(-this.textBox.internalBounds.x, "px ").concat(-this.textBox.internalBounds.y, "px"); // Start by translating the element up so that its (0, 0) is now at the text baseline, like in paper

      calculated.translate(tx, this.textBox.internalBounds.y);
      calculated.append(viewMtx);
      calculated.append(textBoxMtx);
      this.element.style.transform = "matrix(".concat(calculated.a, ", ").concat(calculated.b, ", ").concat(calculated.c, ", ").concat(calculated.d, ",\n             ").concat(calculated.tx, ", ").concat(calculated.ty, ")");
    }
  }, {
    key: "setColorState",
    value: function setColorState(colorState) {
      this.colorState = colorState;
    }
    /** @param {boolean} isRtl True if paint editor is in right-to-left layout (e.g. Hebrew language) */

  }, {
    key: "setRtl",
    value: function setRtl(isRtl) {
      this.rtl = isRtl;
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      var hitResults = _paper.default.project.hitTestAll(event.point, this.getTextEditHitOptions());

      if (hitResults.length) {
        document.body.style.cursor = 'text';
      } else {
        document.body.style.cursor = 'auto';
      }

      this.boundingBoxTool.onMouseMove(event, this.getBoundingBoxHitOptions());
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true; // Check if double clicked

      var doubleClicked = this.lastEvent && event.event.timeStamp - this.lastEvent.event.timeStamp < TextTool.DOUBLE_CLICK_MILLIS;
      this.lastEvent = event;

      if (doubleClicked && this.mode === TextTool.SELECT_MODE && this.textBox.hitTest(event.point)) {
        // Double click in select mode moves you to text edit mode
        this.endSelect();
        this.beginTextEdit(this.textBox);
        this.element.select();
        return;
      } // In select mode staying in select mode


      if (this.boundingBoxTool.onMouseDown(event, false
      /* clone */
      , false
      /* multiselect */
      , false
      /* doubleClicked */
      , this.getBoundingBoxHitOptions())) {
        return;
      } // We clicked away from the item, so end the current mode


      var lastMode = this.mode;

      if (this.mode === TextTool.SELECT_MODE) {
        this.endSelect();

        if (this.isBitmap) {
          this.commitText();
        }
      } else if (this.mode === TextTool.TEXT_EDIT_MODE) {
        this.endTextEdit();
      }

      var hitResults = _paper.default.project.hitTestAll(event.point, this.getTextEditHitOptions());

      if (hitResults.length) {
        // Clicking a different text item to begin text edit mode on that item
        this.beginTextEdit(hitResults[0].item);
      } else if (lastMode === TextTool.TEXT_EDIT_MODE) {
        // In text mode clicking away to begin select mode
        this.beginSelect();
      } else {
        // In no mode or select mode clicking away to begin text edit mode
        this.textBox = new _paper.default.PointText({
          point: event.point,
          content: '',
          font: this.font,
          fontSize: 40,
          // TODO: style using gradient
          // https://github.com/LLK/scratch-paint/issues/1164
          fillColor: this.colorState.fillColor.primary,
          // Default leading for both the HTML text area and paper.PointText
          // is 120%, but for some reason they are slightly off from each other.
          // This value was obtained experimentally.
          leading: 46.15
        });
        this.beginTextEdit(this.textBox);
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.mode === TextTool.SELECT_MODE) {
        this.boundingBoxTool.onMouseDrag(event);
        return;
      }
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.mode === TextTool.SELECT_MODE) {
        this.boundingBoxTool.onMouseUp(event);
        this.isBoundingBoxMode = null;
        return;
      }

      this.active = false;
    }
  }, {
    key: "handleKeyUp",
    value: function handleKeyUp(event) {
      if (event.event.target instanceof HTMLInputElement) {
        // Ignore nudge if a text input field is focused
        return;
      }

      if (this.mode === TextTool.SELECT_MODE) {
        this.nudgeTool.onKeyUp(event);
      }
    }
  }, {
    key: "handleKeyDown",
    value: function handleKeyDown(event) {
      if (event.event.target instanceof HTMLInputElement) {
        // Ignore nudge if a text input field is focused
        return;
      }

      if (this.mode === TextTool.TEXT_EDIT_MODE && event.key === 'escape') {
        this.endTextEdit();
      }

      if (this.mode === TextTool.SELECT_MODE) {
        this.nudgeTool.onKeyDown(event);
      }
    }
  }, {
    key: "handleTextInput",
    value: function handleTextInput(event) {
      // Save undo state if you paused typing for long enough.
      if (this.lastTypeEvent && event.timeStamp - this.lastTypeEvent.timeStamp > TextTool.TYPING_TIMEOUT_MILLIS) {
        // Select the textbox so that it will be selected if the user performs undo.
        this.textBox.selected = true;
        this.onUpdateImage();
        this.textBox.selected = false;
      }

      this.lastTypeEvent = event;

      if (this.mode === TextTool.TEXT_EDIT_MODE) {
        this.textBox.content = this.element.value;
      }

      this.resizeGuide();
    }
  }, {
    key: "resizeGuide",
    value: function resizeGuide() {
      if (this.guide) this.guide.remove();
      this.guide = (0, _guides.hoverBounds)(this.textBox, TextTool.TEXT_PADDING);
      this.guide.dashArray = [4, 4]; // Prevent line from wrapping

      this.element.style.width = "".concat(this.textBox.internalBounds.width + 1, "px");
      this.element.style.height = "".concat(this.textBox.internalBounds.height, "px"); // The transform origin needs to be updated in RTL because this.textBox.internalBounds.x
      // changes as you type

      if (this.rtl) {
        this.element.style.transformOrigin = "".concat(-this.textBox.internalBounds.x, "px ").concat(-this.textBox.internalBounds.y, "px");
      }
    }
  }, {
    key: "beginSelect",
    value: function beginSelect() {
      if (this.textBox) {
        this.mode = TextTool.SELECT_MODE;
        this.textBox.selected = true;
        this.setSelectedItems();
      }
    }
  }, {
    key: "endSelect",
    value: function endSelect() {
      (0, _selection.clearSelection)(this.clearSelectedItems);
      this.mode = null;
    }
    /**
     * @param {paper.PointText} textBox Text object to begin text edit on
     */

  }, {
    key: "beginTextEdit",
    value: function beginTextEdit(textBox) {
      this.textBox = textBox;
      this.mode = TextTool.TEXT_EDIT_MODE;
      this.setTextEditTarget(this.textBox.id);

      if (this.font !== this.textBox.font) {
        this.changeFont(this.textBox.font);
      }

      this.element.style.fontSize = "".concat(this.textBox.fontSize, "px");
      this.element.style.lineHeight = this.textBox.leading / this.textBox.fontSize;
      this.element.style.display = 'initial';
      this.element.value = textBox.content ? textBox.content : '';
      this.calculateMatrix(_paper.default.view.matrix);

      if (this.rtl) {
        // make both the textbox and the textarea element grow to the left
        this.textBox.justification = 'right';
      } else {
        this.textBox.justification = 'left';
      }

      this.element.focus({
        preventScroll: true
      });
      this.eventListener = this.handleTextInput.bind(this);
      this.element.addEventListener('input', this.eventListener);
      this.resizeGuide();
    }
  }, {
    key: "endTextEdit",
    value: function endTextEdit() {
      if (this.mode !== TextTool.TEXT_EDIT_MODE) {
        return;
      }

      this.mode = null; // Remove invisible textboxes

      if (this.textBox && this.textBox.content.trim() === '') {
        this.textBox.remove();
        this.textBox = null;
      } // Remove guide


      if (this.guide) {
        this.guide.remove();
        this.guide = null;
        this.setTextEditTarget();
      }

      this.element.style.display = 'none';

      if (this.eventListener) {
        this.element.removeEventListener('input', this.eventListener);
        this.eventListener = null;
      }

      if (this.textBox && this.lastTypeEvent) {
        // Finished editing a textbox, save undo state
        // Select the textbox so that it will be selected if the user performs undo.
        this.textBox.selected = true;
        this.onUpdateImage();
        this.textBox.selected = false;
        this.lastTypeEvent = null;
      }
    }
  }, {
    key: "commitText",
    value: function commitText() {
      if (!this.textBox || !this.textBox.parent) return; // @todo get crisp text https://github.com/LLK/scratch-paint/issues/508

      var textRaster = this.textBox.rasterize(72, false
      /* insert */
      , this.textBox.drawnBounds);
      this.textBox.remove();
      this.textBox = null;
      (0, _layer.getRaster)().drawImage(textRaster.canvas, new _paper.default.Point(Math.floor(textRaster.bounds.x), Math.floor(textRaster.bounds.y)));
      this.onUpdateImage();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      if (this.textBox && this.textBox.content.trim() === '') {
        this.textBox.remove();
        this.textBox = null;
      }

      this.endTextEdit();

      if (this.isBitmap) {
        this.commitText();
      }

      this.boundingBoxTool.deactivateTool();
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }, {
    key: "TEXT_EDIT_MODE",
    get: function get() {
      return 'TEXT_EDIT_MODE';
    }
  }, {
    key: "SELECT_MODE",
    get: function get() {
      return 'SELECT_MODE';
    }
    /** Clicks registered within this amount of time are registered as double clicks */

  }, {
    key: "DOUBLE_CLICK_MILLIS",
    get: function get() {
      return 250;
    }
    /** Typing with no pauses longer than this amount of type will count as 1 action */

  }, {
    key: "TYPING_TIMEOUT_MILLIS",
    get: function get() {
      return 1000;
    }
  }, {
    key: "TEXT_PADDING",
    get: function get() {
      return 8;
    }
  }]);
  return TextTool;
}(_paper.default.Tool);

var _default = TextTool;
exports.default = _default;