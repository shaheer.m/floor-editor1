"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../../log/log"));

/**
 * Tool for drawing rounded rectangles
 */
var RoundedRectTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(RoundedRectTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(RoundedRectTool);

  /**
   * @param {function} setHoveredItem Callback to set the hovered item
   * @param {function} clearHoveredItem Callback to clear the hovered item
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function RoundedRectTool(setHoveredItem, clearHoveredItem, setSelectedItems, clearSelectedItems, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, RoundedRectTool);
    _this = _super.call(this);
    _this.setHoveredItem = setHoveredItem;
    _this.clearHoveredItem = clearHoveredItem;
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.prevHoveredItemId = null; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    return _this;
  }
  /**
   * To be called when the hovered item changes. When the select tool hovers over a
   * new item, it compares against this to see if a hover item change event needs to
   * be fired.
   * @param {paper.Item} prevHoveredItemId ID of the highlight item that indicates the mouse is
   *     over a given item currently
   */


  (0, _createClass2.default)(RoundedRectTool, [{
    key: "setPrevHoveredItemId",
    value: function setPrevHoveredItemId(prevHoveredItemId) {
      this.prevHoveredItemId = prevHoveredItemId;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown() {
      _log.default.warn('Rounded Rectangle tool not yet implemented');
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove() {}
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag() {}
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp() {}
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {}
  }]);
  return RoundedRectTool;
}(_paper.default.Tool);

var _default = RoundedRectTool;
exports.default = _default;