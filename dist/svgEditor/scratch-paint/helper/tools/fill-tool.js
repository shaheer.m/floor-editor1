"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _hover = require("../hover");

var _math = require("../math");

var _stylePath = require("../style-path");

var _gradientTypes = _interopRequireDefault(require("../../lib/gradient-types"));

var FillTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(FillTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(FillTool);

  /**
   * @param {function} setHoveredItem Callback to set the hovered item
   * @param {function} clearHoveredItem Callback to clear the hovered item
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function FillTool(setHoveredItem, clearHoveredItem, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, FillTool);
    _this = _super.call(this);
    _this.setHoveredItem = setHoveredItem;
    _this.clearHoveredItem = clearHoveredItem;
    _this.onUpdateImage = onUpdateImage; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseUp = _this.handleMouseUp; // Color to fill with

    _this.fillColor = null;
    _this.fillColor2 = null;
    _this.gradientType = null; // The path that's being hovered over.

    _this.fillItem = null; // The style property that we're applying the color to (either fill or stroke).

    _this.fillProperty = null; // If we're hovering over a hole in a compound path, we can't just recolor it. This is the
    // added item that's the same shape as the hole that's drawn over the hole when we fill a hole.

    _this.addedFillItem = null;
    _this.fillItemOrigColor = null;
    _this.prevHoveredItemId = null;
    return _this;
  }

  (0, _createClass2.default)(FillTool, [{
    key: "getHitOptions",
    value: function getHitOptions() {
      var isAlmostClosedPath = function isAlmostClosedPath(item) {
        return item instanceof _paper.default.Path && item.segments.length > 2 && item.lastSegment.point.getDistance(item.firstSegment.point) < 8;
      };

      return {
        segments: false,
        stroke: true,
        curves: false,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          // Allow fills to be hit only if the item has a fill already or the path is closed/nearly closed
          var hitFill = hitResult.item.hasFill() || hitResult.item.closed || isAlmostClosedPath(hitResult.item);

          if (hitResult.item instanceof _paper.default.Path && ( // Disallow hits that don't qualify for the fill criteria, but only if they're fills
          hitFill || hitResult.type !== 'fill')) {
            return true;
          }

          if (hitResult.item instanceof _paper.default.PointText) {
            return true;
          }
        },
        hitUnfilledPaths: true,
        // If the color is transparent/none, then we need to be able to hit "invisible" outlines so that we don't
        // prevent ourselves from hitting an outline when we make it transparent via the fill preview, causing it to
        // flicker back and forth between transparent/its previous color as we hit it, then stop hitting it, etc.
        // If the color *is* visible, then don't hit "invisible" outlines, since this would add visible outlines to
        // non-outlined shapes when you hovered over where their outlines would be.
        hitUnstrokedPaths: this.gradientType === _gradientTypes.default.SOLID && this.fillColor === null,
        tolerance: FillTool.TOLERANCE / _paper.default.view.zoom
      };
    }
  }, {
    key: "setFillColor",
    value: function setFillColor(fillColor) {
      this.fillColor = fillColor;
    }
  }, {
    key: "setFillColor2",
    value: function setFillColor2(fillColor2) {
      this.fillColor2 = fillColor2;
    }
  }, {
    key: "setGradientType",
    value: function setGradientType(gradientType) {
      this.gradientType = gradientType;
    }
    /**
     * To be called when the hovered item changes. When the select tool hovers over a
     * new item, it compares against this to see if a hover item change event needs to
     * be fired.
     * @param {paper.Item} prevHoveredItemId ID of the highlight item that indicates the mouse is
     *     over a given item currently
     */

  }, {
    key: "setPrevHoveredItemId",
    value: function setPrevHoveredItemId(prevHoveredItemId) {
      this.prevHoveredItemId = prevHoveredItemId;
    }
  }, {
    key: "updateFillPreview",
    value: function updateFillPreview(event) {
      var hoveredItem = (0, _hover.getHoveredItem)(event, this.getHitOptions(), true
      /* subselect */
      );

      if (!hoveredItem && this.prevHoveredItemId || // There is no longer a hovered item
      hoveredItem && !this.prevHoveredItemId || // There is now a hovered item
      hoveredItem && this.prevHoveredItemId && hoveredItem.id !== this.prevHoveredItemId) {
        // hovered item changed
        this.setHoveredItem(hoveredItem ? hoveredItem.id : null);
      }

      var hitItem = hoveredItem ? hoveredItem.data.origItem : null;
      var hitType = hoveredItem ? hoveredItem.data.hitResult.type : null; // The hit "target" changes if we switch items or switch between fill/outline on the same item

      var hitTargetChanged = hitItem !== this.fillItem || hitType !== this.fillProperty; // Still hitting the same thing

      if (!hitTargetChanged) {
        // Only radial gradient needs to be updated
        if (this.gradientType === _gradientTypes.default.RADIAL) {
          this._setFillItemColor(this.fillColor, this.fillColor2, this.gradientType, event.point);
        }

        return;
      }

      if (this.fillItem) {
        if (this.addedFillItem) {
          this.addedFillItem.remove();
          this.addedFillItem = null;
        } else {
          this._setFillItemColor(this.fillItemOrigColor);
        }

        this.fillItemOrigColor = null;
        this.fillItem = null;
        this.fillProperty = null;
      }

      if (hitItem) {
        this.fillItem = hitItem;
        this.fillProperty = hitType;
        var colorProp = hitType === 'fill' ? 'fillColor' : 'strokeColor';
        this.fillItemOrigColor = hitItem[colorProp];

        if (hitItem.parent instanceof _paper.default.CompoundPath && hitItem.area < 0 && hitType === 'fill') {
          // hole
          if (!this.fillColor) {
            // Hole filled with transparent is no-op
            this.fillItem = null;
            this.fillProperty = null;
            this.fillItemOrigColor = null;
            return;
          } // Make an item to fill the hole


          this.addedFillItem = hitItem.clone();
          this.addedFillItem.setClockwise(true);
          this.addedFillItem.data.noHover = true;
          this.addedFillItem.data.origItem = hitItem; // This usually fixes it so there isn't a teeny tiny gap in between the fill and the outline
          // when filling in a hole

          (0, _math.expandBy)(this.addedFillItem, .1);
          this.addedFillItem.insertAbove(hitItem.parent);
        } else if (this.fillItem.parent instanceof _paper.default.CompoundPath) {
          this.fillItemOrigColor = hitItem.parent[colorProp];
        }

        this._setFillItemColor(this.fillColor, this.fillColor2, this.gradientType, event.point);
      }
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      // on touch, the user might touch-and-hold to preview what the fill tool would do
      // if they don't move their finger at all after the "mouse down" event
      // then this might be our only chance to give them a good preview
      this.updateFillPreview(event);
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.updateFillPreview(event);
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0) return; // only first mouse button

      if (this.fillItem) {
        // If the hole we're filling in is the same color as the parent, and parent has no outline, remove the hole
        if (this.addedFillItem && this._noStroke(this.fillItem.parent) && this.addedFillItem.fillColor.type !== 'gradient' && this.fillItem.parent.fillColor.toCSS() === this.addedFillItem.fillColor.toCSS()) {
          this.addedFillItem.remove();
          this.addedFillItem = null;
          var parent = this.fillItem.parent;
          this.fillItem.remove();
          parent = parent.reduce();
          parent.fillColor = this.fillColor;
        } else if (this.addedFillItem) {
          // Fill in a hole.
          this.addedFillItem.data.noHover = false;
        } else if (!this.fillColor && this.fillItem.data && this.fillItem.data.origItem) {
          // Filling a hole filler with transparent returns it to being gone
          // instead of making a shape that's transparent
          var group = this.fillItem.parent;
          this.fillItem.remove();

          if (!(group instanceof _paper.default.Layer) && group.children.length === 1) {
            group.reduce();
          }
        }

        this.clearHoveredItem();
        this.fillItem = null;
        this.fillProperty = null;
        this.addedFillItem = null;
        this.fillItemOrigColor = null;
        this.onUpdateImage();
      }
    }
  }, {
    key: "_noStroke",
    value: function _noStroke(item) {
      return !item.strokeColor || item.strokeColor.alpha === 0 || item.strokeWidth === 0;
    } // Either pass in a fully defined paper.Color as color1,
    // or pass in 2 color strings, a gradient type, and a pointer location

  }, {
    key: "_setFillItemColor",
    value: function _setFillItemColor(color1, color2, gradientType, pointerLocation) {
      var item = this._getFillItem();

      if (!item) return;
      var colorProp = this.fillProperty === 'fill' ? 'fillColor' : 'strokeColor'; // Only create a gradient if specifically requested, else use color1 directly
      // This ensures we do not set a gradient by accident (see scratch-paint#830).

      if (gradientType && gradientType !== _gradientTypes.default.SOLID) {
        item[colorProp] = (0, _stylePath.createGradientObject)(color1, color2, gradientType, item.bounds, pointerLocation, item.strokeWidth);
      } else {
        item[colorProp] = color1;
      }
    }
  }, {
    key: "_getFillItem",
    value: function _getFillItem() {
      if (this.addedFillItem) {
        return this.addedFillItem;
      } else if (this.fillItem && this.fillItem.parent instanceof _paper.default.CompoundPath) {
        return this.fillItem.parent;
      }

      return this.fillItem;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      if (this.fillItem) {
        this._setFillItemColor(this.fillItemOrigColor);

        this.fillItemOrigColor = null;
        this.fillItem = null;
        this.fillProperty = null;
      }

      this.clearHoveredItem();
      this.setHoveredItem = null;
      this.clearHoveredItem = null;
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }]);
  return FillTool;
}(_paper.default.Tool);

var _default = FillTool;
exports.default = _default;