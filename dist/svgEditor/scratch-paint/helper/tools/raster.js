"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setRasterImage = setRasterImage;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _undo = require("../../helper/undo");

var _math = require("../../helper/math");

var _group = require("../../helper/group");

var _log = _interopRequireDefault(require("../../log/log"));

var _format = _interopRequireDefault(require("../../lib/format"));

var _view = require("../../helper/view");

// scratch-render-fonts is a playground-only dep. Fonts are expected to be imported
// as a peer dependency, otherwise there will be two copies of them.
// import {performSnapshot} from '../helper/undo';
function setRasterImage() {
  var svg = "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 279.334 279.334\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" enable-background=\"new 0 0 279.334 279.334\">\n <g>\n   <path d=\"m232.423,101.03h-20.497c-3.313,0-6,2.687-6,6s2.687,6 6,6h4.429v31.517l-13.396,7.135c-3.289-4.313-8.479-7.053-14.139-7.053h-32.76v-16.088h20.107c5.563,0 10.744-2.214 14.59-6.233s5.828-9.294 5.582-14.851l-3.899-88.157c-0.48-10.822-9.34-19.3-20.173-19.3h-63.08c-10.833,0-19.694,8.478-20.173,19.3l-3.899,88.156c-0.246,5.557 1.736,10.832 5.582,14.851s9.027,6.233 14.59,6.233h20.107v16.088h-34.799c-5.838,0-11.108,2.846-14.366,7.325l-1.478-.787-11.771-6.269v-22.323-9.193h4.429c3.313,0 6-2.687 6-6s-2.687-6-6-6h-20.498c-3.313,0-6,2.687-6,6s2.687,6 6,6h4.068v35.119c0,2.217 1.223,4.253 3.18,5.296l18.172,9.678-.839,4.529c-0.965,5.205 0.422,10.523 3.807,14.593s8.361,6.404 13.655,6.404h33.159v11.667c0,3.313 2.687,6 6,6h6.712c-0.061,0.344-0.099,0.695-0.099,1.056v14.823c-23.277,0.46-46.396,3.589-68.808,9.333-2.654,0.68-4.511,3.076-4.511,5.816v12.942c-5.265,2.317-8.954,7.576-8.954,13.688 0,8.246 6.708,14.954 14.954,14.954 8.245,0 14.953-6.708 14.953-14.954 0-6.111-3.689-11.37-8.953-13.687v-8.251c20.025-4.786 40.604-7.411 61.318-7.837v16.171c-5.234,2.33-8.897,7.571-8.897,13.66 0,8.246 6.708,14.954 14.953,14.954 8.246,0 14.954-6.708 14.954-14.954 0-6.133-3.715-11.41-9.01-13.714v-16.117c20.713,0.426 41.29,3.051 61.317,7.837v8.251c-5.264,2.317-8.953,7.576-8.953,13.687 0,8.246 6.708,14.954 14.954,14.954 8.245,0 14.953-6.708 14.953-14.954 0-6.112-3.689-11.371-8.954-13.688v-12.791c0.073-2.745-1.753-5.261-4.51-5.967-22.414-5.744-45.532-8.872-68.808-9.332v-14.823c0-0.361-0.038-0.713-0.099-1.056h6.711c3.313,0 6-2.687 6-6v-11.668h31.395c5.396,0 10.434-2.408 13.821-6.607 3.389-4.199 4.678-9.632 3.537-14.906l-.959-4.433 18.042-9.609c1.957-1.042 3.18-3.079 3.18-5.296v-35.119h4.068c3.313,0 6-2.687 6-6s-2.684-6-5.997-6zm-167.969,163.294c-1.42109e-14-1.628 1.325-2.954 2.954-2.954 1.628,0 2.953,1.325 2.953,2.954s-1.325,2.954-2.953,2.954c-1.629,0-2.954-1.325-2.954-2.954zm146.637,0c0-1.628 1.325-2.954 2.954-2.954 1.628,0 2.953,1.325 2.953,2.954s-1.325,2.954-2.953,2.954c-1.629,0-2.954-1.325-2.954-2.954zm-70.309,3.01c-1.628,0-2.953-1.325-2.953-2.954s1.325-2.954 2.953-2.954c1.629,0 2.954,1.325 2.954,2.954s-1.325,2.954-2.954,2.954zm6.557-72.667h-13.226v-5.667h13.226v5.667zm-58.385-17.667c-2.351,0-3.783-1.301-4.429-2.077-0.645-0.775-1.663-2.421-1.234-4.732l1.641-8.852c0.506-2.729 2.888-4.71 5.663-4.71h98.225c2.693,0 5.061,1.91 5.63,4.542l1.914,8.852c0.369,1.71-0.049,3.473-1.147,4.834s-2.734,2.143-4.484,2.143h-101.779zm10.413-62.988c-1.561-1.631-2.364-3.771-2.265-6.025l3.899-88.156c0.194-4.391 3.789-7.831 8.185-7.831h63.08c4.396,0 7.99,3.439 8.185,7.83l3.899,88.156c0.1,2.255-0.704,4.395-2.265,6.025s-3.662,2.529-5.919,2.529h-70.881c-2.256,0.001-4.357-0.897-5.918-2.528zm44.693,14.529v16.088h-6.666v-16.088h6.666z\"/>\n </g>\n</svg>";
  var paperCanvas = this; // Pre-process SVG to prevent parsing errors (discussion from #213)
  // 1. Remove svg: namespace on elements.
  // TODO: remove

  svg = svg.split(/<\s*svg:/).join('<');
  svg = svg.split(/<\/\s*svg:/).join('</'); // 2. Add root svg namespace if it does not exist.

  var svgAttrs = svg.match(/<svg [^>]*>/);

  if (svgAttrs && svgAttrs[0].indexOf('xmlns=') === -1) {
    svg = svg.replace('<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ');
  } // Get the origin which the viewBox is defined relative to. During import, Paper will translate
  // the viewBox to start at (0, 0), and we need to translate it back for some costumes to render
  // correctly.


  var parser = new DOMParser();
  var svgDom = parser.parseFromString(svg, 'text/xml');
  var viewBox = svgDom.documentElement.attributes.viewBox ? svgDom.documentElement.attributes.viewBox.value.match(/\S+/g) : null;

  if (viewBox) {
    for (var i = 0; i < viewBox.length; i++) {
      viewBox[i] = parseFloat(viewBox[i]);
    }
  }

  _paper.default.project.importSVG(svg, {
    expandShapes: true,
    onLoad: function onLoad(item) {
      var _this = this;

      if (!item) {
        _log.default.error('SVG import failed:');

        _log.default.info(svg);

        this.props.changeFormat(_format.default.VECTOR_SKIP_CONVERT);
        (0, _undo.performSnapshot)(paperCanvas.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);
        return;
      }

      item.remove(); // Without the callback, rasters' load function has not been called yet, and they are
      // positioned incorrectly

      paperCanvas.queuedImport = paperCanvas.recalibrateSize(function () {
        var mask;

        if (item.clipped) {
          var _iterator = (0, _createForOfIteratorHelper2.default)(item.children),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var child = _step.value;

              if (child.isClipMask()) {
                mask = child;
                break;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          mask.clipMask = false;
        } else {
          mask = new _paper.default.Shape.Rectangle(item.bounds);
        }

        mask.guide = true;
        mask.locked = true;
        mask.matrix = new _paper.default.Matrix(); // Identity
        // Set the artwork to get clipped at the max costume size

        mask.size.height = _view.MAX_WORKSPACE_BOUNDS.height;
        mask.size.width = _view.MAX_WORKSPACE_BOUNDS.width;
        mask.setPosition(_view.CENTER);

        _paper.default.project.activeLayer.addChild(mask);

        mask.clipMask = true; // Reduce single item nested in groups

        if (item instanceof _paper.default.Group && item.children.length === 1) {
          item = item.reduce();
        }

        (0, _math.ensureClockwise)(item);
        (0, _math.scaleWithStrokes)(item, 2, new _paper.default.Point()); // Import at 2x
        // Apply rotation center

        if (typeof rotationCenterX !== 'undefined' && typeof rotationCenterY !== 'undefined') {
          var rotationPoint = new _paper.default.Point(0, 0);

          if (viewBox && viewBox.length >= 2 && !isNaN(viewBox[0]) && !isNaN(viewBox[1])) {
            rotationPoint = rotationPoint.subtract(viewBox[0], viewBox[1]);
          }

          item.translate(_view.CENTER.subtract(rotationPoint.multiply(2)));
        } else {
          // Center
          item.translate(_view.CENTER.subtract(100, 100));
        }

        _paper.default.project.activeLayer.insertChild(0, item);

        if ((0, _group.isGroup)(item)) {
          // Fixes an issue where we may export empty groups
          var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.children),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var _child = _step2.value;

              if ((0, _group.isGroup)(_child) && _child.children.length === 0) {
                _child.remove();
              }
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          (0, _group.ungroupItems)([item]);
        }

        (0, _undo.performSnapshot)(_this.props.undoSnapshot, _format.default.VECTOR_SKIP_CONVERT);

        _this.maybeZoomToFit(); // paperCanvas.props.updateViewBounds(paper.view.matrix);
        // paperCanvas.initializeSvg(item, rotationCenterX, rotationCenterY, viewBox);

      });
    }
  });
}