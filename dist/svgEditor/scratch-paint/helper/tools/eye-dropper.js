"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ZOOM_SCALE = exports.LOUPE_RADIUS = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("../layer");

var LOUPE_RADIUS = 20;
exports.LOUPE_RADIUS = LOUPE_RADIUS;
var ZOOM_SCALE = 3;
exports.ZOOM_SCALE = ZOOM_SCALE;

var EyeDropperTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(EyeDropperTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(EyeDropperTool);

  function EyeDropperTool(canvas, width, height, pixelRatio, zoom, offsetX, offsetY, isBitmap) {
    var _this;

    (0, _classCallCheck2.default)(this, EyeDropperTool);
    _this = _super.call(this);
    var layer = isBitmap ? (0, _layer.getRaster)().layer : _paper.default.project.activeLayer;
    var contentRaster3x = layer.rasterize(72 * ZOOM_SCALE * _paper.default.view.zoom, false
    /* insert */
    , _paper.default.view.bounds);
    var backgroundRaster3x = (0, _layer.getBackgroundGuideLayer)().rasterize(72 * ZOOM_SCALE * _paper.default.view.zoom, false
    /* insert */
    , _paper.default.view.bounds); // Canvas from which loupe is cut, shows art and grid

    _this.bufferCanvas = (0, _layer.createCanvas)(canvas.width * ZOOM_SCALE, canvas.height * ZOOM_SCALE);

    var bufferCanvasContext = _this.bufferCanvas.getContext('2d'); // Canvas to sample colors from; just the art


    _this.colorCanvas = (0, _layer.createCanvas)(canvas.width * ZOOM_SCALE, canvas.height * ZOOM_SCALE);

    var colorCanvasContext = _this.colorCanvas.getContext('2d');

    backgroundRaster3x.onLoad = function () {
      bufferCanvasContext.drawImage(backgroundRaster3x.canvas, 0, 0);

      contentRaster3x.onLoad = function () {
        colorCanvasContext.drawImage(contentRaster3x.canvas, 0, 0);
        bufferCanvasContext.drawImage(_this.colorCanvas, 0, 0);
        _this.bufferLoaded = true;
      };

      if (contentRaster3x.loaded) contentRaster3x.onLoad();
    };

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onMouseMove = _this.handleMouseMove;
    _this.canvas = canvas;
    _this.pixelRatio = pixelRatio;
    _this.zoom = zoom;
    _this.offsetX = offsetX;
    _this.offsetY = offsetY;
    _this.width = width * _this.zoom * _this.pixelRatio;
    _this.height = height * _this.zoom * _this.pixelRatio;
    _this.rect = canvas.getBoundingClientRect();
    _this.colorString = '';
    _this.pickX = -1;
    _this.pickY = -1;
    _this.hideLoupe = true;
    return _this;
  }

  (0, _createClass2.default)(EyeDropperTool, [{
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      // Set the pickX/Y for the color picker loop to pick up
      this.pickX = (event.point.x - this.offsetX) * this.zoom * this.pixelRatio;
      this.pickY = (event.point.y - this.offsetY) * this.zoom * this.pixelRatio; // check if the x/y are outside of the canvas

      this.hideLoupe = this.pickX > this.width || this.pickX < 0 || this.pickY > this.height || this.pickY < 0;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      // Nothing special on mousedown, just send to move handler which will show the loupe,
      // and the mouse up handler submits the color. This allows touch to drag
      // with the loupe visible to find the correct color
      this.handleMouseMove(event);
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp() {
      if (!this.hideLoupe) {
        var colorInfo = this.getColorInfo(this.pickX, this.pickY, this.hideLoupe);
        if (!colorInfo) return;

        if (colorInfo.color[3] === 0) {
          // Alpha 0
          this.colorString = null;
          return;
        }

        var r = colorInfo.color[0];
        var g = colorInfo.color[1];
        var b = colorInfo.color[2]; // from https://github.com/LLK/scratch-gui/blob/77e54a80a31b6cd4684d4b2a70f1aeec671f229e/src/containers/stage#L218-L222
        // formats the color info from the canvas into hex for parsing by the color picker

        var componentToString = function componentToString(c) {
          var hex = c.toString(16);
          return hex.length === 1 ? "0".concat(hex) : hex;
        };

        this.colorString = "#".concat(componentToString(r)).concat(componentToString(g)).concat(componentToString(b));
      }
    }
  }, {
    key: "getColorInfo",
    value: function getColorInfo(x, y, hideLoupe) {
      var artX = x / this.pixelRatio;
      var artY = y / this.pixelRatio;
      if (!this.bufferLoaded) return null;
      var colorContext = this.colorCanvas.getContext('2d');
      var bufferContext = this.bufferCanvas.getContext('2d');
      var colors = colorContext.getImageData(artX * ZOOM_SCALE, artY * ZOOM_SCALE, 1, 1);
      return {
        x: x,
        y: y,
        color: colors.data,
        data: bufferContext.getImageData(ZOOM_SCALE * (artX - LOUPE_RADIUS), ZOOM_SCALE * (artY - LOUPE_RADIUS), LOUPE_RADIUS * 2 * ZOOM_SCALE, LOUPE_RADIUS * 2 * ZOOM_SCALE).data,
        hideLoupe: hideLoupe
      };
    }
  }]);
  return EyeDropperTool;
}(_paper.default.Tool);

exports.default = EyeDropperTool;