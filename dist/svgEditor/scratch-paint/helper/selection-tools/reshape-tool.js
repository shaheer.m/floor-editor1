"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _log = _interopRequireDefault(require("../../log/log"));

var _keymirror = _interopRequireDefault(require("keymirror"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _item = require("../item");

var _guides = require("../guides");

var _math = require("../math");

var _selection = require("../selection");

var _moveTool = _interopRequireDefault(require("./move-tool"));

var _pointTool = _interopRequireDefault(require("./point-tool"));

var _handleTool = _interopRequireDefault(require("./handle-tool"));

var _selectionBoxTool = _interopRequireDefault(require("./selection-box-tool"));

/** Modes of the reshape tool, which can do many things depending on how it's used. */
var ReshapeModes = (0, _keymirror.default)({
  FILL: null,
  POINT: null,
  HANDLE: null,
  SELECTION_BOX: null
});
/**
 * paper.Tool to handle reshape mode, which allows manipulation of control points and
 * handles of path items. Can be used to select items within groups and points within items.
 * Reshape is made up of 4 tools:
 * - Selection box tool, which is activated by clicking an empty area. Draws a box and selects
 *   points and curves inside it
 * - Move tool, which translates items
 * - Point tool, which translates, adds and removes points
 * - Handle tool, which translates handles, changing the shape of curves
 */

var ReshapeTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(ReshapeTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(ReshapeTool);

  /**
   * @param {function} setHoveredItem Callback to set the hovered item
   * @param {function} clearHoveredItem Callback to clear the hovered item
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {!function} switchToTextTool A callback to call to switch to the text tool
   */
  function ReshapeTool(setHoveredItem, clearHoveredItem, setSelectedItems, clearSelectedItems, onUpdateImage, switchToTextTool) {
    var _this;

    (0, _classCallCheck2.default)(this, ReshapeTool);
    _this = _super.call(this);
    _this.setHoveredItem = setHoveredItem;
    _this.clearHoveredItem = clearHoveredItem;
    _this.onUpdateImage = onUpdateImage;
    _this.prevHoveredItemId = null;
    _this.lastEvent = null;
    _this.active = false;
    _this.mode = ReshapeModes.SELECTION_BOX;
    _this._modeMap = {};
    _this._modeMap[ReshapeModes.FILL] = new _moveTool.default(_modes.default.RESHAPE, setSelectedItems, clearSelectedItems, onUpdateImage, switchToTextTool);
    _this._modeMap[ReshapeModes.POINT] = new _pointTool.default(setSelectedItems, clearSelectedItems, onUpdateImage);
    _this._modeMap[ReshapeModes.HANDLE] = new _handleTool.default(setSelectedItems, clearSelectedItems, onUpdateImage);
    _this._modeMap[ReshapeModes.SELECTION_BOX] = new _selectionBoxTool.default(_modes.default.RESHAPE, setSelectedItems, clearSelectedItems); // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = _this.handleKeyUp;
    _this.onKeyDown = _this.handleKeyDown; // A handle's size is given in diameter, and each handle has a 2.5-pixel stroke that isn't part of its size:
    // https://github.com/LLK/paper.js/blob/a187e4c81cc63f3d48c5097b9a9fbddde9f057da/src/item/Item.js#L4480
    // Size the handles such that clicking on either the stroke or the handle itself will be registered as a drag

    _paper.default.settings.handleSize = ReshapeTool.HANDLE_RADIUS * 2 - 2.5;
    return _this;
  }
  /**
   * Returns the hit options for segments to use when conducting hit tests. Segments are only visible
   * when the shape is selected. Segments take precedence, since they are always over curves and need
   * to be grabbable. (Segments are the little circles)
   * @return {object} See paper.Item.hitTest for definition of options
   */


  (0, _createClass2.default)(ReshapeTool, [{
    key: "getSelectedSegmentHitOptions",
    value: function getSelectedSegmentHitOptions() {
      var hitOptions = {
        segments: true,
        tolerance: ReshapeTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          if (hitResult.type !== 'segment') return false;
          if (hitResult.item.data && hitResult.item.data.noHover) return false;
          if (!hitResult.item.selected) return false;
          return true;
        }
      };
      return hitOptions;
    }
    /**
     * Returns the hit options for handles to use when conducting hit tests. Handles need to be done
     * separately because we want to ignore hidden handles, but we don't want hidden handles to negate
     * legitimate hits on other things (like if the handle is over part of the fill). (Handles are the diamonds)
     * @return {object} See paper.Item.hitTest for definition of options
     */

  }, {
    key: "getHandleHitOptions",
    value: function getHandleHitOptions() {
      var hitOptions = {
        handles: true,
        tolerance: ReshapeTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          if (hitResult.item.data && hitResult.item.data.noHover) return false; // Only hit test against handles that are visible, that is,
          // their segment is selected

          if (!hitResult.segment || !hitResult.segment.selected) return false; // If the entire shape is selected, handles are hidden

          if (hitResult.item.fullySelected) return false;
          return true;
        }
      };
      return hitOptions;
    }
    /**
     * Returns the hit options for curves of selected objects, which take precedence over
     * unselected things and fills.
     * @return {object} See paper.Item.hitTest for definition of options
     */

  }, {
    key: "getSelectedStrokeHitOptions",
    value: function getSelectedStrokeHitOptions() {
      var hitOptions = {
        segments: false,
        stroke: false,
        curves: true,
        handles: false,
        fill: false,
        guide: false,
        tolerance: ReshapeTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          if (hitResult.type !== 'curve') return false;
          if (!hitResult.item.selected) return false;
          if (hitResult.item.data && hitResult.item.data.noHover) return false;
          return true;
        }
      };
      return hitOptions;
    }
    /**
     * Returns the hit options for fills and unselected strokes/curves to use when conducting hit tests.
     * @param {boolean} preselectedOnly True if we should only return results that are already
     *     selected.
     * @return {object} See paper.Item.hitTest for definition of options
     */

  }, {
    key: "getUnselectedAndFillHitOptions",
    value: function getUnselectedAndFillHitOptions() {
      var hitOptions = {
        fill: true,
        stroke: true,
        curves: true,
        tolerance: ReshapeTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          if (hitResult.item.data && hitResult.item.data.noHover) return false;
          return true;
        }
      };
      return hitOptions;
    }
    /**
     * To be called when the hovered item changes. When the select tool hovers over a
     * new item, it compares against this to see if a hover item change event needs to
     * be fired.
     * @param {paper.Item} prevHoveredItemId ID of the highlight item that indicates the mouse is
     *     over a given item currently
     */

  }, {
    key: "setPrevHoveredItemId",
    value: function setPrevHoveredItemId(prevHoveredItemId) {
      this.prevHoveredItemId = prevHoveredItemId;
    }
    /**
     * Given the point at which the mouse is, return the prioritized hit result, or null if nothing was hit.
     * @param {paper.Point} point Point to hit test on canvas
     * @return {?paper.HitResult} hitResult
     */

  }, {
    key: "getHitResult",
    value: function getHitResult(point) {
      // Prefer hits on segments to other types of hits, since segments always overlap curves.
      var hitResults = _paper.default.project.hitTestAll(point, this.getSelectedSegmentHitOptions());

      if (!hitResults.length) {
        hitResults = _paper.default.project.hitTestAll(point, this.getHandleHitOptions());
      }

      if (!hitResults.length) {
        hitResults = _paper.default.project.hitTestAll(point, this.getSelectedStrokeHitOptions());
      }

      if (!hitResults.length) {
        hitResults = _paper.default.project.hitTestAll(point, this.getUnselectedAndFillHitOptions());
      }

      if (!hitResults.length) {
        return null;
      } // Get highest z-index result


      var hitResult;

      var _iterator = (0, _createForOfIteratorHelper2.default)(hitResults),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var result = _step.value;

          if (!hitResult || (0, _math.sortItemsByZIndex)(hitResult.item, result.item) < 0) {
            hitResult = result;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return hitResult;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;
      this.clearHoveredItem(); // Check if double clicked

      var doubleClicked = false;

      if (this.lastEvent) {
        if (event.event.timeStamp - this.lastEvent.event.timeStamp < ReshapeTool.DOUBLE_CLICK_MILLIS) {
          doubleClicked = true;
        } else {
          doubleClicked = false;
        }
      }

      this.lastEvent = event;
      var hitResult = this.getHitResult(event.point);

      if (!hitResult) {
        this._modeMap[ReshapeModes.SELECTION_BOX].onMouseDown(event.modifiers.shift);

        return;
      }

      var hitProperties = {
        hitResult: hitResult,
        clone: event.modifiers.alt,
        multiselect: event.modifiers.shift,
        doubleClicked: doubleClicked,
        subselect: true
      }; // If item is not yet selected, don't behave differently depending on if they clicked a segment
      // (since those were invisible), just select the whole thing as if they clicked the fill.

      if (!hitResult.item.selected || hitResult.type === 'fill' || hitResult.type === 'stroke' || hitResult.type !== 'segment' && doubleClicked) {
        this.mode = ReshapeModes.FILL;

        this._modeMap[this.mode].onMouseDown(hitProperties);
      } else if (hitResult.type === 'segment') {
        this.mode = ReshapeModes.POINT;

        this._modeMap[this.mode].onMouseDown(hitProperties);
      } else if (hitResult.type === 'curve') {
        this.mode = ReshapeModes.POINT;

        this._modeMap[this.mode].addPoint(hitProperties);

        this.onUpdateImage();

        this._modeMap[this.mode].onMouseDown(hitProperties);
      } else if (hitResult.type === 'handle-in' || hitResult.type === 'handle-out') {
        this.mode = ReshapeModes.HANDLE;

        this._modeMap[this.mode].onMouseDown(hitProperties);
      } else {
        _log.default.warn("Unhandled hit result type: ".concat(hitResult.type));

        this.mode = ReshapeModes.FILL;

        this._modeMap[this.mode].onMouseDown(hitProperties);
      }
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      var hitResult = this.getHitResult(event.point);
      var hoveredItem;

      if (hitResult) {
        var item = hitResult.item;

        if (item.selected) {
          hoveredItem = null;
        } else if ((0, _item.isBoundsItem)(item)) {
          hoveredItem = (0, _guides.hoverBounds)(item);
        } else {
          hoveredItem = (0, _guides.hoverItem)(item);
        }
      }

      if (!hoveredItem && this.prevHoveredItemId || // There is no longer a hovered item
      hoveredItem && !this.prevHoveredItemId || // There is now a hovered item
      hoveredItem && this.prevHoveredItemId && hoveredItem.id !== this.prevHoveredItemId) {
        // hovered item changed
        this.setHoveredItem(hoveredItem ? hoveredItem.id : null);
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      this._modeMap[this.mode].onMouseDrag(event);
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.mode === ReshapeModes.SELECTION_BOX) {
        this._modeMap[this.mode].onMouseUpVector(event);
      } else {
        this._modeMap[this.mode].onMouseUp(event);
      }

      this.mode = ReshapeModes.SELECTION_BOX;
      this.active = false;
    }
  }, {
    key: "handleKeyDown",
    value: function handleKeyDown(event) {
      if (event.event.target instanceof HTMLInputElement) {
        // Ignore nudge if a text input field is focused
        return;
      }

      var nudgeAmount = 1 / _paper.default.view.zoom;
      var selected = (0, _selection.getSelectedLeafItems)();
      if (selected.length === 0) return;
      var translation;

      if (event.key === 'up') {
        translation = new _paper.default.Point(0, -nudgeAmount);
      } else if (event.key === 'down') {
        translation = new _paper.default.Point(0, nudgeAmount);
      } else if (event.key === 'left') {
        translation = new _paper.default.Point(-nudgeAmount, 0);
      } else if (event.key === 'right') {
        translation = new _paper.default.Point(nudgeAmount, 0);
      }

      if (translation) {
        var segments = (0, _selection.getSelectedSegments)(); // If no segments are selected, translate selected paths

        if (segments.length === 0) {
          var _iterator2 = (0, _createForOfIteratorHelper2.default)(selected),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var item = _step2.value;
              item.translate(translation);
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
        } else {
          // Translate segments
          var _iterator3 = (0, _createForOfIteratorHelper2.default)(segments),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var seg = _step3.value;
              seg.point = seg.point.add(translation);
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
        }
      }
    }
  }, {
    key: "handleKeyUp",
    value: function handleKeyUp(event) {
      var selected = (0, _selection.getSelectedLeafItems)();
      if (selected.length === 0) return;

      if (event.key === 'up' || event.key === 'down' || event.key === 'left' || event.key === 'right') {
        this.onUpdateImage();
      }
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      _paper.default.settings.handleSize = 0;
      this.clearHoveredItem();
      this.setHoveredItem = null;
      this.clearHoveredItem = null;
      this.onUpdateImage = null;
      this.lastEvent = null;
    }
  }], [{
    key: "TOLERANCE",
    get:
    /** Distance within which mouse is considered to be hitting an item */
    function get() {
      return ReshapeTool.HANDLE_RADIUS + ReshapeTool.HANDLE_PADDING;
    }
    /**
     * Units of padding around the visible handle area that will still register clicks as "touching the handle"
     */

  }, {
    key: "HANDLE_PADDING",
    get: function get() {
      return 1;
    }
    /**
     * Handles' radius, including the stroke
     */

  }, {
    key: "HANDLE_RADIUS",
    get: function get() {
      return 5.25;
    }
    /** Clicks registered within this amount of time are registered as double clicks */

  }, {
    key: "DOUBLE_CLICK_MILLIS",
    get: function get() {
      return 250;
    }
  }]);
  return ReshapeTool;
}(_paper.default.Tool);

var _default = ReshapeTool;
exports.default = _default;