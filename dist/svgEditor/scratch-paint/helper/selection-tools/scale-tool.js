"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _selection = require("../selection");

var _view = require("../view");

var _modes = require("../../lib/modes");

var MIN_SCALE_FACTOR = 0.0001;
/**
 * Tool to handle scaling items by pulling on the handles around the edges of the bounding
 * box when in the bounding box tool.
 */

var ScaleTool = /*#__PURE__*/function () {
  /**
   * @param {Mode} mode Paint editor mode
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function ScaleTool(mode, onUpdateImage) {
    (0, _classCallCheck2.default)(this, ScaleTool);
    this.isBitmap = mode in _modes.BitmapModes;
    this.active = false;
    this.boundsPath = null;
    this.pivot = null;
    this.origPivot = null;
    this.corner = null;
    this.origSize = null;
    this.origCenter = null;
    this.itemGroup = null; // Lowest item above all scale items in z index

    this.itemToInsertBelow = null;
    this.lastPoint = null;
    this.onUpdateImage = onUpdateImage;
  }
  /**
   * @param {!paper.HitResult} hitResult Data about the location of the mouse click
   * @param {!object} boundsPath Where the boundaries of the hit item are
   * @param {!Array.<paper.Item>} selectedItems Set of selected paper.Items
   */


  (0, _createClass2.default)(ScaleTool, [{
    key: "onMouseDown",
    value: function onMouseDown(hitResult, boundsPath, selectedItems) {
      if (this.active) return;
      this.active = true;
      var index = hitResult.item.data.index;
      this.pivot = boundsPath.bounds[this._getOpposingRectCornerNameByIndex(index)].clone();
      this.origPivot = boundsPath.bounds[this._getOpposingRectCornerNameByIndex(index)].clone();
      this.corner = boundsPath.bounds[this._getRectCornerNameByIndex(index)].clone();
      this.selectionAnchor = boundsPath.selectionAnchor;
      this.origSize = this.corner.subtract(this.pivot);
      this.origCenter = boundsPath.bounds.center;
      this.isCorner = this._isCorner(index);
      this.centered = false;
      this.lastSx = 1;
      this.lastSy = 1;
      this.boundsPath = boundsPath; // Set itemGroup
      // get item to insert below so that scaled items stay in same z position

      var items = (0, _selection.getItems)({
        match: function match(item) {
          var _iterator = (0, _createForOfIteratorHelper2.default)(selectedItems),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var scaleItem = _step.value;

              if (!scaleItem.isBelow(item)) {
                return false;
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          return true;
        }
      });

      if (items.length > 0) {
        this.itemToInsertBelow = items[0];
      }

      this.itemGroup = new _paper.default.Group(selectedItems);
      this.itemGroup.addChild(boundsPath);
      this.itemGroup.insertBelow(this.itemToInsertBelow);
      this.itemGroup.data.isHelperItem = true;
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      if (!this.active) return;
      var point = event.point;
      var bounds = (0, _view.getActionBounds)(this.isBitmap);
      point.x = Math.max(bounds.left, Math.min(point.x, bounds.right));
      point.y = Math.max(bounds.top, Math.min(point.y, bounds.bottom));
      if (!this.lastPoint) this.lastPoint = event.lastPoint;
      var delta = point.subtract(this.lastPoint);
      this.lastPoint = point;

      if (event.modifiers.alt) {
        this.centered = true;
        this.itemGroup.position = this.origCenter;
        this.pivot = this.origCenter;
      } else {
        if (this.centered) {
          // Reset position if we were just in alt
          this.centered = false;
          this.itemGroup.scale(1 / this.lastSx, 1 / this.lastSy, this.pivot);

          if (this.selectionAnchor) {
            this.selectionAnchor.scale(this.lastSx, this.lastSy);
          }

          this.lastSx = 1;
          this.lastSy = 1;
        }

        this.pivot = this.origPivot;
      }

      this.corner = this.corner.add(delta);
      var size = this.corner.subtract(this.pivot);

      if (event.modifiers.alt) {
        size = size.multiply(2);
      }

      var sx = 1.0;
      var sy = 1.0;

      if (Math.abs(this.origSize.x) > 0.0000001) {
        sx = size.x / this.origSize.x;
      }

      if (Math.abs(this.origSize.y) > 0.0000001) {
        sy = size.y / this.origSize.y;
      }

      var signx = sx > 0 ? 1 : -1;
      var signy = sy > 0 ? 1 : -1;

      if (this.isCorner && !event.modifiers.shift) {
        sx = sy = Math.max(Math.abs(sx), Math.abs(sy));
        sx *= signx;
        sy *= signy;
      }

      sx = signx * Math.max(Math.abs(sx), MIN_SCALE_FACTOR);
      sy = signy * Math.max(Math.abs(sy), MIN_SCALE_FACTOR);
      this.itemGroup.scale(sx / this.lastSx, sy / this.lastSy, this.pivot);

      if (this.selectionAnchor) {
        this.selectionAnchor.scale(this.lastSx / sx, this.lastSy / sy);
      }

      this.lastSx = sx;
      this.lastSy = sy;
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp() {
      if (!this.active) return;
      this.lastPoint = null;
      this.pivot = null;
      this.origPivot = null;
      this.corner = null;
      this.origSize = null;
      this.origCenter = null;
      this.lastSx = 1;
      this.lastSy = 1;
      this.centered = false;

      if (!this.itemGroup) {
        return;
      }

      this.boundsPath.remove();
      this.boundsPath = null; // mark text items as scaled (for later use on font size calc)

      for (var i = 0; i < this.itemGroup.children.length; i++) {
        var child = this.itemGroup.children[i];

        if (child.data.isPGTextItem) {
          child.data.wasScaled = true;
        }
      }

      if (this.itemToInsertBelow) {
        // No increment step because itemGroup.children is getting depleted
        for (var _i = 0; _i < this.itemGroup.children.length;) {
          this.itemGroup.children[_i].insertBelow(this.itemToInsertBelow);
        }

        this.itemToInsertBelow = null;
      } else if (this.itemGroup.layer) {
        this.itemGroup.layer.addChildren(this.itemGroup.children);
      }

      this.itemGroup.remove();
      this.onUpdateImage();
      this.active = false;
    }
  }, {
    key: "_getRectCornerNameByIndex",
    value: function _getRectCornerNameByIndex(index) {
      switch (index) {
        case 0:
          return 'bottomLeft';

        case 1:
          return 'leftCenter';

        case 2:
          return 'topLeft';

        case 3:
          return 'topCenter';

        case 4:
          return 'topRight';

        case 5:
          return 'rightCenter';

        case 6:
          return 'bottomRight';

        case 7:
          return 'bottomCenter';
      }
    }
  }, {
    key: "_getOpposingRectCornerNameByIndex",
    value: function _getOpposingRectCornerNameByIndex(index) {
      switch (index) {
        case 0:
          return 'topRight';

        case 1:
          return 'rightCenter';

        case 2:
          return 'bottomRight';

        case 3:
          return 'bottomCenter';

        case 4:
          return 'bottomLeft';

        case 5:
          return 'leftCenter';

        case 6:
          return 'topLeft';

        case 7:
          return 'topCenter';
      }
    }
  }, {
    key: "_isCorner",
    value: function _isCorner(index) {
      switch (index) {
        case 0:
        case 2:
        case 4:
        case 6:
          return true;

        default:
          return false;
      }
    }
  }]);
  return ScaleTool;
}();

var _default = ScaleTool;
exports.default = _default;