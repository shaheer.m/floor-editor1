"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _keymirror = _interopRequireDefault(require("keymirror"));

var _selection = require("../selection");

var _guides = require("../guides");

var _layer = require("../layer");

var _cursors = _interopRequireDefault(require("../../lib/cursors"));

var _scaleTool = _interopRequireDefault(require("./scale-tool"));

var _rotateTool = _interopRequireDefault(require("./rotate-tool"));

var _moveTool = _interopRequireDefault(require("./move-tool"));

var SELECTION_ANCHOR_SIZE = 12;
/** SVG for the rotation icon on the bounding box */

var ARROW_PATH = 'M19.28,1.09C19.28.28,19,0,18.2,0c-1.67,0-3.34,0-5,0-.34,0-.88.24-1,.47a1.4,1.4,' + '0,0,0,.36,1.08,15.27,15.27,0,0,0,1.46,1.36A6.4,6.4,0,0,1,6.52,4,5.85,5.85,0,0,1,5.24,3,15.27,15.27,' + '0,0,0,6.7,1.61,1.4,1.4,0,0,0,7.06.54C7,.3,6.44.07,6.1.06c-1.67,0-3.34,0-5,0C.28,0,0,.31,0,1.12c0,1.67,' + '0,3.34,0,5a1.23,1.23,0,0,0,.49,1,1.22,1.22,0,0,0,1-.31A14.38,14.38,0,0,0,2.84,5.26l.73.62a9.45,9.45,' + '0,0,0,7.34,2,9.45,9.45,0,0,0,4.82-2.05l.73-.62a14.38,14.38,0,0,0,1.29,1.51,1.22,1.22,' + '0,0,0,1,.31,1.23,1.23,0,0,0,.49-1C19.31,4.43,19.29,2.76,19.28,1.09Z';
/** Modes of the bounding box tool, which can do many things depending on how it's used. */

var BoundingBoxModes = (0, _keymirror.default)({
  SCALE: null,
  ROTATE: null,
  MOVE: null
});
/**
 * Tool that handles transforming the selection and drawing a bounding box with handles.
 * On mouse down, the type of function (move, scale, rotate) is determined based on what is clicked
 * (scale handle, rotate handle, the object itself). This determines the mode of the tool, which then
 * delegates actions to the MoveTool, RotateTool or ScaleTool accordingly.
 * @param {!function} onUpdateImage A callback to call when the image visibly changes
 */

var BoundingBoxTool = /*#__PURE__*/function () {
  /**
   * @param {Modes} mode Paint editor mode
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {?function} switchToTextTool A callback to call to switch to the text tool
   */
  function BoundingBoxTool(mode, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage, switchToTextTool) {
    (0, _classCallCheck2.default)(this, BoundingBoxTool);
    this.dispatchSetCursor = setCursor;
    this.onUpdateImage = onUpdateImage;
    this.mode = null;
    this.boundsPath = null;
    this.boundsScaleHandles = [];
    this.boundsRotHandles = [];
    this._modeMap = {};
    this._modeMap[BoundingBoxModes.SCALE] = new _scaleTool.default(mode, onUpdateImage);
    this._modeMap[BoundingBoxModes.ROTATE] = new _rotateTool.default(onUpdateImage);
    this._modeMap[BoundingBoxModes.MOVE] = new _moveTool.default(mode, setSelectedItems, clearSelectedItems, onUpdateImage, switchToTextTool);
    this._currentCursor = null;
  }
  /**
   * Should be called if the selection changes to update the bounds of the bounding box.
   * @param {?Array<paper.Item>} selectedItems Array of selected items.
   */


  (0, _createClass2.default)(BoundingBoxTool, [{
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      if (selectedItems && selectedItems.length) {
        this.setSelectionBounds();
      } else {
        this.removeBoundsPath();
      }
    }
    /**
     * @param {!MouseEvent} event The mouse event
     * @param {boolean} clone Whether to clone on mouse down (e.g. alt key held)
     * @param {boolean} multiselect Whether to multiselect on mouse down (e.g. shift key held)
     * @param {?boolean} doubleClicked True if this is the second click in a short amout of time
     * @param {paper.hitOptions} hitOptions The options with which to detect whether mouse down has hit
     *     anything editable
     * @return {boolean} True if there was a hit, false otherwise
     */

  }, {
    key: "onMouseDown",
    value: function onMouseDown(event, clone, multiselect, doubleClicked, hitOptions) {
      if (event.event.button > 0) return; // only first mouse button

      var _this$_determineMode = this._determineMode(event, multiselect, hitOptions),
          hitResult = _this$_determineMode.hitResult,
          mode = _this$_determineMode.mode;

      if (!hitResult) {
        if (!multiselect) {
          this.removeBoundsPath();
        }

        return false;
      }

      this.mode = mode;
      var hitProperties = {
        hitResult: hitResult,
        clone: clone,
        multiselect: multiselect,
        doubleClicked: doubleClicked
      };

      if (this.mode === BoundingBoxModes.MOVE) {
        this._modeMap[this.mode].onMouseDown(hitProperties);

        this.removeBoundsHandles();
      } else if (this.mode === BoundingBoxModes.SCALE) {
        this._modeMap[this.mode].onMouseDown(hitResult, this.boundsPath, (0, _selection.getSelectedRootItems)());

        this.removeBoundsHandles();
      } else if (this.mode === BoundingBoxModes.ROTATE) {
        this.setCursor(_cursors.default.GRABBING);

        this._modeMap[this.mode].onMouseDown(hitResult, this.boundsPath, (0, _selection.getSelectedRootItems)()); // While transforming, don't show bounds


        this.removeBoundsPath();
      }

      return true;
    }
  }, {
    key: "onMouseMove",
    value: function onMouseMove(event, hitOptions) {
      this._updateCursor(event, hitOptions);
    }
  }, {
    key: "_updateCursor",
    value: function _updateCursor(event, hitOptions) {
      var _this$_determineMode2 = this._determineMode(event, false, hitOptions),
          mode = _this$_determineMode2.mode,
          hitResult = _this$_determineMode2.hitResult;

      if (hitResult) {
        if (mode === BoundingBoxModes.MOVE) {
          this.setCursor(_cursors.default.DEFAULT);
        } else if (mode === BoundingBoxModes.ROTATE) {
          this.setCursor(_cursors.default.GRAB);
        } else if (mode === BoundingBoxModes.SCALE) {
          this.setSelectionBounds();

          if (this._impreciseEqual(hitResult.item.position.x, this.boundsPath.position.x)) {
            this.setCursor(_cursors.default.RESIZE_NS);
          } else if (this._impreciseEqual(hitResult.item.position.y, this.boundsPath.position.y)) {
            this.setCursor(_cursors.default.RESIZE_EW);
          } else if (hitResult.item.position.equals(this.boundsPath.bounds.bottomLeft) || hitResult.item.position.equals(this.boundsPath.bounds.topRight)) {
            this.setCursor(_cursors.default.RESIZE_NESW);
          } else {
            this.setCursor(_cursors.default.RESIZE_NWSE);
          }
        }
      } else {
        this.setCursor(_cursors.default.DEFAULT);
      }
    }
  }, {
    key: "_impreciseEqual",
    value: function _impreciseEqual(a, b) {
      // This is the same math paper.js uses to check if two numbers are "equal".
      return Math.abs(a - b) < 1e-8;
    }
  }, {
    key: "_determineMode",
    value: function _determineMode(event, multiselect, hitOptions) {
      var hitResults = _paper.default.project.hitTestAll(event.point, hitOptions);

      var mode; // Prefer scale to trigger over rotate, and scale and rotate to trigger over other hits

      var hitResult = hitResults[0];

      for (var i = 0; i < hitResults.length; i++) {
        if (hitResults[i].item.data && hitResults[i].item.data.isScaleHandle) {
          hitResult = hitResults[i];
          mode = BoundingBoxModes.SCALE;
          break;
        } else if (hitResults[i].item.data && hitResults[i].item.data.isRotHandle) {
          hitResult = hitResults[i];
          mode = BoundingBoxModes.ROTATE;
        }
      }

      if (!mode) {
        mode = BoundingBoxModes.MOVE;
      }

      return {
        mode: mode,
        hitResult: hitResult
      };
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      if (event.event.button > 0 || !this.mode) return; // only first mouse button

      this._modeMap[this.mode].onMouseDrag(event); // Set the cursor for moving a sprite once the drag has actually started (i.e. the mouse has been moved while
      // pressed), so that the mouse doesn't "flash" to the grabbing cursor every time a sprite is clicked.


      if (this.mode === BoundingBoxModes.MOVE) {
        this.setCursor(_cursors.default.GRABBING);
      }
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp(event, hitOptions) {
      if (event.event.button > 0 || !this.mode) return; // only first mouse button

      this._modeMap[this.mode].onMouseUp(event); // After transforming, show bounds again


      this.setSelectionBounds();
      this.mode = null;

      this._updateCursor(event, hitOptions);
    }
  }, {
    key: "setSelectionBounds",
    value: function setSelectionBounds() {
      this.removeBoundsPath();
      var items = (0, _selection.getSelectedRootItems)();
      if (items.length <= 0) return;
      var rect = null;

      var _iterator = (0, _createForOfIteratorHelper2.default)(items),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;

          if (item instanceof _paper.default.Raster && item.loaded === false) {
            item.onLoad = this.setSelectionBounds.bind(this);
            return;
          }

          if (rect) {
            rect = rect.unite(item.bounds);
          } else {
            rect = item.bounds;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      if (!this.boundsPath) {
        this.boundsPath = new _paper.default.Group();
        this.boundsRect = _paper.default.Path.Rectangle(rect);
        this.boundsRect.curves[0].divideAtTime(0.5);
        this.boundsRect.curves[2].divideAtTime(0.5);
        this.boundsRect.curves[4].divideAtTime(0.5);
        this.boundsRect.curves[6].divideAtTime(0.5);
        this.boundsPath.addChild(this.boundsRect);
        var vRect = new _paper.default.Path.Rectangle({
          point: [-1, -6],
          size: [2, 12],
          radius: 1,
          insert: false
        });
        var hRect = new _paper.default.Path.Rectangle({
          point: [-6, -1],
          size: [12, 2],
          radius: 1,
          insert: false
        });
        var anchorIcon = vRect.unite(hRect);
        this.boundsPath.addChild(anchorIcon);
        this.boundsPath.selectionAnchor = anchorIcon;

        this._modeMap[BoundingBoxModes.MOVE].setBoundsPath(this.boundsPath);
      }

      (0, _layer.setGuideItem)(this.boundsPath);
      this.boundsPath.data.isSelectionBound = true;
      this.boundsPath.data.isHelperItem = true;
      this.boundsPath.fillColor = null;
      this.boundsPath.parent = (0, _layer.getGuideLayer)();
      this.boundsPath.strokeWidth = 1 / _paper.default.view.zoom;
      this.boundsPath.strokeColor = (0, _guides.getGuideColor)();
      this.boundsPath.selectionAnchor.scale(SELECTION_ANCHOR_SIZE / _paper.default.view.zoom / this.boundsPath.selectionAnchor.bounds.width);
      this.boundsPath.selectionAnchor.position = rect.center; // Make a template to copy

      var boundsScaleCircleShadow = new _paper.default.Path.Circle({
        center: new _paper.default.Point(0, 0),
        radius: 5.5 / _paper.default.view.zoom,
        fillColor: 'black',
        opacity: .12,
        data: {
          isHelperItem: true,
          noSelect: true,
          noHover: true
        }
      });
      var boundsScaleCircle = new _paper.default.Path.Circle({
        center: new _paper.default.Point(0, 0),
        radius: 4 / _paper.default.view.zoom,
        fillColor: (0, _guides.getGuideColor)(),
        data: {
          isScaleHandle: true,
          isHelperItem: true,
          noSelect: true,
          noHover: true
        }
      });
      var boundsScaleHandle = new _paper.default.Group([boundsScaleCircleShadow, boundsScaleCircle]);
      boundsScaleHandle.parent = (0, _layer.getGuideLayer)();

      for (var index = 0; index < this.boundsRect.segments.length; index++) {
        var segment = this.boundsRect.segments[index];

        if (index === 7) {
          var offset = new _paper.default.Point(0, 20);
          var arrows = new _paper.default.Path(ARROW_PATH);
          arrows.translate(segment.point.add(offset).add(-10.5, -5));
          var line = new _paper.default.Path.Rectangle(segment.point.add(offset).subtract(1, 0), segment.point);
          var rotHandle = arrows.unite(line);
          line.remove();
          arrows.remove();
          rotHandle.scale(1 / _paper.default.view.zoom, segment.point);
          rotHandle.data = {
            offset: offset,
            isRotHandle: true,
            isHelperItem: true,
            noSelect: true,
            noHover: true
          };
          rotHandle.fillColor = (0, _guides.getGuideColor)();
          rotHandle.parent = (0, _layer.getGuideLayer)();
          this.boundsRotHandles[index] = rotHandle;
        }

        this.boundsScaleHandles[index] = boundsScaleHandle.clone();
        this.boundsScaleHandles[index].position = segment.point;

        var _iterator2 = (0, _createForOfIteratorHelper2.default)(this.boundsScaleHandles[index].children),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var child = _step2.value;
            child.data.index = index;
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }

        this.boundsScaleHandles[index].data = {
          index: index,
          isScaleHandle: true,
          isHelperItem: true,
          noSelect: true,
          noHover: true
        };
      } // Remove the template


      boundsScaleHandle.remove();
    }
  }, {
    key: "removeBoundsPath",
    value: function removeBoundsPath() {
      (0, _guides.removeBoundsPath)();
      this.boundsPath = null;
      this.boundsRect = null;
      this.boundsScaleHandles.length = 0;
      this.boundsRotHandles.length = 0;
    }
  }, {
    key: "removeBoundsHandles",
    value: function removeBoundsHandles() {
      (0, _guides.removeBoundsHandles)();
      this.boundsScaleHandles.length = 0;
      this.boundsRotHandles.length = 0;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.removeBoundsPath();
      this.setCursor(_cursors.default.DEFAULT);
    }
  }, {
    key: "setCursor",
    value: function setCursor(cursorString) {
      if (this._currentCursor !== cursorString) {
        this.dispatchSetCursor(cursorString);
        this._currentCursor = cursorString;
      }
    }
  }]);
  return BoundingBoxTool;
}();

var _default = BoundingBoxTool;
exports.default = _default;