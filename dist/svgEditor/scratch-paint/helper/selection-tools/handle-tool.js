"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _selection = require("../selection");

/** Sub tool of the Reshape tool for moving handles, which adjust bezier curves. */
var HandleTool = /*#__PURE__*/function () {
  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function HandleTool(setSelectedItems, clearSelectedItems, onUpdateImage) {
    (0, _classCallCheck2.default)(this, HandleTool);
    this.hitType = null;
    this.setSelectedItems = setSelectedItems;
    this.clearSelectedItems = clearSelectedItems;
    this.onUpdateImage = onUpdateImage;
    this.selectedItems = [];
  }
  /**
   * @param {!object} hitProperties Describes the mouse event
   * @param {?boolean} hitProperties.multiselect Whether to multiselect on mouse down (e.g. shift key held)
   *     select the whole group.
   */


  (0, _createClass2.default)(HandleTool, [{
    key: "onMouseDown",
    value: function onMouseDown(hitProperties) {
      if (!hitProperties.multiselect) {
        (0, _selection.clearSelection)(this.clearSelectedItems);
      }

      hitProperties.hitResult.segment.handleIn.selected = true;
      hitProperties.hitResult.segment.handleOut.selected = true;
      this.hitType = hitProperties.hitResult.type;
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      this.selectedItems = (0, _selection.getSelectedLeafItems)();

      var _iterator = (0, _createForOfIteratorHelper2.default)(this.selectedItems),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;

          var _iterator2 = (0, _createForOfIteratorHelper2.default)(item.segments),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var seg = _step2.value;

              // add the point of the segment before the drag started
              // for later use in the snap calculation
              if (!seg.origPoint) {
                seg.origPoint = seg.point.clone();
              }

              if (seg.handleOut.selected && this.hitType === 'handle-out') {
                // if option is pressed or handles have been split,
                // they're no longer parallel and move independently
                if (event.modifiers.option || !seg.handleOut.isColinear(seg.handleIn)) {
                  seg.handleOut = seg.handleOut.add(event.delta);
                } else {
                  seg.handleOut = seg.handleOut.add(event.delta);
                  seg.handleIn = seg.handleOut.multiply(-seg.handleIn.length / seg.handleOut.length);
                }
              } else if (seg.handleIn.selected && this.hitType === 'handle-in') {
                // if option is pressed or handles have been split,
                // they're no longer parallel and move independently
                if (event.modifiers.option || !seg.handleOut.isColinear(seg.handleIn)) {
                  seg.handleIn = seg.handleIn.add(event.delta);
                } else {
                  seg.handleIn = seg.handleIn.add(event.delta);
                  seg.handleOut = seg.handleIn.multiply(-seg.handleOut.length / seg.handleIn.length);
                }
              }
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp() {
      // resetting the items and segments origin points for the next usage
      var moved = false;

      var _iterator3 = (0, _createForOfIteratorHelper2.default)(this.selectedItems),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var item = _step3.value;

          if (!item.segments) {
            return;
          }

          var _iterator4 = (0, _createForOfIteratorHelper2.default)(item.segments),
              _step4;

          try {
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              var seg = _step4.value;

              if (seg.origPoint && !seg.equals(seg.origPoint)) {
                moved = true;
              }

              seg.origPoint = null;
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      if (moved) {
        this.setSelectedItems();
        this.onUpdateImage();
      }

      this.selectedItems = [];
    }
  }]);
  return HandleTool;
}();

var _default = HandleTool;
exports.default = _default;