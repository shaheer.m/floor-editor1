"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

/**
 * Tool to handle rotation when dragging the rotation handle in the bounding box tool.
 */
var RotateTool = /*#__PURE__*/function () {
  /**
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function RotateTool(onUpdateImage) {
    (0, _classCallCheck2.default)(this, RotateTool);
    this.rotItems = [];
    this.rotGroupPivot = null;
    this.prevRot = 90;
    this.onUpdateImage = onUpdateImage;
  }
  /**
   * @param {!paper.HitResult} hitResult Data about the location of the mouse click
   * @param {!object} boundsPath Where the boundaries of the hit item are
   * @param {!Array.<paper.Item>} selectedItems Set of selected paper.Items
   */


  (0, _createClass2.default)(RotateTool, [{
    key: "onMouseDown",
    value: function onMouseDown(hitResult, boundsPath, selectedItems) {
      this.rotGroupPivot = boundsPath.bounds.center;

      var _iterator = (0, _createForOfIteratorHelper2.default)(selectedItems),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var item = _step.value;

          // Rotate only root items
          if (item.parent instanceof _paper.default.Layer) {
            this.rotItems.push(item);
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this.prevRot = 90;
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      var rotAngle = event.point.subtract(this.rotGroupPivot).angle;

      if (event.modifiers.shift) {
        rotAngle = Math.round(rotAngle / 45) * 45;
      }

      for (var i = 0; i < this.rotItems.length; i++) {
        var item = this.rotItems[i];
        item.rotate(rotAngle - this.prevRot, this.rotGroupPivot);
      }

      this.prevRot = rotAngle;
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.rotItems.length = 0;
      this.rotGroupPivot = null;
      this.prevRot = 90;
      this.onUpdateImage();
    }
  }]);
  return RotateTool;
}();

var _default = RotateTool;
exports.default = _default;