"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _selection = require("../selection");

var _view = require("../view");

var _modes = require("../../lib/modes");

var NUDGE_MORE_MULTIPLIER = 15;
/**
 * Tool containing handlers for arrow key events for nudging the selection.
 * Note that this tool is built for selection mode, not reshape mode.
 */

var NudgeTool = /*#__PURE__*/function () {
  /**
   * @param {Mode} mode Paint editor mode
   * @param {function} boundingBoxTool to control the bounding box
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function NudgeTool(mode, boundingBoxTool, onUpdateImage) {
    (0, _classCallCheck2.default)(this, NudgeTool);
    this.boundingBoxTool = boundingBoxTool;
    this.onUpdateImage = onUpdateImage;
    this.boundingBoxTool.isBitmap = mode in _modes.BitmapModes;
  }

  (0, _createClass2.default)(NudgeTool, [{
    key: "onKeyDown",
    value: function onKeyDown(event) {
      if (event.event.target instanceof HTMLInputElement) {
        // Ignore nudge if a text input field is focused
        return;
      }

      var nudgeAmount = 1 / _paper.default.view.zoom;
      if (event.modifiers.shift) nudgeAmount *= NUDGE_MORE_MULTIPLIER;
      var selected = (0, _selection.getSelectedRootItems)();
      if (selected.length === 0) return; // Get bounds. Don't let item bounds go out of bounds.

      var rect;

      var _iterator = (0, _createForOfIteratorHelper2.default)(selected),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var _item = _step.value;

          if (rect) {
            rect = rect.unite(_item.bounds);
          } else {
            rect = _item.bounds;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      var bounds = (0, _view.getActionBounds)(this.boundingBoxTool.isBitmap);
      var bottom = bounds.bottom - rect.top - 1;
      var top = bounds.top - rect.bottom + 1;
      var left = bounds.left - rect.right + 1;
      var right = bounds.right - rect.left - 1;
      var translation;

      if (event.key === 'up') {
        translation = new _paper.default.Point(0, Math.min(bottom, Math.max(-nudgeAmount, top)));
      } else if (event.key === 'down') {
        translation = new _paper.default.Point(0, Math.max(top, Math.min(nudgeAmount, bottom)));
      } else if (event.key === 'left') {
        translation = new _paper.default.Point(Math.min(right, Math.max(-nudgeAmount, left)), 0);
      } else if (event.key === 'right') {
        translation = new _paper.default.Point(Math.max(left, Math.min(nudgeAmount, right)), 0);
      }

      if (translation) {
        var _iterator2 = (0, _createForOfIteratorHelper2.default)(selected),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var item = _step2.value;
            item.translate(translation);
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }

        this.boundingBoxTool.setSelectionBounds();
        event.preventDefault();
      }
    }
  }, {
    key: "onKeyUp",
    value: function onKeyUp(event) {
      var selected = (0, _selection.getSelectedRootItems)();
      if (selected.length === 0) return;

      if (event.key === 'up' || event.key === 'down' || event.key === 'left' || event.key === 'right') {
        this.onUpdateImage();
      }
    }
  }]);
  return NudgeTool;
}();

var _default = NudgeTool;
exports.default = _default;