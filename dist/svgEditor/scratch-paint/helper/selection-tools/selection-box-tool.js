"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _guides = require("../guides");

var _selection = require("../selection");

var _layer = require("../layer");

var _view = require("../view");

var _bitmap = require("../bitmap");

/** Tool to handle drag selection. A dotted line box appears and everything enclosed is selected. */
var SelectionBoxTool = /*#__PURE__*/function () {
  /**
   * @param {!Modes} mode Current paint editor mode
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   */
  function SelectionBoxTool(mode, setSelectedItems, clearSelectedItems) {
    (0, _classCallCheck2.default)(this, SelectionBoxTool);
    this.selectionRect = null;
    this.mode = mode;
    this.setSelectedItems = setSelectedItems;
    this.clearSelectedItems = clearSelectedItems;
  }
  /**
   * @param {boolean} multiselect Whether to multiselect on mouse down (e.g. shift key held)
   */


  (0, _createClass2.default)(SelectionBoxTool, [{
    key: "onMouseDown",
    value: function onMouseDown(multiselect) {
      if (!multiselect) {
        (0, _selection.clearSelection)(this.clearSelectedItems);
        this.clearSelectedItems();
      }
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      if (event.event.button > 0) return; // only first mouse button

      if (this.selectionRect) {
        this.selectionRect.remove();
      }

      this.selectionRect = (0, _guides.rectSelect)(event);
    }
  }, {
    key: "onMouseUpVector",
    value: function onMouseUpVector(event) {
      if (event.event.button > 0) return; // only first mouse button

      if (this.selectionRect) {
        (0, _selection.processRectangularSelection)(event, this.selectionRect, this.mode);
        this.selectionRect.remove();
        this.selectionRect = null;
        this.setSelectedItems();
      }
    }
  }, {
    key: "onMouseUpBitmap",
    value: function onMouseUpBitmap(event) {
      if (event.event.button > 0) return; // only first mouse button

      if (this.selectionRect) {
        var rect = new _paper.default.Rectangle({
          from: new _paper.default.Point(Math.max(0, Math.round(this.selectionRect.bounds.topLeft.x)), Math.max(0, Math.round(this.selectionRect.bounds.topLeft.y))),
          to: new _paper.default.Point(Math.min(_view.ART_BOARD_WIDTH, Math.round(this.selectionRect.bounds.bottomRight.x)), Math.min(_view.ART_BOARD_HEIGHT, Math.round(this.selectionRect.bounds.bottomRight.y)))
        }); // Trim/tighten selection bounds inwards to only the opaque region, excluding transparent pixels

        rect = (0, _bitmap.getHitBounds)((0, _layer.getRaster)(), rect);

        if (rect.area) {
          // Pull selected raster to active layer
          var raster = (0, _layer.getRaster)().getSubRaster(rect);
          raster.parent = _paper.default.project.activeLayer;
          raster.canvas.getContext('2d').imageSmoothingEnabled = false;
          raster.selected = true; // Gather a bit of extra data so that we can avoid aliasing at edges

          var expanded = (0, _layer.getRaster)().getSubRaster(rect.expand(4));
          expanded.remove();
          raster.data = {
            expanded: expanded
          }; // Clear area from raster layer

          var context = (0, _layer.getRaster)().getContext(true
          /* modify */
          );
          context.clearRect(rect.x, rect.y, rect.width, rect.height);
          this.setSelectedItems();
        } // Remove dotted rectangle


        this.selectionRect.remove();
        this.selectionRect = null;
      }
    }
  }]);
  return SelectionBoxTool;
}();

var _default = SelectionBoxTool;
exports.default = _default;