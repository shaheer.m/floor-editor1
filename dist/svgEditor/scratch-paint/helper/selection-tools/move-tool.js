"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard").default;

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireWildcard(require("../../lib/modes"));

var _group = require("../group");

var _item = require("../item");

var _math = require("../math");

var _view = require("../view");

var _selection = require("../selection");

var _layer = require("../layer");

/** Snap to align selection center to rotation center within this distance */
var SNAPPING_THRESHOLD = 4;
var FADE_DISTANCE = 10;
/**
 * Tool to handle dragging an item to reposition it in a selection mode.
 */

var MoveTool = /*#__PURE__*/function () {
  /**
   * @param {Modes} mode Paint editor mode
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {?function} switchToTextTool A callback to call to switch to the text tool
   */
  function MoveTool(mode, setSelectedItems, clearSelectedItems, onUpdateImage, switchToTextTool) {
    (0, _classCallCheck2.default)(this, MoveTool);
    this.mode = mode;
    this.setSelectedItems = setSelectedItems;
    this.clearSelectedItems = clearSelectedItems;
    this.selectedItems = null;
    this.selectionCenter = null;
    this.onUpdateImage = onUpdateImage;
    this.switchToTextTool = switchToTextTool;
    this.boundsPath = null;
    this.firstDrag = false;
  }
  /**
   * @param {!object} hitProperties Describes the mouse event
   * @param {!paper.HitResult} hitProperties.hitResult Data about the location of the mouse click
   * @param {?boolean} hitProperties.clone Whether to clone on mouse down (e.g. alt key held)
   * @param {?boolean} hitProperties.multiselect Whether to multiselect on mouse down (e.g. shift key held)
   * @param {?boolean} hitProperties.doubleClicked True if this is the second click in a short amout of time
   * @param {?boolean} hitProperties.subselect True if we allow selection of subgroups, false if we should
   *     select the whole group.
   */


  (0, _createClass2.default)(MoveTool, [{
    key: "onMouseDown",
    value: function onMouseDown(hitProperties) {
      var item = hitProperties.hitResult.item;

      if (!hitProperties.subselect) {
        var root = (0, _item.getRootItem)(hitProperties.hitResult.item);
        item = (0, _item.isCompoundPathItem)(root) || (0, _group.isGroup)(root) ? root : hitProperties.hitResult.item;
      }

      if (item.selected) {
        // Double click causes all points to be selected in subselect mode. If the target is text, it
        // enters text edit.
        if (hitProperties.doubleClicked) {
          if (!hitProperties.multiselect) {
            if (this.switchToTextTool && item instanceof _paper.default.PointText) {
              this.switchToTextTool();
              return;
            }

            (0, _selection.clearSelection)(this.clearSelectedItems);
          }

          this._select(item, true
          /* state */
          , hitProperties.subselect, true
          /* fullySelect */
          );
        } else if (hitProperties.multiselect) {
          this._select(item, false
          /* state */
          , hitProperties.subselect);
        }
      } else {
        // deselect all by default if multiselect isn't on
        if (!hitProperties.multiselect) {
          (0, _selection.clearSelection)(this.clearSelectedItems);
        }

        this._select(item, true, hitProperties.subselect);
      }

      if (hitProperties.clone) (0, _selection.cloneSelection)(hitProperties.subselect, this.onUpdateImage);
      this.selectedItems = this.mode === _modes.default.RESHAPE ? (0, _selection.getSelectedLeafItems)() : (0, _selection.getSelectedRootItems)();

      if (this.selectedItems.length === 0) {
        return;
      }

      var selectionBounds;

      var _iterator = (0, _createForOfIteratorHelper2.default)(this.selectedItems),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var selectedItem = _step.value;

          if (selectionBounds) {
            selectionBounds = selectionBounds.unite(selectedItem.bounds);
          } else {
            selectionBounds = selectedItem.bounds;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      this.selectionCenter = selectionBounds.center;

      if (this.boundsPath) {
        this.selectedItems.push(this.boundsPath);
      }

      this.firstDrag = true;
    }
  }, {
    key: "setBoundsPath",
    value: function setBoundsPath(boundsPath) {
      this.boundsPath = boundsPath;
    }
    /**
     * Sets the selection state of an item.
     * @param {!paper.Item} item Item to select or deselect
     * @param {?boolean} state True if item should be selected, false if deselected
     * @param {?boolean} subselect True if a subset of all points in an item are allowed to be
     *     selected, false if items must be selected all or nothing.
     * @param {?boolean} fullySelect True if in addition to the item being selected, all of its
     *     control points should be selected. False if the item should be selected but not its
     *     points. Only relevant when subselect is true.
     */

  }, {
    key: "_select",
    value: function _select(item, state, subselect, fullySelect) {
      if (subselect) {
        item.selected = false;

        if (fullySelect) {
          item.fullySelected = state;
        } else {
          item.selected = state;
        }
      } else {
        (0, _selection.setItemSelection)(item, state);
      }

      this.setSelectedItems();
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      var point = event.point;
      var actionBounds = (0, _view.getActionBounds)(this.mode in _modes.BitmapModes);
      point.x = Math.max(actionBounds.left, Math.min(point.x, actionBounds.right));
      point.y = Math.max(actionBounds.top, Math.min(point.y, actionBounds.bottom));
      var dragVector = point.subtract(event.downPoint);
      var snapVector; // Snapping to align center. Not in reshape mode, because reshape doesn't show center crosshair

      if (!event.modifiers.shift && this.mode !== _modes.default.RESHAPE) {
        if ((0, _math.checkPointsClose)(this.selectionCenter.add(dragVector), _view.CENTER, SNAPPING_THRESHOLD / _paper.default.view.zoom
        /* threshold */
        )) {
          snapVector = _view.CENTER.subtract(this.selectionCenter);
        }
      }

      if (this.selectedItems.length === 0) {
        return;
      }

      var bounds;

      var _iterator2 = (0, _createForOfIteratorHelper2.default)(this.selectedItems),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var item = _step2.value;

          // add the position of the item before the drag started
          // for later use in the snap calculation
          if (!item.data.origPos) {
            item.data.origPos = item.position;
          }

          if (snapVector) {
            item.position = item.data.origPos.add(snapVector);
          } else if (event.modifiers.shift) {
            item.position = item.data.origPos.add((0, _math.snapDeltaToAngle)(dragVector, Math.PI / 4));
          } else {
            item.position = item.data.origPos.add(dragVector);
          }

          if (bounds) {
            bounds = bounds.unite(item.bounds);
          } else {
            bounds = item.bounds;
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      if (this.firstDrag) {
        // Show the center crosshair above the selected item while dragging.
        (0, _layer.getDragCrosshairLayer)().visible = true;
        this.firstDrag = false;
      } // The rotation center crosshair should be opaque over the entire selection bounding box, and fade out to
      // totally transparent outside the selection bounding box.


      var opacityMultiplier = 1;
      var newCenter = this.selectionCenter.add(dragVector);

      if (_view.CENTER.y < bounds.top && _view.CENTER.x < bounds.left || _view.CENTER.y > bounds.bottom && _view.CENTER.x < bounds.left || _view.CENTER.y < bounds.top && _view.CENTER.x > bounds.right || _view.CENTER.y > bounds.bottom && _view.CENTER.x > bounds.right) {
        // rotation center is to one of the 4 corners of the selection bounding box
        var distX = Math.max(_view.CENTER.x - bounds.right, bounds.left - _view.CENTER.x);
        var distY = Math.max(_view.CENTER.y - bounds.bottom, bounds.top - _view.CENTER.y);
        var dist = Math.sqrt(distX * distX + distY * distY);
        opacityMultiplier = Math.max(0, 1 - dist / (FADE_DISTANCE / _paper.default.view.zoom));
      } else if (_view.CENTER.y < bounds.top || _view.CENTER.y > bounds.bottom) {
        // rotation center is above or below the selection bounding box
        opacityMultiplier = Math.max(0, 1 - (Math.abs(_view.CENTER.y - newCenter.y) - bounds.height / 2) / (FADE_DISTANCE / _paper.default.view.zoom));
      } else if (_view.CENTER.x < bounds.left || _view.CENTER.x > bounds.right) {
        // rotation center is left or right of the selection bounding box
        opacityMultiplier = Math.max(0, 1 - (Math.abs(_view.CENTER.x - newCenter.x) - bounds.width / 2) / (FADE_DISTANCE / _paper.default.view.zoom));
      } // else the rotation center is within selection bounds, always show drag crosshair at full opacity


      (0, _layer.getDragCrosshairLayer)().opacity = _layer.CROSSHAIR_FULL_OPACITY * opacityMultiplier;
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp() {
      this.firstDrag = false;
      var moved = false; // resetting the items origin point for the next usage

      var _iterator3 = (0, _createForOfIteratorHelper2.default)(this.selectedItems),
          _step3;

      try {
        for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
          var item = _step3.value;

          if (item.data.origPos) {
            if (!item.position.equals(item.data.origPos)) moved = true;
            delete item.data.origPos;
          }
        }
      } catch (err) {
        _iterator3.e(err);
      } finally {
        _iterator3.f();
      }

      this.selectedItems = null;
      this.selectionCenter = null;

      if (moved) {
        this.onUpdateImage();
      } // Hide the crosshair we showed earlier.


      (0, _layer.getDragCrosshairLayer)().visible = false;
    }
  }]);
  return MoveTool;
}();

var _default = MoveTool;
exports.default = _default;