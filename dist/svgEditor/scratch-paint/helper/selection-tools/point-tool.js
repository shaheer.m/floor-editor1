"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _math = require("../math");

var _view = require("../view");

var _selection = require("../selection");

/** Subtool of ReshapeTool for moving control points. */
var PointTool = /*#__PURE__*/function () {
  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function PointTool(setSelectedItems, clearSelectedItems, onUpdateImage) {
    (0, _classCallCheck2.default)(this, PointTool);

    /**
     * Deselection often does not happen until mouse up. If the mouse is dragged before
     * mouse up, deselection is cancelled. This variable keeps track of which paper.Item to deselect.
     */
    this.deselectOnMouseUp = null;
    /**
     * Delete control point does not happen until mouse up. If the mouse is dragged before
     * mouse up, delete is cancelled. This variable keeps track of the hitResult that triggers delete.
     */

    this.deleteOnMouseUp = null;
    /**
     * There are 2 cases for deselection: Deselect this, or deselect everything but this.
     * When invert deselect is true, deselect everything but the item in deselectOnMouseUp.
     */

    this.invertDeselect = false;
    this.selectedItems = null;
    this.setSelectedItems = setSelectedItems;
    this.clearSelectedItems = clearSelectedItems;
    this.lastPoint = null;
    this.onUpdateImage = onUpdateImage;
  }
  /**
   * @param {!object} hitProperties Describes the mouse event
   * @param {!paper.HitResult} hitProperties.hitResult Data about the location of the mouse click
   * @param {?boolean} hitProperties.multiselect Whether to multiselect on mouse down (e.g. shift key held)
   * @param {?boolean} hitProperties.doubleClicked Whether this is the second click in a short time
   */


  (0, _createClass2.default)(PointTool, [{
    key: "onMouseDown",
    value: function onMouseDown(hitProperties) {
      // Remove point
      if (hitProperties.doubleClicked) {
        this.deleteOnMouseUp = hitProperties.hitResult;
      }

      if (hitProperties.hitResult.segment.selected) {
        // selected points with no handles get handles if selected again
        if (hitProperties.multiselect) {
          this.deselectOnMouseUp = hitProperties.hitResult.segment;
        } else {
          this.deselectOnMouseUp = hitProperties.hitResult.segment;
          this.invertDeselect = true;
          hitProperties.hitResult.segment.selected = true;
        }
      } else {
        if (!hitProperties.multiselect) {
          (0, _selection.clearSelection)(this.clearSelectedItems);
        }

        hitProperties.hitResult.segment.selected = true;
      }

      this.selectedItems = (0, _selection.getSelectedLeafItems)();
    }
    /**
     * @param {!object} hitProperties Describes the mouse event
     * @param {!paper.HitResult} hitProperties.hitResult Data about the location of the mouse click
     * @param {?boolean} hitProperties.multiselect Whether to multiselect on mouse down (e.g. shift key held)
     */

  }, {
    key: "addPoint",
    value: function addPoint(hitProperties) {
      var newSegment = hitProperties.hitResult.item.divideAt(hitProperties.hitResult.location); // If we're adding a point in the middle of a straight line, it won't be smooth by default, so smooth it

      if (!newSegment.hasHandles()) newSegment.smooth();
      hitProperties.hitResult.segment = newSegment;

      if (!hitProperties.multiselect) {
        (0, _selection.clearSelection)(this.clearSelectedItems);
      }

      newSegment.selected = true;
    }
  }, {
    key: "removePoint",
    value: function removePoint(hitResult) {
      var index = hitResult.segment.index;
      hitResult.item.removeSegment(index); // Adjust handles of curve before and curve after to account for new curve length

      var beforeSegment = hitResult.item.segments[index - 1];
      var afterSegment = hitResult.item.segments[index];
      var curveLength = beforeSegment ? beforeSegment.curve ? beforeSegment.curve.length : null : null;

      if (beforeSegment && beforeSegment.handleOut) {
        if (afterSegment) {
          beforeSegment.handleOut = beforeSegment.handleOut.multiply(curveLength * _math.HANDLE_RATIO / beforeSegment.handleOut.length);
        } else {
          beforeSegment.handleOut = null;
        }
      }

      if (afterSegment && afterSegment.handleIn) {
        if (beforeSegment) {
          afterSegment.handleIn = afterSegment.handleIn.multiply(curveLength * _math.HANDLE_RATIO / afterSegment.handleIn.length);
        } else {
          afterSegment.handleIn = null;
        }
      }
    }
  }, {
    key: "onMouseDrag",
    value: function onMouseDrag(event) {
      // A click will deselect, but a drag will not
      this.deselectOnMouseUp = null;
      this.invertDeselect = false;
      this.deleteOnMouseUp = null;
      var point = event.point;
      var bounds = (0, _view.getActionBounds)();
      point.x = Math.max(bounds.left, Math.min(point.x, bounds.right));
      point.y = Math.max(bounds.top, Math.min(point.y, bounds.bottom));
      if (!this.lastPoint) this.lastPoint = event.lastPoint;
      var dragVector = point.subtract(event.downPoint);
      var delta = point.subtract(this.lastPoint);
      this.lastPoint = point;
      var selectedSegments = (0, _selection.getSelectedSegments)();

      var _iterator = (0, _createForOfIteratorHelper2.default)(selectedSegments),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var seg = _step.value;

          // add the point of the segment before the drag started
          // for later use in the snap calculation
          if (!seg.origPoint) {
            seg.origPoint = seg.point.clone();
          }

          if (event.modifiers.shift) {
            seg.point = seg.origPoint.add((0, _math.snapDeltaToAngle)(dragVector, Math.PI / 4));
          } else {
            seg.point = seg.point.add(delta);
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
  }, {
    key: "onMouseUp",
    value: function onMouseUp() {
      this.lastPoint = null; // resetting the items and segments origin points for the next usage

      var moved = false;
      var selectedSegments = (0, _selection.getSelectedSegments)();

      var _iterator2 = (0, _createForOfIteratorHelper2.default)(selectedSegments),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var seg = _step2.value;

          if (seg.origPoint && !seg.equals(seg.origPoint)) {
            moved = true;
          }

          seg.origPoint = null;
        } // If no drag occurred between mouse down and mouse up, then we can go through with deselect
        // and delete

      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      if (this.deselectOnMouseUp) {
        if (this.invertDeselect) {
          (0, _selection.clearSelection)(this.clearSelectedItems);
          this.deselectOnMouseUp.selected = true;
        } else {
          this.deselectOnMouseUp.selected = false;
        }

        this.deselectOnMouseUp = null;
        this.invertDeselect = false;
      }

      if (this.deleteOnMouseUp) {
        this.removePoint(this.deleteOnMouseUp);
      }

      this.selectedItems = null;
      this.setSelectedItems();

      if (moved || this.deleteOnMouseUp) {
        this.deleteOnMouseUp = null;
        this.onUpdateImage();
      }
    }
  }]);
  return PointTool;
}();

var _default = PointTool;
exports.default = _default;