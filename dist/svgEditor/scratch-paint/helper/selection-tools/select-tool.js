"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _hover = require("../hover");

var _selection = require("../selection");

var _boundingBoxTool = _interopRequireDefault(require("./bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("./nudge-tool"));

var _selectionBoxTool = _interopRequireDefault(require("./selection-box-tool"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

/**
 * paper.Tool that handles select mode. This is made up of 2 subtools.
 * - The selection box tool is active when the user clicks an empty space and drags.
 *   It selects all items in the rectangle.
 * - The bounding box tool is active if the user clicks on a non-empty space. It handles
 *   reshaping the item that was clicked.
 */
var SelectTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(SelectTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(SelectTool);

  /**
   * @param {function} setHoveredItem Callback to set the hovered item
   * @param {function} clearHoveredItem Callback to clear the hovered item
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {!function} switchToTextTool A callback to call to switch to the text tool
   */
  function SelectTool(setHoveredItem, clearHoveredItem, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage, switchToTextTool) {
    var _this;

    (0, _classCallCheck2.default)(this, SelectTool);
    _this = _super.call(this);
    _this.setHoveredItem = setHoveredItem;
    _this.clearHoveredItem = clearHoveredItem;
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.SELECT, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage, switchToTextTool);
    var nudgeTool = new _nudgeTool.default(_modes.default.SELECT, _this.boundingBoxTool, onUpdateImage);
    _this.selectionBoxTool = new _selectionBoxTool.default(_modes.default.SELECT, setSelectedItems, clearSelectedItems);
    _this.selectionBoxMode = false;
    _this.prevHoveredItemId = null;
    _this.active = false; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;
    (0, _selection.selectRootItem)();
    setSelectedItems();

    _this.boundingBoxTool.setSelectionBounds();

    return _this;
  }
  /**
   * To be called when the hovered item changes. When the select tool hovers over a
   * new item, it compares against this to see if a hover item change event needs to
   * be fired.
   * @param {paper.Item} prevHoveredItemId ID of the highlight item that indicates the mouse is
   *     over a given item currently
   */


  (0, _createClass2.default)(SelectTool, [{
    key: "setPrevHoveredItemId",
    value: function setPrevHoveredItemId(prevHoveredItemId) {
      this.prevHoveredItemId = prevHoveredItemId;
    }
    /**
     * Should be called if the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);
    }
    /**
     * Returns the hit options to use when conducting hit tests.
     * @param {boolean} preselectedOnly True if we should only return results that are already
     *     selected.
     * @return {object} See paper.Item.hitTest for definition of options
     */

  }, {
    key: "getHitOptions",
    value: function getHitOptions(preselectedOnly) {
      // Tolerance needs to be scaled when the view is zoomed in in order to represent the same
      // distance for the user to move the mouse.
      var hitOptions = {
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        tolerance: SelectTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          // Don't match helper items, unless they are handles.
          if (!hitResult.item.data || !hitResult.item.data.isHelperItem) return true;
          return hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle;
        }
      };

      if (preselectedOnly) {
        hitOptions.selected = true;
      }

      return hitOptions;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;
      this.clearHoveredItem(); // Check if double clicked

      var doubleClicked = false;

      if (this.lastEvent) {
        if (event.event.timeStamp - this.lastEvent.event.timeStamp < SelectTool.DOUBLE_CLICK_MILLIS) {
          doubleClicked = true;
        } else {
          doubleClicked = false;
        }
      }

      this.lastEvent = event; // If bounding box tool does not find an item that was hit, use selection box tool.

      if (!this.boundingBoxTool.onMouseDown(event, event.modifiers.alt, event.modifiers.shift, doubleClicked, this.getHitOptions(false
      /* preseelectedOnly */
      ))) {
        this.selectionBoxMode = true;
        this.selectionBoxTool.onMouseDown(event.modifiers.shift);
      }
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      var hoveredItem = (0, _hover.getHoveredItem)(event, this.getHitOptions());

      if (!hoveredItem && this.prevHoveredItemId || // There is no longer a hovered item
      hoveredItem && !this.prevHoveredItemId || // There is now a hovered item
      hoveredItem && this.prevHoveredItemId && hoveredItem.id !== this.prevHoveredItemId) {
        // hovered item changed
        this.setHoveredItem(hoveredItem ? hoveredItem.id : null);
      }

      if (!this.selectionBoxMode) {
        this.boundingBoxTool.onMouseMove(event, this.getHitOptions(false));
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.selectionBoxMode) {
        this.selectionBoxTool.onMouseDrag(event);
      } else {
        this.boundingBoxTool.onMouseDrag(event);
      }
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.selectionBoxMode) {
        this.selectionBoxTool.onMouseUpVector(event);
      } else {
        this.boundingBoxTool.onMouseUp(event, this.getHitOptions(false));
      }

      this.selectionBoxMode = false;
      this.active = false;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.clearHoveredItem();
      this.boundingBoxTool.deactivateTool();
      this.setHoveredItem = null;
      this.clearHoveredItem = null;
      this.onUpdateImage = null;
      this.boundingBoxTool = null;
      this.selectionBoxTool = null;
    }
  }], [{
    key: "TOLERANCE",
    get:
    /** The distance within which mouse events count as a hit against an item */
    function get() {
      return 2;
    }
    /** Clicks registered within this amount of time are registered as double clicks */

  }, {
    key: "DOUBLE_CLICK_MILLIS",
    get: function get() {
      return 250;
    }
  }]);
  return SelectTool;
}(_paper.default.Tool);

var _default = SelectTool;
exports.default = _default;