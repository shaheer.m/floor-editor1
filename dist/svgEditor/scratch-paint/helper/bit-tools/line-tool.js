"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("../layer");

var _bitmap = require("../bitmap");

var _view = require("../view");

/**
 * Tool for drawing lines with the bitmap brush.
 */
var LineTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(LineTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(LineTool);

  /**
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function LineTool(onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, LineTool);
    _this = _super.call(this);
    _this.onUpdateImage = onUpdateImage; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.colorState = null;
    _this.active = false;
    _this.startPoint = null;
    _this.cursorPreview = null; // Raster to which to draw

    _this.drawTarget = null;
    return _this;
  }

  (0, _createClass2.default)(LineTool, [{
    key: "setColor",
    value: function setColor(color) {
      this.color = color;
      this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color);
    }
  }, {
    key: "setLineSize",
    value: function setLineSize(size) {
      // For performance, make sure this is an integer
      this.size = Math.max(1, ~~size);
      this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color);
    } // Draw a brush mark at the given point

  }, {
    key: "draw",
    value: function draw(x, y) {
      var roundedUpRadius = Math.ceil(this.size / 2);
      this.drawTarget.drawImage(this.tmpCanvas, new _paper.default.Point(~~x - roundedUpRadius, ~~y - roundedUpRadius));
    }
  }, {
    key: "updateCursorIfNeeded",
    value: function updateCursorIfNeeded() {
      if (!this.size) {
        return;
      } // The cursor preview was unattached from the view by an outside process,
      // such as changing costumes or undo.


      if (this.cursorPreview && !this.cursorPreview.parent) {
        this.cursorPreview = null;
      }

      if (!this.cursorPreview || !(this.lastSize === this.size && this.lastColor === this.color)) {
        if (this.cursorPreview) {
          this.cursorPreview.remove();
        }

        this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color);
        this.cursorPreview = new _paper.default.Raster(this.tmpCanvas);
        this.cursorPreview.guide = true;
        this.cursorPreview.parent = (0, _layer.getGuideLayer)();
        this.cursorPreview.data.isHelperItem = true;
      }

      this.lastSize = this.size;
      this.lastColor = this.color;
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.updateCursorIfNeeded();
      this.cursorPreview.position = new _paper.default.Point(~~event.point.x, ~~event.point.y);
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;
      if (this.cursorPreview) this.cursorPreview.remove();
      var tmpCanvas = (0, _layer.createCanvas)();
      this.drawTarget = new _paper.default.Raster(tmpCanvas);
      this.drawTarget.parent = (0, _layer.getGuideLayer)();
      this.drawTarget.guide = true;
      this.drawTarget.locked = true;
      this.drawTarget.position = (0, _layer.getRaster)().position;
      this.draw(event.point.x, event.point.y);
      this.startPoint = event.point;
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button
      // Clear

      var context = this.drawTarget.canvas.getContext('2d');
      context.clearRect(0, 0, _view.ART_BOARD_WIDTH, _view.ART_BOARD_HEIGHT);
      (0, _bitmap.forEachLinePoint)(this.startPoint, event.point, this.draw.bind(this));
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      this.drawTarget.remove();
      this.drawTarget = (0, _layer.getRaster)();
      (0, _bitmap.forEachLinePoint)(this.startPoint, event.point, this.draw.bind(this));
      this.drawTarget = null;
      this.onUpdateImage();
      this.lastPoint = null;
      this.active = false;
      this.updateCursorIfNeeded();
      this.cursorPreview.position = new _paper.default.Point(~~event.point.x, ~~event.point.y);
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.active = false;
      this.tmpCanvas = null;

      if (this.cursorPreview) {
        this.cursorPreview.remove();
        this.cursorPreview = null;
      }
    }
  }]);
  return LineTool;
}(_paper.default.Tool);

var _default = LineTool;
exports.default = _default;