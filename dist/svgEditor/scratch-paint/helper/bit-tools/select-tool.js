"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _layer = require("../layer");

var _bitmap = require("../bitmap");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

var _selectionBoxTool = _interopRequireDefault(require("../selection-tools/selection-box-tool"));

/**
 * paper.Tool that handles select mode in bitmap. This is made up of 2 subtools.
 * - The selection box tool is active when the user clicks an empty space and drags.
 *   It selects all items in the rectangle.
 * - The bounding box tool is active if the user clicks on a non-empty space. It handles
 *   reshaping the selection.
 */
var SelectTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(SelectTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(SelectTool);

  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function SelectTool(setSelectedItems, clearSelectedItems, setCursor, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, SelectTool);
    _this = _super.call(this);
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.BIT_SELECT, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    var nudgeTool = new _nudgeTool.default(_modes.default.BIT_SELECT, _this.boundingBoxTool, onUpdateImage);
    _this.selectionBoxTool = new _selectionBoxTool.default(_modes.default.BIT_SELECT, setSelectedItems, clearSelectedItems);
    _this.selectionBoxMode = false;
    _this.selection = null;
    _this.active = false; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;

    _this.boundingBoxTool.setSelectionBounds();

    return _this;
  }
  /**
   * Should be called if the selection changes to update the bounds of the bounding box.
   * @param {Array<paper.Item>} selectedItems Array of selected items.
   */


  (0, _createClass2.default)(SelectTool, [{
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);

      if (this.selection && this.selection.parent && !this.selection.selected) {
        // Selection got deselected
        this.commitSelection();
      }

      if ((!this.selection || !this.selection.parent) && selectedItems && selectedItems.length === 1 && selectedItems[0] instanceof _paper.default.Raster) {
        // Track the new active selection. This may happen via undo, paste, or drag to select.
        this.selection = selectedItems[0];
      }
    }
    /**
     * Returns the hit options to use when conducting hit tests.
     * @return {object} See paper.Item.hitTest for definition of options
     */

  }, {
    key: "getHitOptions",
    value: function getHitOptions() {
      // Tolerance needs to be scaled when the view is zoomed in in order to represent the same
      // distance for the user to move the mouse.
      return {
        segments: true,
        stroke: true,
        curves: true,
        fill: true,
        guide: false,
        tolerance: SelectTool.TOLERANCE / _paper.default.view.zoom,
        match: function match(hitResult) {
          // Don't match helper items, unless they are handles.
          if (!hitResult.item.data || !hitResult.item.data.isHelperItem) return true;
          return hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle;
        }
      };
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true; // If bounding box tool does not find an item that was hit, rasterize the old selection,
      // then use selection box tool.

      if (!this.boundingBoxTool.onMouseDown(event, event.modifiers.alt, event.modifiers.shift, false
      /* doubleClicked */
      , this.getHitOptions())) {
        this.commitSelection();
        this.selectionBoxMode = true;
        this.selectionBoxTool.onMouseDown(event.modifiers.shift);
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.selectionBoxMode) {
        this.selectionBoxTool.onMouseDrag(event);
      } else {
        this.boundingBoxTool.onMouseDrag(event);
      }
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.boundingBoxTool.onMouseMove(event, this.getHitOptions());
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.selectionBoxMode) {
        this.selectionBoxTool.onMouseUpBitmap(event);
      } else {
        this.boundingBoxTool.onMouseUp(event);
      }

      this.selectionBoxMode = false;
      this.active = false;
    }
  }, {
    key: "commitSelection",
    value: function commitSelection() {
      if (!this.selection || !this.selection.parent) return;
      (0, _bitmap.commitSelectionToBitmap)(this.selection, (0, _layer.getRaster)());
      this.selection.remove();
      this.selection = null;
      this.onUpdateImage();
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.commitSelection();
      this.boundingBoxTool.deactivateTool();
      this.boundingBoxTool = null;
      this.selectionBoxTool = null;
    }
  }], [{
    key: "TOLERANCE",
    get:
    /** The distance within which mouse events count as a hit against an item */
    function get() {
      return 2;
    }
  }]);
  return SelectTool;
}(_paper.default.Tool);

var _default = SelectTool;
exports.default = _default;