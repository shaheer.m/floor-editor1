"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _stylePath = require("../style-path");

var _bitmap = require("../bitmap");

var _layer = require("../layer");

var _selection = require("../selection");

var _math = require("../math");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

/**
 * Tool for drawing rects.
 */
var RectTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(RectTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(RectTool);

  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function RectTool(setSelectedItems, clearSelectedItems, setCursor, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, RectTool);
    _this = _super.call(this);
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.BIT_RECT, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    var nudgeTool = new _nudgeTool.default(_modes.default.BIT_RECT, _this.boundingBoxTool, onUpdateImage); // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;
    _this.rect = null;
    _this.color = null;
    _this.active = false;
    return _this;
  }

  (0, _createClass2.default)(RectTool, [{
    key: "getHitOptions",
    value: function getHitOptions() {
      return {
        segments: false,
        stroke: true,
        curves: false,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item.data && (hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle) || hitResult.item.selected;
        },
        // Allow hits on bounding box and selected only
        tolerance: RectTool.TOLERANCE / _paper.default.view.zoom
      };
    }
    /**
     * Should be called if the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);

      if ((!this.rect || !this.rect.isInserted()) && selectedItems && selectedItems.length === 1 && selectedItems[0].shape === 'rectangle') {
        // Infer that an undo occurred and get back the active rect
        this.rect = selectedItems[0];

        if (this.rect.data.zoomLevel !== _paper.default.view.zoom) {
          this.rect.strokeWidth = this.rect.strokeWidth / this.rect.data.zoomLevel * _paper.default.view.zoom;
          this.rect.data.zoomLevel = _paper.default.view.zoom;
          this.thickness = this.rect.strokeWidth;
        }

        this.filled = this.rect.strokeWidth === 0;
      } else if (this.rect && this.rect.isInserted() && !this.rect.selected) {
        // Rectangle got deselected
        this.commitRect();
      }
    }
  }, {
    key: "styleRect",
    value: function styleRect() {
      (0, _stylePath.styleShape)(this.rect, {
        fillColor: this.filled ? this.color : null,
        strokeColor: this.filled ? null : this.color,
        strokeWidth: this.filled ? 0 : this.thickness
      });
    }
  }, {
    key: "setColor",
    value: function setColor(color) {
      this.color = color;
      if (this.rect) this.styleRect();
    }
  }, {
    key: "setFilled",
    value: function setFilled(filled) {
      if (this.filled === filled) return;
      this.filled = filled;

      if (this.rect && this.rect.isInserted()) {
        this.styleRect();
        this.onUpdateImage();
      }
    }
  }, {
    key: "setThickness",
    value: function setThickness(thickness) {
      if (this.thickness === thickness * _paper.default.view.zoom) return;
      this.thickness = thickness * _paper.default.view.zoom;

      if (this.rect && this.rect.isInserted() && !this.filled) {
        this.rect.strokeWidth = this.thickness;
      }

      if (this.rect && this.rect.isInserted()) {
        this.rect.data.zoomLevel = _paper.default.view.zoom;
        this.onUpdateImage();
      }
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;

      if (this.boundingBoxTool.onMouseDown(event, false
      /* clone */
      , false
      /* multiselect */
      , false
      /* doubleClicked */
      , this.getHitOptions())) {
        this.isBoundingBoxMode = true;
      } else {
        this.isBoundingBoxMode = false;
        (0, _selection.clearSelection)(this.clearSelectedItems);
        this.commitRect();
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseDrag(event);
        return;
      }

      var dimensions = event.point.subtract(event.downPoint);
      var baseRect = new _paper.default.Rectangle(event.downPoint, event.point);
      var squareDimensions = (0, _math.getSquareDimensions)(event.downPoint, event.point);

      if (event.modifiers.shift) {
        baseRect.size = squareDimensions.size.abs();
      }

      if (this.rect) this.rect.remove();
      this.rect = new _paper.default.Shape.Rectangle(baseRect);
      this.rect.strokeJoin = 'round';
      this.rect.strokeScaling = false;
      this.rect.data = {
        zoomLevel: _paper.default.view.zoom
      };
      this.styleRect();

      if (event.modifiers.alt) {
        this.rect.position = event.downPoint;
      } else if (event.modifiers.shift) {
        this.rect.position = squareDimensions.position;
      } else {
        this.rect.position = event.downPoint.add(dimensions.multiply(.5));
      }
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.boundingBoxTool.onMouseMove(event, this.getHitOptions());
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseUp(event);
        this.isBoundingBoxMode = null;
        return;
      }

      if (this.rect) {
        if (Math.abs(this.rect.size.width * this.rect.size.height) < RectTool.TOLERANCE / _paper.default.view.zoom) {
          // Tiny shape created unintentionally?
          this.rect.remove();
          this.rect = null;
        } else {
          // Hit testing does not work correctly unless the width and height are positive
          this.rect.size = new _paper.default.Point(Math.abs(this.rect.size.width), Math.abs(this.rect.size.height));
          this.rect.selected = true;
          this.styleRect();
          this.setSelectedItems();
        }
      }

      this.active = false;
      this.onUpdateImage();
    }
  }, {
    key: "commitRect",
    value: function commitRect() {
      if (!this.rect || !this.rect.isInserted()) return;
      (0, _bitmap.commitRectToBitmap)(this.rect, (0, _layer.getRaster)());
      this.rect.remove();
      this.rect = null;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.commitRect();
      this.boundingBoxTool.deactivateTool();
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }]);
  return RectTool;
}(_paper.default.Tool);

var _default = RectTool;
exports.default = _default;