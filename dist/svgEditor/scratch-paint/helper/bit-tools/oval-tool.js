"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _modes = _interopRequireDefault(require("../../lib/modes"));

var _stylePath = require("../style-path");

var _bitmap = require("../bitmap");

var _layer = require("../layer");

var _selection = require("../selection");

var _math = require("../math");

var _boundingBoxTool = _interopRequireDefault(require("../selection-tools/bounding-box-tool"));

var _nudgeTool = _interopRequireDefault(require("../selection-tools/nudge-tool"));

/**
 * Tool for drawing ovals.
 */
var OvalTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(OvalTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(OvalTool);

  /**
   * @param {function} setSelectedItems Callback to set the set of selected items in the Redux state
   * @param {function} clearSelectedItems Callback to clear the set of selected items in the Redux state
   * @param {function} setCursor Callback to set the visible mouse cursor
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function OvalTool(setSelectedItems, clearSelectedItems, setCursor, onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, OvalTool);
    _this = _super.call(this);
    _this.setSelectedItems = setSelectedItems;
    _this.clearSelectedItems = clearSelectedItems;
    _this.onUpdateImage = onUpdateImage;
    _this.boundingBoxTool = new _boundingBoxTool.default(_modes.default.BIT_OVAL, setSelectedItems, clearSelectedItems, setCursor, onUpdateImage);
    var nudgeTool = new _nudgeTool.default(_modes.default.BIT_OVAL, _this.boundingBoxTool, onUpdateImage); // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseUp = _this.handleMouseUp;
    _this.onKeyUp = nudgeTool.onKeyUp;
    _this.onKeyDown = nudgeTool.onKeyDown;
    _this.oval = null;
    _this.color = null;
    _this.active = false;
    return _this;
  }

  (0, _createClass2.default)(OvalTool, [{
    key: "getHitOptions",
    value: function getHitOptions() {
      return {
        segments: false,
        stroke: true,
        curves: false,
        fill: true,
        guide: false,
        match: function match(hitResult) {
          return hitResult.item.data && (hitResult.item.data.isScaleHandle || hitResult.item.data.isRotHandle) || hitResult.item.selected;
        },
        // Allow hits on bounding box and selected only
        tolerance: OvalTool.TOLERANCE / _paper.default.view.zoom
      };
    }
    /**
     * Should be called if the selection changes to update the bounds of the bounding box.
     * @param {Array<paper.Item>} selectedItems Array of selected items.
     */

  }, {
    key: "onSelectionChanged",
    value: function onSelectionChanged(selectedItems) {
      this.boundingBoxTool.onSelectionChanged(selectedItems);

      if ((!this.oval || !this.oval.isInserted()) && selectedItems && selectedItems.length === 1 && selectedItems[0].shape === 'ellipse') {
        // Infer that an undo occurred and get back the active oval
        this.oval = selectedItems[0];

        if (this.oval.data.zoomLevel !== _paper.default.view.zoom) {
          this.oval.strokeWidth = this.oval.strokeWidth / this.oval.data.zoomLevel * _paper.default.view.zoom;
          this.oval.data.zoomLevel = _paper.default.view.zoom;
          this.thickness = this.oval.strokeWidth;
        }

        this.filled = this.oval.strokeWidth === 0; // We don't need to set our color from the selected oval's color because the color state reducers will
        // do that for us every time the selection changes.
      } else if (this.oval && this.oval.isInserted() && !this.oval.selected) {
        // Oval got deselected
        this.commitOval();
      }
    }
  }, {
    key: "styleOval",
    value: function styleOval() {
      (0, _stylePath.styleShape)(this.oval, {
        fillColor: this.filled ? this.color : null,
        strokeColor: this.filled ? null : this.color,
        strokeWidth: this.filled ? 0 : this.thickness
      });
    }
  }, {
    key: "setColor",
    value: function setColor(color) {
      this.color = color;
      if (this.oval) this.styleOval();
    }
  }, {
    key: "setFilled",
    value: function setFilled(filled) {
      if (this.filled === filled) return;
      this.filled = filled;

      if (this.oval && this.oval.isInserted()) {
        this.styleOval();
        this.onUpdateImage();
      }
    }
  }, {
    key: "setThickness",
    value: function setThickness(thickness) {
      if (this.thickness === thickness * _paper.default.view.zoom) return;
      this.thickness = thickness * _paper.default.view.zoom;

      if (this.oval && this.oval.isInserted() && !this.filled) {
        this.oval.strokeWidth = this.thickness;
      }

      if (this.oval && this.oval.isInserted()) {
        this.oval.data.zoomLevel = _paper.default.view.zoom;
        this.onUpdateImage();
      }
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;

      if (this.boundingBoxTool.onMouseDown(event, false
      /* clone */
      , false
      /* multiselect */
      , false
      /* doubleClicked */
      , this.getHitOptions())) {
        this.isBoundingBoxMode = true;
      } else {
        this.isBoundingBoxMode = false;
        (0, _selection.clearSelection)(this.clearSelectedItems);
        this.commitOval();
        this.oval = new _paper.default.Shape.Ellipse({
          point: event.downPoint,
          size: 0,
          strokeScaling: false
        });
        this.styleOval();
        this.oval.data = {
          zoomLevel: _paper.default.view.zoom
        };
      }
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseDrag(event);
        return;
      }

      var downPoint = new _paper.default.Point(event.downPoint.x, event.downPoint.y);
      var point = new _paper.default.Point(event.point.x, event.point.y);
      var squareDimensions = (0, _math.getSquareDimensions)(event.downPoint, event.point);

      if (event.modifiers.shift) {
        this.oval.size = squareDimensions.size.abs();
      } else {
        this.oval.size = downPoint.subtract(point);
      }

      if (event.modifiers.alt) {
        this.oval.position = downPoint;
      } else if (event.modifiers.shift) {
        this.oval.position = squareDimensions.position;
      } else {
        this.oval.position = downPoint.subtract(this.oval.size.multiply(0.5));
      }

      this.styleOval();
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.boundingBoxTool.onMouseMove(event, this.getHitOptions());
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      if (this.isBoundingBoxMode) {
        this.boundingBoxTool.onMouseUp(event);
        this.isBoundingBoxMode = null;
        return;
      }

      if (this.oval) {
        if (Math.abs(this.oval.size.width * this.oval.size.height) < OvalTool.TOLERANCE / _paper.default.view.zoom) {
          // Tiny oval created unintentionally?
          this.oval.remove();
          this.oval = null;
        } else {
          // Hit testing does not work correctly unless the width and height are positive
          this.oval.size = new _paper.default.Point(Math.abs(this.oval.size.width), Math.abs(this.oval.size.height));
          this.oval.selected = true;
          this.styleOval();
          this.setSelectedItems();
        }
      }

      this.active = false;
      this.onUpdateImage();
    }
  }, {
    key: "commitOval",
    value: function commitOval() {
      if (!this.oval || !this.oval.isInserted()) return;
      (0, _bitmap.commitOvalToBitmap)(this.oval, (0, _layer.getRaster)());
      this.oval.remove();
      this.oval = null;
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.commitOval();
      this.boundingBoxTool.deactivateTool();
    }
  }], [{
    key: "TOLERANCE",
    get: function get() {
      return 2;
    }
  }]);
  return OvalTool;
}(_paper.default.Tool);

var _default = OvalTool;
exports.default = _default;