"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("../layer");

var _bitmap = require("../bitmap");

/**
 * Tool for drawing with the bitmap brush and eraser
 */
var BrushTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(BrushTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(BrushTool);

  /**
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   * @param {boolean} isEraser True if brush should erase
   */
  function BrushTool(onUpdateImage, isEraser) {
    var _this;

    (0, _classCallCheck2.default)(this, BrushTool);
    _this = _super.call(this);
    _this.onUpdateImage = onUpdateImage;
    _this.isEraser = isEraser; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseMove = _this.handleMouseMove;
    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.onMouseUp = _this.handleMouseUp;
    _this.colorState = null;
    _this.active = false;
    _this.lastPoint = null;
    _this.cursorPreview = null;
    return _this;
  }

  (0, _createClass2.default)(BrushTool, [{
    key: "setColor",
    value: function setColor(color) {
      this.color = color;
      this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color, this.isEraser || !this.color);
    }
  }, {
    key: "setBrushSize",
    value: function setBrushSize(size) {
      // For performance, make sure this is an integer
      this.size = Math.max(1, ~~size);
      this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color, this.isEraser || !this.color);
    } // Draw a brush mark at the given point

  }, {
    key: "draw",
    value: function draw(x, y) {
      var roundedUpRadius = Math.ceil(this.size / 2);
      var context = (0, _layer.getRaster)().getContext('2d');

      if (this.isEraser || !this.color) {
        context.globalCompositeOperation = 'destination-out';
      }

      (0, _layer.getRaster)().drawImage(this.tmpCanvas, new _paper.default.Point(~~x - roundedUpRadius, ~~y - roundedUpRadius));

      if (this.isEraser || !this.color) {
        context.globalCompositeOperation = 'source-over';
      }
    }
  }, {
    key: "updateCursorIfNeeded",
    value: function updateCursorIfNeeded() {
      if (!this.size) {
        return;
      } // The cursor preview was unattached from the view by an outside process,
      // such as changing costumes or undo.


      if (this.cursorPreview && !this.cursorPreview.parent) {
        this.cursorPreview = null;
      }

      if (!this.cursorPreview || !(this.lastSize === this.size && this.lastColor === this.color)) {
        if (this.cursorPreview) {
          this.cursorPreview.remove();
        }

        this.tmpCanvas = (0, _bitmap.getBrushMark)(this.size, this.color, this.isEraser || !this.color);
        this.cursorPreview = new _paper.default.Raster(this.tmpCanvas);
        this.cursorPreview.guide = true;
        this.cursorPreview.parent = (0, _layer.getGuideLayer)();
        this.cursorPreview.data.isHelperItem = true;
      }

      this.lastSize = this.size;
      this.lastColor = this.color;
    }
  }, {
    key: "handleMouseMove",
    value: function handleMouseMove(event) {
      this.updateCursorIfNeeded();
      this.cursorPreview.position = new _paper.default.Point(~~event.point.x, ~~event.point.y);
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      if (event.event.button > 0) return; // only first mouse button

      this.active = true;

      if (this.cursorPreview) {
        this.cursorPreview.remove();
      }

      this.draw(event.point.x, event.point.y);
      this.lastPoint = event.point;
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      (0, _bitmap.forEachLinePoint)(this.lastPoint, event.point, this.draw.bind(this));
      this.lastPoint = event.point;
    }
  }, {
    key: "handleMouseUp",
    value: function handleMouseUp(event) {
      if (event.event.button > 0 || !this.active) return; // only first mouse button

      (0, _bitmap.forEachLinePoint)(this.lastPoint, event.point, this.draw.bind(this));
      this.onUpdateImage();
      this.lastPoint = null;
      this.active = false;
      this.updateCursorIfNeeded();
      this.cursorPreview.position = new _paper.default.Point(~~event.point.x, ~~event.point.y);
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {
      this.active = false;
      this.tmpCanvas = null;

      if (this.cursorPreview) {
        this.cursorPreview.remove();
        this.cursorPreview = null;
      }
    }
  }]);
  return BrushTool;
}(_paper.default.Tool);

var _default = BrushTool;
exports.default = _default;