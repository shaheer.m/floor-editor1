"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _bitmap = require("../bitmap");

var _stylePath = require("../style-path");

var _layer = require("../layer");

var _gradientTypes = _interopRequireDefault(require("../../lib/gradient-types"));

var TRANSPARENT = 'rgba(0,0,0,0)';
/**
 * Tool for drawing fills.
 */

var FillTool = /*#__PURE__*/function (_paper$Tool) {
  (0, _inherits2.default)(FillTool, _paper$Tool);

  var _super = (0, _createSuper2.default)(FillTool);

  /**
   * @param {!function} onUpdateImage A callback to call when the image visibly changes
   */
  function FillTool(onUpdateImage) {
    var _this;

    (0, _classCallCheck2.default)(this, FillTool);
    _this = _super.call(this);
    _this.onUpdateImage = onUpdateImage; // We have to set these functions instead of just declaring them because
    // paper.js tools hook up the listeners in the setter functions.

    _this.onMouseDown = _this.handleMouseDown;
    _this.onMouseDrag = _this.handleMouseDrag;
    _this.color = null;
    _this.color2 = null;
    _this.gradientType = null;
    _this.active = false;
    return _this;
  }

  (0, _createClass2.default)(FillTool, [{
    key: "setColor",
    value: function setColor(color) {
      this.color = color;
    }
  }, {
    key: "setColor2",
    value: function setColor2(color2) {
      this.color2 = color2;
    }
  }, {
    key: "setGradientType",
    value: function setGradientType(gradientType) {
      this.gradientType = gradientType;
    }
  }, {
    key: "handleMouseDown",
    value: function handleMouseDown(event) {
      this.paint(event);
    }
  }, {
    key: "handleMouseDrag",
    value: function handleMouseDrag(event) {
      this.paint(event);
    }
  }, {
    key: "paint",
    value: function paint(event) {
      var _this2 = this;

      var sourceContext = (0, _layer.getRaster)().getContext('2d');
      var destContext = sourceContext;
      var color = this.color; // Paint to a mask instead of the original canvas when drawing

      if (this.gradientType !== _gradientTypes.default.SOLID) {
        var tmpCanvas = (0, _layer.createCanvas)();
        destContext = tmpCanvas.getContext('2d');
        color = 'black';
      } else if (!color) {
        // Null color means transparent because that is the standard in vector
        color = TRANSPARENT;
      }

      var changed = false;

      if (event.event.shiftKey) {
        changed = (0, _bitmap.floodFillAll)(event.point.x, event.point.y, color, sourceContext, destContext);
      } else {
        changed = (0, _bitmap.floodFill)(event.point.x, event.point.y, color, sourceContext, destContext);
      }

      if (changed && this.gradientType !== _gradientTypes.default.SOLID) {
        var raster = new _paper.default.Raster({
          insert: false
        });
        raster.canvas = destContext.canvas;

        raster.onLoad = function () {
          raster.position = (0, _layer.getRaster)().position; // Erase what's already there

          (0, _layer.getRaster)().getContext().globalCompositeOperation = 'destination-out';
          (0, _layer.getRaster)().drawImage(raster.canvas, new _paper.default.Point());
          (0, _layer.getRaster)().getContext().globalCompositeOperation = 'source-over'; // Create the gradient to be masked

          var hitBounds = (0, _bitmap.getHitBounds)(raster);
          if (!hitBounds.area) return;
          var gradient = new _paper.default.Shape.Rectangle({
            insert: false,
            rectangle: {
              topLeft: hitBounds.topLeft,
              bottomRight: hitBounds.bottomRight
            }
          });
          gradient.fillColor = (0, _stylePath.createGradientObject)(_this2.color, _this2.color2, _this2.gradientType, gradient.bounds, event.point);
          var rasterGradient = gradient.rasterize((0, _layer.getRaster)().resolution.width, false
          /* insert */
          ); // Mask gradient

          raster.getContext().globalCompositeOperation = 'source-in';
          raster.drawImage(rasterGradient.canvas, rasterGradient.bounds.topLeft); // Draw masked gradient into raster layer

          (0, _layer.getRaster)().drawImage(raster.canvas, new _paper.default.Point());

          _this2.onUpdateImage();
        };
      } else if (changed) {
        this.onUpdateImage();
      }
    }
  }, {
    key: "deactivateTool",
    value: function deactivateTool() {}
  }]);
  return FillTool;
}(_paper.default.Tool);

var _default = FillTool;
exports.default = _default;