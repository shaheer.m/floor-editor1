"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shouldShowSendBackward = exports.shouldShowBringForward = exports.sendToBack = exports.sendBackward = exports.bringToFront = exports.bringForward = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _selection = require("./selection");

var bringToFront = function bringToFront(onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)();

  var _iterator = (0, _createForOfIteratorHelper2.default)(items),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var item = _step.value;
      item.bringToFront();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  onUpdateImage();
};

exports.bringToFront = bringToFront;

var sendToBack = function sendToBack(onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)();

  for (var i = items.length - 1; i >= 0; i--) {
    items[i].sendToBack();
  }

  onUpdateImage();
};

exports.sendToBack = sendToBack;

var bringForward = function bringForward(onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)(); // Already at front

  if (items.length === 0 || !items[items.length - 1].nextSibling) {
    return;
  }

  var nextSibling = items[items.length - 1].nextSibling;

  for (var i = items.length - 1; i >= 0; i--) {
    items[i].insertAbove(nextSibling);
  }

  onUpdateImage();
};

exports.bringForward = bringForward;

var sendBackward = function sendBackward(onUpdateImage) {
  var items = (0, _selection.getSelectedRootItems)(); // Already at front

  if (items.length === 0 || !items[0].previousSibling) {
    return;
  }

  var previousSibling = items[0].previousSibling;

  var _iterator2 = (0, _createForOfIteratorHelper2.default)(items),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var item = _step2.value;
      item.insertBelow(previousSibling);
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }

  onUpdateImage();
};

exports.sendBackward = sendBackward;

var shouldShowSendBackward = function shouldShowSendBackward() {
  var items = (0, _selection.getSelectedRootItems)();

  if (items.length === 0 || !items[0].previousSibling) {
    return false;
  }

  return true;
};

exports.shouldShowSendBackward = shouldShowSendBackward;

var shouldShowBringForward = function shouldShowBringForward() {
  var items = (0, _selection.getSelectedRootItems)();

  if (items.length === 0 || !items[items.length - 1].nextSibling) {
    return false;
  }

  return true;
};

exports.shouldShowBringForward = shouldShowBringForward;