"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectAllBitmap = exports.scaleBitmap = exports.outlineRect = exports.getTrimmedRaster = exports.getHitBounds = exports.getBrushMark = exports.forEachLinePoint = exports.floodFillAll = exports.floodFill = exports.flipBitmapVertical = exports.flipBitmapHorizontal = exports.fillRect = exports.drawEllipse = exports.convertToVector = exports.convertToBitmap = exports.commitSelectionToBitmap = exports.commitRectToBitmap = exports.commitOvalToBitmap = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _layer = require("./layer");

var _guides = require("./guides");

var _selection = require("./selection");

var _view = require("./view");

var _format = _interopRequireDefault(require("../lib/format"));

var _log = _interopRequireDefault(require("../log/log"));

var forEachLinePoint = function forEachLinePoint(point1, point2, callback) {
  // Bresenham line algorithm
  var x1 = ~~point1.x;
  var x2 = ~~point2.x;
  var y1 = ~~point1.y;
  var y2 = ~~point2.y;
  var dx = Math.abs(x2 - x1);
  var dy = Math.abs(y2 - y1);
  var sx = x1 < x2 ? 1 : -1;
  var sy = y1 < y2 ? 1 : -1;
  var err = dx - dy;
  callback(x1, y1);

  while (x1 !== x2 || y1 !== y2) {
    var e2 = err * 2;

    if (e2 > -dy) {
      err -= dy;
      x1 += sx;
    }

    if (e2 < dx) {
      err += dx;
      y1 += sy;
    }

    callback(x1, y1);
  }
};
/**
 * @param {!number} a Coefficient in ax^2 + bx + c = 0
 * @param {!number} b Coefficient in ax^2 + bx + c = 0
 * @param {!number} c Coefficient in ax^2 + bx + c = 0
 * @return {Array<number>} Array of 2 solutions, with the larger solution first
 */


exports.forEachLinePoint = forEachLinePoint;

var solveQuadratic_ = function solveQuadratic_(a, b, c) {
  var soln1 = (-b + Math.sqrt(b * b - 4 * a * c)) / 2 / a;
  var soln2 = (-b - Math.sqrt(b * b - 4 * a * c)) / 2 / a;
  return soln1 > soln2 ? [soln1, soln2] : [soln2, soln1];
};
/**
 * @param {!object} options drawing options
 * @param {!number} options.centerX center of ellipse, x
 * @param {!number} options.centerY center of ellipse, y
 * @param {!number} options.radiusX major radius of ellipse
 * @param {!number} options.radiusY minor radius of ellipse
 * @param {!number} options.shearSlope slope of the sheared x axis
 * @param {?boolean} options.isFilled true if isFilled
 * @param {?function} options.drawFn The function called on each point in the outline, used only
 *     if isFilled is false.
 * @param {!CanvasRenderingContext2D} context for drawing
 * @return {boolean} true if anything was drawn, false if not
 */


var drawShearedEllipse_ = function drawShearedEllipse_(options, context) {
  var centerX = ~~options.centerX;
  var centerY = ~~options.centerY;
  var radiusX = ~~Math.abs(options.radiusX) - .5;
  var radiusY = ~~Math.abs(options.radiusY) - .5;
  var shearSlope = options.shearSlope;
  var isFilled = options.isFilled;
  var drawFn = options.drawFn;

  if (shearSlope === Infinity || radiusX < 1 || radiusY < 1) {
    return false;
  } // A, B, and C represent Ax^2 + Bxy + Cy^2 = 1 coefficients in a skewed ellipse formula


  var A = 1 / radiusX / radiusX + shearSlope * shearSlope / radiusY / radiusY;
  var B = -2 * shearSlope / radiusY / radiusY;
  var C = 1 / radiusY / radiusY; // Line with slope1 intersects the ellipse where its derivative is 1

  var slope1 = (-2 * A - B) / (2 * C + B); // Line with slope2 intersects the ellipse where its derivative is -1

  var slope2 = (-(2 * A) + B) / (-(2 * C) + B);
  var verticalStepsFirst = slope1 > slope2;
  /**
   * Vertical stepping portion of ellipse drawing algorithm
   * @param {!number} startY y to start drawing from
   * @param {!function} conditionFn function which should become true when we should stop stepping
   * @return {object} last point drawn to the canvas, or null if no points drawn
   */

  var drawEllipseStepVertical_ = function drawEllipseStepVertical_(startY, conditionFn) {
    // Points on the ellipse
    var y = startY;
    var x = solveQuadratic_(A, B * y, C * y * y - 1); // last pixel position at which a draw was performed

    var pY;
    var pX1;
    var pX2;

    while (conditionFn(x[0], y)) {
      pY = Math.floor(y);
      pX1 = Math.floor(x[0]);
      pX2 = Math.floor(x[1]);

      if (isFilled) {
        context.fillRect(centerX - pX1 - 1, centerY + pY, pX1 - pX2 + 1, 1);
        context.fillRect(centerX + pX2, centerY - pY - 1, pX1 - pX2 + 1, 1);
      } else {
        drawFn(centerX - pX1 - 1, centerY + pY);
        drawFn(centerX + pX1, centerY - pY - 1);
      }

      y--;
      x = solveQuadratic_(A, B * y, C * y * y - 1);
    }

    return pX1 || pY ? {
      x: pX1,
      y: pY
    } : null;
  };
  /**
   * Horizontal stepping portion of ellipse drawing algorithm
   * @param {!number} startX x to start drawing from
   * @param {!function} conditionFn function which should become false when we should stop stepping
   * @return {object} last point drawn to the canvas, or null if no points drawn
   */


  var drawEllipseStepHorizontal_ = function drawEllipseStepHorizontal_(startX, conditionFn) {
    // Points on the ellipse
    var x = startX;
    var y = solveQuadratic_(C, B * x, A * x * x - 1); // last pixel position at which a draw was performed

    var pX;
    var pY1;
    var pY2;

    while (conditionFn(x, y[0])) {
      pX = Math.floor(x);
      pY1 = Math.floor(y[0]);
      pY2 = Math.floor(y[1]);

      if (isFilled) {
        context.fillRect(centerX - pX - 1, centerY + pY2, 1, pY1 - pY2 + 1);
        context.fillRect(centerX + pX, centerY - pY1 - 1, 1, pY1 - pY2 + 1);
      } else {
        drawFn(centerX - pX - 1, centerY + pY1);
        drawFn(centerX + pX, centerY - pY1 - 1);
      }

      x++;
      y = solveQuadratic_(C, B * x, A * x * x - 1);
    }

    return pX || pY1 ? {
      x: pX,
      y: pY1
    } : null;
  }; // Last point drawn


  var lastPoint;

  if (verticalStepsFirst) {
    var forwardLeaning = false;
    if (slope1 > 0) forwardLeaning = true; // step vertically

    lastPoint = drawEllipseStepVertical_(forwardLeaning ? -radiusY : radiusY, function (x, y) {
      if (x === 0 && y > 0) return true;
      if (x === 0 && y < 0) return false;
      return y / x > slope1;
    }); // step horizontally while slope is flat

    lastPoint = drawEllipseStepHorizontal_(lastPoint ? -lastPoint.x + .5 : .5, function (x, y) {
      return y / x > slope2;
    }) || {
      x: -lastPoint.x - .5,
      y: -lastPoint.y - .5
    }; // step vertically until back to start

    drawEllipseStepVertical_(lastPoint.y - .5, function (x, y) {
      if (forwardLeaning) return y > -radiusY;
      return y > radiusY;
    });
  } else {
    // step horizontally forward
    lastPoint = drawEllipseStepHorizontal_(.5, function (x, y) {
      return y / x > slope2;
    }); // step vertically while slope is steep

    lastPoint = drawEllipseStepVertical_(lastPoint ? lastPoint.y - .5 : radiusY, function (x, y) {
      if (x === 0 && y > 0) return true;
      if (x === 0 && y < 0) return false;
      return y / x > slope1;
    }) || lastPoint; // step horizontally until back to start

    drawEllipseStepHorizontal_(-lastPoint.x + .5, function (x) {
      return x < 0;
    });
  }

  return true;
};
/**
 * @param {!number} size The diameter of the brush
 * @param {!string} color The css color of the brush
 * @param {?boolean} isEraser True if we want the brush mark for the eraser
 * @return {HTMLCanvasElement} a canvas with the brush mark printed on it
 */


var getBrushMark = function getBrushMark(size, color, isEraser) {
  size = ~~size;
  var canvas = document.createElement('canvas');
  var roundedUpRadius = Math.ceil(size / 2);
  canvas.width = roundedUpRadius * 2;
  canvas.height = roundedUpRadius * 2;
  var context = canvas.getContext('2d');
  context.imageSmoothingEnabled = false;
  context.fillStyle = isEraser ? 'white' : color; // Small squares for pixel artists

  if (size <= 5) {
    var offset = 0;
    if (size % 2) offset = 1;

    if (isEraser) {
      context.fillStyle = (0, _guides.getGuideColor)();
      context.fillRect(offset, offset, size, size);
      context.fillStyle = 'white';
      context.fillRect(offset + 1, offset + 1, size - 2, size - 2);
    } else {
      context.fillRect(offset, offset, size, size);
    }
  } else {
    drawShearedEllipse_({
      centerX: size / 2,
      centerY: size / 2,
      radiusX: size / 2,
      radiusY: size / 2,
      shearSlope: 0,
      isFilled: true
    }, context);

    if (isEraser) {
      // Add outline
      context.fillStyle = (0, _guides.getGuideColor)();
      drawShearedEllipse_({
        centerX: size / 2,
        centerY: size / 2,
        radiusX: size / 2,
        radiusY: size / 2,
        shearSlope: 0,
        isFilled: false,
        drawFn: function drawFn(x, y) {
          return context.fillRect(x, y, 1, 1);
        }
      }, context);
    }
  }

  return canvas;
};
/**
 * Draw an ellipse, given the original axis-aligned radii and
 * an affine transformation. Returns false if the ellipse could
 * not be drawn; for instance, the matrix is non-invertible.
 *
 * @param {!options} options Parameters for the ellipse
 * @param {!paper.Point} options.position Center of ellipse
 * @param {!number} options.radiusX x-aligned radius of ellipse
 * @param {!number} options.radiusY y-aligned radius of ellipse
 * @param {!paper.Matrix} options.matrix affine transformation matrix
 * @param {?boolean} options.isFilled true if isFilled
 * @param {?number} options.thickness Thickness of outline, used only if isFilled is false.
 * @param {!CanvasRenderingContext2D} context for drawing
 * @return {boolean} true if anything was drawn, false if not
 */


exports.getBrushMark = getBrushMark;

var drawEllipse = function drawEllipse(options, context) {
  var positionX = options.position.x;
  var positionY = options.position.y;
  var radiusX = options.radiusX;
  var radiusY = options.radiusY;
  var matrix = options.matrix;
  var isFilled = options.isFilled;
  var thickness = options.thickness;
  var drawFn = null;
  if (!matrix.isInvertible()) return false;
  var inverse = matrix.clone().invert();
  var isGradient = context.fillStyle instanceof CanvasGradient; // If drawing a gradient, we need to draw the shape onto a temporary canvas, then draw the gradient atop that canvas
  // only where the shape appears. drawShearedEllipse draws some pixels twice, which would be a problem if the
  // gradient fades to transparent as those pixels would end up looking more opaque. Instead, mask in the gradient.
  // https://github.com/LLK/scratch-paint/issues/1152
  // Outlines are drawn as a series of brush mark images and as such can't be drawn as gradients in the first place.

  var origContext;
  var tmpCanvas;
  var _context$canvas = context.canvas,
      canvasWidth = _context$canvas.width,
      canvasHeight = _context$canvas.height;

  if (isGradient) {
    tmpCanvas = (0, _layer.createCanvas)(canvasWidth, canvasHeight);
    origContext = context;
    context = tmpCanvas.getContext('2d');
  }

  if (!isFilled) {
    var brushMark = getBrushMark(thickness, isGradient ? 'black' : context.fillStyle);
    var roundedUpRadius = Math.ceil(thickness / 2);

    drawFn = function drawFn(x, y) {
      context.drawImage(brushMark, ~~x - roundedUpRadius, ~~y - roundedUpRadius);
    };
  } // Calculate the ellipse formula
  // A, B, and C represent Ax^2 + Bxy + Cy^2 = 1 coefficients in a transformed ellipse formula


  var A = inverse.a * inverse.a / radiusX / radiusX + inverse.b * inverse.b / radiusY / radiusY;
  var B = 2 * inverse.a * inverse.c / radiusX / radiusX + 2 * inverse.b * inverse.d / radiusY / radiusY;
  var C = inverse.c * inverse.c / radiusX / radiusX + inverse.d * inverse.d / radiusY / radiusY; // Convert to a sheared ellipse formula. All ellipses are equivalent to some sheared axis-aligned ellipse.
  // radiusA, radiusB, and slope are parameters of a skewed ellipse with the above formula

  var radiusB = 1 / Math.sqrt(C);
  var radiusA = Math.sqrt(-4 * C / (B * B - 4 * A * C));
  var slope = B / 2 / C;
  var wasDrawn = drawShearedEllipse_({
    centerX: positionX,
    centerY: positionY,
    radiusX: radiusA,
    radiusY: radiusB,
    shearSlope: slope,
    isFilled: isFilled,
    drawFn: drawFn
  }, context); // Mask in the gradient only where the shape was drawn, and draw it. Then draw the gradientified shape onto the
  // original canvas normally.

  if (isGradient && wasDrawn) {
    context.globalCompositeOperation = 'source-in';
    context.fillStyle = origContext.fillStyle;
    context.fillRect(0, 0, canvasWidth, canvasHeight);
    origContext.drawImage(tmpCanvas, 0, 0);
  }

  return wasDrawn;
};

exports.drawEllipse = drawEllipse;

var rowBlank_ = function rowBlank_(imageData, width, y) {
  for (var x = 0; x < width; ++x) {
    if (imageData.data[(y * width << 2) + (x << 2) + 3] !== 0) return false;
  }

  return true;
};

var columnBlank_ = function columnBlank_(imageData, width, x, top, bottom) {
  for (var y = top; y < bottom; ++y) {
    if (imageData.data[(y * width << 2) + (x << 2) + 3] !== 0) return false;
  }

  return true;
};
/**
 * Get bounds around the contents of a raster, trimming transparent pixels from edges.
 * Adapted from Tim Down's https://gist.github.com/timdown/021d9c8f2aabc7092df564996f5afbbf
 * @param {paper.Raster} raster The raster to get the bounds around
 * @param {paper.Rectangle} [rect] Optionally, an alternative bounding rectangle to limit the check to.
 * @returns {paper.Rectangle} The bounds around the opaque area of the passed raster
 * (or opaque within the passed rectangle)
 */


var getHitBounds = function getHitBounds(raster, rect) {
  var bounds = rect || raster.bounds;
  var width = bounds.width;
  var imageData = raster.getImageData(bounds);
  var top = 0;
  var bottom = imageData.height;
  var left = 0;
  var right = imageData.width;

  while (top < bottom && rowBlank_(imageData, width, top)) {
    ++top;
  }

  while (bottom - 1 > top && rowBlank_(imageData, width, bottom - 1)) {
    --bottom;
  }

  while (left < right && columnBlank_(imageData, width, left, top, bottom)) {
    ++left;
  }

  while (right - 1 > left && columnBlank_(imageData, width, right - 1, top, bottom)) {
    --right;
  } // Center an empty bitmap


  if (top === bottom) {
    top = bottom = imageData.height / 2;
  }

  if (left === right) {
    left = right = imageData.width / 2;
  }

  return new _paper.default.Rectangle(left + bounds.left, top + bounds.top, right - left, bottom - top);
};

exports.getHitBounds = getHitBounds;

var trim_ = function trim_(raster) {
  var hitBounds = getHitBounds(raster);

  if (hitBounds.width && hitBounds.height) {
    return raster.getSubRaster(getHitBounds(raster));
  }

  return null;
};
/**
 * @param {boolean} shouldInsert True if the trimmed raster should be added to the active layer.
 * @returns {paper.Raster} raster layer with whitespace trimmed from ends, or null if there is
 * nothing on the raster layer.
 */


var getTrimmedRaster = function getTrimmedRaster(shouldInsert) {
  var trimmedRaster = trim_((0, _layer.getRaster)());
  if (!trimmedRaster) return null;

  if (shouldInsert) {
    _paper.default.project.activeLayer.addChild(trimmedRaster);
  } else {
    trimmedRaster.remove();
  }

  return trimmedRaster;
};

exports.getTrimmedRaster = getTrimmedRaster;

var convertToBitmap = function convertToBitmap(clearSelectedItems, onUpdateImage, optFontInlineFn) {
  // @todo if the active layer contains only rasters, drawing them directly to the raster layer
  // would be more efficient.
  (0, _selection.clearSelection)(clearSelectedItems); // Export svg

  var guideLayers = (0, _layer.hideGuideLayers)(true
  /* includeRaster */
  );
  var bounds = _paper.default.project.activeLayer.drawnBounds;

  var svg = _paper.default.project.exportSVG({
    bounds: 'content',
    matrix: new _paper.default.Matrix().translate(-bounds.x, -bounds.y)
  });

  (0, _layer.showGuideLayers)(guideLayers); // Get rid of anti-aliasing
  // @todo get crisp text https://github.com/LLK/scratch-paint/issues/508

  svg.setAttribute('shape-rendering', 'crispEdges');
  var svgString = new XMLSerializer().serializeToString(svg);

  if (optFontInlineFn) {
    svgString = optFontInlineFn(svgString);
  } else {
    _log.default.error('Fonts may be converted to bitmap incorrectly if fontInlineFn prop is not set on PaintEditor.');
  } // Put anti-aliased SVG into image, and dump image back into canvas


  var img = new Image();

  img.onload = function () {
    if (img.width && img.height) {
      (0, _layer.getRaster)().drawImage(img, new _paper.default.Point(Math.floor(bounds.topLeft.x), Math.floor(bounds.topLeft.y)));
    }

    for (var i = _paper.default.project.activeLayer.children.length - 1; i >= 0; i--) {
      var item = _paper.default.project.activeLayer.children[i];

      if (item.clipMask === false) {
        item.remove();
      } else {
        // Resize mask for bitmap bounds
        item.size.height = _view.ART_BOARD_HEIGHT;
        item.size.width = _view.ART_BOARD_WIDTH;
        item.setPosition(_view.CENTER);
      }
    }

    onUpdateImage(false
    /* skipSnapshot */
    , _format.default.BITMAP
    /* formatOverride */
    );
  };

  img.onerror = function () {
    // Fallback if browser does not support SVG data URIs in images.
    // The problem with rasterize is that it will anti-alias.
    var raster = _paper.default.project.activeLayer.rasterize(72, false
    /* insert */
    );

    raster.onLoad = function () {
      if (raster.canvas.width && raster.canvas.height) {
        (0, _layer.getRaster)().drawImage(raster.canvas, raster.bounds.topLeft);
      }

      _paper.default.project.activeLayer.removeChildren();

      onUpdateImage(false
      /* skipSnapshot */
      , _format.default.BITMAP
      /* formatOverride */
      );
    };
  }; // Hash tags will break image loading without being encoded first


  img.src = "data:image/svg+xml;utf8,".concat(encodeURIComponent(svgString));
};

exports.convertToBitmap = convertToBitmap;

var convertToVector = function convertToVector(clearSelectedItems, onUpdateImage) {
  (0, _selection.clearSelection)(clearSelectedItems);

  var _iterator = (0, _createForOfIteratorHelper2.default)(_paper.default.project.activeLayer.children),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var item = _step.value;

      if (item.clipMask === true) {
        // Resize mask for vector bounds
        item.size.height = _view.MAX_WORKSPACE_BOUNDS.height;
        item.size.width = _view.MAX_WORKSPACE_BOUNDS.width;
        item.setPosition(_view.CENTER);
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  getTrimmedRaster(true
  /* shouldInsert */
  );
  (0, _layer.clearRaster)();
  onUpdateImage(false
  /* skipSnapshot */
  , _format.default.VECTOR
  /* formatOverride */
  );
};

exports.convertToVector = convertToVector;

var getColor_ = function getColor_(x, y, context) {
  return context.getImageData(x, y, 1, 1).data;
};

var matchesColor_ = function matchesColor_(x, y, imageData, oldColor) {
  var index = (y * imageData.width + x) * 4;
  return imageData.data[index + 0] === oldColor[0] && imageData.data[index + 1] === oldColor[1] && imageData.data[index + 2] === oldColor[2] && imageData.data[index + 3] === oldColor[3];
};

var colorPixel_ = function colorPixel_(x, y, imageData, newColor) {
  var index = (y * imageData.width + x) * 4;
  imageData.data[index + 0] = newColor[0];
  imageData.data[index + 1] = newColor[1];
  imageData.data[index + 2] = newColor[2];
  imageData.data[index + 3] = newColor[3];
};
/**
 * Flood fill beginning at the given point.
 * Based on http://www.williammalone.com/articles/html5-canvas-javascript-paint-bucket-tool/
 *
 * @param {!int} x The x coordinate on the context at which to begin
 * @param {!int} y The y coordinate on the context at which to begin
 * @param {!ImageData} sourceImageData The image data to sample from. This is edited by the function.
 * @param {!ImageData} destImageData The image data to edit. May match sourceImageData. Should match
 *     size of sourceImageData.
 * @param {!Array<number>} newColor The color to replace with. A length 4 array [r, g, b, a].
 * @param {!Array<number>} oldColor The color to replace. A length 4 array [r, g, b, a].
 *     This must be different from newColor.
 * @param {!Array<Array<int>>} stack The stack of pixels we need to look at
 */


var floodFillInternal_ = function floodFillInternal_(x, y, sourceImageData, destImageData, newColor, oldColor, stack) {
  while (y > 0 && matchesColor_(x, y - 1, sourceImageData, oldColor)) {
    y--;
  }

  var lastLeftMatchedColor = false;
  var lastRightMatchedColor = false;

  for (; y < sourceImageData.height; y++) {
    if (!matchesColor_(x, y, sourceImageData, oldColor)) break;
    colorPixel_(x, y, sourceImageData, newColor);
    colorPixel_(x, y, destImageData, newColor);

    if (x > 0) {
      if (matchesColor_(x - 1, y, sourceImageData, oldColor)) {
        if (!lastLeftMatchedColor) {
          stack.push([x - 1, y]);
          lastLeftMatchedColor = true;
        }
      } else {
        lastLeftMatchedColor = false;
      }
    }

    if (x < sourceImageData.width - 1) {
      if (matchesColor_(x + 1, y, sourceImageData, oldColor)) {
        if (!lastRightMatchedColor) {
          stack.push([x + 1, y]);
          lastRightMatchedColor = true;
        }
      } else {
        lastRightMatchedColor = false;
      }
    }
  }
};
/**
 * Given a fill style string, get the color
 * @param {string} fillStyleString the fill style
 * @return {Array<int>} Color, a length 4 array
 */


var fillStyleToColor_ = function fillStyleToColor_(fillStyleString) {
  var tmpCanvas = document.createElement('canvas');
  tmpCanvas.width = 1;
  tmpCanvas.height = 1;
  var context = tmpCanvas.getContext('2d');
  context.fillStyle = fillStyleString;
  context.fillRect(0, 0, 1, 1);
  return context.getImageData(0, 0, 1, 1).data;
};
/**
 * Flood fill beginning at the given point
 * @param {!number} x The x coordinate on the context at which to begin
 * @param {!number} y The y coordinate on the context at which to begin
 * @param {!string} color A color string, which would go into context.fillStyle
 * @param {!HTMLCanvas2DContext} sourceContext The context from which to sample to determine where to flood fill
 * @param {!HTMLCanvas2DContext} destContext The context to which to draw. May match sourceContext. Should match
 *     the size of sourceContext.
 * @return {boolean} True if image changed, false otherwise
 */


var floodFill = function floodFill(x, y, color, sourceContext, destContext) {
  x = ~~x;
  y = ~~y;
  var newColor = fillStyleToColor_(color);
  var oldColor = getColor_(x, y, sourceContext);
  var sourceImageData = sourceContext.getImageData(0, 0, sourceContext.canvas.width, sourceContext.canvas.height);
  var destImageData = sourceImageData;

  if (destContext !== sourceContext) {
    destImageData = new ImageData(sourceContext.canvas.width, sourceContext.canvas.height);
  }

  if (oldColor[0] === newColor[0] && oldColor[1] === newColor[1] && oldColor[2] === newColor[2] && oldColor[3] === newColor[3]) {
    // no-op
    return false;
  }

  var stack = [[x, y]];

  while (stack.length) {
    var pop = stack.pop();
    floodFillInternal_(pop[0], pop[1], sourceImageData, destImageData, newColor, oldColor, stack);
  }

  destContext.putImageData(destImageData, 0, 0);
  return true;
};
/**
 * Replace all instances of the color at the given point
 * @param {!number} x The x coordinate on the context of the start color
 * @param {!number} y The y coordinate on the context of the start color
 * @param {!string} color A color string, which would go into context.fillStyle
 * @param {!HTMLCanvas2DContext} sourceContext The context from which to sample to determine where to flood fill
 * @param {!HTMLCanvas2DContext} destContext The context to which to draw. May match sourceContext. Should match
 * @return {boolean} True if image changed, false otherwise
 */


exports.floodFill = floodFill;

var floodFillAll = function floodFillAll(x, y, color, sourceContext, destContext) {
  x = ~~x;
  y = ~~y;
  var newColor = fillStyleToColor_(color);
  var oldColor = getColor_(x, y, sourceContext);
  var sourceImageData = sourceContext.getImageData(0, 0, sourceContext.canvas.width, sourceContext.canvas.height);
  var destImageData = sourceImageData;

  if (destContext !== sourceContext) {
    destImageData = new ImageData(sourceContext.canvas.width, sourceContext.canvas.height);
  }

  if (oldColor[0] === newColor[0] && oldColor[1] === newColor[1] && oldColor[2] === newColor[2] && oldColor[3] === newColor[3]) {
    // no-op
    return false;
  }

  for (var i = 0; i < sourceImageData.width; i++) {
    for (var j = 0; j < sourceImageData.height; j++) {
      if (matchesColor_(i, j, sourceImageData, oldColor)) {
        colorPixel_(i, j, destImageData, newColor);
      }
    }
  }

  destContext.putImageData(destImageData, 0, 0);
  return true;
};
/**
 * @param {!paper.Shape.Rectangle} rect The rectangle to draw to the canvas
 * @param {!HTMLCanvas2DContext} context The context in which to draw
 */


exports.floodFillAll = floodFillAll;

var fillRect = function fillRect(rect, context) {
  // No rotation component to matrix
  if (rect.matrix.b === 0 && rect.matrix.c === 0) {
    var width = rect.size.width * rect.matrix.a;
    var height = rect.size.height * rect.matrix.d;
    context.fillRect(Math.round(rect.matrix.tx - width / 2), Math.round(rect.matrix.ty - height / 2), Math.round(width), Math.round(height));
    return;
  }

  var startPoint = rect.matrix.transform(new _paper.default.Point(-rect.size.width / 2, -rect.size.height / 2));
  var widthPoint = rect.matrix.transform(new _paper.default.Point(rect.size.width / 2, -rect.size.height / 2));
  var heightPoint = rect.matrix.transform(new _paper.default.Point(-rect.size.width / 2, rect.size.height / 2));
  var endPoint = rect.matrix.transform(new _paper.default.Point(rect.size.width / 2, rect.size.height / 2));
  var center = rect.matrix.transform(new _paper.default.Point());
  var points = [startPoint, widthPoint, heightPoint, endPoint].sort(function (a, b) {
    return a.x - b.x;
  });

  var solveY = function solveY(point1, point2, x) {
    if (point2.x === point1.x) return center.x > point1.x ? Number.NEGATIVE_INFINITY : Number.POSITIVE_INFINITY;
    return (point2.y - point1.y) / (point2.x - point1.x) * (x - point1.x) + point1.y;
  };

  for (var x = Math.round(points[0].x); x < Math.round(points[3].x); x++) {
    var ys = [solveY(startPoint, widthPoint, x + .5), solveY(startPoint, heightPoint, x + .5), solveY(endPoint, widthPoint, x + .5), solveY(endPoint, heightPoint, x + .5)].sort(function (a, b) {
      return a - b;
    });
    context.fillRect(x, Math.round(ys[1]), 1, Math.max(1, Math.round(ys[2]) - Math.round(ys[1])));
  }
};
/**
 * @param {!paper.Shape.Rectangle} rect The rectangle to draw to the canvas
 * @param {!number} thickness The thickness of the outline
 * @param {!HTMLCanvas2DContext} context The context in which to draw
 */


exports.fillRect = fillRect;

var outlineRect = function outlineRect(rect, thickness, context) {
  var brushMark = getBrushMark(thickness, context.fillStyle);
  var roundedUpRadius = Math.ceil(thickness / 2);

  var drawFn = function drawFn(x, y) {
    context.drawImage(brushMark, ~~x - roundedUpRadius, ~~y - roundedUpRadius);
  };

  var isGradient = context.fillStyle instanceof CanvasGradient; // If drawing a gradient, we need to draw the shape onto a temporary canvas, then draw the gradient atop that canvas
  // only where the shape appears. Outlines are drawn as a series of brush mark images and as such can't be drawn as
  // gradients.

  var origContext;
  var tmpCanvas;
  var _context$canvas2 = context.canvas,
      canvasWidth = _context$canvas2.width,
      canvasHeight = _context$canvas2.height;

  if (isGradient) {
    tmpCanvas = (0, _layer.createCanvas)(canvasWidth, canvasHeight);
    origContext = context;
    context = tmpCanvas.getContext('2d');
  }

  var startPoint = rect.matrix.transform(new _paper.default.Point(-rect.size.width / 2, -rect.size.height / 2));
  var widthPoint = rect.matrix.transform(new _paper.default.Point(rect.size.width / 2, -rect.size.height / 2));
  var heightPoint = rect.matrix.transform(new _paper.default.Point(-rect.size.width / 2, rect.size.height / 2));
  var endPoint = rect.matrix.transform(new _paper.default.Point(rect.size.width / 2, rect.size.height / 2));
  forEachLinePoint(startPoint, widthPoint, drawFn);
  forEachLinePoint(startPoint, heightPoint, drawFn);
  forEachLinePoint(endPoint, widthPoint, drawFn);
  forEachLinePoint(endPoint, heightPoint, drawFn); // Mask in the gradient only where the shape was drawn, and draw it. Then draw the gradientified shape onto the
  // original canvas normally.

  if (isGradient) {
    context.globalCompositeOperation = 'source-in';
    context.fillStyle = origContext.fillStyle;
    context.fillRect(0, 0, canvasWidth, canvasHeight);
    origContext.drawImage(tmpCanvas, 0, 0);
  }
};

exports.outlineRect = outlineRect;

var flipBitmapHorizontal = function flipBitmapHorizontal(canvas) {
  var tmpCanvas = (0, _layer.createCanvas)(canvas.width, canvas.height);
  var context = tmpCanvas.getContext('2d');
  context.save();
  context.scale(-1, 1);
  context.drawImage(canvas, 0, 0, -tmpCanvas.width, tmpCanvas.height);
  context.restore();
  return tmpCanvas;
};

exports.flipBitmapHorizontal = flipBitmapHorizontal;

var flipBitmapVertical = function flipBitmapVertical(canvas) {
  var tmpCanvas = (0, _layer.createCanvas)(canvas.width, canvas.height);
  var context = tmpCanvas.getContext('2d');
  context.save();
  context.scale(1, -1);
  context.drawImage(canvas, 0, 0, tmpCanvas.width, -tmpCanvas.height);
  context.restore();
  return tmpCanvas;
};

exports.flipBitmapVertical = flipBitmapVertical;

var scaleBitmap = function scaleBitmap(canvas, scale) {
  var tmpCanvas = (0, _layer.createCanvas)(Math.round(canvas.width * Math.abs(scale.x)), canvas.height);

  if (scale.x < 0) {
    canvas = flipBitmapHorizontal(canvas);
  }

  tmpCanvas.getContext('2d').drawImage(canvas, 0, 0, tmpCanvas.width, tmpCanvas.height);
  canvas = tmpCanvas;
  tmpCanvas = (0, _layer.createCanvas)(canvas.width, Math.round(canvas.height * Math.abs(scale.y)));

  if (scale.y < 0) {
    canvas = flipBitmapVertical(canvas);
  }

  tmpCanvas.getContext('2d').drawImage(canvas, 0, 0, tmpCanvas.width, tmpCanvas.height);
  return tmpCanvas;
};
/**
 * Given a raster, take the scale on the transform and apply it to the raster's canvas, then remove
 * the scale from the item's transform matrix. Do this only if scale.x or scale.y is less than 1.
 * @param {paper.Raster} item raster to change
 */


exports.scaleBitmap = scaleBitmap;

var maybeApplyScaleToCanvas_ = function maybeApplyScaleToCanvas_(item) {
  // context.drawImage will anti-alias the image if both width and height are reduced.
  // However, it will preserve pixel colors if only one or the other is reduced, and
  // imageSmoothingEnabled is set to false. Therefore, we can avoid aliasing by scaling
  // down images in a 2 step process.
  var decomposed = item.matrix.decompose(); // Decomposition order: translate, rotate, scale, skew

  if (Math.abs(decomposed.scaling.x) < 1 && Math.abs(decomposed.scaling.y) < 1 && decomposed.scaling.x !== 0 && decomposed.scaling.y !== 0) {
    item.canvas = scaleBitmap(item.canvas, decomposed.scaling);

    if (item.data && item.data.expanded) {
      item.data.expanded.canvas = scaleBitmap(item.data.expanded.canvas, decomposed.scaling);
    } // Remove the scale from the item's matrix


    item.matrix.append(new _paper.default.Matrix().scale(new _paper.default.Point(1 / decomposed.scaling.x, 1 / decomposed.scaling.y)));
  }
};
/**
 * Given a raster, apply its transformation matrix to its canvas. Call maybeApplyScaleToCanvas_ first
 * to avoid introducing anti-aliasing to scaled-down rasters.
 * @param {paper.Raster} item raster to resolve transform of
 * @param {paper.Raster} destination raster to draw selection to
 */


var commitArbitraryTransformation_ = function commitArbitraryTransformation_(item, destination) {
  // Create a canvas to perform masking
  var tmpCanvas = (0, _layer.createCanvas)();
  var context = tmpCanvas.getContext('2d'); // Draw mask

  var rect = new _paper.default.Shape.Rectangle(new _paper.default.Point(), item.size);
  rect.matrix = item.matrix;
  fillRect(rect, context);
  rect.remove();
  context.globalCompositeOperation = 'source-in'; // Draw image onto mask

  var m = item.matrix;
  context.transform(m.a, m.b, m.c, m.d, m.tx, m.ty);
  var canvas = item.canvas;

  if (item.data && item.data.expanded) {
    canvas = item.data.expanded.canvas;
  }

  context.transform(1, 0, 0, 1, -canvas.width / 2, -canvas.height / 2);
  context.drawImage(canvas, 0, 0); // Draw temp canvas onto raster layer

  destination.drawImage(tmpCanvas, new _paper.default.Point());
};
/**
 * Given a raster item, take its transform matrix and apply it to its canvas. Try to avoid
 * introducing anti-aliasing.
 * @param {paper.Raster} selection raster to resolve transform of
 * @param {paper.Raster} bitmap raster to draw selection to
 */


var commitSelectionToBitmap = function commitSelectionToBitmap(selection, bitmap) {
  if (!selection.matrix.isInvertible()) {
    return;
  }

  maybeApplyScaleToCanvas_(selection);
  commitArbitraryTransformation_(selection, bitmap);
};
/**
 * Converts a Paper.js color style (an item's fillColor or strokeColor) into a canvas-applicable color style.
 * Note that a "color style" as applied to an item is different from a plain paper.Color or paper.Gradient.
 * For instance, a gradient "color style" has origin and destination points whereas an unattached paper.Gradient
 * does not.
 * @param {paper.Color} color The color to convert to a canvas color/gradient
 * @param {CanvasRenderingContext2D} context The rendering context on which the style will be used
 * @returns {string|CanvasGradient} The canvas fill/stroke style.
 */


exports.commitSelectionToBitmap = commitSelectionToBitmap;

var _paperColorToCanvasStyle = function _paperColorToCanvasStyle(color, context) {
  if (!color) return null;

  if (color.type === 'gradient') {
    var canvasGradient;
    var origin = color.origin,
        destination = color.destination;

    if (color.gradient.radial) {
      // Adapted from:
      // https://github.com/paperjs/paper.js/blob/b081fd72c72cd61331313c3961edb48f3dfaffbd/src/style/Color.js#L926-L935
      var highlight = color.highlight;
      var start = highlight || origin;
      var radius = destination.getDistance(origin);

      if (highlight) {
        var vector = highlight.subtract(origin);

        if (vector.getLength() > radius) {
          // Paper ¯\_(ツ)_/¯
          highlight = origin.add(vector.normalize(radius - 0.1));
        }
      }

      canvasGradient = context.createRadialGradient(start.x, start.y, 0, origin.x, origin.y, radius);
    } else {
      canvasGradient = context.createLinearGradient(origin.x, origin.y, destination.x, destination.y);
    }

    var stops = color.gradient.stops; // Adapted from:
    // https://github.com/paperjs/paper.js/blob/b081fd72c72cd61331313c3961edb48f3dfaffbd/src/style/Color.js#L940-L950

    for (var i = 0, len = stops.length; i < len; i++) {
      var stop = stops[i];
      var offset = stop.offset;
      canvasGradient.addColorStop(offset || i / (len - 1), stop.color.toCSS());
    }

    return canvasGradient;
  }

  return color.toCSS();
};
/**
 * @param {paper.Shape.Ellipse} oval Vector oval to convert
 * @param {paper.Raster} bitmap raster to draw selection
 * @return {bool} true if the oval was drawn
 */


var commitOvalToBitmap = function commitOvalToBitmap(oval, bitmap) {
  var radiusX = Math.abs(oval.size.width / 2);
  var radiusY = Math.abs(oval.size.height / 2);
  var context = bitmap.getContext('2d');
  var filled = oval.strokeWidth === 0;

  var canvasColor = _paperColorToCanvasStyle(filled ? oval.fillColor : oval.strokeColor, context); // If the color is null (e.g. fully transparent/"no fill"), don't bother drawing anything


  if (!canvasColor) return;
  context.fillStyle = canvasColor;
  var drew = drawEllipse({
    position: oval.position,
    radiusX: radiusX,
    radiusY: radiusY,
    matrix: oval.matrix,
    isFilled: filled,
    thickness: oval.strokeWidth / _paper.default.view.zoom
  }, context);
  return drew;
};
/**
 * @param {paper.Rectangle} rect Vector rectangle to convert
 * @param {paper.Raster} bitmap raster to draw selection to
 */


exports.commitOvalToBitmap = commitOvalToBitmap;

var commitRectToBitmap = function commitRectToBitmap(rect, bitmap) {
  var tmpCanvas = (0, _layer.createCanvas)();
  var context = tmpCanvas.getContext('2d');
  var filled = rect.strokeWidth === 0;

  var canvasColor = _paperColorToCanvasStyle(filled ? rect.fillColor : rect.strokeColor, context); // If the color is null (e.g. fully transparent/"no fill"), don't bother drawing anything


  if (!canvasColor) return;
  context.fillStyle = canvasColor;

  if (filled) {
    fillRect(rect, context);
  } else {
    outlineRect(rect, rect.strokeWidth / _paper.default.view.zoom, context);
  }

  bitmap.drawImage(tmpCanvas, new _paper.default.Point());
};

exports.commitRectToBitmap = commitRectToBitmap;

var selectAllBitmap = function selectAllBitmap(clearSelectedItems) {
  (0, _selection.clearSelection)(clearSelectedItems); // Copy trimmed raster to active layer. If the raster layer was empty, nothing is selected.

  var trimmedRaster = getTrimmedRaster(true
  /* shouldInsert */
  );

  if (trimmedRaster) {
    trimmedRaster.selected = true;
  } // Clear raster layer


  (0, _layer.clearRaster)();
};

exports.selectAllBitmap = selectAllBitmap;