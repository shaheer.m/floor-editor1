"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isCompoundPathChild = exports.isCompoundPath = exports.getItemsCompoundPath = void 0;

var isCompoundPath = function isCompoundPath(item) {
  return item && item.className === 'CompoundPath';
};

exports.isCompoundPath = isCompoundPath;

var isCompoundPathChild = function isCompoundPathChild(item) {
  if (item.parent) {
    return item.parent.className === 'CompoundPath';
  }

  return false;
};

exports.isCompoundPathChild = isCompoundPathChild;

var getItemsCompoundPath = function getItemsCompoundPath(item) {
  var itemParent = item.parent;

  if (isCompoundPath(itemParent)) {
    return itemParent;
  }

  return null;
};

exports.getItemsCompoundPath = getItemsCompoundPath;