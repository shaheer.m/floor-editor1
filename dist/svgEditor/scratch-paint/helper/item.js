"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setPositionInView = exports.setPivot = exports.isPointTextItem = exports.isPathItem = exports.isPGTextItem = exports.isGroupItem = exports.isCompoundPathItem = exports.isBoundsItem = exports.getRootItem = exports.getPositionInView = void 0;

var _paper = _interopRequireDefault(require("@scratch/paper"));

var getRootItem = function getRootItem(item) {
  if (item.parent.className === 'Layer') {
    return item;
  }

  return getRootItem(item.parent);
};

exports.getRootItem = getRootItem;

var isBoundsItem = function isBoundsItem(item) {
  if (item.className === 'PointText' || item.className === 'Shape' || item.className === 'PlacedSymbol' || item.className === 'Raster') {
    return true;
  }

  return false;
};

exports.isBoundsItem = isBoundsItem;

var isPathItem = function isPathItem(item) {
  return item.className === 'Path';
};

exports.isPathItem = isPathItem;

var isCompoundPathItem = function isCompoundPathItem(item) {
  return item.className === 'CompoundPath';
};

exports.isCompoundPathItem = isCompoundPathItem;

var isGroupItem = function isGroupItem(item) {
  return item && item.className && item.className === 'Group';
};

exports.isGroupItem = isGroupItem;

var isPointTextItem = function isPointTextItem(item) {
  return item.className === 'PointText';
};

exports.isPointTextItem = isPointTextItem;

var isPGTextItem = function isPGTextItem(item) {
  return getRootItem(item).data.isPGTextItem;
};

exports.isPGTextItem = isPGTextItem;

var setPivot = function setPivot(item, point) {
  if (isBoundsItem(item)) {
    item.pivot = item.globalToLocal(point);
  } else {
    item.pivot = point;
  }
};

exports.setPivot = setPivot;

var getPositionInView = function getPositionInView(item) {
  var itemPos = new _paper.default.Point();
  itemPos.x = item.position.x - _paper.default.view.bounds.x;
  itemPos.y = item.position.y - _paper.default.view.bounds.y;
  return itemPos;
};

exports.getPositionInView = getPositionInView;

var setPositionInView = function setPositionInView(item, pos) {
  item.position.x = _paper.default.view.bounds.x + pos.x;
  item.position.y = _paper.default.view.bounds.y + pos.y;
};

exports.setPositionInView = setPositionInView;