"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getHoveredItem = void 0;

var _createForOfIteratorHelper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createForOfIteratorHelper"));

var _paper = _interopRequireDefault(require("@scratch/paper"));

var _item = require("./item");

var _guides = require("./guides");

var _group = require("./group");

var _math = require("./math");

/**
 * @param {!MouseEvent} event mouse event
 * @param {?object} hitOptions hit options to use
 * @param {?boolean} subselect Whether items within groups can be hovered. If false, the
 *    entire group should be hovered.
 * @return {paper.Item} the hovered item or null if there is none
 */
var getHoveredItem = function getHoveredItem(event, hitOptions, subselect) {
  var oldMatch = hitOptions.match;

  hitOptions.match = function (hitResult) {
    if (hitResult.item.data && hitResult.item.data.noHover) return false;
    return oldMatch ? oldMatch(hitResult) : true;
  };

  var hitResults = _paper.default.project.hitTestAll(event.point, hitOptions);

  if (hitResults.length === 0) {
    return null;
  } // Get highest z-index result


  var hitResult;

  var _iterator = (0, _createForOfIteratorHelper2.default)(hitResults),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var result = _step.value;

      if (!hitResult || (0, _math.sortItemsByZIndex)(hitResult.item, result.item) < 0) {
        hitResult = result;
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  var item = hitResult.item; // If the hovered item is already selected, then there should be no hovered item.

  if (!item || item.selected) {
    return null;
  }

  var hoverGuide;

  if ((0, _item.isBoundsItem)(item)) {
    hoverGuide = (0, _guides.hoverBounds)(item);
  } else if (!subselect && (0, _group.isGroupChild)(item)) {
    hoverGuide = (0, _guides.hoverBounds)((0, _item.getRootItem)(item));
  } else {
    hoverGuide = (0, _guides.hoverItem)(item);
  }

  hoverGuide.data.hitResult = hitResult;
  return hoverGuide;
};

exports.getHoveredItem = getHoveredItem;