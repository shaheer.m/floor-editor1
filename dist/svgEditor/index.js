"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SVGEditor = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/inherits"));

var _createSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/createSuper"));

var _redux = require("redux");

var _combineReducers = _interopRequireDefault(require("./scratch-paint/playground/reducers/combine-reducers"));

var _intl = require("./scratch-paint/playground/reducers/intl.js");

var _lodash = _interopRequireDefault(require("lodash.bindall"));

var _react = _interopRequireDefault(require("react"));

var _paintEditor = _interopRequireDefault(require("./scratch-paint/containers/paint-editor"));

var _wrapperModule = _interopRequireDefault(require("../wrapper.module.css"));

var _reactRedux = require("react-redux");

require("./App.css");

var store = (0, _redux.createStore)(_combineReducers.default, _intl.intlInitialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

var SVGEditor = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(SVGEditor, _React$Component);

  var _super = (0, _createSuper2.default)(SVGEditor);

  function SVGEditor(props) {
    var _this;

    (0, _classCallCheck2.default)(this, SVGEditor);
    _this = _super.call(this, props);
    (0, _lodash.default)((0, _assertThisInitialized2.default)(_this), ['handleUpdateName', 'handleUpdateImage', 'onUploadImage', 'downloadImage', 'b64toByteArray', 'handleAlertMessage', 'handleBack', 'onSave']);
    _this.id = 0;
    _this.state = {
      name: _this.props.name,
      image: _this.props.image,
      imageFormat: _this.props.type,
      rotationCenterX: undefined,
      rotationCenterY: undefined,
      imageId: _this.id
    };
    _this.reusableCanvas = document.createElement('canvas');
    return _this;
  }

  (0, _createClass2.default)(SVGEditor, [{
    key: "componentDidMount",
    value: function componentDidMount() {// if (this.props.uploadImage) {
      //   const file = this.props.uploadImage();
      //   if (file) {
      //     this.onUploadImage(file);
      //   }
      // }
    }
  }, {
    key: "handleBack",
    value: function handleBack() {
      this.props.handleBack();
    }
  }, {
    key: "handleUpdateName",
    value: function handleUpdateName(val) {
      this.setState({
        name: val
      });
    }
  }, {
    key: "handleAlertMessage",
    value: function handleAlertMessage(msg) {
      this.props.handleAlertMessage(msg);
    }
  }, {
    key: "handleUpdateImage",
    value: function handleUpdateImage(isVector, image, rotationCenterX, rotationCenterY) {
      this.setState({
        imageFormat: isVector ? 'svg' : 'png'
      });

      if (!isVector) {}

      if (isVector) {
        this.setState({
          image: image,
          rotationCenterX: rotationCenterX,
          rotationCenterY: rotationCenterY
        });
      } else {
        // is Bitmap
        // image parameter has type ImageData
        // paint editor takes dataURI as input
        this.reusableCanvas.width = image.width;
        this.reusableCanvas.height = image.height;
        var context = this.reusableCanvas.getContext('2d');
        context.putImageData(image, 0, 0);
        this.setState({
          image: this.reusableCanvas.toDataURL('image/png'),
          rotationCenterX: rotationCenterX,
          rotationCenterY: rotationCenterY
        });
      }
    }
  }, {
    key: "onUploadImage",
    value: function onUploadImage(file) {
      //  var file = event.target.files[0];
      var type = file.type === 'image/svg+xml' ? 'svg' : file.type === 'image/png' ? 'png' : file.type === 'image/jpg' ? 'jpg' : file.type === 'image/jpeg' ? 'jpg' : null;
      var reader = new FileReader();

      if (type === 'svg') {
        reader.readAsText(file, 'UTF-8');
      } else if (type === 'png' || type === 'jpg') {
        reader.readAsDataURL(file);
      } else {
        alert("Couldn't read file type: " + file.type);
      }

      var that = this;

      reader.onload = function (readerEvent) {
        var content = readerEvent.target.result; // this is the content!

        that.setState({
          image: content,
          name: file.name.split('.').slice(0, -1).join('.'),
          imageId: ++that.id,
          imageFormat: type,
          rotationCenterX: undefined,
          rotationCenterY: undefined
        });
      };
    }
  }, {
    key: "onSave",
    value: function onSave() {
      this.props.handleSave({
        image: this.state.image,
        name: this.state.name
      });
    }
  }, {
    key: "downloadImage",
    value: function downloadImage() {
      var downloadLink = document.createElement('a');
      document.body.appendChild(downloadLink);
      var format = this.state.imageFormat;
      var data = this.state.image;

      if (format === 'png' || format === 'jpg') {
        data = this.b64toByteArray(data);
      } else {
        data = [data];
      }

      var blob = new Blob(data, {
        type: format
      });
      var filename = "".concat(this.state.name, ".").concat(format);
      var file = new File([blob], filename); // this.props.sendFileData(file);
      // if ('download' in HTMLAnchorElement.prototype) {
      //   const url = window.URL.createObjectURL(blob);
      //   downloadLink.href = url;
      //   downloadLink.download = filename;
      //   downloadLink.type = blob.type;
      //   downloadLink.click();
      //   window.URL.revokeObjectURL(url);
      // } else {
      //   // iOS Safari, open a new page and set href to data-uri
      //   let popup = window.open('', '_blank');
      //   const reader = new FileReader();
      //   reader.onloadend = function () {
      //     popup.location.href = reader.result;
      //     popup = null;
      //   };
      //   reader.readAsDataURL(blob);
      // }
      // document.body.removeChild(downloadLink);
    }
  }, {
    key: "b64toByteArray",
    value: function b64toByteArray(b64Data) {
      var sliceSize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 512;
      // Remove header
      b64Data = b64Data.substring(b64Data.indexOf('base64,') + 7);
      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);

        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      return byteArrays;
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement(_reactRedux.Provider, {
        store: store
      }, /*#__PURE__*/_react.default.createElement(_intl.IntlProvider, null, /*#__PURE__*/_react.default.createElement("div", {
        className: _wrapperModule.default.wrapper
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: _wrapperModule.default.playgroundContainer
      }, /*#__PURE__*/_react.default.createElement(_paintEditor.default, Object.assign({
        sendAlertMessage: this.handleAlertMessage,
        onBackButton: this.handleBack,
        onSave: this.onSave
      }, this.state, {
        onUpdateName: this.handleUpdateName,
        onUpdateImage: this.handleUpdateImage
      }))))));
    }
  }]);
  return SVGEditor;
}(_react.default.Component);

exports.SVGEditor = SVGEditor;