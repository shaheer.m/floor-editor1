import { createStore } from 'redux';
import reducer from './scratch-paint/playground/reducers/combine-reducers'
import { intlInitialState, IntlProvider } from './scratch-paint/playground/reducers/intl.js';
import bindAll from 'lodash.bindall';
import React from 'react';
import PaintEditor from './scratch-paint/containers/paint-editor'
import styles from '../wrapper.module.css'
import { Provider } from 'react-redux'
import './App.css'

const store = createStore(
  reducer,
  intlInitialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export class SVGEditor extends React.Component {
  constructor(props) {
    super(props);
    bindAll(this, [
      'handleUpdateName',
      'handleUpdateImage',
      'onUploadImage',
      'downloadImage',
      'b64toByteArray',
      'handleAlertMessage',
      'handleBack',
      'onSave'
    ]);
    this.id = 0;
    this.state = {
      name: this.props.name,
      image: this.props.image,
      imageFormat: this.props.type,
      rotationCenterX: undefined,
      rotationCenterY: undefined,
      imageId: this.id
    }
    this.reusableCanvas = document.createElement('canvas');
  }

  componentDidMount() {
      // if (this.props.uploadImage) {
      //   const file = this.props.uploadImage();
      //   if (file) {
      //     this.onUploadImage(file);
      //   }
        
      // }
  }
  handleBack() {
    this.props.handleBack();
  }

  handleUpdateName(val) {
    this.setState({ name: val });
  }
  handleAlertMessage(msg) {
    this.props.handleAlertMessage(msg)
  }

  handleUpdateImage(isVector, image, rotationCenterX, rotationCenterY) {
    this.setState({
      imageFormat: isVector ? 'svg' : 'png'
    });
    if (!isVector) {
    }
    if (isVector) {
      this.setState({ image, rotationCenterX, rotationCenterY });
    } else { // is Bitmap
      // image parameter has type ImageData
      // paint editor takes dataURI as input
      this.reusableCanvas.width = image.width;
      this.reusableCanvas.height = image.height;
      const context = this.reusableCanvas.getContext('2d');
      context.putImageData(image, 0, 0);
      this.setState({
        image: this.reusableCanvas.toDataURL('image/png'),
        rotationCenterX: rotationCenterX,
        rotationCenterY: rotationCenterY
      });
    }
  }

  onUploadImage(file) {
  //  var file = event.target.files[0];
    var type = file.type === 'image/svg+xml' ? 'svg' :
      file.type === 'image/png' ? 'png' :
        file.type === 'image/jpg' ? 'jpg' :
          file.type === 'image/jpeg' ? 'jpg' :
            null;

    var reader = new FileReader();
    if (type === 'svg') {
      reader.readAsText(file, 'UTF-8');
    } else if (type === 'png' || type === 'jpg') {
      reader.readAsDataURL(file);
    } else {
      alert("Couldn't read file type: " + file.type);
    }

    const that = this;
    reader.onload = readerEvent => {
      var content = readerEvent.target.result; // this is the content!

      that.setState({
        image: content,
        name: file.name.split('.').slice(0, -1).join('.'),
        imageId: ++that.id,
        imageFormat: type,
        rotationCenterX: undefined,
        rotationCenterY: undefined,
      });
    }
  }

  onSave() {
    this.props.handleSave({
      image: this.state.image,
      name: this.state.name,
    });
  }

  downloadImage() {
    const downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);

    const format = this.state.imageFormat;
    let data = this.state.image;
    if (format === 'png' || format === 'jpg') {
      data = this.b64toByteArray(data);
    } else {
      data = [data];
    }
    const blob = new Blob(data, { type: format });
    const filename = `${this.state.name}.${format}`;
    var file = new File([blob], filename);
   // this.props.sendFileData(file);
    // if ('download' in HTMLAnchorElement.prototype) {
    //   const url = window.URL.createObjectURL(blob);
    //   downloadLink.href = url;
    //   downloadLink.download = filename;
    //   downloadLink.type = blob.type;
    //   downloadLink.click();
    //   window.URL.revokeObjectURL(url);
    // } else {
    //   // iOS Safari, open a new page and set href to data-uri
    //   let popup = window.open('', '_blank');
    //   const reader = new FileReader();
    //   reader.onloadend = function () {
    //     popup.location.href = reader.result;
    //     popup = null;
    //   };
    //   reader.readAsDataURL(blob);
    // }
    // document.body.removeChild(downloadLink);
  }
  b64toByteArray(b64Data, sliceSize = 512) {
    // Remove header
    b64Data = b64Data.substring(b64Data.indexOf('base64,') + 7);

    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    return byteArrays;
  }

  render() {
    return (<Provider store={store}>
      <IntlProvider>
        <div className={styles.wrapper}>
          <div className={styles.playgroundContainer}>
            {/* <input type="file" name="name" onChange={this.onUploadImage} /> */}
            <PaintEditor sendAlertMessage={this.handleAlertMessage} onBackButton={this.handleBack} onSave={this.onSave} {...this.state} onUpdateName={this.handleUpdateName} onUpdateImage={this.handleUpdateImage} />
          </div>
        </div>
      </IntlProvider>
    </Provider>)
  }

}
