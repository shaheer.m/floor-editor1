/* eslint-disable no-restricted-globals */
import bindAll from 'lodash.bindall';
import React from 'react';
import ReactDOM from 'react-dom';
import PaintEditor from '..';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers/combine-reducers';
import { intlInitialState, IntlProvider } from './reducers/intl.js';
import styles from './playground.module.css';
import paper from '@scratch/paper'
import { performSnapshot } from '../helper/undo';
import { undoSnapshot, clearUndoState } from '../reducers/undo';
import { ensureClockwise, scaleWithStrokes } from '../helper/math';
import { isGroup, ungroupItems } from '../helper/group';
// scratch-render-fonts is a playground-only dep. Fonts are expected to be imported
// as a peer dependency, otherwise there will be two copies of them.
import { FONTS } from 'scratch-render-fonts';
// import svgImage from '../../../logo.svg'
import log from '../log/log';
import format from '../lib/format';
// import {performSnapshot} from '../helper/undo';
import Formats from '../lib/format';
import {
    CENTER, MAX_WORKSPACE_BOUNDS
} from '../helper/view';
// import { Point } from 'paper/dist/paper-core';

import { setRasterImage } from '../../scratch-paint/helper/tools/raster-img'

const appTarget = document.createElement('div');
appTarget.setAttribute('class', styles.playgroundContainer);
document.body.appendChild(appTarget);
const store = createStore(
    reducer,
    intlInitialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const svgString =
    '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"' +
    ' x="0px" y="0px" width="32px" height="32px" viewBox="0.5 384.5 32 32"' +
    ' enable-background="new 0.5 384.5 32 32" xml:space="preserve">' +
    '<path fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="10" d="M7.5,392.241h7.269' +
    'c4.571,0,8.231,5.555,8.231,10.123v7.377"/>' +
    '<polyline points="10.689,399.492 3.193,391.997 10.689,384.5 "/>' +
    '<polyline points="30.185,405.995 22.689,413.491 15.192,405.995 "/>' +
    '</svg>';
export default class Playground extends React.Component {
    constructor(props) {
        super(props);
        bindAll(this, [
            'downloadImage',
            'handleUpdateName',
            'handleUpdateImage',
            'onUploadImage',
            'setCustomImage',
            'recalibrateSize'
            // 'importSvg'
        ]);
        // Append ?dir=rtl to URL to get RTL layout
        const match = location.search.match(/dir=([^&]+)/);
        const rtl = match && match[1] == 'rtl';
        this.id = 0;
        this.state = {
            name: 'meow',
            rotationCenterX: 20,
            rotationCenterY: 400,
            imageFormat: 'svg', // 'svg', 'png', or 'jpg'
            image: svgString, // svg string or data URI
            imageId: this.id, // If this changes, the paint editor will reload
            rtl: rtl,
        };
        this.reusableCanvas = document.createElement('canvas');
    }
    handleUpdateName(name) {
        this.setState({ name });
    }
    handleUpdateImage(isVector, image, rotationCenterX, rotationCenterY) {
        this.setState({
            imageFormat: isVector ? 'svg' : 'png'
        });
        if (!isVector) {
        }
        if (isVector) {
            this.setState({ image, rotationCenterX, rotationCenterY });
        } else { // is Bitmap
            // image parameter has type ImageData
            // paint editor takes dataURI as input
            this.reusableCanvas.width = image.width;
            this.reusableCanvas.height = image.height;
            const context = this.reusableCanvas.getContext('2d');
            context.putImageData(image, 0, 0);
            this.setState({
                image: this.reusableCanvas.toDataURL('image/png'),
                rotationCenterX: rotationCenterX,
                rotationCenterY: rotationCenterY
            });
        }
    }
    recalibrateSize(callback) {
        // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
        // When these are out of sync, the mouse events in the paint editor don't line up correctly.
        return window.setTimeout(() => {
            // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
            // This could also be called before paper.view exists.
            // In either case, return early if so without running the callback.
            if (!paper.view) return;
            // Prevent blurriness caused if the "CSS size" of the element is a float--
            // setting canvas dimensions to floats floors them, but we need to round instead
            const elemSize = paper.DomElement.getSize(paper.view.element);
            elemSize.width = Math.round(elemSize.width);
            elemSize.height = Math.round(elemSize.height);
            paper.view.setViewSize(elemSize);

            if (callback) callback();
        }, 0);
    }

    setCustomImage(asset) {
        const paperCanvas = this;
        setRasterImage(paperCanvas, asset)
    }




    downloadImage() {
        const downloadLink = document.createElement('a');
        document.body.appendChild(downloadLink);

        const format = this.state.imageFormat;
        let data = this.state.image;
        if (format === 'png' || format === 'jpg') {
            data = this.b64toByteArray(data);
        } else {
            data = [data];
        }
        const blob = new Blob(data, { type: format });
        const filename = `${this.state.name}.${format}`;
        if ('download' in HTMLAnchorElement.prototype) {
            const url = window.URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = filename;
            downloadLink.type = blob.type;
            downloadLink.click();
            window.URL.revokeObjectURL(url);
        } else {
            // iOS Safari, open a new page and set href to data-uri
            let popup = window.open('', '_blank');
            const reader = new FileReader();
            reader.onloadend = function () {
                popup.location.href = reader.result;
                popup = null;
            };
            reader.readAsDataURL(blob);
        }
        document.body.removeChild(downloadLink);
    }
    b64toByteArray(b64Data, sliceSize = 512) {
        // Remove header
        b64Data = b64Data.substring(b64Data.indexOf('base64,') + 7);

        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        return byteArrays;
    }
    uploadImage() {
        document.getElementById(styles.fileInput).click();
    }
    onUploadImage(event) {
        var file = event.target.files[0];
        var type = file.type === 'image/svg+xml' ? 'svg' :
            file.type === 'image/png' ? 'png' :
                file.type === 'image/jpg' ? 'jpg' :
                    file.type === 'image/jpeg' ? 'jpg' :
                        null;

        var reader = new FileReader();
        if (type === 'svg') {
            reader.readAsText(file, 'UTF-8');
        } else if (type === 'png' || type === 'jpg') {
            reader.readAsDataURL(file);
        } else {
            alert("Couldn't read file type: " + file.type);
        }

        const that = this;
        reader.onload = readerEvent => {
            var content = readerEvent.target.result; // this is the content!

            that.setState({
                image: content,
                name: file.name.split('.').slice(0, -1).join('.'),
                imageId: ++that.id,
                imageFormat: type,
                rotationCenterX: undefined,
                rotationCenterY: undefined,
            });
        }
    }
    render() {
        return (
            <div className={styles.wrapper}>
                {/* <img id="mona" class="visuallyhidden" src={svgImage} width="320" height="491"></img> */}
                <PaintEditor
                    {...this.state}
                    onUpdateName={this.handleUpdateName}
                    onUpdateImage={this.handleUpdateImage}
                    addShape={this.setCustomImage}
                />
                <button className={styles.playgroundButton} onClick={this.uploadImage}>Upload</button>
                <input id={styles.fileInput} type="file" name="name" onChange={this.onUploadImage} />
                <button className={styles.playgroundButton} onClick={this.downloadImage}>Download</button>
                {/* <button className={styles.playgroundButton} onClick={event => this.setCustomImage('chair')}>chair</button>
                <button className={styles.playgroundButton} onClick={event => this.setCustomImage('table')}>table</button> */}
            </div>
        );
    }

}
ReactDOM.render((
    <Provider store={store}>
        <IntlProvider>
            <Playground />
        </IntlProvider>
    </Provider>
), appTarget);
