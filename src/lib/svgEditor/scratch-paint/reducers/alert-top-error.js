const SHOW_ALERT = 'SHOW ALERT ON TOP';
const HIDE_ALERT = 'HIDE ALERT ON TOP'
const initialState = {
    status: false,
    msg: ''
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
    case SHOW_ALERT:
        return {
            status: true,
            msg: action.alertText
        }
    case HIDE_ALERT:
        return {
            status: false,
            msg: ''
        }
    default:
        return state;
    }
};

const setAlertText = function (alertText) {
    return {
        type: SHOW_ALERT,
        alertText: alertText ? alertText : null
    };
};

const hideAlertText = function () {
    return {
        type: HIDE_ALERT,
        alertText: ''
    };
};

export {
    reducer as default,
    setAlertText,
    hideAlertText
};
