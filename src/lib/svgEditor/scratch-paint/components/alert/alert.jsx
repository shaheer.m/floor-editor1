/* DO NOT EDIT
@todo This file is copied from GUI and should be pulled out into a shared library.
See #13 */

/* ACTUALLY, THIS IS EDITED ;)
THIS WAS CHANGED ON 10/25/2017 BY @mewtaylor TO ADD HANDLING FOR DISABLED STATES.*/

import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import Alert from '@mui/material/Alert';
import styles from './alert.module.css';

const AlertComponent = ({
    className,
    children,
    ...props
}) => {
    

    return (
        <div className={styles.yellowAlertWrp}>
        <Alert  {...props} className={className}>{children}</Alert>
        </div>
        // <span
        //     className={classNames(
        //         styles.alert,
        //         className,
        //         {
        //             [styles.modDisabled]: disabled,
        //             [styles.highlighted]: highlighted
        //         }
        //     )}
        //     {...props}
        // >
        //     {children}
        // </span>
    );
};

AlertComponent.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};
export default AlertComponent;
