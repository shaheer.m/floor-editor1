import paper from "@scratch/paper";
import classNames from "classnames";
import { defineMessages, injectIntl, intlShape } from "react-intl";
import React, { useEffect, useLayoutEffect } from "react";
import PropTypes from "prop-types";

import PaperCanvas from "../../containers/paper-canvas";
import ScrollableCanvas from "../../containers/scrollable-canvas";

import BitBrushMode from "../../containers/bit-brush-mode";
import BitLineMode from "../../containers/bit-line-mode";
import BitOvalMode from "../../containers/bit-oval-mode";
import BitRectMode from "../../containers/bit-rect-mode";
import BitFillMode from "../../containers/bit-fill-mode";
import BitEraserMode from "../../containers/bit-eraser-mode";
import BitSelectMode from "../../containers/bit-select-mode";
import Box from "../box/box";
import Button from "../button/button";
import ButtonGroup from "../button-group/button-group";
import BrushMode from "../../containers/brush-mode";
import EraserMode from "../../containers/eraser-mode";
import FillColorIndicatorComponent from "../../containers/fill-color-indicator";
import FillMode from "../../containers/fill-mode";
import InputGroup from "../input-group/input-group";
import LineMode from "../../containers/line-mode";
import Loupe from "../loupe/loupe";
import FixedToolsContainer from "../../containers/fixed-tools";
import ModeToolsContainer from "../../containers/mode-tools";
import OvalMode from "../../containers/oval-mode";
import RectMode from "../../containers/rect-mode";
import ReshapeMode from "../../containers/reshape-mode";
import SelectMode from "../../containers/select-mode";
import CustomShapeChair from "../../containers/custom-shape-chair";
import StrokeColorIndicatorComponent from "../../containers/stroke-color-indicator";
import StrokeWidthIndicatorComponent from "../../containers/stroke-width-indicator";
import ItemNameIndicatorComponent from "../../containers/item-name-indicator";
import GroupNameIndicatorComponent from "../../containers/group-name-indicator";
import TextMode from "../../containers/text-mode";

import Formats, { isBitmap, isVector } from "../../lib/format";
import styles from "./paint-editor.module.css";

import bitmapIcon from "./icons/bitmap.svg";
import zoomInIcon from "./icons/zoom-in.svg";
import zoomOutIcon from "./icons/zoom-out.svg";
import zoomResetIcon from "./icons/zoom-reset.svg";
import backButton from "./icons/back-button.svg";
import SvgInfo from "../fixed-tools/svg-info";
import editButton from "./icons/edit_icon.svg"
import MediaQuery from 'react-responsive';
import Label from '../forms/label';
import layout from '../../lib/layout-constants';
import BufferedInputHOC from '../forms/buffered-input-hoc';
import Input from '../forms/input';
import {useState, Fragment} from 'react';
import AlertComponent from "../alert/alert";
import AlertTopError from "../../containers/alert-top-error";
const BufferedInput = BufferedInputHOC(Input);

const messages = defineMessages({
  costume: {
    id: 'paint.paintEditor.costume',
    description: 'Label for the name of a costume',
    defaultMessage: 'Name'
},
  bitmap: {
    defaultMessage: "Convert to Bitmap",
    description:
      "Label for button that converts the paint editor to bitmap mode",
    id: "paint.paintEditor.bitmap",
  },
  vector: {
    defaultMessage: "Convert to Vector",
    description:
      "Label for button that converts the paint editor to vector mode",
    id: "paint.paintEditor.vector",
  },
});

const PaintEditorComponent = (props) => {
  useEffect(() => {
    if (isBitmap(props.format)) {
      setTimeout(() => {
        props.onSwitchToVector();
      }, 100);
    }
  }, [props.format]);
  const [editName, setEditName] = useState(false);
  const onSubmit = (name) => {
      props.onUpdateName(name)
      setEditName(false)
  }
  return (
    <div className={styles.editorContainer} dir={props.rtl ? "rtl" : "ltr"}>
     {/* <div className="yellowAlertWrp">
       <Alert severity="error" className="yellowAlert">This is an error alert — check it out!</Alert>
     </div> */}
     {props.alertOnTop.status && <AlertTopError /> }

      {props.canvas !== null ? ( // eslint-disable-line no-negated-condition
        <div className={styles.editorContainerTop}>
          {/* First row */}
          <button onClick={props.onBackButton} className={styles.paintEditorBackButton}>
          <img src={backButton}  alt="Back button" />
          </button>
          <div className={styles.editorTopTools} >
         
          <div className={styles.row}>
            <FixedToolsContainer
              canRedo={props.canRedo}
              canUndo={props.canUndo}
              name={props.name}
              onRedo={props.onRedo}
              onUndo={props.onUndo}
              onUpdateImage={props.onUpdateImage}
              onUpdateName={props.onUpdateName}
            />
          </div>
          {/* Second Row */}
          {isVector(props.format) ? (
            <div className={styles.row}>
                <InputGroup className={styles.modModeTools}>
                <ModeToolsContainer onUpdateImage={props.onUpdateImage} />
              </InputGroup>
              <InputGroup
                className={classNames(
                  styles.row,
                  styles.modDashedBorder,
                  styles.modLabeledIconHeight
                )}
              >
                {/* fill */}
                {/* <FillColorIndicatorComponent
                  className={styles.modMarginAfter}
                  onUpdateImage={props.onUpdateImage}
                /> */}
                {/* stroke */}
                {/* <StrokeColorIndicatorComponent
                  onUpdateImage={props.onUpdateImage}
                /> */}
                {/* stroke width */}
                <div className={styles.verticalLineRight}></div>

                <StrokeWidthIndicatorComponent
                  onUpdateImage={props.onUpdateImage}
                />
                <div className={styles.verticalLine}></div>
                <ItemNameIndicatorComponent
                  onUpdateImage={props.onUpdateImage}
                  sendAlertMessage={props.sendAlertMessage}
                />
                {/* <GroupNameIndicatorComponent
                  onUpdateImage={props.onUpdateImage}
                /> */}
              </InputGroup>
            {/* <button className={styles.paintEditorGradBtn} >Save</button> */}
            </div>) : isBitmap(props.format) ? (
            <div className={styles.row}>
              <InputGroup
                className={classNames(
                  styles.row,
                  styles.modDashedBorder,
                  styles.modLabeledIconHeight
                )}
              >
                {/* fill */}
                <FillColorIndicatorComponent
                  className={styles.modMarginAfter}
                  onUpdateImage={props.onUpdateImage}
                />
              </InputGroup>
              <InputGroup className={styles.modModeTools}>
                <ModeToolsContainer onUpdateImage={props.onUpdateImage} />
              </InputGroup>
            </div>
          ) : null}
            </div>
            <button onClick={props.onSave} className={styles.paintEditorGradBtn} >Save</button>
        </div>
      ) : null}

      <div className={styles.topAlignRow}>
        {/* Modes */}
        {props.canvas !== null && isVector(props.format) ? ( // eslint-disable-line no-negated-condition
          <div className={styles.modeSelector}>
            <SelectMode onUpdateImage={props.onUpdateImage} />
            {/* <ReshapeMode onUpdateImage={props.onUpdateImage} /> */}
          {/* <BrushMode onUpdateImage={props.onUpdateImage} /> */}
            {/* <EraserMode onUpdateImage={props.onUpdateImage} /> */}
            {/* <FillMode onUpdateImage={props.onUpdateImage} /> */}
            <CustomShapeChair addShape={props.addShape} onUpdateImage={props.onUpdateImage} />
           
            <TextMode
              textArea={props.textArea}
              onUpdateImage={props.onUpdateImage}
            />
            {/* <LineMode onUpdateImage={props.onUpdateImage} />
            <OvalMode onUpdateImage={props.onUpdateImage} />
            <RectMode onUpdateImage={props.onUpdateImage} /> */}
          </div>
        ) : null}

        {props.canvas !== null && isBitmap(props.format) ? ( // eslint-disable-line no-negated-condition
          <div className={styles.modeSelector}>
            <BitBrushMode onUpdateImage={props.onUpdateImage} />
            <BitLineMode onUpdateImage={props.onUpdateImage} />
            <BitOvalMode onUpdateImage={props.onUpdateImage} />
            <BitRectMode onUpdateImage={props.onUpdateImage} />
            <TextMode
              isBitmap
              textArea={props.textArea}
              onUpdateImage={props.onUpdateImage}
            />
            <BitFillMode onUpdateImage={props.onUpdateImage} />
            <BitEraserMode onUpdateImage={props.onUpdateImage} />
            <BitSelectMode onUpdateImage={props.onUpdateImage} />
          </div>
        ) : null}

        <div className={styles.controlsContainer}>
       
             <div>
                <span className="colonTitle">Name:</span>
                
                {editName &&  <Fragment>
                 {/* <InputGroup> */}
                {/* <MediaQuery minWidth={layout.fullSizeEditorMinWidth}> */}
                    {/* <Label text={props.intl.formatMessage(messages.costume)}> */}
                        <BufferedInput
                            maxlength={80}
                            className={styles.costumeInput}
                            type="text"
                            value={props.name}
                            onSubmit={onSubmit}
                        />
                    {/* </Label> */}
                {/* </MediaQuery> */}
                {/* <MediaQuery maxWidth={layout.fullSizeEditorMinWidth - 1}> */}
                    {/* <BufferedInput
                        className={styles.costumeInput}
                        type="text"
                        value={props.name}
                        onSubmit={onSubmit}
                    /> */}
                {/* </MediaQuery> */}
                 {/* </InputGroup>} */}
                 </Fragment>
              }

                {!editName &&  <Fragment> <span className="colonPara">{props.name}</span>
                  <button className= {classNames(
                    styles.imageButton,
                    styles.ml10
                  )} onClick={() => { setEditName(true) }}><img src={editButton} className={styles.editIcon}  alt="Edit button" /></button>
                </Fragment>
                }
              </div>
            {props.canvas !== null ? (<div><SvgInfo /></div>) : null}
          {/* Canvas */}
          <ScrollableCanvas
            canvas={props.canvas}
            hideScrollbars={props.isEyeDropping}
            style={styles.canvasContainer}
          >
            <PaperCanvas
              canvasRef={props.setCanvas}
              image={props.image}
              imageFormat={props.imageFormat}
              imageId={props.imageId}
              rotationCenterX={props.rotationCenterX}
              rotationCenterY={props.rotationCenterY}
              zoomLevelId={props.zoomLevelId}
              onUpdateImage={props.onUpdateImage}
            />
            <textarea
              className={styles.textArea}
              ref={props.setTextArea}
              spellCheck={false}
            />
            {props.isEyeDropping &&
              props.colorInfo !== null &&
              !props.colorInfo.hideLoupe ? (
              <Box className={styles.colorPickerWrapper}>
                <Loupe
                  colorInfo={props.colorInfo}
                  pixelRatio={paper.project.view.pixelRatio}
                />
              </Box>
            ) : null}
          </ScrollableCanvas>
          <div className={styles.canvasControls}>
            {/* {isVector(props.format) ? (
              <Button
                className={styles.bitmapButton}
                onClick={props.onSwitchToBitmap}
              >
                <img
                  className={styles.bitmapButtonIcon}
                  draggable={false}
                  src={bitmapIcon}
                />
                <span className={styles.buttonText}>
                  {props.intl.formatMessage(messages.bitmap)}
                </span>
              </Button>
            ) : isBitmap(props.format) ? (
              <Button
                className={styles.bitmapButton}
                onClick={props.onSwitchToVector}
              >
                <img
                  className={styles.bitmapButtonIcon}
                  draggable={false}
                  src={bitmapIcon}
                />
                <span className={styles.buttonText}>
                  {props.intl.formatMessage(messages.vector)}
                </span>
              </Button>
            ) : null} */}
            {/* Zoom controls */}
            <InputGroup className={styles.zoomControls}>
              <ButtonGroup>
                <Button
                  className={styles.buttonGroupButton}
                  onClick={props.onZoomOut}
                >
                  <img
                    alt="Zoom Out"
                    className={styles.buttonGroupButtonIcon}
                    draggable={false}
                    src={zoomOutIcon}
                  />
                </Button>
                <Button
                  className={styles.buttonGroupButton}
                  onClick={props.onZoomReset}
                >
                  <img
                    alt="Zoom Reset"
                    className={styles.buttonGroupButtonIcon}
                    draggable={false}
                    src={zoomResetIcon}
                  />
                </Button>
                <Button
                  className={styles.buttonGroupButton}
                  onClick={props.onZoomIn}
                >
                  <img
                    alt="Zoom In"
                    className={styles.buttonGroupButtonIcon}
                    draggable={false}
                    src={zoomInIcon}
                  />
                </Button>
              </ButtonGroup>
            </InputGroup>
          </div>
        </div>
      </div>
    </div>
  );
};

PaintEditorComponent.propTypes = {
  canRedo: PropTypes.func.isRequired,
  canUndo: PropTypes.func.isRequired,
  canvas: PropTypes.instanceOf(Element),
  colorInfo: Loupe.propTypes.colorInfo,
  format: PropTypes.oneOf(Object.keys(Formats)),
  image: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(HTMLImageElement),
  ]),
  imageFormat: PropTypes.string,
  imageId: PropTypes.string,
  intl: intlShape,
  isEyeDropping: PropTypes.bool,
  name: PropTypes.string,
  onRedo: PropTypes.func.isRequired,
  onSwitchToBitmap: PropTypes.func.isRequired,
  onSwitchToVector: PropTypes.func.isRequired,
  onUndo: PropTypes.func.isRequired,
  onUpdateImage: PropTypes.func.isRequired,
  onUpdateName: PropTypes.func.isRequired,
  onZoomIn: PropTypes.func.isRequired,
  onZoomOut: PropTypes.func.isRequired,
  onZoomReset: PropTypes.func.isRequired,
  rotationCenterX: PropTypes.number,
  rotationCenterY: PropTypes.number,
  rtl: PropTypes.bool,
  setCanvas: PropTypes.func.isRequired,
  setTextArea: PropTypes.func.isRequired,
  textArea: PropTypes.instanceOf(Element),
  zoomLevelId: PropTypes.string,
};

export default injectIntl(PaintEditorComponent);
