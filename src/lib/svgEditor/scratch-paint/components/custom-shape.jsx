import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from './button/button';
import styles from './tool-select-base/tool-select-base.module.css';
import Popover from 'react-popover';
import bindAll from 'lodash.bindall';
import classNames from 'classnames';
class CustomShapeComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: false,
        };
        bindAll(this, [
            'handleAttributeModal',
        ]);

    }
    handleAttributeModal() {
        this.setState(
            (prevState) => ({
                modalStatus: !prevState.modalStatus
            })
        )
    }

render() {

    return (
        <Popover
            body={
                <Fragment>
                       {this.props.imgSrc.map((el, i) =>
                        <img title={this.props.title[i].defaultMessage} onClick={() => this.props.addShape(i)} src={el} />)}
                </Fragment>
            }
            isOpen={this.state.modalStatus}
            preferPlace="right"
            onOuterAction={this.handleAttributeModal}
            className="sidebarPopover"
        >
         <Button title={this.props.imgDescriptor.defaultMessage} className={
            classNames(styles.modToolDropdown, styles.modToolSelect,)} onClick={this.handleAttributeModal} >
            <img
                // alt={''}
                className={styles.toolSelectIcon}
                draggable={true}
                src={this.props.imgSrc[1]}
            />
        </Button>
        </Popover>
        
);
        }
    }
// CustomShapeComponent.propTypes = {
//     className: PropTypes.string,
//     disabled: PropTypes.bool.isRequired,
//     color: PropTypes.string,
//     color2: PropTypes.string,
//     colorModalVisible: PropTypes.bool.isRequired,
//     onChangeGradientType: PropTypes.func.isRequired,
//     onOpenColor: PropTypes.func.isRequired,
//     onSwap: PropTypes.func.isRequired,
// };

    // <Button className={styles.modToolSelect} onClick={() => this.props.addShape(this.props.customShape)}>
    //     <img
    //         // alt={''}
    //         className={styles.toolSelectIcon}
    //         draggable={true}
    //         src={this.props.imgSrc}
    //     />
    // </Button>




export default CustomShapeComponent;