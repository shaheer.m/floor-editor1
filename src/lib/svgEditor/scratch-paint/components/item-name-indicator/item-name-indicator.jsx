import React from 'react';
import PropTypes from 'prop-types';

import Input from '../forms/input';
import InputGroup from '../input-group/input-group';
import LiveInputHOC from '../forms/live-input-string-hoc';
import {MAX_STROKE_WIDTH} from '../../reducers/stroke-width';

const LiveInput = LiveInputHOC(Input);
const ItemNameIndicatorComponent = props => (
    <InputGroup disabled={props.disabled}>
        <label>Element Name</label>
        <LiveInput
            disabled={props.disabled}
            value={props.itemName}
            onSubmit={props.onChangeItemName}
            setBtnError={props.setBtnError}
        />
    </InputGroup>
);

ItemNameIndicatorComponent.propTypes = {
    disabled: PropTypes.bool.isRequired,
    onChangeStrokeWidth: PropTypes.func.isRequired,
    strokeWidth: PropTypes.number
};

export default ItemNameIndicatorComponent;
