import React from 'react';
import PropTypes from 'prop-types';

import Input from './forms/input';
import InputGroup from './input-group/input-group';
import LiveInputHOC from './forms/live-input-hoc';

import {MAX_STROKE_WIDTH} from '../reducers/stroke-width';
import LabeledIconButton from './labeled-icon-button/labeled-icon-button';
import styles from  "./paint-editor/paint-editor.module.css";

const LiveInput = LiveInputHOC(Input);
const StrokeWidthIndicatorComponent = props => (
    <div>
    <InputGroup disabled={props.disabled}>
        <LiveInput
            range
            small
            disabled={props.disabled}
            max={MAX_STROKE_WIDTH}
            min="0"
            type="number"
            value={props.strokeWidth ? props.strokeWidth : 0}
            onSubmit={props.onChangeStrokeWidth}
        />

    </InputGroup>
    <span className={props.disabled ? styles.bottomTextDisabled : styles.bottomText}>Outline</span>

    </div>
);

StrokeWidthIndicatorComponent.propTypes = {
    disabled: PropTypes.bool.isRequired,
    onChangeStrokeWidth: PropTypes.func.isRequired,
    strokeWidth: PropTypes.number
};

export default StrokeWidthIndicatorComponent;
