import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-popover';

import ColorButton from './color-button/color-button';
import ColorPicker from '../containers/color-picker';
import InputGroup from './input-group/input-group';
import Label from './forms/label';

import GradientTypes from '../lib/gradient-types';
import { Fragment } from 'react';

import colorSvg from '../../../assets/chair-icon-2.svg';

const colorOptions = [
    { color: '#44339a', icon: colorSvg },
    { color: '#9a144e', icon: colorSvg },
    { color: '#B0BF1A', icon: colorSvg },
    { color: '#CC5500', icon: colorSvg }
]
const ColorIndicatorComponent = props => (
    <InputGroup
        className={props.className}
        disabled={props.disabled}
    >
        <Popover
            body={
                <Fragment>
                    {colorOptions.map(el =>
                        <button onClick={() => props.onChangeColor(el.color)}><img src={el.icon} /></button>)}
                </Fragment>
                // <ColorPicker
                //     color={props.color}
                //     color2={props.color2}
                //     gradientType={props.gradientType}
                //     shouldShowGradientTools={props.shouldShowGradientTools}
                //     onChangeColor={props.onChangeColor}
                //     onChangeGradientType={props.onChangeGradientType}
                //     onSwap={props.onSwap}
                // />
            }
            isOpen={props.colorModalVisible}
            preferPlace="below"
            onOuterAction={props.onCloseColor}
        >
            <Label text={props.label}>
                <ColorButton
                    color={props.color}
                    color2={props.color2}
                    gradientType={props.gradientType}
                    onClick={props.onOpenColor}
                    outline={props.outline}
                />
            </Label>
        </Popover>
    </InputGroup>
);

ColorIndicatorComponent.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool.isRequired,
    color: PropTypes.string,
    color2: PropTypes.string,
    colorModalVisible: PropTypes.bool.isRequired,
    gradientType: PropTypes.oneOf(Object.keys(GradientTypes)).isRequired,
    label: PropTypes.string.isRequired,
    onChangeColor: PropTypes.func.isRequired,
    onChangeGradientType: PropTypes.func.isRequired,
    onCloseColor: PropTypes.func.isRequired,
    onOpenColor: PropTypes.func.isRequired,
    onSwap: PropTypes.func.isRequired,
    outline: PropTypes.bool.isRequired,
    shouldShowGradientTools: PropTypes.bool.isRequired
};

export default ColorIndicatorComponent;
