import React, { useCallback, Component } from "react";
import bindAll from 'lodash.bindall';
import styles from './input.module.css';
import classNames from 'classnames';
import messages from '../../lib/messages.js';

export default class SetCapacity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            capacity: null,
            capacityError: false
        }
        bindAll(this, [
            'handleChangeChk',
        ]);
    }

    handleChangeChk(e) {
        let value = e.target.value.replace(/^0+/, '');
        if (!value.includes('.')) {
            if (value >= 0) {
                this.setState({ capacity: value })
                this.props.onCapacityChange(value)
            }
            if (value < 1) {
                this.props.setBtnError(true)
                this.setState({capacityError: true});
            }else {
                this.props.setBtnError(false);
                this.setState({capacityError: false});
            }
        }
    }

    render() {
        const liveValue = this.state.capacity === null ? this.props.capacity : this.state.capacity;
        const disabled = this.props.itemName ? false : true
        return (
            <div className={styles.spaceBottom}>
                <label>Capacity</label>
                <input disabled={disabled} type="number" min="1" className={classNames(styles.inputForm, this.state.capacityError && styles.redBorder)} value={liveValue} onChange={this.handleChangeChk} />
                {this.state.capacityError ? <span className={styles.error} >{messages.errorCapacity.description}</span> : null}
            </div>
        );
    }
}
