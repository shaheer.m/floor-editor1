/* eslint-disable import/no-anonymous-default-export */
import bindAll from 'lodash.bindall';
import PropTypes from 'prop-types';
import React from 'react';
import { Fragment } from 'react';
import styles from './input.module.css';
import classNames from 'classnames';
import messages from '../../lib/messages.js';

/**
 * Higher Order Component to manage inputs that submit on change and <enter>
 * @param {React.Component} Input text input that consumes onChange, onBlur, onKeyPress
 * @returns {React.Component} Live input that calls onSubmit on change and <enter>
 */
export default function (Input) {
    class LiveInput extends React.Component {
        constructor (props) {
            super(props);
            bindAll(this, [
                'handleChange',
                'handleKeyPress',
                'handleFlush',
                'containsSpecialChars'
            ]);
            this.state = {
                value: null,
                errorLimit: false,
            //    errorRest: false,
                errorSpecialChar: false
            };
        }
        handleKeyPress (e) {
            if (e.key === 'Enter') {
                this.handleChange(e);
                e.target.blur();
            }
        }
        handleFlush () {
            this.setState({value: null});
        }

        containsSpecialChars(str) {
            const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
            return specialChars.test(str);
        }

        handleChange (e) {
            var errorLimit = false
        //    var errorRest = false
            var errorSpecialChar = false
            // if(e.target.value.toLowerCase().includes('unavailable') || 
            // (e.target.value.toLowerCase().includes('clickable'))) {
            //     this.setState({errorRest: true});
            //     errorRest = true;
            //     // this.setState({errorLimit: false});
            //     // this.setState({errorSpecialChar: false});
            //     this.setState({value: e.target.value});
            //     this.props.setBtnError(true);
            // }else {
            //     this.setState({errorRest: false});
            // }
            if(this.containsSpecialChars(e.target.value)) {
                this.setState({errorSpecialChar: true});
                errorSpecialChar = true;
                // this.setState({errorLimit: false});
                // this.setState({errorRest: false});
                this.setState({value: e.target.value});
                this.props.setBtnError(true);
            }else {
                this.setState({errorSpecialChar: false});
            }
            if(e.target.value.length > 15) {
                this.setState({errorLimit: true});
                errorLimit = true;
                // this.setState({errorRest: false});
                // this.setState({errorSpecialChar: false});
                this.setState({value: e.target.value});
                this.props.setBtnError(true);
            }else {
                this.setState({errorLimit: false});
            }
            if (errorLimit || errorSpecialChar) {
                return null
            }else{
                this.props.setBtnError(false);
            }
            let val = e.target.value;
            this.props.onSubmit(val);
            this.setState({value: e.target.value});
        }
        render () {
            const liveValue = this.state.value === null ? this.props.value : this.state.value;
            return (
                <Fragment>
                <Input
                    {...this.props}
                    value={liveValue}
                    onBlur={this.handleFlush}
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress}
                    className={classNames(styles.spaceBottom,
                        this.state.errorLimit && styles.redBorder ||
                        this.state.errorSpecialChar && styles.redBorder
                        )}
                />
                {this.state.errorLimit ? <span className={styles.error} >{messages.errorMaxChar.description}</span> : null}
                {/* {this.state.errorRest ?  <span className={styles.error} >{messages.errorRestChar.description}</span> : null} */}
                {this.state.errorSpecialChar ? <span className={styles.error} >{messages.errorSpecialChar.description}</span> : null}
                </Fragment>
            );
        }
    }

    LiveInput.propTypes = {
        max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        onSubmit: PropTypes.func.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    };

    return LiveInput;
}
