import React, { useCallback, Component } from "react";
import bindAll from 'lodash.bindall';
import styles from './set-available.module.css'
import classNames from "classnames";

export default class SetAvailable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null
        }
        bindAll(this, [
            'handleChangeChk',
        ]);
    }

    handleChangeChk(e) {
        this.setState(
            (prevState) => ({
                selected: !prevState.selected
            })
        )
        this.props.onAvailableChange(e.target.checked)
    }

    render() {
        const liveValue = this.state.selected === null ? this.props.available : this.state.selected;
        const disabled = this.props.itemName ? false : true
        return (
            <label   className={classNames(styles.spaceBottom,styles.checkboxContainer)}>
                <input className={styles.checkbox} type="checkbox" disabled={disabled} defaultChecked={liveValue} onChange={this.handleChangeChk} />
                <span>Mark as Unavailable</span>    
                
                {/* <span className={styles.name}></span> */}
                <span className={styles.checkmark} ></span>
            </label>
        );
    }
}
