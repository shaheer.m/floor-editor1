import React, { useCallback, Component } from "react";
import CreatableSelect from "react-select/creatable";
import { groupNameStackOfAllItems } from '../../helper/selection';

export default class GroupSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
            options: []
        };
    }
    componentDidMount() {
        const values = groupNameStackOfAllItems();
        const uniqueValues = [];
        const uniqueObjects = [];
        const unique = values.filter(element => {
          const isDuplicate = uniqueValues.includes(element.value);
        
          if (!isDuplicate) {
            uniqueValues.push(element.value);
            uniqueObjects.push(element)        
            return true;
          }
        
          return false;
        });
        this.setState({ options: uniqueObjects });
    }
    handleChange =
        // useCallback(
        (inputValue) => {
            this.setState({ value: inputValue });
            this.props.onGroupNameChange(inputValue);
        }
    // , []);

    handleCreate =
        //    useCallback(
        (inputValue) => {
            const newValue = { value: inputValue.toLowerCase(), label: inputValue };
            this.setState(
                (prevState) => ({
                    options: [...prevState.options, newValue]
                })
            )
            this.setState({ value: newValue })
            this.props.onGroupNameChange(newValue);
        }
    //     ,[this.state.options]
    // );

    render() {
      
        const liveValue = this.state.value === null ? this.props.groupName : this.state.value;
        const disabled = this.props.itemName ? false : true
        return (
            <>
            <label htmlFor="">Select ODC</label>
            <CreatableSelect
                isClearable
                value={liveValue}
                options={this.state.options}
                onChange={this.handleChange}
                onCreateOption={this.handleCreate}
                isDisabled={disabled}
                className='popover-select-container'
                classNamePrefix="popover-select"
                placeholder={'Select'}

            />
            </>    
        );
    }
}
