
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { default as ReactSelect } from "react-select";
import { components } from "react-select";
import styles from './input.module.css';

const options = [
    { value: "clickable", label: "Set as seat" },
    { value: 'monitortype', label: 'MonitorType' },
];
const Option = (props) => {
    return (
        <div>
            <components.Option {...props}>
               <div className={styles.checkSelect}>
                <input
                    type="checkbox"
                    checked={props.isSelected}
                    onChange={() => null}
                />{" "}
                <label>{props.label}</label>
                </div>
            </components.Option>
        </div>
    );
};

export default class MultiSelectDropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            optionSelected: null
        };
    }

    handleChange = (selected) => {
        this.setState({
            optionSelected: selected
        });
        this.props.handleAttributeChange(selected);
    };

    render() {
        const liveValue = this.state.optionSelected === null ? this.props.attributeValues : this.state.optionSelected;
        const disabled = this.props.itemName ? false : true
        return (
            <>
            <label>Attributes (optional)</label>
            
            <ReactSelect
                isDisabled={disabled}
                options={options}
                isMulti
                closeMenuOnSelect={false}
                hideSelectedOptions={false}
                components={{
                    Option
                }}
                onChange={this.handleChange}
                allowSelectAll={true}
                value={liveValue}
                className='popover-select-container'
                classNamePrefix="popover-select"
                placeholder={'Select'}
                menuPlacement="auto"
                isClearable = {false}
            />
            </>
        );
    }
}
