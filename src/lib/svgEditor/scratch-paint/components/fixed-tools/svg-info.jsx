import React, { Fragment, useEffect, useState } from 'react';
import classNames from "classnames";
import { connect } from 'react-redux';
import { getAllSelectableRootItems, groupNameStackOfAllItems } from '../../helper/selection'
import styles from './fixed-tools.module.css';

const SvgInfo = () => {

    const [seatNumber, setSeatNumber] = useState();
    const [odcNumber, setOdcNumber] = useState();

    // useEffect(() => {
    //     let seatCount = fetchSvgInfo()
    //     setSeatNumber(seatCount) 
    // }, [])

    useEffect(() => {
       let seatInfo = fetchSvgSeatInfo();
       setSeatNumber(seatInfo[0]);
       setOdcNumber(seatInfo[1].length)
    }, [getAllSelectableRootItems()])

    // in the case where the item is grouped and does not satisfy the criteria to become a seat
    //ie an id and 'clickable' attribute, then check inside as well.
    const fetchSvgSeatInfo = (items=getAllSelectableRootItems(), seatNumber = 0, groupNames = []) => {
        for (const item of items) {
            if (item._data && item._data.id && item._data.customAttributes) {
                const attr = item._data.customAttributes;
                const unavailable = item._data.unavailable;
                let capacity = parseInt(item._data.capacity);
                let capacityValue = capacity ? capacity : 1;
                if(attr.some(obj => obj.value === "clickable" && !unavailable)){
                    seatNumber = seatNumber + capacityValue;
                    if (item._data.groupName && !groupNames.includes(item._data.groupName.value)) {
                        groupNames.push(item._data.groupName.value)
                    }
                }else { //has an id but no clickable attribute
                    if(item.className && item.className === 'Group') {
                        [seatNumber, groupNames] = fetchSvgSeatInfo(item._children, seatNumber, groupNames);
                    }
                }
            } // has no id
            else if (!item._data || (item._data && !item._data.id)) {
                if(item.className && item.className === 'Group') {
                    [seatNumber, groupNames] = fetchSvgSeatInfo(item._children, seatNumber, groupNames);
                }
            }
        }        
        return [seatNumber, groupNames]
    }
    
    // const fetchSvgOdcCount = () => {
    //     const values = groupNameStackOfAllItems();
    //     const uniqueValues = [];
    //     const uniqueObjects = [];
    //     const unique = values.filter(element => {
    //       const isDuplicate = uniqueValues.includes(element.value);
        
    //       if (!isDuplicate) {
    //         uniqueValues.push(element.value);
    //         uniqueObjects.push(element)        
    //         return true;
    //       }
        
    //       return false;
    //     });
    //     return uniqueObjects.length
    // }

        return (
            <div className={styles.mt10}>
                {/* <span className="colonTitle">Zones: </span><span className='colonPara'>{odcNumber}</span> 
                <span className= {`colonTitle ${classNames(
                  styles.ml56
                )}`}>Seats: </span> <span className='colonPara'>{seatNumber}</span> */}
                <span className="colonTitle">Seats: </span><span className='colonPara'>{seatNumber}</span>
            </div>
        )
}

export default SvgInfo