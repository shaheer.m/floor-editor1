import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import bindAll from 'lodash.bindall';
import parseColor from 'parse-color';
import { changeStrokeColor, changeStrokeColor2, changeStrokeGradientType } from '../reducers/stroke-style';
import { changeStrokeWidth } from '../reducers/stroke-width';
import ItemNameIndicatorComponent from '../components/item-name-indicator/item-name-indicator';
import { groupNameStackOfAllItems, findGreaterParent, findGreaterParentName, findGreaterParentAttribute, findGreaterParentAvailable, getSelectedRootItems } from '../helper/selection';
import { applyNameAttributesToSelection, applyGroupNameToSelection } from '../helper/style-path';
import GradientTypes from '../lib/gradient-types';
import Modes from '../lib/modes';
import Formats, { isBitmap } from '../lib/format';
import Popover from 'react-popover';
import { selectedSingleItem } from '../helper/group';
import LabeledIconButton from '../components/labeled-icon-button/labeled-icon-button'
import assignBtnDropdown from '../components/fixed-tools/icons/assign-dropdown.svg';
import MultiSelect from '../components/forms/multiselect-dropdown'
import GroupSelect from '../components/forms/group-name-select'
import SetAvailable from '../components/forms/set-available'
import SetCapacity from '../components/forms/max-capacity'
import classNames from 'classnames';
import styles from '../components/forms/input.module.css';
import { setAlertText } from '../reducers/alert-top-error'

class ItemNameIndicator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: false,
            appliedName: null,
            attributes: [],
            groupName: null,
            available: null,
            capacityVal: null,
            btnDisabled: false,
            showCapacity: false
        };
        bindAll(this, [
            'handleNameChange',
            'handleAttributeModal',
            'handleSubmit',
            'handleAttributeChange',
            'groupNameChange',
            'handleAvailableChange',
            'handleCapacityChange',
            'handleBtn'
        ]);
    }

    componentWillReceiveProps(nextProps) {
        this.handleNameChange(nextProps.selectedName);
        this.handleAttributeChange(nextProps.attributeValues);
        this.groupNameChange(nextProps.groupName);
        this.handleAvailableChange(nextProps.available)
        this.handleCapacityChange(nextProps.capacityMax)
    }

    handleNameChange(name) {
        this.setState({ appliedName: name })
    }
    handleAttributeChange(value) {
        this.setState({ attributes: value })
        if (value && (value.find(e => e.value === 'clickable'))) {
            this.setState({ showCapacity: true });
        } else {
            this.setState({ showCapacity: false });
        }
    }
    groupNameChange(value) {
        this.setState({ groupName: value })
    }
    handleAvailableChange(value) {
        this.setState({ available: value })
    }
    handleCapacityChange(value) {
        this.setState({ capacityVal: value })
    }
    handleBtn(value) {
        this.setState({ btnDisabled: value })
    }
    handleAttributeModal() {
        this.setState(
            (prevState) => ({
                modalStatus: !prevState.modalStatus
            })
        )
    }

    handleSubmit() {
        const name = this.state.appliedName ? this.state.appliedName : null;
        let attributes, groupName, available, capacity;
        attributes = [];
        groupName = available = capacity = null;

        if (name) {
            attributes = this.state.attributes ? this.state.attributes : [];
            groupName = this.state.groupName ? this.state.groupName : null;
            available = this.state.available ? this.state.available : null;
            if (attributes && (attributes.find(e => e.value === 'clickable'))) {
                capacity = this.state.capacityVal ? this.state.capacityVal : null;
            } else {
                capacity = null;
            }
        }

        let changed = applyNameAttributesToSelection(name, attributes, available, capacity, groupName, this.props.textEditTarget);
        if (changed[0]) this.props.onUpdateImage();
        if (changed[1]) {
            this.props.showAlert(changed[1])
            this.props.sendAlertMessage(changed[1])
            this.handleNameChange(this.props.selectedName ? this.props.selectedName : '');
        }else {
            this.handleAttributeModal()
        }
    }
    render() {

        return (
            <Popover
                body={
                    <Fragment>
                        <h4 className={styles.spaceBottom} >Assign Seat</h4>
                        <ItemNameIndicatorComponent
                            disabled={this.props.disabled}
                            itemName={this.state.appliedName}
                            onChangeItemName={this.handleNameChange}
                            setBtnError={this.handleBtn}
                        />
                        <MultiSelect
                            itemName={this.state.appliedName}
                            attributeValues={this.props.attributeValues}
                            handleAttributeChange={this.handleAttributeChange}></MultiSelect>
                        {/* <GroupSelect
                            groupName={this.props.groupName}
                            onGroupNameChange={this.groupNameChange}
                            itemName={this.state.appliedName}
                        /> */}
                        {this.state.showCapacity && <SetCapacity
                            itemName={this.state.appliedName}
                            setBtnError={this.handleBtn}
                            capacity={this.props.capacityMax}
                            onCapacityChange={this.handleCapacityChange} />}
                        <SetAvailable
                            available={this.props.available}
                            itemName={this.state.appliedName}
                            onAvailableChange={this.handleAvailableChange} />

                        <div className={styles.twoBtnWrp}>
                            <button className={classNames(
                                styles.btn,
                                styles.gradBtn,
                            )}
                                disabled={this.state.btnDisabled || !this.state.appliedName}
                                onClick={this.handleSubmit}>Assign</button>
                            <button
                                onClick={this.handleAttributeModal}
                                className={classNames(
                                    styles.btn,
                                    styles.whiteBtn
                                )}
                            >Cancel</button>
                        </div>

                    </Fragment>}
                isOpen={this.state.modalStatus}
                preferPlace="below"
                onOuterAction={this.handleAttributeModal}
                className="itemNamePopover"
            >

                <div >
                    <button className={classNames(styles.redBorderBtn)} disabled={!selectedSingleItem()} onClick={this.handleAttributeModal}>
                        {this.props.selectedName ? <span>Edit Attributes</span> : <span>Assign</span>}
                        <img src={assignBtnDropdown} alt="arrow" />
                    </button>
                </div>


            </Popover>
        );
    }
}

const mapStateToProps = state => {
    let name;
    let attribute;
    let groupName;
    let availableAttribute;
    let capacity;
    if (state.scratchPaint.selectedItems && state.scratchPaint.selectedItems[0]) {
        const parents = [];
        parents.push(state.scratchPaint.selectedItems[0]);
        const parentStack = findGreaterParent(state.scratchPaint.selectedItems[0], parents)
        const greaterParent = parentStack[parentStack.length - 2];
        const greaterParentName = greaterParent._data.id;
        const greaterParentAttributes = greaterParent._data.customAttributes;
        const greaterParentAvailable = greaterParent._data.unavailable;
        const greaterParentCapacity = greaterParent._data.capacity;
        const greaterParentGroupName = greaterParent._data.groupName;

        attribute = greaterParentAttributes;
        name = greaterParentName ? greaterParentName : null;
        availableAttribute = greaterParentAvailable ? greaterParentAvailable : null;
        capacity = greaterParentCapacity ? greaterParentCapacity : 1;
        groupName = greaterParentGroupName ? greaterParentGroupName : null;
    }

    return {
        disabled: state.scratchPaint.mode === Modes.BRUSH ||
            state.scratchPaint.mode === Modes.TEXT ||
            state.scratchPaint.mode === Modes.FILL,
        textEditTarget: state.scratchPaint.textEditTarget,
        selectedName: name,
        attributeValues: attribute,
        groupName: groupName,
        selectedItems: state.scratchPaint.selectedItems,
        available: availableAttribute,
        capacityMax: capacity
    }
};
const mapDispatchToProps = dispatch => ({
    showAlert: (msg) => {
        dispatch(setAlertText(msg))
    }
});

ItemNameIndicator.propTypes = {
    disabled: PropTypes.bool.isRequired,
    onUpdateImage: PropTypes.func.isRequired,
    strokeWidth: PropTypes.number,
    textEditTarget: PropTypes.number
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemNameIndicator);
