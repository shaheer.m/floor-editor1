import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import bindAll from 'lodash.bindall';
import Modes from '../lib/modes';

import {changeMode} from '../reducers/modes';
import {clearSelectedItems} from '../reducers/selected-items';
import {clearSelection} from '../helper/selection';

import BitEraserModeComponent from '../components/bit-eraser-mode/bit-eraser-mode';
import BitBrushTool from '../helper/bit-tools/brush-tool';
import AlertComponent from '../components/alert/alert';
import { hideAlertText } from '../reducers/alert-top-error'

class AlertTopError extends React.Component {
    constructor (props) {
        super(props);
        bindAll(this, [
            'showAlert',
        ]);
    }
    
    componentDidMount() {
        setTimeout(() => {
            this.props.removeAlert()
        }, 5000)
    }

    showAlert () {
    }
   
    render () {
        return (
            <AlertComponent
                className="yellowAlert" 
                severity="error"
            >{this.props.alertMessage}</AlertComponent>
        );
    }
}



const mapStateToProps = state => ({
    alertMessage: state.scratchPaint.showAlertOnTop.msg
});
const mapDispatchToProps = dispatch => ({
    removeAlert: () => {
        dispatch(hideAlertText())
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AlertTopError);
