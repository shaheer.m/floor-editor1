import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React,{ Fragment } from 'react';
import bindAll from 'lodash.bindall';
import parseColor from 'parse-color';
import { changeStrokeColor, changeStrokeColor2, changeStrokeGradientType } from '../reducers/stroke-style';
import { changeStrokeWidth } from '../reducers/stroke-width';
import ItemNameIndicatorComponent from '../components/item-name-indicator/item-name-indicator';
import { groupNameStackOfAllItems } from '../helper/selection';
import { applyNameAttributesToSelection, applyGroupNameToSelection } from '../helper/style-path';
import GradientTypes from '../lib/gradient-types';
import Modes from '../lib/modes';
import Formats, { isBitmap } from '../lib/format';
import Popover from 'react-popover';
import { selectedAtleastOne } from '../helper/group';
import LabeledIconButton from '../components/labeled-icon-button/labeled-icon-button'
import groupIcon from '../components/fixed-tools/icons/group.svg';
import MultiSelect from '../components/forms/multiselect-dropdown'
import GroupSelect from '../components/forms/group-name-select'



class ItemNameIndicator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalStatus: false,
            groupName: null
        };
        bindAll(this, [
            'handleAttributeModal',
            'handleSubmit',
            'groupNameChange'
        ]);
    }

    groupNameChange(value) {
        this.setState({ groupName: value })
    }
    handleAttributeModal() {
        this.setState(
            (prevState) => ({
                modalStatus: !prevState.modalStatus
            })
        )
    }
    handleSubmit() {
        const groupName = this.state.groupName ? this.state.groupName : null;
        let changedGroupName = applyGroupNameToSelection(groupName, this.props.textEditTarget);
        if (changedGroupName) this.props.onUpdateImage();
    }
    render() {

        return (
            <Popover
                body={
                    <Fragment>
                        <GroupSelect
                            groupName={this.props.groupName}
                            onGroupNameChange={this.groupNameChange}
                        />
                        <button onClick={this.handleSubmit}>Update group name</button>
                    </Fragment>}
                isOpen={this.state.modalStatus}
                preferPlace="below"
                onOuterAction={this.handleAttributeModal}
            >
                <LabeledIconButton
                    disabled={!selectedAtleastOne()}
                    hideLabel={false}
                    imgSrc={groupIcon}
                    title={'change group name'}
                    onClick={this.handleAttributeModal}
                />
            </Popover>
        );
    }
}

const mapStateToProps = state => {
    let groupName;
    let itemGroupNames
    if (state.scratchPaint.selectedItems && state.scratchPaint.selectedItems[0]) {
        // show only if all the selected items are of same groupName
        itemGroupNames = groupNameStackOfAllItems(state.scratchPaint.selectedItems)
        groupName = state.scratchPaint.selectedItems[0]._data.groupName ?
            state.scratchPaint.selectedItems[0]._data.groupName : null;
    }

    return {
        disabled: state.scratchPaint.mode === Modes.BRUSH ||
            state.scratchPaint.mode === Modes.TEXT ||
            state.scratchPaint.mode === Modes.FILL,
        textEditTarget: state.scratchPaint.textEditTarget,
        groupName: groupName
    }
};
const mapDispatchToProps = dispatch => ({
});

ItemNameIndicator.propTypes = {
    disabled: PropTypes.bool.isRequired,
    onUpdateImage: PropTypes.func.isRequired,
    strokeWidth: PropTypes.number,
    textEditTarget: PropTypes.number
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemNameIndicator);
