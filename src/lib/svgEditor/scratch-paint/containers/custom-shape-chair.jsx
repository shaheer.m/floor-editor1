import React from 'react';
import PropTypes from 'prop-types';
import chairIcon1 from '../../../assets/chair-icon-1.svg'
import chairIcon2 from '../../../assets/chair-icon-2.svg'
import chairIcon3 from '../../../assets/chair-icon-3.svg'

// import chairIcon from '../../../assets/chair.svg';
import CustomShape from '../components/custom-shape'
import icon from '../components/fill-mode/fill.svg'
import ToolSelectComponent from '../components/tool-select-base/tool-select-base';
import { connect } from 'react-redux';
import messages from '../lib/messages.js';

const CustomShapeComponent = props => (
    <CustomShape
        imgDescriptor={messages.seat}
        imgSrc={[chairIcon1,chairIcon2,chairIcon3]}
        title={[messages.front, messages.top, messages.side]}
        customShape={'chair'}
        addShape={props.addShape}
    // isSelected={props.isSelected}
    //  onMouseDown={props.onMouseDown}
    />
);
//check comment
// const mapStateToProps = state => ({
//     colorModalVisible: state.scratchPaint.modals.fillColor,
// });

 export default CustomShapeComponent;
//check comment
// export default connect(
//     mapStateToProps
// )(CustomShapeComponent);