import PaintEditor from './containers/paint-editor';
import ScratchPaintReducer from './reducers/scratch-paint-reducer';

export {
    PaintEditor as default,
    ScratchPaintReducer
};
