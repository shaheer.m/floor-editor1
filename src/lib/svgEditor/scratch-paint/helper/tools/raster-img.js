import paper from '@scratch/paper'
import log from '../../log/log';
import Formats from '../../lib/format';
import {performSnapshot} from '../../helper/undo';
import {MAX_WORKSPACE_BOUNDS, CENTER} from '../../helper/view';
import {ensureClockwise, scaleWithStrokes} from '../../helper/math';
import {isGroup, ungroupItems} from '../../helper/group'
import chairIcon1 from '../../../../assets/chair-icon-1.svg';
import chairIcon2 from '../../../../assets/chair-icon-2.svg';
import chairIcon3 from '../../../../assets/chair-icon-3.svg';


export function setRasterImage(paperCanvas, asset) {

    var svg;
    if (asset === 0) {
        svg = chairIcon1;
    }else if(asset === 1) {
        svg = chairIcon2;
    }else {
        svg = chairIcon3;
    }
  //  const paperCanvas = this;
    // Pre-process SVG to prevent parsing errors (discussion from #213)
    // 1. Remove svg: namespace on elements.
    // TODO: remove
    svg = svg.split(/<\s*svg:/).join('<');
    svg = svg.split(/<\/\s*svg:/).join('</');
    // 2. Add root svg namespace if it does not exist.
    const svgAttrs = svg.match(/<svg [^>]*>/);
    if (svgAttrs && svgAttrs[0].indexOf('xmlns=') === -1) {
        svg = svg.replace(
            '<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ');
    }

    // Get the origin which the viewBox is defined relative to. During import, Paper will translate
    // the viewBox to start at (0, 0), and we need to translate it back for some costumes to render
    // correctly.
    const parser = new DOMParser();
    const svgDom = parser.parseFromString(svg, 'text/xml');
    const viewBox = svgDom.documentElement.attributes.viewBox ?
        svgDom.documentElement.attributes.viewBox.value.match(/\S+/g) : null;
    if (viewBox) {
        for (let i = 0; i < viewBox.length; i++) {
            viewBox[i] = parseFloat(viewBox[i]);
        }
    }

    paper.project.importSVG(svg, {
        expandShapes: true,
        onLoad: function (item) {
            if (!item) {
                log.error('SVG import failed:');
                log.info(svg);
                this.props.changeFormat(Formats.VECTOR_SKIP_CONVERT);
                performSnapshot(paperCanvas.props.undoSnapshot, Formats.VECTOR_SKIP_CONVERT);
                return;
            }
            item.remove();
            
            // Without the callback, rasters' load function has not been called yet, and they are
            // positioned incorrectly
            paperCanvas.queuedImport = recalibrateSize(() => {
                let mask;
                if (item.clipped) {
                    for (const child of item.children) {
                        if (child.isClipMask()) {
                            mask = child;
                            break;
                        }
                    }
                    mask.clipMask = false;
                } else {
                    mask = new paper.Shape.Rectangle(item.bounds);
                }
                mask.guide = true;
                mask.locked = true;
                mask.matrix = new paper.Matrix(); // Identity
                // Set the artwork to get clipped at the max costume size
                // mask.size.height = MAX_WORKSPACE_BOUNDS.height;
                // mask.size.width = MAX_WORKSPACE_BOUNDS.width;
                // mask.setPosition(CENTER);
                // paper.project.activeLayer.addChild(mask);
                // mask.clipMask = true;

                // Reduce single item nested in groups
                if (item instanceof paper.Group && item.children.length === 1) {
                    item = item.reduce();

                }

                ensureClockwise(item);
                scaleWithStrokes(item, 2, new paper.Point()); // Import at 2x

                // Apply rotation center
                if (typeof rotationCenterX !== 'undefined' && typeof rotationCenterY !== 'undefined') {
                    let rotationPoint = new paper.Point(0, 0);
                    if (viewBox && viewBox.length >= 2 && !isNaN(viewBox[0]) && !isNaN(viewBox[1])) {
                        rotationPoint = rotationPoint.subtract(viewBox[0], viewBox[1]);
                    }
                    item.translate(CENTER.subtract(rotationPoint.multiply(2)));
                } else {
                    // Center
                    item.translate(CENTER.subtract(100, 100));
                }

                paper.project.activeLayer.insertChild(0, item);
                setTimeout(() => {
                    item.bringToFront();
                    if (isGroup(item)) {
                        // Fixes an issue where we may export empty groups
                        for (const child of item.children) {
                            if (isGroup(child) && child.children.length === 0) {
                                child.remove();
                            }
                        }
                        ungroupItems([item]);
                    }
                  }, 100)

                performSnapshot(this.props.undoSnapshot, Formats.VECTOR_SKIP_CONVERT);
                this.maybeZoomToFit();
                // paperCanvas.props.updateViewBounds(paper.view.matrix);
                // paperCanvas.initializeSvg(item, rotationCenterX, rotationCenterY, viewBox);
            });

        }
    });
}

function recalibrateSize(callback) {
    // Sets the size that Paper thinks the canvas is to the size the canvas element actually is.
    // When these are out of sync, the mouse events in the paint editor don't line up correctly.
    return window.setTimeout(() => {
        // If the component unmounts, the canvas will be removed from the page, detaching paper.view.
        // This could also be called before paper.view exists.
        // In either case, return early if so without running the callback.
        if (!paper.view) return;
        // Prevent blurriness caused if the "CSS size" of the element is a float--
        // setting canvas dimensions to floats floors them, but we need to round instead
        const elemSize = paper.DomElement.getSize(paper.view.element);
        elemSize.width = Math.round(elemSize.width);
        elemSize.height = Math.round(elemSize.height);
        paper.view.setViewSize(elemSize);

        if (callback) callback();
    }, 0);
}

