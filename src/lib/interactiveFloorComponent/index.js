import React, { Fragment, ReactNode, useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import { SvgLoader, SvgProxy } from "react-svgmt";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import {
  Box,
  Button,
  ButtonGroup,
  Chip,
  Stack,
  Popover,
  TextField,
  MenuItem,
} from "@mui/material";
import floorSvg from "../assets/individualSeats.svg";
import zoomInIcon from "../assets/zoom-in.svg";
import zoomOutIcon from "../assets/zoom-out.svg";
import fitToScreenIcon from "../assets/fit-to-screen.svg";

const CustomPopover = ({
  open,
  anchorEl,
  children,
  sx,
  handleClose,
  anchorOrigin,
}) => {
  return (
    <>
      <Popover
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={anchorOrigin}
        onClose={handleClose}
      >
        <Box sx={sx}>{children}</Box>
      </Popover>
    </>
  );
};

const Legend = ({ color, title, showBorder }) => (
  <Box sx={{ p: 1, display: "flex", alignItems: "center" }}>
    <Box
      sx={{ background: color, width: "1.25rem", height: "1.25rem" }}
      border={showBorder ? "1px solid black" : ""}
    />
    <Box pl={1}>{title}</Box>
  </Box>
);

export function InteractiveFloorComponent(props) {
  console.log("InteractiveFloorComponent1", props.data, props);
  useEffect(() => {
    console.log("InteractiveFloorComponent2", props.data, props);
  }, [props]);

  // seats (path or group) with clickable attributes
  const selectAllSeats = [
    'path[data-paper-data*="\\"value\\":\\"clickable\\""]',
  ];
  // id of all booked seats
  const bookedIds = props.bookedIds ? Object.keys(props.bookedIds) : [];
  const maxCapacity = props.bookingLimit;
  const [anchorEl, setAnchorEl] = React.useState();
  const [paperData, setPaperData] = useState();
  // selected seat properties
  const [newSelectedSeats, setNewSelectedSeats] = useState([]);
  // check if user has selected any meeting room (seat with more than 1 capacity)
  const [meetingRoomChosen, setMeetingRoomChosen] = useState(false);
  const [bookedCapacity, setBookedCapacity] = useState(0);
  const [timeSlot, setTimeSlot] = useState(0);
  const [otherUserId, setOtherUserId] = useState(
    props.otherUserIds && props.otherUserIds[0]
      ? props.otherUserIds[0].userId
      : null
  );
  const handleSelectSeat = () => {
    if (paperData?.capacity) {
      const capacity = Number(paperData?.capacity);
      if (capacity === 1) {
        setBookedCapacity((count) => count + 1);
      }
      if (capacity > 1) {
        setMeetingRoomChosen(true);
      }
    }
    if (props.isBusinessManager) {
      props.seatSelectionCallback(paperData?.id, newSelectedSeats, otherUserId);
    } else {
      props.seatSelectionCallback(
        paperData?.id,
        newSelectedSeats,
        props.currentUserId
      );
    }
    setNewSelectedSeats([...newSelectedSeats, paperData?.id]);
    handleClose();
    // Laravel callback function can write here to pass the clicked seat id
  };

  // When the user clicks on a seat that he has already booked
  const updateNewSelectedSeats = (id, capacity) => {
    props.removeSeatCallback(id, newSelectedSeats);
    const newSelectedSeatsCopy = newSelectedSeats.slice(0);
    const index = newSelectedSeatsCopy.indexOf(id);
    if (index > -1) {
      newSelectedSeatsCopy.splice(index, 1);
    }
    const seatCapacity = Number(capacity);
    if (seatCapacity > 1) {
      // if the seat the user is trying to remove a  meeting room
      setMeetingRoomChosen(false);
    } else if (seatCapacity === 1) {
      setBookedCapacity((count) => count - 1);
    }
    setNewSelectedSeats(newSelectedSeatsCopy);
  };

  // if a meeting room is chosen
  // or bookedCapacity is greater than max capacity
  // or user tryuing to book a meeting room when he has already chosen a seat
  //      then no other seat should be selected
  const restrictedCases = (capacity) => {
    if (meetingRoomChosen) {
      // alert("Cant book....Meeting Room already chosen");
      props.sendAlert("Cant book....Meeting Room already chosen");
      //   dispatch(warningAlert(['Cant book....Meeting Room already chosen']))
      return true;
    }
    if (capacity + bookedCapacity > maxCapacity && capacity == 1) {
      // alert("Cant book....Max capacity reached");
      props.sendAlert("Cant book....Max capacity reached");
      //  dispatch(warningAlert(['Cant book....Max capacity reached']))
      return true;
    }
    if (bookedCapacity > 0 && capacity > 1) {
      // alert(
      //   "Cant book....Seat already chosen, you cant chose another meeting room"
      // );
      props.sendAlert(
        "Cant book....Seat already chosen, you cant chose another meeting room"
      );
      //  dispatch(warningAlert(['Cant book....Seat already chosen, you cant chose another meeting room']))
      return true;
    }
  };

  const getBookingDetails = (selectedID) => {
    let details = [];
    if (props.bookedIds && props.bookedIds[selectedID]) {
      details = props.bookedIds[selectedID];
    }
    return details;
  };

  const handleClick = (event) => {
    setTimeSlot(0);
    const target = event.target;
    const pathPaperInfo = target.getAttribute("data-paper-data") || "";
    const pathAttributes = JSON.parse(pathPaperInfo);
    if (pathAttributes?.parentData) {
      if (pathAttributes?.parentData.unavailable) {
        return null;
      }
      // In booking view, clicks on booked seats should be restricted
      // if (!props.loadingInMapView && bookedIds.includes(pathAttributes.parentData.id)) {
      //   return null;
      // }
      if (newSelectedSeats.includes(pathAttributes.parentData.id)) {
        updateNewSelectedSeats(
          pathAttributes.parentData.id,
          pathAttributes.parentData.capacity
        );
        return null;
      }
      const capacity = Number(pathAttributes.parentData.capacity);
      if (restrictedCases(capacity)) {
        return null;
      }
      // setPaperData(pathAttributes.parentData);
      const details = getBookingDetails(pathAttributes.parentData.id);
      const wholeData = { ...pathAttributes.parentData, slotData: details };
      setPaperData(wholeData);
    } else {
      if (pathAttributes?.unavailable) {
        return null;
      }
      // // In booking view, clicks on booked seats should be restricted
      // if (!props.loadingInMapView && bookedIds.includes(pathAttributes.id)) {
      //   return null;
      // }
      if (newSelectedSeats.includes(pathAttributes.id)) {
        updateNewSelectedSeats(pathAttributes.id, pathAttributes.capacity);
        return null;
      }
      const capacity = Number(pathAttributes.capacity);
      if (restrictedCases(capacity)) {
        return null;
      }
      // setPaperData(pathAttributes);
      const details = getBookingDetails(pathAttributes.id);
      const wholeData = { ...pathAttributes, slotData: details };
      setPaperData(wholeData);
    }
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <>
      <Box
        height={"100%"}
        width={"100%"}
        display={"flex"}
        flexDirection={"column"}
      >
        <Box sx={{ display: "flex", flexWrap: "wrap", minHeight: "8%" }}>
          <Box sx={{ display: "flex", flexWrap: "wrap", flexGrow: 1 }}>
            <Box sx={{ p: 1, display: "flex", alignItems: "center" }}>
              <Typography color="#4b4b4b" variant="subtitle1">
                Total Seats&nbsp;:&nbsp;{props.totalSeats}
              </Typography>
            </Box>
            <Box sx={{ p: 1, display: "flex", alignItems: "center" }}>
              <Typography color="#4b4b4b" variant="subtitle1">
                Booked Seats&nbsp;:&nbsp;{props.bookedSeats}
              </Typography>
            </Box>
          </Box>
          <Box sx={{ display: "flex", flexWrap: "wrap" }}>
            <Legend
              color="#5ccb8b"
              title={
                <Typography color="#4b4b4b" variant="subtitle1">
                  Booked
                </Typography>
              }
            />
            <Legend
              color="#f9f7f5"
              title={
                <Typography color="#4b4b4b" variant="subtitle1">
                  Available
                </Typography>
              }
              showBorder
            />
            <Legend
              color="#d1d1d1"
              title={
                <Typography color="#4b4b4b" variant="subtitle1">
                  Unavailable
                </Typography>
              }
            />
          </Box>
        </Box>

        <Box pb={"1.538rem"} flexGrow={1} flexShrink={1}>
          <TransformWrapper initialScale={1}>
            {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
              <React.Fragment>
                <Box sx={{ height: "98%", width: "100%" }}>
                  <TransformComponent
                    wrapperStyle={{ height: "100%", width: "100%" }}
                    contentStyle={{
                      height: "100%",
                      width: "100%",
                      position: "absolute",
                    }}
                  >
                    <SvgLoader height="100%" width="100%" svgXML={props.image}>
                      {/* <SvgProxy
                          selector='path'
                        
                          // onMouseEnter={handlePopoverOpen}
                          // onMouseLeave={handlePopoverClose}
                        /> */}

                      {/* In map view, user should not click on other seat */}
                      {props.loadingInMapView
                        ? selectAllSeats.map((selector, ix) => (
                            <SvgProxy
                              key={ix}
                              selector={selector}
                              stroke="#bcbcbc"
                              fill="white"
                              onClick={() => {
                                alert(
                                  "In map view, user should not click on other seat"
                                );
                              }}
                            />
                          ))
                        : selectAllSeats.map((selector, ix) => (
                            <SvgProxy
                              key={ix}
                              selector={selector}
                              stroke="#bcbcbc"
                              fill="white"
                              onClick={handleClick}
                            />
                          ))}
                      <SvgProxy
                        selector={
                          'path[data-paper-data*="\\"unavailable\\":true"]'
                        }
                        stroke="#bcbcbc"
                        fill="#bcbcbc"
                      />
                      {/* In booking view, user should not click on booked seat */}
                      {!props.loadingInMapView
                        ? bookedIds.map((selector, ix) => (
                            <SvgProxy
                              key={ix}
                              selector={
                                'path[data-paper-data*="\\"id\\":\\"' +
                                selector +
                                '\\""]'
                              }
                              fill="#5ccb8b"
                              onClick={() => {
                                alert(
                                  "In booking view, user should not click on booked seat"
                                );
                              }}
                            />
                          ))
                        : bookedIds.map((selector, ix) => (
                            <SvgProxy
                              key={ix}
                              selector={
                                'path[data-paper-data*="\\"id\\":\\"' +
                                selector +
                                '\\""]'
                              }
                              fill="#5ccb8b"
                              onClick={handleClick}
                            />
                          ))}
                      {newSelectedSeats.map((selector, ix) => (
                        <SvgProxy
                          key={ix}
                          selector={
                            'path[data-paper-data*="\\"id\\":\\"' +
                            selector +
                            '\\""]'
                          }
                          fill="#ffd538"
                        />
                      ))}
                    </SvgLoader>
                  </TransformComponent>
                </Box>
                <Box
                 sx={{
                  minHeight: "5%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  color:'#d1d1d1'
                }}
                pr={2}
                >
                  <ButtonGroup
                    variant="outlined"
                    size="small"
                    sx={{ height: "2rem" }}
                    color="inherit"
                  >
                    <Button key="one" onClick={() => zoomOut()}>
                      <img src={zoomOutIcon} alt="" />
                    </Button>
                    <Button key="two" onClick={() => resetTransform()}>
                      <img src={fitToScreenIcon} alt="" />
                    </Button>
                    <Button key="three" onClick={() => zoomIn()}>
                      <img src={zoomInIcon} alt="" />
                    </Button>
                  </ButtonGroup>
                </Box>
              </React.Fragment>
            )}
          </TransformWrapper>
        </Box>

        <CustomPopover
          id={id}
          open={open}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          handleClose={handleClose}
          sx={{
            minWidth: "17.188rem",
            maxWidth: "17.188rem",
            padding: "8px",
            margin: "3px",
          }}
        >
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <Box my={1}>
              <Legend
                color="#5ccb8b"
                title={
                  <Typography variant="h6" fontWeight={600}>
                    {paperData?.id}
                  </Typography>
                }
              />
              {paperData?.capacity !== 1 && (
                <Typography variant="subtitle2" p={1}>
                  Capacity&nbsp;:&nbsp;{paperData?.capacity}
                </Typography>
              )}
            </Box>

            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                flexWrap: "wrap",
              }}
            >
              {paperData?.customAttributes
                ?.filter((item) => item.value !== "clickable")
                .map((item) => (
                  <Chip
                    key={item.value}
                    label={item.label}
                    sx={{ background: "#e1e1e1", m: "3px", mb: "15px" }}
                  />
                ))}
            </Box>

            {/* Displayed only in booking view and if the user is a business manager - book for others */}
            {!props.loadingInMapView && props.isBusinessManager ? (
              <TextField
                select
                fullWidth
                size="small"
                // sx={{ marginTop: "1rem" }}
                value={otherUserId}
                onChange={(e) => {
                  setOtherUserId(e.target.value);
                }}
                inputProps={{ "aria-label": "Without label" }}
              >
                {props.otherUserIds &&
                  props.otherUserIds.map((item, i) => (
                    <MenuItem key={i} value={item.userId}>
                      {item.emailId}
                    </MenuItem>
                  ))}
              </TextField>
            ) : null}

            {/* Displayed only in map view - booking details */}
            {props.loadingInMapView ? (
              <Fragment>
                <TextField
                  select
                  fullWidth
                  size="small"
                  // sx={{ marginTop: "1rem" }}
                  value={timeSlot}
                  onChange={(e) => {
                    setTimeSlot(e.target.value);
                  }}
                  inputProps={{ "aria-label": "Without label" }}
                >
                  {paperData?.slotData?.map((val, i) => (
                    <MenuItem key={i} value={i}>
                      {val.time}
                    </MenuItem>
                  ))}
                </TextField>
                <Typography
                  variant="subtitle1"
                  sx={{ color: "#4b4b4b" }}
                  mt={2}
                  fontWeight={600}
                >
                  {paperData?.slotData[timeSlot]?.name}
                </Typography>
                <Typography variant="subtitle2" sx={{ color: "#4b4b4b" }}>
                  {paperData?.slotData[timeSlot]?.mob}
                </Typography>
                <Typography variant="subtitle2" sx={{ color: "#4b4b4b" }}>
                  {paperData?.slotData[timeSlot]?.email}
                </Typography>
              </Fragment>
            ) : null}
            <Stack
              mt="1rem"
              direction={{ xs: "column", sm: "row" }}
              spacing={1}
            >
              {/* Displayed only in booking view - select seat button*/}
              {!props.loadingInMapView ? (
                <Button
                  variant="outlined"
                  color="error"
                  sx={{
                    borderRadius: "2rem",
                    fontSize: ".875rem",
                    fontWeight: "600",
                    width: "6rem",
                  }}
                  onClick={handleSelectSeat}
                >
                  Select
                </Button>
              ) : null}
              {/* <Button
                variant='outlined'
                sx={{ borderRadius: '2rem', fontSize: '.875rem', fontWeight: '400',width:'6rem'  }}
              >
                Assign
              </Button> */}
            </Stack>
          </Box>
        </CustomPopover>
      </Box>
    </>
  );
}
