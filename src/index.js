import React from 'react';
import { render } from "react-dom";
import { InteractiveFloorComponent } from './lib/interactiveFloorComponent';
import { SVGEditor } from './lib/svgEditor';

// import { TextInput } from './lib/TextInput';
const image =
    '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"' +
    ' x="0px" y="0px" width="32px" height="32px" viewBox="0.5 384.5 32 32"' +
    ' enable-background="new 0.5 384.5 32 32" xml:space="preserve">' +
    '<path fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="10" d="M7.5,392.241h7.269' +
    'c4.571,0,8.231,5.555,8.231,10.123v7.377"/>' +
    '<polyline points="10.689,399.492 3.193,391.997 10.689,384.5 "/>' +
    '<polyline points="30.185,405.995 22.689,413.491 15.192,405.995 "/>' +
    '</svg>';



const bookedIds = {
    's1': [{ time: '10-11', name: 'sree', email: 'ab@qb', mob: '019292929292' }, { time: '11-12', name: 'du', email: 'dd@qb' }],
    's2': [{ time: '10-11', name: 'an', email: 'an@qb' }],
    // 'm1' : [{ time: '10-11', name: 'Tom Thomas', email: 'tom@qb.com' }, { time: '11-12', name: 'VP Ash', email: 'ash@qb.com' }]
};
const currentUserId = '1011'
const otherUserIds = [
    { userId: '1121', emailId: 'aa@aa.com' },
    { userId: '2211', emailId: 'bb@bb.com' }
]
const seatSelectionCallback = (seatId, previousSelection, userId) => {
}
const removeSeatCallback = (seatId, previousSelection) => {
}
const sendAlert = (msg) => {

}
const handleBack = () => {

}
const handleAlertMessage = () => {
}
const handleSave = (obj) => {

}
const bookingLimit = 4;
const App = () => (
    <>
        {/* <SVGEditor
            type='svg'
            handleSave={handleSave} 
            handleAlertMessage={handleAlertMessage} 
            handleBack={handleBack} 
            name='Floor Plan 001' 
            image={image}/> */}

        <div style={{ height: "100vh" }}>
            <InteractiveFloorComponent
                totalSeats={50}
                bookedSeats={35}
                sendAlert={sendAlert}
                image={image}
                bookingLimit={bookingLimit}
                seatSelectionCallback={seatSelectionCallback}
                removeSeatCallback={removeSeatCallback}
                bookedIds={bookedIds}
                otherUserIds={otherUserIds}
                currentUserId={currentUserId}
                loadingInMapView={false}
                isBusinessManager={true} />
        </div>

    </>
);

render(<App />, document.getElementById("root"));
